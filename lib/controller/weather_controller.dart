import 'dart:convert';

import 'package:agromate/model/weatherMol.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class WeatherController extends GetxController {
  static WeatherController weatherController = Get.find();
  RxList<WeatherModel> commModel = <WeatherModel>[].obs;

  Future<WeatherModel> fetchCovidData(placename) async {
    var responses;
    try {
      final response = await http.get(Uri.parse(
          'https://nepal-weather-api.herokuapp.com/api/?place=$placename'));
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);

        responses = data;
      } else {
        print('Failed to load Data');
        throw Exception('Failed to load Covid Data');
      }
    } on Exception {}
    return WeatherModel.fromJson(responses);
  }
}
