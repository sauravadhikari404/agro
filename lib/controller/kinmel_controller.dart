import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/kinmel_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class KinmelController extends GetxController {
  static KinmelController instance = Get.find();
  RxList<UserKinmelModel> userkmodel = <UserKinmelModel>[].obs;
  RxList<KinmelModel> kmodel = <KinmelModel>[].obs;
  final CollectionReference kinmelRef = firebaseFirestore.collection('Kinmel');

  @override
  void onInit() {
    kmodel.bindStream(allKinmelData());
    if (userController.userModel.value.uid != null) {
      return userkmodel.bindStream(userKinemPost());
    }
    super.onInit();
  }

  Stream<List<KinmelModel>> allKinmelData() {
    Stream<QuerySnapshot> stream =
        firebaseFirestore.collection('Kinmel').snapshots();

    return stream.map((qShot) => qShot.docs
        .map(
          (doc) => KinmelModel(
              pid: doc.id,
              quantity: doc.get('quantity') ?? '',
              by: doc.get('by') ?? '',
              date: doc.get('pdate') ?? Timestamp.fromDate(DateTime.now()),
              votes: doc.get('votes') ?? [],
              pType: doc.get('pType') ?? '',
              pname: doc.get('pName') ?? '',
              price: doc.get('price') ?? '',
              quality: doc.get('quality') ?? '',
              validDate:
                  doc.get('validDate') ?? Timestamp.fromDate(DateTime.now()),
              image: doc.get('image').toList() ?? [],
              address: doc.get('address') ?? '',
              phone: doc.get('phone') ?? '',
              views: doc.get('views') ?? [],
              description: doc.get('description') ?? '',
              perItemTag: doc.get('perItemTag') ?? ''),
        )
        .toList());
  }

  Stream<List<UserKinmelModel>> userKinemPost() {
    Stream<QuerySnapshot> stream = firebaseFirestore
        .collection('Kinmel')
        .where('by', isEqualTo: userController.userModel.value.uid)
        .snapshots();

    return stream.map((qShot) => qShot.docs
        .map(
          (doc) => UserKinmelModel(
              pid: doc.id,
              quantity: doc.get('quantity') ?? '',
              by: doc.get('by') ?? '',
              date: doc.get('pdate') ?? Timestamp.fromDate(DateTime.now()),
              votes: doc.get('votes') ?? [],
              pType: doc.get('pType') ?? '',
              pname: doc.get('pName') ?? '',
              price: doc.get('price') ?? '',
              quality: doc.get('quality') ?? '',
              validDate:
                  doc.get('validDate') ?? Timestamp.fromDate(DateTime.now()),
              image: doc.get('image').toList() ?? [],
              address: doc.get('address') ?? '',
              phone: doc.get('phone') ?? '',
              views: doc.get('views') ?? [],
              description: doc.get('description') ?? '',
              perItemTag: doc.get('perItemTag') ?? ''),
        )
        .toList());
  }

  void addViewsToKinmel(pid, index) async {
    var usrData = userController.userModel.value;
    if (userController.firebaseUser.value?.isAnonymous ?? false) {
    } else {
      if (isCurrentUserLikeIt(kinmelController.kmodel.value[index].views)) {
      } else {
        try {
          await kinmelRef.doc(pid).update({
            'views': FieldValue.arrayUnion([usrData.uid])
          }).then((value) {});
        } catch (e) {
          if (kDebugMode) print(e.toString());
        }
      }
    }
  }

  void addCommentOnKinmelPost(pid, body) async {
    var usrData = userController.userModel.value;
    try {
      await kinmelRef.doc(pid).collection('Comments').add({
        'body': body,
        'by': usrData.uid,
        'date': FieldValue.serverTimestamp(),
        'votes': []
      }).then((value) {
        // Get.snackbar('message', 'Successfully Posted.',
        //     snackPosition: SnackPosition.BOTTOM);
      });
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }
}
