import 'package:agromate/constant/datahelper.dart';
import 'package:agromate/model/hisabkitab.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class HisabKitabIncomeController extends GetxController {
  static HisabKitabIncomeController instance = Get.find();
  RxList hisabkitabIncomeData = <HisabKitabIncomeModel>[].obs;

  TextEditingController? addHisabIncomeTitleController;
  TextEditingController? addHisabIncomeAmountController;
  TextEditingController? addHisabIncomeDateController;

  @override
  void onInit() {
    addHisabIncomeTitleController = TextEditingController();
    addHisabIncomeAmountController = TextEditingController();
    addHisabIncomeDateController = TextEditingController();
    _getIncomeData();
    super.onInit();
  }

  void _getIncomeData() {
    DatabaseHelper.instance.queryAllRowsIncome().then((value) {
      value.forEach((element) {
        hisabkitabIncomeData.add(HisabKitabIncomeModel(id: element['id'], title: element['title'],
          amount:element['amount'],date: element['date'] ));
      });
    });
  }

  ///addIncomeData
  void addIncomeData() async {
    await DatabaseHelper.instance
        .insertIncome(HisabKitabIncomeModel(title: addHisabIncomeTitleController!.text,
        date: addHisabIncomeDateController!.text,amount:double.parse(addHisabIncomeAmountController!.text) ));
    hisabkitabIncomeData.insert(
        0, HisabKitabIncomeModel(id: hisabkitabIncomeData.length, title: addHisabIncomeTitleController!.text,
        date: addHisabIncomeDateController!.text,amount: double.parse(addHisabIncomeAmountController!.text)));
    addHisabIncomeTitleController!.clear();
    addHisabIncomeAmountController!.clear();
    addHisabIncomeDateController!.clear();
    Get.back();
  }
  void deleteIncomeData(int id) async {
    await DatabaseHelper.instance.deleteIncome(id);
    hisabkitabIncomeData.removeWhere((element) => element.id == id);
  }
}