import 'package:agromate/model/tipot.dart';
import 'package:agromate/model/tipot.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

class TipotController extends GetxController {
  static TipotController instance = Get.find();
  TextEditingController titleController = TextEditingController();
  TextEditingController bodyController = TextEditingController();
  FocusNode titleFocus = FocusNode();
  FocusNode bodyFocus = FocusNode();
  var tipots = [].obs;
  var done = [].obs;
  var remaining = [].obs;

  @override
  void onInit() {
    try {
      Hive.registerAdapter(TipotModelAdapter());
    } catch (e) {
      debugPrint(e.toString());
    }
    openHiveBox('db');
    getTipots();
    super.onInit();
  }

  Future<Box> openHiveBox(String boxName) async {
    if (!kIsWeb && !Hive.isBoxOpen(boxName)) {
      Hive.init((await getApplicationDocumentsDirectory()).path);
    }
    return await Hive.openBox(boxName);
  }

  addToTipot(TipotModel tipot) async {
    tipots.add(tipot);
    var box = await Hive.openBox('db');
    box.put('tipots', tipots.toList());
    debugPrint("To Do Object added ${tipots.value.length} \n\n\n\n\n");
  }

  Future getTipots() async {
    Box box;

    debugPrint("Getting Tipot Data");
    try {
      box = Hive.box('db');
    } catch (e) {
      box = await Hive.openBox('db');
      debugPrint(e.toString());
    }
    var tps = box.get('tipots');
    if (kDebugMode) ("Tipots $tps");

    if (tps != null) tipots.value = tps;

    for (TipotModel tipot in tipots) {
      if (tipot.isComplete!) {
        done.add(tipot);
      } else {
        remaining.add(tipot);
      }
    }
  }

  clearTipots() {
    try {
      Hive.deleteBoxFromDisk('db');
    } catch (e) {
      debugPrint(e.toString());
    }
    tipots.value = [];
  }

  deleteTipots(TipotModel tipot) async {
    tipots.remove(tipot);
    var box = await Hive.openBox('db');
    box.put('tipots', tipots.toList());
  }

  toggleTipots(TipotModel tipot) async {
    var index = tipots.indexOf(tipot);
    var editTipot = tipots[index];
    editTipot.isComplete = !editTipot.isComplete;
    if (editTipot.isComplete) {
      done.add(editTipot);
    } else {
      done.remove(editTipot);
      remaining.add(editTipot);
    }

    tipots[index] = editTipot;
    var box = await Hive.openBox('db');
    box.put('tipots', tipots.toList());
  }
}
