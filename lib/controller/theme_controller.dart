import 'package:agromate/config/configs.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeController extends GetxController {
  static ThemeController instance = Get.find();
  late ThemeData _darkTheme;
  late ThemeData _lightTheme;
  late bool isDarkTheme;

  @override
  void onInit() {
    initializeThemeData();

    super.onInit();
  }

  Future<void> initializeThemeData() async {
    _darkTheme = DarkTheme().buildDarkTheme();
    _lightTheme = LightTheme().buildLightTheme();
    Get.isDarkMode ? isDarkTheme = true : isDarkTheme = false;
    SharedPreferences _pref;
    _pref = await SharedPreferences.getInstance();
    bool isDark = _pref.getBool('isDarkMode') ?? false;
    if (isDark) {
      Get.changeTheme(getDarkTheme());
    } else {
      Get.changeTheme(getLightheme());
    }
  }

  ThemeData getDarkTheme() {
    return _darkTheme;
  }

  ThemeData getLightheme() {
    return _lightTheme;
  }

  ThemeMode getThemeMode() {
    return Get.isDarkMode ? ThemeMode.dark : ThemeMode.light;
  }

  void changeTheme() async {
    Get.changeTheme(Get.isDarkMode ? _lightTheme : _darkTheme);
    SharedPreferences _pref;
    _pref = await SharedPreferences.getInstance();
    _pref.setBool('isDarkMode', Get.isDarkMode ? false : true);
  }
}
