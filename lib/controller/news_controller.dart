import 'dart:async';

import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/news.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class NewsController extends GetxController {
  static NewsController instace = Get.find();
  RxList<NewsModelClk> newsModelClk = <NewsModelClk>[].obs;
  RxList<VideoModelClk> videoModelClk = <VideoModelClk>[].obs;

  RxList<VideoModel> videoModel = <VideoModel>[].obs;

  @override
  void onInit() async {
    newsModelClk.bindStream(krishiNews());
    videoModelClk.bindStream(krishiVideo());
   
    super.onInit();
  }

//Stream List Of News Model
  Stream<List<NewsModelClk>> krishiNews() {
    Stream<QuerySnapshot> stream = firebaseFirestore
        .collection('KrishiNews')
        .where('by', isEqualTo: userController.userModel.value.uid)
        .snapshots();

    return stream.map((qShot) => qShot.docs.map((doc) {
          return NewsModelClk(url: doc.get('url'));
        }).toList());
  }

//Stream List of Video News
  Stream<List<VideoModelClk>> krishiVideo() {
    Stream<QuerySnapshot> stream = firebaseFirestore
        .collection('VideoNews')
        .where('by', isEqualTo: userController.userModel.value.uid)
        .snapshots();

    return stream.map((qShot) => qShot.docs.map((doc) {
          return VideoModelClk(
            url: doc.get('url') ?? "",
          );
        }).toList());
  }
}
