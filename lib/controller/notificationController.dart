// ignore: file_names
import 'package:agromate/constant/datahelper.dart';
import 'package:agromate/model/notification.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';

class NotificationController extends GetxController {
  static NotificationController instance = Get.find();
  RxList notificationData = <NotificationModel>[].obs;

  String? addNotificationTitle;
  String? addNotificationBody;
  String? addNotificationImage;
  String? addNotificationDate;

  @override
  void onInit() async {
    try {
      FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);
      terminateNotification();
      _getNotificationData();
    } catch (E) {}
    super.onInit();
  }

  Future<void> onBackgroundMessage(RemoteMessage? message) async {
    message = await FirebaseMessaging.instance.getInitialMessage();
    if (message != null) {
      addNotificationTitle = message.notification?.title ?? '';
      addNotificationBody = message.notification?.body ?? '';
      addNotificationImage = message.notification?.android!.imageUrl ?? '';
      addNotificationDate =
          // ignore: deprecated_member_use
          NepaliDateTime.fromDateTime(DateTime.now()).toString();
      if (!isNullField()) {
        addNotificationData();
        addNotificationDate = "";
        addNotificationTitle = "";
        addNotificationImage = "";
        addNotificationBody = "";
      }
    }
  }

  void _getNotificationData() {
    DatabaseHelper.instance.queryAllRowsNotification().then((value) {
      for (var element in value) {
        notificationData.add(NotificationModel(
            id: element['id'] ?? '',
            title: element['title'] ?? '',
            body: element['body'] ?? '',
            image: element['image'] ?? '',
            date: element['date'] ?? ''));
      }
    }).then((value) {
      debugPrint('Table Exist Notification Table');
    });
  }

  void addNotificationData() async {
    await DatabaseHelper.instance.insertNotification(NotificationModel(
        title: addNotificationTitle ?? '',
        date: addNotificationDate ?? '',
        body: addNotificationBody ?? '',
        image: addNotificationImage ?? ''));
    notificationData.insert(
        0,
        NotificationModel(
            id: notificationData.length,
            title: addNotificationTitle ?? '',
            date: addNotificationDate ?? '',
            body: addNotificationBody ?? '',
            image: addNotificationImage ?? ''));
  }

  void deleteExpensesData(int id) async {
    await DatabaseHelper.instance.deleteExpenses(id);
    notificationData.removeWhere((element) => element.id == id);
  }

  void deleteAllNoty() async {
    await DatabaseHelper.instance.clearTableNotification();
    notificationData.clear();
  }

  foregroundNotifications() {
    // FirebaseMessaging.onMessage.listen((event) {
    //   addNotificationTitle = event.notification?.title;
    //   addNotificationBody = event.notification?.body;
    //   addNotificationImage = event.notification?.android!.imageUrl;
    //   addNotificationDate =
    //       // ignore: deprecated_member_use
    //       NepaliDateTime.fromDateTime(DateTime.now()).toString();
    //   if (!isNullField()) {
    //     addNotificationData();
    //   }
    // });
  }

  terminateNotification() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      addNotificationTitle = initialMessage.notification?.title ?? '';
      addNotificationBody = initialMessage.notification?.body ?? '';
      addNotificationImage =
          initialMessage.notification?.android!.imageUrl ?? '';
      addNotificationDate =
          // ignore: deprecated_member_use
          NepaliDateTime.fromDateTime(DateTime.now()).toString();
      if (!isNullField()) {
        addNotificationData();
      }
    }
  }

  backgroundNotifications() {
    // FirebaseMessaging.onMessageOpenedApp.listen((event) {
    //   if (event.notification != null) {
    //     addNotificationTitle = event.notification?.title;
    //     addNotificationBody = event.notification?.body;
    //     addNotificationImage = event.notification?.android!.imageUrl;
    //     addNotificationDate =
    //         // ignore: deprecated_member_use
    //         NepaliDateTime.fromDateTime(DateTime.now()).toString();
    //     if (!isNullField()) {
    //       addNotificationData();
    //     }
    //   }
    // });
  }

  bool isNullField() {
    if (addNotificationTitle == null &&
        addNotificationImage == null &&
        addNotificationImage == null &&
        addNotificationDate == null) {
      return true;
    }
    return false;
  }
}
