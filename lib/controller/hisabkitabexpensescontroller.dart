import 'package:agromate/constant/datahelper.dart';
import 'package:agromate/model/hisabkitab.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class HisabKitabExpensesController extends GetxController {
  static HisabKitabExpensesController instance = Get.find();
  RxList hisabkitabExpensesData = <HisabKitabExpensesModel>[].obs;
  TextEditingController? addHisabExpensesTitleController;
  TextEditingController? addHisabExpensesDateController;
  TextEditingController? addHisabExpensesAmountController;
  TextEditingController? addHisabExpensesTypeController;

  List get hisabkitabIncomeData => <HisabKitabExpensesModel>[];

  @override
  void onInit() {
    addHisabExpensesTitleController = TextEditingController();
    addHisabExpensesAmountController = TextEditingController();
    addHisabExpensesTypeController = TextEditingController();
    addHisabExpensesDateController = TextEditingController();
    addHisabExpensesDateController!.clear();
    addHisabExpensesTitleController!.clear();
    addHisabExpensesAmountController!.clear();
    addHisabExpensesTypeController!.clear();
    _getExpensesData();
    super.onInit();
  }
  void _getExpensesData() {
    hisabkitabExpensesData.clear();
    DatabaseHelper.instance.queryAllRowsExpenses().then((value) {
      value.forEach((element) {
        hisabkitabExpensesData.add(HisabKitabExpensesModel(id: element['id'], title: element['title'],
            amount:element['amount'],type: element['type'],date: element['date'] ));
        debugPrint(element['type']);
      });
    }).then((value){
      debugPrint('Table Exist Expenses Table');
    });
  }

  void addExpensesData() async {
    await DatabaseHelper.instance
        .insertExpenses(HisabKitabExpensesModel(title: addHisabExpensesTitleController!.text,
        date: addHisabExpensesDateController!.text,type: addHisabExpensesTypeController!.text,
        amount: double.parse(addHisabExpensesAmountController!.text)));
    hisabkitabExpensesData.insert(
        0, HisabKitabExpensesModel(id: hisabkitabExpensesData.length,
        title: addHisabExpensesTitleController!.text,
        date: addHisabExpensesDateController!.text,
        type: addHisabExpensesTypeController!.text,
        amount: double.parse(addHisabExpensesAmountController!.text)));
    addHisabExpensesTitleController!.clear();
    addHisabExpensesAmountController!.clear();
    addHisabExpensesTypeController!.clear();
    addHisabExpensesDateController!.clear();
    Get.back();
  }
  void deleteExpensesData(int id) async {
    await DatabaseHelper.instance.deleteExpenses(id);
    hisabkitabExpensesData.removeWhere((element) => element.id == id);
  }

}