import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/users.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class AllUsersController extends GetxController {
  static AllUsersController instance = Get.find();
  RxList<UsersModel> usersModel = <UsersModel>[].obs;

  @override
  void onInit() async {
    usersModel.bindStream(allUsersData());
    super.onInit();
  }

  Stream<List<UsersModel>> allUsersData() {
    Stream<QuerySnapshot> stream =
        firebaseFirestore.collection('Users').snapshots();
    return stream.map((qShot) => qShot.docs
        .map(
          (doc) => UsersModel(
            uid: doc.id, //done
            fname: doc.get('fname') ?? '', //done
            lname: doc.get('lname') ?? '', //done
            farmName: doc.get('farmName') ?? '', //done
            email: doc.get('email') ?? '', //done
            userType: doc.get('userType') ?? '',
            phone: doc.get('phone') ?? '',
            district: doc.get('district') ?? '',
            province: doc.get('province') ?? '',//done
            includedBusiness: doc.get('includedBusiness') ?? '',
            userRegisteredDate: doc.get('userRegistered'),//done
            photoUrl: doc.get('photoUrl') ?? '',//done
            address: doc.get('address') ?? '', //done
          ),
        )
        .toList());
  }
}
