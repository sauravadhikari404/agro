import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

const kTestDeviceId = "053122508C006177";
const kNativeAdsId = "ca-app-pub-2211889863657710/4536450730";
const kBannerAdsId = "ca-app-pub-2211889863657710/3035666808";
const kSwitchAdsId = "ca-app-pub-2211889863657710/1462553071";
const kInterstitialAdsId = "ca-app-pub-2211889863657710/5797561513";

class AdsController extends GetxController {
  late InterstitialAd _interstitialAd;
  static AdsController instance = Get.find();
  final ads = MobileAds.instance;
  RxInt adsShown = 0.obs;

  @override
  void onInit() async {
    super.onInit();
    try {
      await ads.initialize();
      if (kDebugMode) {
        final cfg = RequestConfiguration(testDeviceIds: [kTestDeviceId]);
        await MobileAds.instance.updateRequestConfiguration(cfg);
      }
    } catch (e) {}
  }

  @override
  void onClose() {
    super.onClose();
  }

  BannerAd getBannerAd() {
    return BannerAd(
      size: AdSize.banner,
      adUnitId: 'ca-app-pub-2211889863657710/3035666808',
      listener: BannerAdListener(onAdLoaded: (Ad ads) {
        debugPrint(ads.adUnitId.toString() + '\n\n\n');
      }, onAdFailedToLoad: (Ad ad, LoadAdError error) {
        ad.dispose();
        debugPrint(
            "Some Error Hits:" + error.toString() + " While Loading Ads");
      }),
      request: const AdRequest(),
    )..load();
  }

  BannerAd getBannerAd2() {
    try {
      return BannerAd(
        size: AdSize.mediumRectangle,
        adUnitId: 'ca-app-pub-2211889863657710/3035666808',
        listener: BannerAdListener(onAdLoaded: (Ad ads) {
          debugPrint(ads.adUnitId.toString() + '\n\n\n');
        }, onAdFailedToLoad: (Ad ad, LoadAdError error) {
          ad.dispose();
          debugPrint(
              "Some Error Hits:" + error.toString() + " While Loading Ads");
        }),
        request: const AdRequest(),
      )..load();
    } on Exception {
      throw Exception();
    }
  }

  loadAds() async {
    try {
      await InterstitialAd.load(
        adUnitId: kInterstitialAdsId,
        request: const AdRequest(),
        adLoadCallback: InterstitialAdLoadCallback(
          onAdFailedToLoad: (LoadAdError error) {},
          onAdLoaded: (InterstitialAd ad) {
            if (adsShown.toInt() >= 5) {
            } else {
              ad.show();
            }
          },
        ),
      );
    } on Exception {
      throw Exception();
    }
  }
}
