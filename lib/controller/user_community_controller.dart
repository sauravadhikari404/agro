import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/comm.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class UserCommunityPostController extends GetxController {
  static UserCommunityPostController instance = Get.find();
  RxList<UserCommunityModel> userCommunityModel = <UserCommunityModel>[].obs;
  final CollectionReference communityPostRef =
      firebaseFirestore.collection('CummunityPosts');

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  InternalFinalCallback<void> get onDelete => super.onDelete;
}
