import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/sliderdata.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class SliderAdsController extends GetxController {
  static SliderAdsController instance = Get.find();
  RxList sliderAdsData = <SliderAdsModel>[].obs;

  @override
  void onInit() {
    sliderAdsData.bindStream(sliderAdsImage());
    super.onInit();
  }

  Stream<List<SliderAdsModel>> sliderAdsImage() {
    Stream<QuerySnapshot> stream =
        firebaseFirestore.collection('SlideShowAds').snapshots();

    return stream.map((qShot) => qShot.docs.map((doc) {
          return SliderAdsModel(
            img: doc.get('img') ?? "",
            url: doc.get('url') ?? "",
            redirect: doc.get('redirect') ?? false,
          );
        }).toList());
  }
}
