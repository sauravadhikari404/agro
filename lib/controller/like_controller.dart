import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LikeController extends GetxController {
  static LikeController instance = Get.find();
  var commRef = firebaseFirestore.collection('CummunityPosts');
  var commRef2 = firebaseFirestore.collection('Kinmel');

  void addLikeToComment(pid, cid) {
    var usrData = userController.userModel.value;
    try {
      commRef.doc(pid).collection('Comments').doc(cid).update({
        'votes': FieldValue.arrayUnion([usrData.uid])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void removeLikeFromComment(pid, cid) {
    var usrData = userController.userModel.value;
    try {
      commRef.doc(pid).collection('Comments').doc(cid).update({
        'votes': FieldValue.arrayRemove([usrData.uid])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void addLikeToCommunityPosts(pid) {
    var usrData = userController.userModel.value;
    try {
      commRef.doc(pid).update({
        'votes': FieldValue.arrayUnion([usrData.uid])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void removeLikeFromCommunityPosts(pid) async {
    var usrData = userController.firebaseUser.value;
    debugPrint(usrData?.uid.toString());
    try {
      await commRef.doc(pid).update({
        'votes': FieldValue.arrayRemove([usrData?.uid ?? ''])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void addLikeToCommentKinmel(pid, cid) {
    var usrData = userController.userModel.value;
    try {
      commRef2.doc(pid).collection('Comments').doc(cid).update({
        'votes': FieldValue.arrayUnion([usrData.uid])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void removeLikeFromCommentKinmel(pid, cid) {
    var usrData = userController.userModel.value;
    try {
      commRef2.doc(pid).collection('Comments').doc(cid).update({
        'votes': FieldValue.arrayRemove([usrData.uid ?? ''])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void addLikeToKinmel(pid) {
    var usrData = userController.userModel.value;
    try {
      commRef2.doc(pid).update({
        'votes': FieldValue.arrayUnion([usrData.uid ?? ''])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }

  void removeLikeFromKinmel(pid) async {
    var usrData = userController.firebaseUser.value;
    debugPrint(usrData?.uid.toString());
    try {
      await commRef2.doc(pid).update({
        'votes': FieldValue.arrayRemove([usrData?.uid ?? ''])
      }).then((value) {});
    } catch (e) {
      Get.snackbar('alert', 'Cannot Perform Action');
    }
  }
}
