import 'dart:convert';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/controller/adscontroller.dart';
import 'package:agromate/controller/all_user_controller.dart';
import 'package:agromate/controller/app_controller.dart';
import 'package:agromate/controller/communitypost_controller.dart';
import 'package:agromate/controller/kinmel_controller.dart';
import 'package:agromate/controller/notificationController.dart';
import 'package:agromate/loading_dialog.dart';
import 'package:agromate/model/user.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/splash/sdsplash_screen.dart';
import 'package:agromate/screen/widget/mainnav.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserController extends GetxController {
  static UserController instance = Get.find();
  Rx<User?> firebaseUser = Rx<User?>(auth.currentUser);
  RxBool isLoggedIn = false.obs;
  String usersCollection = "Users";
  Rx<UserModel> userModel = UserModel().obs;

  final DateTime _dateTime = DateTime.now();

  @override
  void onReady() {
    FirebaseAuth auth = FirebaseAuth.instance;
    firebaseUser = Rx<User?>(auth.currentUser);
    firebaseUser.bindStream(auth.userChanges());
    ever(firebaseUser, _setInitialScreen);
    userModel.bindStream(listenToUser());
    super.onReady();
  }

  int getUserIndex(uid) {
    int data = allUsersController.usersModel
        .indexWhere((element) => element.uid == uid);
    return data;
  }

  _setInitialScreen(User? user) async {
    SharedPreferences _prefs;
    _prefs = await SharedPreferences.getInstance();
    bool isLogin = _prefs.getBool("isLogin") ?? false;
    int preDate = _prefs.getInt("time") ?? 0;
    int dif = _dateTime.minute - preDate;

    if (isLogin) {
      allUsersController.allUsersData();
      firebaseUser = Rx<User?>(auth.currentUser);
      if (dif.abs() >= 50 || preDate == 0) {
        commPostCont.userCommModel
            .bindStream(commPostCont.currentUserCommunityPost());
        _prefs.setInt("time", _dateTime.minute);
        Get.offAll(() => const SDSplashScreen());
      }
    } else {
      if (user?.uid != null) {
        allUsersController.allUsersData();

        firebaseUser = Rx<User?>(auth.currentUser);
        if (dif.abs() >= 20 || preDate == 0) {
          _prefs.setInt("time", _dateTime.minute);
          _prefs.setBool("isLogin", true);
          commPostCont.userCommModel
              .bindStream(commPostCont.currentUserCommunityPost());
          Get.offAll(() => const SDSplashScreen());
        }
      } else {
        Get.offAll(() => const AccountPage());
      }
    }
  }

  void signIn(String email, String pass) async {
    String emails = email.trim();
    try {
      CustomFullScreenLoadingDialog.showDialog();
      await auth
          .signInWithEmailAndPassword(email: emails, password: pass)
          .then((result) {
        if (result.user?.uid == null) {
        } else {
          userModel.bindStream(listenToUser());
          Get.offAll(() => const SDSplashScreen());
        }
      }).whenComplete(() {});
      CustomFullScreenLoadingDialog.cancelDialog();
    } on FirebaseAuthException catch (e) {
      Get.offAll(() => const AccountPage());
      if (e.code == "id-token-expired") {
        Get.snackbar(
            'सूचना', 'तपाइको ID TOKEN Expire भएको छ, पूर्ण प्रयास गर्नुहोस।',
            snackPosition: SnackPosition.BOTTOM);
      } else if (e.code == "claims-too-large") {
        Get.snackbar('सूचना',
            'AgroMate को टेक्निकल समस्याक कारण अहिले तपाइको प्रयास सफल हुनसकेन।',
            snackPosition: SnackPosition.BOTTOM);
      } else if (e.code == "internal-error") {
        Get.snackbar('सूचना',
            'AgroMate को टेक्निकल समस्याक कारण अहिले तपाइको प्रयास सफल हुनसकेन।',
            snackPosition: SnackPosition.BOTTOM);
      } else {
        Get.snackbar(
            "सूचना", "तपाइको लगइन सफल हुन सकेन पूर्ण प्रयाश गर्नुहोस।");
      }
    }
  }

  void signInAnonymously() async {
    try {
      CustomFullScreenLoadingDialog.showDialog();
      await auth.signInAnonymously().then((value) {
        if (value.user?.isAnonymous ?? false) {
          FirebaseFirestore.instance.enablePersistence();
          Get.offAll(() => const BottomSheetNavSD());
        }
      }).whenComplete(() {
        Get.put(AppController());
        Get.put(UserController());
        Get.put(AllUsersController());
        Get.put(CommunityPostController());
        Get.put(KinmelController());
        Get.put(NotificationController());
        Get.put(AdsController());
      });

      CustomFullScreenLoadingDialog.cancelDialog();
    } on FirebaseAuthException catch (e) {
      Get.offAll(() => const AccountPage());
      Get.snackbar(
          "ERROR", "Please Try Again with your Correct Email and Password.");
    }
  }

  void signUp(
      TextEditingController fname,
      TextEditingController lname,
      TextEditingController email,
      TextEditingController pass,
      TextEditingController phone,
      TextEditingController userType,
      TextEditingController pradesh,
      TextEditingController jilla,
      TextEditingController includedBusiness) async {
    CustomFullScreenLoadingDialog.showDialog();
    try {
      await auth
          .createUserWithEmailAndPassword(
              email: email.text.trim(), password: pass.text.trim())
          .then((result) {
        if (result.user?.uid != null) {
          String _userId = result.user!.uid;
          _addUserToFirestore(
              _userId,
              phone.text,
              fname.text,
              lname.text,
              email.text,
              pass.text,
              userType.text,
              pradesh.text,
              jilla.text,
              includedBusiness.text);
          FirebaseFirestore.instance.enablePersistence();
          Get.offAll(() => const BottomSheetNavSD());
        }
      }).whenComplete(() {
        phone.clear();
        fname.clear();
        lname.clear();
        email.clear();
        pass.clear();
        userType.clear();
        pradesh.clear();
        jilla.clear();
        email.clear();
        pass.clear();
        includedBusiness.clear();
      });
      userModel.bindStream(userController.listenToUser());
      CustomFullScreenLoadingDialog.cancelDialog();
    } on FirebaseAuthException catch (e) {
      if (e.code.toLowerCase() == "email-already-in-use") {
        Get.snackbar('सूचना', 'तपाइको $email बाट खाता खुलिसकेको छ।',
            snackPosition: SnackPosition.BOTTOM);
      } else if (e.code == "id-token-expired") {
        Get.snackbar(
            'सूचना', 'तपाइको ID TOKEN Expire भएको छ, पूर्ण प्रयास गर्नुहोस।',
            snackPosition: SnackPosition.BOTTOM);
      } else if (e.code == "claims-too-large") {
        Get.snackbar('सूचना',
            'AgroMate को टेक्निकल समस्याक कारण अहिले तपाइको प्रयास सफल हुनसकेन।',
            snackPosition: SnackPosition.BOTTOM);
      } else if (e.code == "internal-error") {
        Get.snackbar('सूचना',
            'AgroMate को टेक्निकल समस्याक कारण अहिले तपाइको प्रयास सफल हुनसकेन।',
            snackPosition: SnackPosition.BOTTOM);
      } else {
        Get.snackbar(
            "सूचना", "तपाइको लगइन सफल हुन सकेन पूर्ण प्रयाश गर्नुहोस।");
      }
    }
  }

  void signOut() async {
    CustomFullScreenLoadingDialog.showDialog();
    SharedPreferences _prefs;
    _prefs = await SharedPreferences.getInstance();
    _prefs.setBool("isLogin", false);
    auth
        .signOut()
        .then((value) => null)
        .whenComplete(() => userModel.bindStream(listenToUser()));
    CustomFullScreenLoadingDialog.cancelDialog();
  }

  void _addUserToFirestore(
      String userId,
      String phone,
      String fname,
      String lname,
      String email,
      String pass,
      String userType,
      String pradesh,
      String jilla,
      String includedBusiness) async {
    String fName = fname.trim();
    String lName = lname.trim();
    String emails = email.trim();
    String phones = phone.trim();
    String password = md5.convert(utf8.encode(pass)).toString();
    String userTypes = userType.trim();
    await firebaseFirestore.collection(usersCollection).doc(userId).set({
      "fname": fName,
      "lname": lName,
      "userId": userId,
      "email": emails,
      "password": password,
      "phone": phones,
      "userType": userTypes,
      'userRegistered': Timestamp.now(),
      'farmName': '',
      'includedBusiness': includedBusiness,
      'address': pradesh + ' ' + jilla,
      'province': pradesh,
      'district': jilla,
      'photoUrl': ''
    });
  }

  updateUserData(Map<String, dynamic> data) {
    firebaseFirestore
        .collection(usersCollection)
        .doc(firebaseUser.value!.uid)
        .update(data);
  }

  Stream<UserModel> listenToUser() => firebaseFirestore
          .collection(usersCollection)
          .doc(auth.currentUser?.uid)
          .snapshots()
          .map((snapshot) => UserModel.fromDocumentSnapshot(snapshot))
          .handleError((onError) {
        debugPrint(
            '\n\n\n\n Error While Fetching User Data : ${onError.toString()} \n\n\n\n\n');
      });
}
