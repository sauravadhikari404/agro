import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/comm.dart';
import 'package:agromate/screen/users/content/community_post.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class CommunityPostController extends GetxController {
  static CommunityPostController instance = Get.find();
  RxList<CommunityModel> commModel = <CommunityModel>[].obs;
  RxList<UserCommunityModel> userCommModel = <UserCommunityModel>[].obs;
  final CollectionReference communityPostRef =
      firebaseFirestore.collection('CummunityPosts');

  @override
  void onInit() {
    commModel.bindStream(allCommunityData());
    super.onInit();
  }

  Stream<List<CommunityModel>> allCommunityData() {
    Stream<QuerySnapshot> stream =
        firebaseFirestore.collection('CummunityPosts').snapshots();

    return stream.map((qShot) => qShot.docs
        .map(
          (doc) => CommunityModel(
            pid: doc.id,
            body: doc.get('body') ?? '',
            by: doc.get('by') ?? '',
            date: doc.get('pdate') ?? 0,
            votes: doc.get('votes') ?? [],
          ),
        )
        .toList());
  }

// ignore: unused_element
  void addPosts(String body, String by) async {
    await firebaseFirestore.collection('CummunityPosts').add({
      'body': body,
      'by': by,
      'votes': [],
      'pdate': Timestamp.now(),
    }).then((value) => Get.snackbar('Message', 'तपाइको प्रश्न सुची गरिएको छ '));
  }

  void updatePosts(String body, String pid) async {
    await firebaseFirestore.collection('CummunityPosts').doc(pid).update({
      'body': body,
      'votes': [],
      'pupdated_at': Timestamp.now(),
    }).then((value) => Get.snackbar('सूचन', 'तपाइको प्रश्न अपडेट गरिएको छ '));
  }

// ignore: unused_element
  void _addPostsNotLoggedUser(
      String body, String by, String name, String address) async {
    await firebaseFirestore.collection('CommunityPosts').add({
      'body': body,
      'by': by,
      'votes': [],
      'pdate': Timestamp.now(),
    }).then((value) => Get.snackbar('Message', 'Your Post is Submitted'));
  }

  void _addComments(String pid, String body, String by) async {
    await communityPostRef
        .doc(pid)
        .collection('Comments')
        .add({'pid': pid, 'body': body, 'by': by}).then(
            (value) => Get.snackbar('Message', 'You have Commented'));
  }

  Stream<QuerySnapshot> getData() {
    return communityPostRef.snapshots();
  }

  Stream<QuerySnapshot> getCommentData(pid) {
    return communityPostRef.doc(pid).collection('Comments').snapshots();
  }

  void addCommentOnPost(pid, body) async {
    var usrData = userController.userModel.value;
    try {
      await communityPostRef.doc(pid).collection('Comments').add({
        'body': body,
        'by': usrData.uid,
        'date': FieldValue.serverTimestamp(),
        'votes': []
      }).then((value) {
        // Get.snackbar('message', 'Successfully Posted.',
        //     snackPosition: SnackPosition.BOTTOM);
      });
    } catch (e) {
      SnackPosition snackPosition = SnackPosition.BOTTOM;
      Get.snackbar('alert', 'Cannot Perform Action',
          snackPosition: snackPosition);
    }
  }

  Stream<List<UserCommunityModel>> currentUserCommunityPost() {
    try {
      String uid = userController.firebaseUser.value!.uid;
      var query = firebaseFirestore.collection('CummunityPosts');
      Stream<QuerySnapshot> stream =
          query.where('by', isEqualTo: uid).snapshots();

      return stream.map((qShot) => qShot.docs.map((doc) {
            return UserCommunityModel(
              pid: doc.id,
              body: doc.get('body') ?? '',
              by: doc.get('by') ?? '',
              date: doc.get('pdate') ?? 0,
              votes: doc.get('votes') ?? [],
            );
          }).toList());
    } on Exception {
      if (!kDebugMode) {
        throw Exception("Some Error Occured While getting Community Post");
      }
      throw [];
    }
  }
}
