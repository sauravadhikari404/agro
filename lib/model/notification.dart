
class NotificationModel {
  final int? id;
  final String title;
  final String body;
  final String image;
  final String date;

  NotificationModel({this.id,required this.title,required this.body,required this.image,required this.date});

  Map<String, dynamic> toMap() {
    return {'id':id,'title':title,'body':body,'image':image,'date':date};
  }

  factory NotificationModel.fromJson(Map<String, dynamic> json)=>NotificationModel(
    title: json['title'],
    body: json['body'],
    image: json['image'],
    date: json['date'],
  );


  // @override
  // String toString() {
  //   return '$title, $body,$image, $date';
  // }
}