import 'package:agromate/model/tipot.dart';
import 'package:hive/hive.dart';

class TipotModelAdapter extends TypeAdapter<TipotModel> {
  @override
  final int typeId = 1;

  @override
  TipotModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TipotModel(
      title: fields[0] as String?,
      body: fields[1] as String?,
      isComplete: fields[2] as bool?,
      color: fields[3] as int?,
      category: fields[4] as String?,
      created: fields[5] as DateTime?,
      id: fields[6] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, TipotModel obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.body)
      ..writeByte(2)
      ..write(obj.isComplete)
      ..writeByte(3)
      ..write(obj.color)
      ..writeByte(4)
      ..write(obj.category)
      ..writeByte(5)
      ..write(obj.created)
      ..writeByte(6)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TipotModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
