import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/model/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class CommunityModel {
  static const PID = 'pid';
  static const BODY = 'body';
  static const BY = 'by';
  static const DATE = 'pdate';

  String? pid;
  String? body;
  String? by;
  Timestamp? date;
  List? votes = [];

  CommunityModel({this.votes, this.pid, this.body, this.by, this.date});

  factory CommunityModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return CommunityModel(
      pid: snapshot.id,
      body: snapshot.get(BODY) ?? '',
      by: snapshot.get(BY) ?? '',
      date: snapshot.get(DATE),
      votes: snapshot.get('votes') ?? '',
    );
  }
}

class CommCommentModel {
  String? cid;
  String? body;
  String? by;
  Timestamp? date;
  List? votes = [];
  CommCommentModel({this.cid, this.body, this.by, this.date, this.votes});

  factory CommCommentModel.fromDocsSnapshot(DocumentSnapshot snap) {
    return CommCommentModel(
      cid: snap.id,
      body: snap.get('body') ?? "",
      by: snap.get('by') ?? "",
      date: snap.get('date') ?? Timestamp.now(),
      votes: snap.get('votes') ?? [],
    );
  }
}

class UserCommunityModel {
  static const PID = 'pid';
  static const BODY = 'body';
  static const BY = 'by';
  static const DATE = 'pdate';

  String? pid;
  String? body;
  String? by;
  Timestamp? date;
  List? votes = [];
 

  UserCommunityModel(
      { this.votes, this.pid, this.body, this.by, this.date});

  factory UserCommunityModel.fromDocumentSnapshot(
      DocumentSnapshot snapshot, List<CommCommentModel> commentMdl) {
    return UserCommunityModel(
      pid: snapshot.id,
      body: snapshot.get(BODY) ?? '',
      by: snapshot.get(BY) ?? '',
      date: snapshot.get(DATE),
      votes: snapshot.get('votes') ?? '',
  
    );
  }
}
