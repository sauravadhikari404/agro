import 'package:hive/hive.dart';

@HiveType(typeId: 1)
class TipotModel {
  @HiveField(0)
  String? title;

  @HiveField(1)
  String? body;

  @HiveField(2)
  bool? isComplete;

  @HiveField(3)
  int? color;

  @HiveField(4)
  String? category;

  @HiveField(5)
  DateTime? created;

  @HiveField(5)
  String? id;

  TipotModel(
      {required this.title,
      required this.body,
      this.isComplete,
      this.color,
      required this.category,
      this.created,
      required this.id});

  TipotModel.fromJson(Map json)
      : title = json['title'],
        body = json['body'],
        isComplete = json['isComplete'],
        category = json['json'],
        color = json['color'],
        created = json['created'];

  toJson() {
    return {
      'title': title,
      'body': body,
      'color': color,
      'isComplete': isComplete,
      'category': category,
      'created': created,
      'id': id,
    };
  }
}
