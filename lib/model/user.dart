import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  static const UID = 'uid';
  static const FNAME = 'fname';
  static const LNAME = 'lname';
  static const EMAIL = 'email';
  static const USERTYPE = 'userType';
  static const PHONE = 'phone';
  static const PASSWORD = 'password';
  String? uid;
  String? fname;
  String? lname;
  String? email;
  String? userType;
  String? phone;
  String? password;
  String? address;
  String? farmName;
  String? includedBusiness;
  String? province;
  String? district;
  String? photoUrl;
  Timestamp? userRegistered;

  UserModel(
      {this.uid,
      this.email,
      this.fname,
      this.lname,
      this.farmName,
      this.password,
      this.phone,
      this.userType,
      this.address,
      this.district,
      this.province,
      this.includedBusiness,
      this.userRegistered,
      this.photoUrl});

  factory UserModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return UserModel(
        uid: snapshot.id,
        fname: snapshot.get('fname') ?? '',
        lname: snapshot['lname'] ?? '',
        email: snapshot.get('email') ?? '',
        userType: snapshot.get('userType') ?? '',
        phone: snapshot.get('phone') ?? '',
        address: snapshot.get('address') ?? '',
        password: snapshot.get('password') ?? '',
        province: snapshot.get('province') ?? '',
        district: snapshot.get('district') ?? '',
        includedBusiness: snapshot.get('includedBusiness') ?? '',
        userRegistered: snapshot.get('userRegistered'),
        photoUrl: snapshot.get('photoUrl'),
        farmName: snapshot.get('farmName') ?? '');
  }
}


class OtherUserModel {
  static const UID = 'uid';
  static const FNAME = 'fname';
  static const LNAME = 'lname';
  static const EMAIL = 'email';
  static const USERTYPE = 'userType';
  static const PHONE = 'phone';
  static const PASSWORD = 'password';
  String? uid;
  String? fname;
  String? lname;
  String? email;
  String? userType;
  String? phone;
  String? password;
  String? address;
  String? farmName;
  String? includedBusiness;
  String? province;
  String? district;
  String? photoUrl;
  Timestamp? userRegistered;

  OtherUserModel(
      {this.uid,
      this.email,
      this.fname,
      this.lname,
      this.farmName,
      this.password,
      this.phone,
      this.userType,
      this.address,
      this.district,
      this.province,
      this.includedBusiness,
      this.userRegistered,
      this.photoUrl});

  factory OtherUserModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return OtherUserModel(
        uid: snapshot.id,
        fname: snapshot.get('fname') ?? '',
        lname: snapshot['lname'] ?? '',
        email: snapshot.get('email') ?? '',
        userType: snapshot.get('userType') ?? '',
        phone: snapshot.get('phone') ?? '',
        address: snapshot.get('address') ?? '',
        password: snapshot.get('password') ?? '',
        province: snapshot.get('province') ?? '',
        district: snapshot.get('district') ?? '',
        includedBusiness: snapshot.get('includedBusiness') ?? '',
        userRegistered: snapshot.get('userRegistered'),
        photoUrl: snapshot.get('photoUrl'),
        farmName: snapshot.get('farmName') ?? '');
  }
}