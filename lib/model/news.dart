import 'package:flutter/foundation.dart';

class NewsModel {
  String? title;
  String? image;
  String? description;
  String? favIcon;
  String? appleIcon;
  String? link;
  static final Map<String, NewsModel> _cache = <String, NewsModel>{};

  factory NewsModel(String title, String image, String description,
      String favIcon, String appleIcon, String link) {
    return _cache.putIfAbsent(
        link,
        () => NewsModel._internal(
            link, description, title, favIcon, appleIcon, image));
  }

  NewsModel._internal(this.link, this.description, this.title, this.favIcon,
      this.appleIcon, this.image) {
    if (kDebugMode) {
      print("New cache created with link ${this.link}");
    }
  }

  void log(String msg) {
    if (kDebugMode) print("${this.link}: $msg");
  }

  Map toJson() => {
        'link': link,
        'title': title,
        'description': description,
        'favIcon': favIcon,
        'appleIcon': appleIcon,
        'image': image,
      };
}

class VideoModel {
  String? title;
  String? image;
  String? description;
  String? favIcon;
  String? appleIcon;
  String? link;
  VideoModel(
      {this.title,
      this.appleIcon,
      this.image,
      this.description,
      this.link,
      this.favIcon});

  Map toJson() => {
        'link': link,
        'title': title,
        'description': description,
        'favIcon': favIcon,
        'appleIcon': appleIcon,
        'image': image,
      };
}

class NewsModelClk {
  String? url;
  NewsModelClk({
    this.url,
  });
}

class VideoModelClk {
  String? url;
  VideoModelClk({this.url});
}
