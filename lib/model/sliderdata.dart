import 'dart:ui';

class SliderAdsModel {
  String? img;
  String? url;
  bool? redirect;

  SliderAdsModel({this.redirect, required this.url, required this.img});

  Map<String, dynamic> toMap() {
    return {'img': img, 'url': url};
  }

  Map toJson() => {
        'img': img,
        'url': url,
        'redirect': redirect,
      };

  factory SliderAdsModel.fromJson(Map<String, dynamic> json) => SliderAdsModel(
      img: json['img'], url: json['url'], redirect: json['redirect']);

  @override
  String toString() {
    return '$img, $url';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is SliderAdsModel && other.img == img && other.url == url;
  }

  @override
  int get hashCode => hashValues(img, url);
}
