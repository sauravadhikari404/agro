class WeatherModel {
  String? status;
  String? place;
  String? max;
  String? min;
  String? rain;

  WeatherModel({this.status, this.place, this.max, this.min, this.rain});

  WeatherModel.fromJson(Map<String, dynamic> json) {
    status = json['status']??'';
    place = json['place']??'';
    max = json['max']??'';
    min = json['min']??'';
    rain = json['rain']??'';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['place'] = this.place;
    data['max'] = this.max;
    data['min'] = this.min;
    data['rain'] = this.rain;
    return data;
  }
}