import 'package:cloud_firestore/cloud_firestore.dart';

class KinmelModel {
  static const PID = 'pid';
  static const QTY = 'quantity';
  static const QLTY = 'quality';
  static const BY = 'by';
  static const PRICE = 'price';
  static const DATE = 'pdate';
  static const VALIDDATE = 'validDate';

  String? pid;
  String? quantity;
  String? by;
  String? price;
  String? quality;
  String? pname;
  Timestamp? date;
  Timestamp? validDate;
  String? pType;
  List? image=[];
  String? address;
  String? phone;
  String? description;
  String? perItemTag;

  List? views = [];
  List? votes = [];

  KinmelModel(
      {this.votes,
      this.pid,
      this.quantity,
      this.by,
      this.date,
      this.price,
      this.pname,
      this.quality,
      this.validDate,
      this.pType,
      this.image,
      this.address,
      this.phone,
      this.views,
      this.description,
      this.perItemTag});

  factory KinmelModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return KinmelModel(
      pid: snapshot.id,
      quantity: snapshot.get('quantity') ?? '',
      by: snapshot.get(BY) ?? '',
      date: snapshot.get('pdate') ?? 0,
      votes: snapshot.get('votes') ?? '',
      pname: snapshot.get('pname') ?? '',
      price: snapshot.get(PRICE) ?? '',
      pType: snapshot.get('pType') ?? '',
      quality: snapshot.get(QLTY) ?? '',
      validDate: snapshot.get(VALIDDATE) ?? 0,
      image: snapshot.get('image') ?? '',
      address: snapshot.get('address') ?? '',
      phone: snapshot.get('phone') ?? '',
      views: snapshot.get('views') ?? [],
    );
  }
}


class UserKinmelModel {
  static const PID = 'pid';
  static const QTY = 'quantity';
  static const QLTY = 'quality';
  static const BY = 'by';
  static const PRICE = 'price';
  static const DATE = 'pdate';
  static const VALIDDATE = 'validDate';

  String? pid;
  String? quantity;
  String? by;
  String? price;
  String? quality;
  String? pname;
  Timestamp? date;
  Timestamp? validDate;
  String? pType;
  List? image=[];
  String? address;
  String? phone;
  String? description;
  String? perItemTag;
  List? views = [];
  List? votes = [];

  UserKinmelModel(
      {this.votes,
      this.pid,
      this.quantity,
      this.by,
      this.date,
      this.price,
      this.pname,
      this.quality,
      this.validDate,
      this.pType,
      this.image,
      this.address,
      this.phone,
      this.views,
      this.description,
      this.perItemTag});

  factory UserKinmelModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return UserKinmelModel(
      pid: snapshot.id,
      quantity: snapshot.get('quantity') ?? '',
      by: snapshot.get(BY) ?? '',
      date: snapshot.get('pdate') ?? 0,
      votes: snapshot.get('votes') ?? '',
      pname: snapshot.get('pname') ?? '',
      price: snapshot.get(PRICE) ?? '',
      pType: snapshot.get('pType') ?? '',
      quality: snapshot.get(QLTY) ?? '',
      validDate: snapshot.get(VALIDDATE) ?? 0,
      image: snapshot.get('image') ?? '',
      address: snapshot.get('address') ?? '',
      phone: snapshot.get('phone') ?? '',
      views: snapshot.get('views') ?? [],
    );
  }
}
