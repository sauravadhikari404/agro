import 'package:cloud_firestore/cloud_firestore.dart';

class UsersModel {
  static const UID = 'uid';
  static const FNAME = 'fname';
  static const LNAME = 'lname';
  static const EMAIL = 'email';
  static const USERTYPE = 'userType';
  static const PHONE = 'phone';
  static const FARMNAME = 'farmName';
  static const ADDRESS = 'address';
  static const USERREGISTEREDDATE = 'userRegistered';
  String? uid;
  String? fname;
  String? lname;
  String? email;
  String? userType;
  String? phone;
  String? farmName;
  String? address;
  String? province;
  String? district;
  String? photoUrl;
  String? includedBusiness;
  Timestamp? userRegisteredDate;

  UsersModel(
      {this.uid,
      this.address,
        this.userRegisteredDate,
      this.email,
      this.fname,
      this.lname,
      this.farmName,
      this.phone,
      this.userType,this.province,this.district,this.includedBusiness,this.photoUrl});

  factory UsersModel.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return UsersModel(
      uid: snapshot.id,
      fname: snapshot.get(FNAME) ?? '',
      lname: snapshot[LNAME] ?? '',
      email: snapshot.get(EMAIL) ?? '',
      userType: snapshot.get(USERTYPE) ?? '',
      phone: snapshot.get(PHONE) ?? '',
      farmName: snapshot.get(FARMNAME) ?? '',
      userRegisteredDate: snapshot.get('userRegistered'),
      address: snapshot.get('address'),
      province: snapshot.get('province')??'',
      district: snapshot.get('district')??'',
        photoUrl: snapshot.get('photoUrl')??'',
      includedBusiness: snapshot.get('includedBusiness')??''
    );
  }
  Map<String, dynamic>? toMap() {
    var map = <String, dynamic>{};

    if (uid != null) {
      map['uid'] = uid;
      map['fname'] = fname;
      map['lname'] = lname;
      map['email'] = email;
      map['userType'] = userType;
      map['phone'] = phone;
      map['address']=address;
      map['farmName'] = farmName;
      map['district'] = district;
      map['photoUrl'] = photoUrl;
      map['province'] = province;
      map['userRegistered'] = userRegisteredDate;
      map['includedBusiness'] = includedBusiness;
      return map;
    }
  }
}
