import 'package:flutter/cupertino.dart';

class HisabKitabExpensesModel {
  HisabKitabExpensesModel(
      {this.id,
      required this.title,
      required this.type,
      required this.amount,
      required this.date});

  final int? id;
  final String title;
  final String type;
  final double amount;
  final String date;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'type': type,
      'amount': amount,
      'date': date
    };
  }

  factory HisabKitabExpensesModel.fromJson(Map<String, dynamic> json) =>
      HisabKitabExpensesModel(
        title: json['title'],
        type: json['type'],
        date: json['date'],
        amount: json['amount'],
      );

  @override
  String toString() {
    return '$title, $type, $amount, $date';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is HisabKitabExpensesModel &&
        other.title == title &&
        other.type == type &&
        other.amount == amount &&
        other.date == date;
  }

  @override
  int get hashCode => hashValues(title, type, amount, date);
}

//Income Model Start
class HisabKitabIncomeModel {
  HisabKitabIncomeModel(
      {this.id, required this.title, required this.amount, required this.date});

  final int? id;
  final String title;
  final double amount;
  final String date;

  Map<String, dynamic> toMap() {
    return {'id': id, 'title': title, 'amount': amount, 'date': date};
  }

  factory HisabKitabIncomeModel.fromJson(Map<String, dynamic> json) =>
      HisabKitabIncomeModel(
        title: json['title'],
        date: json['date'],
        amount: json['amount'],
      );

  @override
  String toString() {
    return '$title, $amount, $date';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is HisabKitabIncomeModel &&
        other.title == title &&
        other.amount == amount &&
        other.date == date;
  }

  @override
  int get hashCode => hashValues(title, amount, date);
}
