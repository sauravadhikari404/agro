import 'package:agromate/services/fetchpreview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:share_plus/share_plus.dart';
import 'package:webviewx/webviewx.dart';

class CoWebViewer extends StatefulWidget {
  final String? title;
  final String? url;
  final String? link;
  const CoWebViewer({Key? key, this.title, this.url, this.link})
      : super(key: key);

  @override
  _CoWebViewerState createState() => _CoWebViewerState();
}

class _CoWebViewerState extends State<CoWebViewer> {
  late WebViewXController webViewXController;
  final initialContent =
      '<h4> This is some hardcoded HTML code embedded inside the webview <h4> <h2> Hello world! <h2>';
  final executeJsErrorMessage =
      'Failed to execute this task because the current content is (probably) URL that allows iframe embedding, on Web.\n\n'
      'A short reason for this is that, when a normal URL is embedded in the iframe, you do not actually own that content so you cant call your custom functions\n'
      '(read the documentation to find out why).';

  @override
  void initState() {
    super.initState();
    didUpdateWidget(const CoWebViewer());
  }

  @override
  void didUpdateWidget(covariant CoWebViewer oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    webViewXController.dispose();
    super.dispose();
  }

  String title = '';
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    final uri = Uri.parse(widget.url!);
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text(
            title == '' ? widget.title! : title.toString(),
            maxLines: 2,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                color: Colors.black, fontFamily: 'poppins', fontSize: 12),
          ),
          bottom: PreferredSize(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        color: Colors.white,
                        height: 23.0,
                        child: Center(
                          child: Text(
                            uri.host.toString(),
                            style: const TextStyle(fontFamily: 'poppins'),
                          ),
                        ),
                      ),
                      IconButton(
                          onPressed: () async {
                            webViewXController.reload();
                          },
                          icon: const Icon(Ionicons.refresh_circle))
                    ],
                  ),
                  isLoading
                      ? Center(
                          child: LinearProgressIndicator(
                            backgroundColor: Colors.white,
                            color: Colors.green.shade300,
                          ),
                        )
                      : Container(),
                ],
              ),
              preferredSize: const Size.fromHeight(30.0)),
        ),
        floatingActionButton: FloatingActionButton.extended(
            onPressed: () {
              Share.share(
                  widget.url.toString() +
                      '#AgroMateApp->https://bit.ly/3AebqXI',
                  subject: '#AgroMateApp->https://bit.ly/3AebqXI',
                  sharePositionOrigin:
                      Rect.fromCircle(center: Offset.zero, radius: 50));
            },
            label: const Icon(Ionicons.share_social_outline)),
        body: WebViewX(
          key: const ValueKey('SDTechWebView'),
          initialContent: widget.url ?? initialContent,
          initialSourceType:
              widget.url == '' ? SourceType.html : SourceType.url,
          onWebViewCreated: (controller) => webViewXController = controller,
          onPageStarted: (src) {
            debugPrint('A new page has started loading: $src\n');
            setState(() {
              setState(() {
                isLoading = true;
              });
              FetchUrlPreview().fetch(src).then((value) {
                setState(() {
                  title = value['title'];
                });
              }).whenComplete(() {
                // ignore: unnecessary_statements
                initialContent;
              });
              setState(() {});
            });
          },
          onPageFinished: (src) {
            debugPrint('The page has finished loading: $src\n');
            // setState(() {
            //   FetchUrlPreview().fetch(src).then((value) {
            //     setState(() {
            //       title = value['title'];
            //     });
            //   }).whenComplete(() {
            //     this.initialContent;
            //   });
            //   setState(() {});
            // });
            setState(() {
              isLoading = false;
            });
          },
          jsContent: const {
            EmbeddedJsContent(
              js: "function testPlatformIndependentMethod() { console.log('Hi from JS') }",
            ),
            EmbeddedJsContent(
              webJs:
                  "function testPlatformSpecificMethod(msg) { TestDartCallback('Web callback says: ' + msg) }",
              mobileJs:
                  "function testPlatformSpecificMethod(msg) { TestDartCallback.postMessage('Mobile callback says: ' + msg) }",
            ),
          },
          dartCallBacks: {
            DartCallback(
              name: 'TestDartCallback',
              callBack: (msg) => Get.snackbar('Alert', msg.toString()),
            )
          },
          webSpecificParams: const WebSpecificParams(
            printDebugInfo: true,
          ),
          navigationDelegate: (navigation) {
            debugPrint(navigation.content.sourceType.toString());
            return NavigationDecision.navigate;
          },
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
        ));
  }

  Future<void> _goForward() async {
    if (await webViewXController.canGoForward()) {
      await webViewXController.goForward();
      Get.snackbar('Alert', 'Did go forward');
    } else {
      Get.snackbar('Alert', 'Cannot go forward');
    }
  }

  Future<void> _goBack() async {
    if (await webViewXController.canGoBack()) {
      await webViewXController.goBack();
      Get.snackbar('Alert', 'Did go back');
    } else {
      Get.snackbar('Alert', 'Cannot go back');
    }
  }

  void _reload() {
    webViewXController.reload();
  }

  void _toggleIgnore() {
    final ignoring = webViewXController.ignoresAllGestures;
    webViewXController.setIgnoreAllGestures(!ignoring);
    Get.snackbar('Alert', 'Ignore events = ${!ignoring}');
  }

  Future<void> _evalRawJsInGlobalContext() async {
    try {
      final result = await webViewXController.evalRawJavascript(
        '2+2',
        inGlobalContext: true,
      );
      Get.snackbar('Alert', 'The result is $result');
    } catch (e) {
      Get.defaultDialog(
          cancel: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Ionicons.exit)),
          middleText: executeJsErrorMessage);
    }
  }

  Future<void> _callPlatformIndependentJsMethod() async {
    try {
      await webViewXController
          .callJsMethod('testPlatformIndependentMethod', []);
    } catch (e) {
      Get.defaultDialog(
          cancel: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Ionicons.exit)),
          middleText: executeJsErrorMessage);
    }
  }

  Future<void> _callPlatformSpecificJsMethod() async {
    try {
      await webViewXController
          .callJsMethod('testPlatformSpecificMethod', ['Hi']);
    } catch (e) {
      Get.defaultDialog(
          cancel: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Ionicons.exit)),
          middleText: executeJsErrorMessage);
    }
  }

  Future<void> _getWebviewContent() async {
    try {
      final content = await webViewXController.getContent();
      Get.defaultDialog(
          cancel: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Ionicons.exit)),
          middleText: content.source);
    } catch (e) {
      Get.defaultDialog(
          cancel: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Ionicons.exit)),
          middleText: 'Failed to execute this task.');
    }
  }

  Widget buildSpace({
    Axis direction = Axis.horizontal,
    double amount = 0.2,
    bool flex = true,
  }) {
    return flex
        ? Flexible(
            child: FractionallySizedBox(
              widthFactor: direction == Axis.horizontal ? amount : null,
              heightFactor: direction == Axis.vertical ? amount : null,
            ),
          )
        : SizedBox(
            width: direction == Axis.horizontal ? amount : null,
            height: direction == Axis.vertical ? amount : null,
          );
  }

  List<Widget> _buildButtons() {
    return [
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: MaterialButton(onPressed: _goBack, child: Text('Back'))),
          buildSpace(amount: 12, flex: false),
          Expanded(
              child: MaterialButton(
                  onPressed: _goForward, child: Text('Forward'))),
          buildSpace(amount: 12, flex: false),
          Expanded(
              child: MaterialButton(onPressed: _reload, child: Text('Reload'))),
        ],
      ),
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      MaterialButton(
        child: Text('Toggle on/off ignore any events (click, scroll etc)'),
        onPressed: _toggleIgnore,
      ),
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      MaterialButton(
        child: Text('Evaluate 2+2 in the global "window" (javascript side)'),
        onPressed: _evalRawJsInGlobalContext,
      ),
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      MaterialButton(
        child: Text('Call platform independent Js method (console.log)'),
        onPressed: _callPlatformIndependentJsMethod,
      ),
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      MaterialButton(
        child: Text(
            'Call platform specific Js method, that calls back a Dart function'),
        onPressed: _callPlatformSpecificJsMethod,
      ),
      buildSpace(direction: Axis.vertical, flex: false, amount: 20.0),
      MaterialButton(
        child: Text('Show current webview content'),
        onPressed: _getWebviewContent,
      ),
    ];
  }
}
