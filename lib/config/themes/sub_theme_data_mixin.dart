import 'package:agromate/config/configs.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum FaButtonType { extended, small, larger }
mixin SubThemeData {
  AppBarTheme getAppBarThemeLight() {
    return const AppBarTheme(
        elevation: 0,
        backgroundColor: ColorManager.kPrimaryColor,
        titleTextStyle: TextStyle(
            fontWeight: FontWeight.w600, fontFamily: 'Poppins', fontSize: 20));
  }

  AppBarTheme getAppBarThemeDark() {
    return const AppBarTheme(
        elevation: 0,
        backgroundColor: ColorManager.kPrimaryColorDark,
        titleTextStyle: TextStyle(
            fontWeight: FontWeight.w600, fontFamily: 'Poppins', fontSize: 20));
  }

  FloatingActionButtonThemeData getFloatingActionButtonThemeLight() {
    return const FloatingActionButtonThemeData(
      backgroundColor: ColorManager.kPrimaryButtonColor,
      extendedTextStyle: TextStyle(
        fontFamily: 'Poppins',
      ),
      splashColor: ColorManager.kPrimaryColorLight,
    );
  }

  FloatingActionButtonThemeData getFloatingActionButtonThemeDark() {
    return const FloatingActionButtonThemeData(
      backgroundColor: ColorManager.kPrimaryColorDark,
      extendedTextStyle: TextStyle(
        fontFamily: 'Poppins',
      ),
      splashColor: ColorManager.kPrimaryColorDarkDim,
    );
  }

  static List<BoxShadow> getGridItemBoxShadow(BuildContext context) =>
      UIParameters.isDarkMode(context)
          ? [
              const BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Theme.of(context).primaryColor,
                  blurRadius: 5,
                  offset: const Offset(1, 2)),
            ]
          : const [
              BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Colors.white, blurRadius: 5, offset: Offset(1, 2)),
              BoxShadow(color: Colors.white, blurRadius: 8),
            ];
            
  static List<BoxShadow> getListItemBoxShadow(BuildContext context) =>
      UIParameters.isDarkMode(context)
          ? [
              const BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Theme.of(context).primaryColor,
                  blurRadius: 5,
                  offset: const Offset(1, 2)),
            ]
          : const [
              BoxShadow(
                  color: Colors.green, blurRadius: 0.5, spreadRadius: 0.5),
              BoxShadow(
                  color: Colors.white, blurRadius: 1.0, spreadRadius: 1.5),
              BoxShadow(
                  color: Colors.black87, blurRadius: 1.0, spreadRadius: 1.5),
              BoxShadow(
                  color: Colors.white, blurRadius: 1.5, spreadRadius: 1.8),
            ];

  // BottomAppBarTheme getBottomAppBarTheme() {
  //   return const BottomAppBarTheme(color: Colors.transparent, elevation: 0);
  // }

  TextTheme getTextThemes() {
    return GoogleFonts.quicksandTextTheme(const TextTheme(
        bodyText1: TextStyle(fontWeight: FontWeight.w400),
        bodyText2: TextStyle(fontWeight: FontWeight.w400)));
    // return const TextTheme(
    //     bodyText1: TextStyle(fontWeight: FontWeight.bold),
    //     bodyText2: TextStyle(fontWeight: FontWeight.bold));
  }

  IconThemeData getPrimaryIconThemeData() {
    return const IconThemeData(
      color: ColorManager.kIconColorLT,
      size: 20,
    );
  }

  TextButtonThemeData getTextButtonThemeData() {
    return TextButtonThemeData(style: getElavatedButtonTheme());
  }

  // InputDecorationTheme getInputDecoration() {
  //   return const InputDecorationTheme();
  // }

  IconThemeData getIconTheme() {
    return const IconThemeData(
      color: ColorManager.kIconColorLT,
      size: 18,
    );
  }

  ButtonStyle getElavatedButtonTheme() {
    return ElevatedButton.styleFrom(
      primary: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
  }

  BottomNavigationBarThemeData getBottomNavigationBarTheme() {
    return const BottomNavigationBarThemeData(
        type: BottomNavigationBarType.fixed,
        elevation: 10,
        showSelectedLabels: false,
        showUnselectedLabels: false);
  }
}
