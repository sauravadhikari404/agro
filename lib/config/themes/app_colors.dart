import 'package:agromate/config/configs.dart';
import 'package:agromate/constant/constant.dart';
import 'package:flutter/material.dart';

// main gradient pattern for light theme
const mainGradientLT = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      ColorManager.kPrimaryColor,
      ColorManager.kPrimaryColorLight,
    ]);

IconData themeChangedIcon(BuildContext context) =>
    UIParameters.isDarkMode(context) ? Icons.nightlight : Icons.sunny;

// Color customScaffoldColor(BuildContext context) =>
//     UIParameters.isDarkMode(context)
//         ? const Color.fromARGB(255, 14, 44, 23)
//         : const Color.fromARGB(255, 237, 255, 247);

Color answerBorderColor(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryColor
        : const Color.fromARGB(255, 221, 221, 221);

Color getAppBarColor(BuildContext context) => UIParameters.isDarkMode(context)
    ? ColorManager.kPrimaryColorDark
    : ColorManager.kPrimaryColor;

Color getDrawerBgColor(BuildContext context) => UIParameters.isDarkMode(context)
    ? ColorManager.kPrimaryColorDark
    : ColorManager.kPrimaryTextLightColor;

LinearGradient getLightGradient() => const LinearGradient(colors: [
      ColorManager.kPrimaryColor,
      ColorManager.kPrimaryColorLight,
    ]);

LinearGradient getDarkGradient() => const LinearGradient(colors: [
      ColorManager.kPrimaryColorDark,
      ColorManager.kPrimaryColorDarkDim,
    ]);

LinearGradient getCardGradient(BuildContext context) =>
    UIParameters.isDarkMode(context) ? getLightGradient() : getDarkGradient();

Color answerSelectedColor(BuildContext context) =>
    Theme.of(context).primaryColor;

//BottomNav Menu Style
Color customGNavBackGroundColor(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kContainerColorLT
        : ColorManager.kPrimaryColor;
Color customGNavRippleBackGroundColor(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryColorDark.withOpacity(0.4)
        : ColorManager.kPrimaryColor.withOpacity(0.4);
Color customGNavIconColorUnselected(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryTextLightColor
        : ColorManager.kPrimaryTextColor;
Color customGNavIconColorSelected(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryTextColor
        : ColorManager.kPrimaryTextLightColor;
Color customGnavContainerColor(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryColorDark //dark huda container color
        : ColorManager.kContainerColorLT; //nahuda container color

Color customGnavBoxShadowColor(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryTextLightColor
        : ColorManager.kPrimaryTextColor.withOpacity(0.2);

Color customDrawerIconColorChanged(BuildContext context) =>
    UIParameters.isDarkMode(context)
        ? ColorManager.kPrimaryTextLightColor
        : ColorManager.kPrimaryTextColor;
