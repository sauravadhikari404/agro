import 'package:agromate/config/configs.dart';
import 'package:flutter/material.dart';
import 'sub_theme_data_mixin.dart';

class DarkTheme with SubThemeData {
  ThemeData buildDarkTheme() {
    final ThemeData systemDarkTheme = ThemeData.dark();
    return systemDarkTheme.copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: ColorManager.kScafoldBacgroundColorDark,
        splashColor: ColorManager.kPrimaryColorDark.withOpacity(0.1),
        cardTheme: const CardTheme(
          clipBehavior: Clip.antiAlias,
          color: ColorManager.kCardColorDK,
        ),
        iconTheme: const IconThemeData(
          color: ColorManager.kPrimaryTextLightColor,
        ),
        //floating action btn theme
        floatingActionButtonTheme: getFloatingActionButtonThemeDark(),
        //appbar theme
        appBarTheme: getAppBarThemeDark(),
        
        highlightColor: ColorManager.kPrimaryColor.withOpacity(0.05),
        textTheme: getTextThemes().apply(
            bodyColor: ColorManager.kPrimaryTextLightColor,
            displayColor: ColorManager.kPrimaryTextLightColor),
        cardColor: ColorManager.kCardColorDK,
        primaryColor: ColorManager.kPrimaryColorDark);
  }
}
