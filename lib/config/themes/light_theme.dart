import 'package:agromate/constant/manager/color_manager.dart';
import 'package:flutter/material.dart';
import 'sub_theme_data_mixin.dart';

class LightTheme with SubThemeData {
  ThemeData buildLightTheme() {
    final ThemeData systemLightTheme = ThemeData.light();
    return systemLightTheme.copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: const Color.fromARGB(255, 237, 254, 243),
        iconTheme: getIconTheme(),
        
        appBarTheme: getAppBarThemeLight(),
        floatingActionButtonTheme: getFloatingActionButtonThemeLight(),
        splashColor: ColorManager.kPrimaryColorLight.withOpacity(0.1),
        highlightColor: ColorManager.kPrimaryColorLight.withOpacity(0.05),
        splashFactory: InkRipple.splashFactory,
        textTheme: getTextThemes().apply(
            bodyColor: ColorManager.kPrimaryTextColor,
            displayColor: ColorManager.kPrimaryTextColorLT),
        primaryColor: ColorManager.kPrimaryColor,
        cardColor: ColorManager.kCardColorLT,
        colorScheme: ColorScheme.fromSwatch(
          accentColor: ColorManager.kPrimaryColorLight,
          primarySwatch: Colors.green,
        ),
        buttonTheme: ButtonThemeData(
          splashColor: ColorManager.kPrimaryButtonColor.withOpacity(0.4),
          buttonColor: Colors.white,
        ),
      
        canvasColor: Colors.white,
        drawerTheme: const DrawerThemeData(
            backgroundColor: ColorManager.kScafoldBacgroundColor, elevation: 2),

        //list tile theme
        listTileTheme: const ListTileThemeData(
          textColor: ColorManager.kPrimaryTextColor,
          iconColor: ColorManager.kPrimaryColorLight,
        ));
  }
}
