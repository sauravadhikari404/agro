import 'package:agromate/config/configs.dart';
import 'package:flutter/material.dart';

const kHeaderTS = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.w700,
    color: ColorManager.kAppBarTextColorLT);

const kDetailsTS = TextStyle(fontSize: 12);

TextStyle cardTitleTs(context) => TextStyle(
    color: UIParameters.isDarkMode(context)
        ? Theme.of(context).textTheme.bodyText1!.color
        : Theme.of(context).primaryColor,
    fontSize: 18,
    fontWeight: FontWeight.bold);

const kQuizeTS = TextStyle(fontSize: 16, fontWeight: FontWeight.w800);

const kAppBarTS = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 16,
    color: ColorManager.kAppBarTextColorLT);


//BottomNavBar Style
TextStyle customGNavTextStyle(BuildContext context) =>
    UIParameters.isDarkMode(context) ? const TextStyle(
                            fontSize: 10,
                            fontFamily: 'Poppins',
                            color: Colors.black) :
                            const TextStyle(
                            fontSize: 10,
                            fontFamily: 'Poppins',
                            color: Colors.white);
