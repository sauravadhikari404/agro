import 'package:agromate/binding/instancebinding.dart';
import 'package:agromate/constant/route.dart';
import 'package:agromate/controller/theme_controller.dart';
import 'package:agromate/firebase_options.dart';
import 'package:agromate/screen/splash/sdsplash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:nepali_utils/nepali_utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  ).then((value) {
    InstanceBinding().dependencies();
  });
  
  NepaliUtils(Language.nepali);
  runApp(const AgroMate());
}

class AgroMate extends StatefulWidget {
  const AgroMate({Key? key}) : super(key: key);

  @override
  State<AgroMate> createState() => _AgroMateState();
}

class _AgroMateState extends State<AgroMate> {
  DateTime preBackpress = DateTime.now();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return GetMaterialApp(
      initialBinding: InstanceBinding(),
      themeMode: Get.find<ThemeController>().getThemeMode(),
      darkTheme: Get.find<ThemeController>().getDarkTheme(),
      theme: Get.find<ThemeController>().getLightheme(),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: RouteGenerator.generateRoute,
      home: const SDSplashScreen(),
    );
  }
}
