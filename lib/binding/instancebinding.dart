import 'package:agromate/controller/adscontroller.dart';
import 'package:agromate/controller/all_user_controller.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/controller/communitypost_controller.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/controller/kinmel_controller.dart';
import 'package:agromate/controller/like_controller.dart';
import 'package:agromate/controller/news_controller.dart';
import 'package:agromate/controller/notificationController.dart';
import 'package:agromate/controller/slider_ads_controller.dart';
import 'package:agromate/controller/theme_controller.dart';
import 'package:get/get.dart';

class InstanceBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<AdsController>(AdsController());
    Get.lazyPut<UserController>(() => UserController());
    Get.put<UserController>(UserController(), permanent: true);
    Get.lazyPut<SliderAdsController>(() => SliderAdsController());
    Get.put<SliderAdsController>(SliderAdsController());
    Get.lazyPut<AllUsersController>(() => AllUsersController());
    Get.put<AllUsersController>(AllUsersController());
    Get.lazyPut<CommunityPostController>(() => CommunityPostController());
    Get.put<CommunityPostController>(CommunityPostController());
    Get.lazyPut<LikeController>(() => LikeController());
    Get.put<LikeController>(LikeController());
    Get.lazyPut<KinmelController>(() => KinmelController());
    Get.put<KinmelController>(KinmelController());
    Get.lazyPut<HisabKitabExpensesController>(
        () => HisabKitabExpensesController());
    Get.put<HisabKitabExpensesController>(HisabKitabExpensesController());
    Get.lazyPut<HisabKitabIncomeController>(() => HisabKitabIncomeController());
    Get.put<HisabKitabIncomeController>(HisabKitabIncomeController());

    Get.lazyPut<NotificationController>(() => NotificationController());
    Get.put<NotificationController>(NotificationController());
    Get.lazyPut<SliderAdsController>(() => SliderAdsController());
    Get.put<SliderAdsController>(SliderAdsController());
    Get.lazyPut<NewsController>(() => NewsController());
    Get.put<NewsController>(NewsController());
    Get.lazyPut<ThemeController>(() => ThemeController());
    Get.put<ThemeController>(ThemeController());
  }
}
