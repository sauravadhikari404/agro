import 'package:agromate/model/hisabkitab.dart';
import 'package:agromate/model/notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

///ExpensesDataHelper Class
class DatabaseHelper {
  static const _databaseName = "hisabkitabDb.db";
  static const _databaseVersion = 1;
  //Tables
  static const table = "dailyexpenses";
  static const table2 = "dailyincome";
  static const table3 = "agronoty";
  static const table4 = "tipot";
  //Column
  static const columnId = 'id';
  static const columnTitle = 'title';
  static const columnBody = 'body';
  static const columnImage = 'image';
  static const columnType = 'type';
  static const columnAmount = 'amount';
  static const columnDate = 'date';
  static const columnDateCreated = 'datecreated';
  static const columnDateLastUpdated = 'datelastedited';
  static const columnTipotColor = 'tipotcolor';
  static const columnIsArchived = 'isarchived';
  static const columnCategory = 'category';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) {
      debugPrint('Database Cha Hai Null Chaina');
      return _database!;
    }
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    String path = await getDatabasesPath();
    return await openDatabase(join(path, _databaseName),
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
  CREATE TABLE $table (
    $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
    $columnTitle TEXT NOT NULL,
    $columnType TEXT NOT NULL,
    $columnAmount REAL NOT NULL,
    $columnDate TEXT NOT NULL
  )
  ''').then((value) => null);

    await db.execute('''
  CREATE TABLE $table3 (
    $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
    $columnTitle TEXT NOT NULL,
    $columnBody TEXT NOT NULL,
    $columnImage TEXT NOT NULL,
    $columnDate TEXT NOT NULL
  )
  ''').then((value) => null);

    await db.execute('''
  CREATE TABLE $table2 (
    $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
    $columnTitle TEXT NOT NULL,
    $columnAmount REAL NOT NULL,
    $columnDate TEXT NOT NULL
  )
  ''').then((value) => null);
  }

  Future<int> insertExpenses(HisabKitabExpensesModel hisab) async {
    try {
      Database db = await instance.database;
      var res = await db
          .insert(table, hisab.toMap())
          .whenComplete(() => null)
          .catchError((error) {
        Get.snackbar('Error', error.toString());
      });
      return res;
    } catch (e) {
      Get.snackbar('error', e.toString());
    }
    return 0;
  }

  ///Update Data of DailyExpenses Table
  Future<int> updateExpenses(HisabKitabExpensesModel hisab, int id) async {
    Database db = await instance.database;
    var res = await db
        .update(table, hisab.toMap(), where: '$columnId = ?', whereArgs: [id]);
    return res;
  }

  Future<List<Map<String, dynamic>>> queryAllRowsExpenses() async {
    Database db = await instance.database;
    var res = await db.query(table, orderBy: "$columnId DESC");
    return res;
  }

  Future<int> deleteExpenses(int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<List<Map<String, Object?>>> clearTableExpenses() async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM $table");
  }

  //Income Table Query Started

  Future<int> insertIncome(HisabKitabIncomeModel hisab) async {
    Database db = await instance.database;
    var res = await db.insert(table2, hisab.toMap());
    return res;
  }

  ///Update Data of DailyExpenses Table
  Future<int> updateIncome(HisabKitabIncomeModel hisab, int id) async {
    Database db = await instance.database;
    var res = await db
        .update(table2, hisab.toMap(), where: '$columnId = ?', whereArgs: [id]);
    return res;
  }

  Future<List<Map<String, dynamic>>> queryAllRowsIncome() async {
    Database db = await instance.database;
    var res = await db.query(table2, orderBy: "$columnId DESC");
    return res;
  }

  Future<int> deleteIncome(int id) async {
    Database db = await instance.database;
    return await db.delete(table2, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<List<Map<String, Object?>>> clearTableIncome() async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM $table2");
  }

  Future<int> insertNotification(NotificationModel noty) async {
    try {
      Database db = await instance.database;
      var res = await db
          .insert(table3, noty.toMap())
          .whenComplete(() => null)
          .catchError((error) {
        Get.snackbar('Error', error.toString());
      });
      return res;
    } catch (e) {
      Get.snackbar('error', e.toString());
    }
    return 0;
  }

  Future<int> deleteNotification(int id) async {
    Database db = await instance.database;
    return await db.delete(table3, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<List<Map<String, Object?>>> clearTableNotification() async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM $table3");
  }

  Future<List<Map<String, dynamic>>> queryAllRowsNotification() async {
    Database db = await instance.database;
    var res = await db.query(table3, orderBy: "$columnId DESC");
    return res;
  }
}
