const String imagePath = "images";
const String jsonPath = "assets";

class ImageAssets {
  static const String splashLogo = "$imagePath/agronepal.png";
  static const String splashLogoNoName = "$imagePath/agrologo.png";
  static const String splashLogoWithStraightName = "$imagePath/agrologo.png";
  static const String avatar = "$imagePath/avatar.png";
  static const String noImg = "$imagePath/noimg.png";
}

class JsonAssets {
  static const String emptyJson = "$jsonPath/empt.json";
  static const String imgLoading = "$jsonPath/img_loading.json";
  static const String loadingDot = "$jsonPath/loading_dot.json";
  static const String errorExplamation = "$jsonPath/error_explamation.json";
}


