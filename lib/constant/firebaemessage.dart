class FirebaseMessage {
  final String? title;
  final String? body;
  final String? image;
  const FirebaseMessage({this.title, this.body, this.image});
}
