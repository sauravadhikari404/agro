import 'dart:async';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;

import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/controller/all_user_controller.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/loading_dialog.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/widget/helpers/validator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:nepali_utils/nepali_utils.dart';

const hisabExpensesKey = 'hisabExpenses';
const hisabIncomeKey = 'hisabIncome';

///yo Chai Image Error Handling ko Lagi headerMap Banako
Map<String, String> headersMap = {
  'Cookie':
      'jwt-cookie=eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI0IiwiaXNzIjoiMSIsInN1YiI6InRtYSIsImlhdCI6MTU1NjExNTY2MCwiZXhwIjoxNTU2NzIwNDYwfQ.DQMV59lTlGSgVN_viwlUaJIxZNO_Sru0gQT31EnKZEdD533OR9VUCRYaj5pY8ist48zRUmn6HXs4M_oWkkzm7A'
};

Size sizeProvider(context) {
  return MediaQuery.of(context).size;
}

///Yo chai Body Text Styling ko lagi banako
TextStyle bodyTextStyle(
    Color? textColor, String? fontFamily, FontWeight? fontWeight) {
  return TextStyle(
    color: textColor ?? Colors.black,
    fontFamily: fontFamily ?? 'Ubuntu',
    fontWeight: fontWeight ?? FontWeight.normal,
  );
}

///yesle chai user ko detail store garxa ani Map Value Return Garxa
Map<String, dynamic> storeIt(by) {
  final AllUsersController usrData = Get.put(AllUsersController());
  var map = <String, dynamic>{};
  for (var element in usrData.usersModel) {
    if (by == element.uid) {
      map['by'] = element.uid ?? "";
      map['fname'] = element.fname ?? '';
      map['lname'] = element.lname ?? '';
      map['farmName'] = element.farmName ?? '';
      map['address'] = element.email ?? '';
      map['phone'] = element.phone ?? '';
      map['userType'] = element.userType ?? '';
      map['address'] = element.address ?? '';
      map['district'] = element.district ?? '';
      map['province'] = element.province ?? '';
      map['includedBusiness'] = element.includedBusiness ?? '';
      map['userRegistered'] = element.userRegisteredDate;
      map['photoUrl'] = element.photoUrl ?? '';
    }
  }
  return map;
}

///Yesle Chai Current User chai Viewed or Voted User ko List Ma Xa Ki Nai Check Garxa Ra Boolean Value Return Garxa
bool isCurrentUserLikeIt(var votes) {
  var usrData = userController.userModel.value;
  bool? isVoted;

  for (int i = 0; i < votes.length; i++) {
    if (votes[i] == usrData.uid) {
      isVoted = true;
    }
  }
  if (kDebugMode) {
    print(isVoted.toString());
  }
  return isVoted ?? false;
}

///Is User Login As Anonymous? It will return boolean value (true/false) if the user is anonymous.
bool isLoginUser() {
  var ur = userController.firebaseUser.value;
  if (ur!.isAnonymous) {
    return false;
  }
  return true;
}

///Kati Samaya Agadi Ho Bhanerw Dekhauna ko Lagi banako Function ho yesle String value Return Garxa
// String timeAgo(Timestamp? date) {
//   DateTime dates =
//       DateTime.parse(date?.toDate().toString() ?? '${DateTime.now()}');
//   String finalVal = GetTimeAgo.parse(dates);
//   return finalVal;
// }

/// English Numberic Data lai Nepali Numeric Data ma Convert Gari String value Return Garxa
String nepNumber(data) {
  var num = NepaliNumberFormat(
      decimalDigits: data.toDouble(), language: Language.nepali);
  return num.toString();
}

bool isCurrentUser(String by) {
  var ur = userController.firebaseUser.value;
  if (ur!.uid == by) {
    return true;
  }
  return false;
}

SnackBar getSnackbar(String text, {int ms = 20}) {
  return SnackBar(
      duration: Duration(milliseconds: ms),
      content: SizedBox(
        width: double.infinity,
        height: 50,
        child: Text(
          text,
          style: const TextStyle(fontFamily: 'ubuntu', color: Colors.white),
        ),
      ));
}

Dialog getDialog(confirmAction,
    {String message = "Are you Sure You want to delete your comment?",
    String pid = "",
    String cid = "",
    List? img,
    BuildContext? context}) {
  Dialog confirmDialog = Dialog(
    backgroundColor: Colors.greenAccent,
    shape: const RoundedRectangleBorder(
        side: BorderSide(color: Colors.black),
        borderRadius: BorderRadius.all(Radius.circular(8))),
    child: SizedBox(
      height: 150.0,
      width: 200.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(FontAwesomeIcons.warning),
            const SizedBox(
              height: 10,
            ),
            Text(
              message,
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MaterialButton(
                  onPressed: () {
                    if (img == null && context == null) {
                      Get.back();
                      confirmAction(pid, cid);
                    } else if (img != null) {
                      Get.back();
                      confirmAction(pid, context, img);
                    }
                  },
                  child: const Text(
                    'Yes',
                    style: TextStyle(
                      fontFamily: 'ubuntu',
                    ),
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text(
                    'No',
                    style: TextStyle(
                      fontFamily: 'ubuntu',
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    ),
  );
  return confirmDialog;
}

Dialog getDialogCommunity(confirmAction,
    {String message = "Are you Sure You want to Delete Community Post?",
    String pid = "",
    BuildContext? context}) {
  Dialog confirmDialog = Dialog(
    backgroundColor: Colors.greenAccent,
    shape: const RoundedRectangleBorder(
        side: BorderSide(color: Colors.black),
        borderRadius: BorderRadius.all(Radius.circular(8))),
    child: SizedBox(
      height: 150.0,
      width: 200.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(FontAwesomeIcons.warning),
            const SizedBox(
              height: 10,
            ),
            Text(
              message,
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MaterialButton(
                  onPressed: () {
                    confirmAction(pid, context);
                  },
                  child: const Text(
                    'Yes',
                    style: TextStyle(
                      fontFamily: 'ubuntu',
                    ),
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text(
                    'No',
                    style: TextStyle(
                      fontFamily: 'ubuntu',
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    ),
  );
  return confirmDialog;
}

getNotLoggedDialog(BuildContext context) {
  return Get.defaultDialog(
      title: 'सूचना',
      middleText: 'कृपया लगइन गर्नुहोस',
      textCancel: 'रद्दगर्नुहोस',
      textConfirm: 'लगइनगर्नुहोस',
      confirmTextColor: Colors.white,
      onConfirm: () {
        Get.back();
        Get.to(const AccountPage());
      },
      onCancel: () {
        Get.back();
      });
}

Dialog getReportDialog(BuildContext context) {
  return Dialog(
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        side: BorderSide(color: Colors.black54)),
    child: SingleChildScrollView(
      child: Container(
        width: 200,
        height: 350,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black54,
                      spreadRadius: 0.5,
                      offset: Offset(0, 0.2)),
                  BoxShadow(color: Colors.white),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Select options to Report',
                    style: TextStyle(
                      fontFamily: 'ubuntu',
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: const Icon(
                        FontAwesomeIcons.close,
                        color: Colors.black54,
                        size: 16,
                      ))
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SingleChildScrollView(child: getReportList(context))
          ],
        ),
      ),
    ),
  );
}

Widget getReportList(BuildContext context) {
  ScrollController scrollController = ScrollController();

  ListView mylist = ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      shrinkWrap: true,
      controller: scrollController,
      itemCount: reportList.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
              onTap: () {
                Dialog tick = const Dialog(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  child: SizedBox(
                    width: 20,
                    height: 50,
                    child: Icon(
                      FontAwesomeIcons.thumbsUp,
                      color: Colors.greenAccent,
                    ),
                  ),
                );

                showDialog(
                    context: context,
                    builder: (_) {
                      return tick;
                    });
                Future.delayed(const Duration(milliseconds: 1000), () {
                  Get.back();
                });
                Future.delayed(const Duration(milliseconds: 500), () {
                  Get.back();
                });
              },
              visualDensity: const VisualDensity(horizontal: 0.0, vertical: -2),
              leading: Text(reportList[index]['title'] ?? ''),
              trailing: const Icon(
                Ionicons.arrow_forward,
                size: 13,
              ),
            ),
          ),
        );
      });
  return mylist;
}

// ignore: prefer_function_declarations_over_variables
///Image Load huda Error Handle garna Loading Builder Banako
// ignore: prefer_function_declarations_over_variables
ImageLoadingBuilder loadingBuilder = (context, child, loadingProgress) {
  if (loadingProgress == null) return child;
  return Center(
    child: CircularProgressIndicator(
      value: loadingProgress.expectedTotalBytes != null
          ? loadingProgress.cumulativeBytesLoaded /
              loadingProgress.expectedTotalBytes!
          : null,
    ),
  );
};

Future<void> editComment(
    pid, cid, bodys, BuildContext context, parentCollection) async {
  await firebaseFirestore
      .collection(parentCollection)
      .doc(pid)
      .collection('Comments')
      .doc(cid)
      .update({"body": bodys.toString()})
      .then((value) => null)
      .whenComplete(() {
        ScaffoldMessenger.of(context)
            .showSnackBar(getSnackbar('Your Comment Updated', ms: 1500));
        Get.back();
      });
}

Future<void> deleteComment(
    pid, cid, BuildContext context, parentCollection) async {
  await firebaseFirestore
      .collection(parentCollection)
      .doc(pid)
      .collection('Comments')
      .doc(cid)
      .delete()
      .then((value) => ScaffoldMessenger.of(context).removeCurrentSnackBar())
      .whenComplete(() {
    ScaffoldMessenger.of(context)
        .showSnackBar(getSnackbar('Your Comment has been deleted', ms: 600));
    Get.back();
  });
}

Future<void> deleteKinemlPost(pid, BuildContext context, List img) async {
  CustomFullScreenLoadingDialog.showDialog();
  String by = userController.userModel.value.uid ?? "";
  await firebaseFirestore
      .collection("Kinmel")
      .doc(pid)
      .delete()
      .then((value) async {
    for (String url in img) {
      FirebaseStorage storage = FirebaseStorage.instance;
      Reference _ref = storage.refFromURL(url);
      await _ref.delete().then((value) => null).whenComplete(() {
        Future.delayed(const Duration(seconds: 4), () {
          CustomFullScreenLoadingDialog.cancelDialog();
          ScaffoldMessenger.of(context).showSnackBar(getSnackbar(
              'तपाइको समान किनमेल/कृषि बजारबाट मिटाइएको छ।',
              ms: 600));
        });
      });
    }
  }).whenComplete(() {});
}

Future<void> deleteCommunityPost(pid, BuildContext context) async {
  await firebaseFirestore
      .collection("CummunityPosts")
      .doc(pid)
      .delete()
      .then((value) => ScaffoldMessenger.of(context).removeCurrentSnackBar())
      .whenComplete(() {
    ScaffoldMessenger.of(context).showSnackBar(
        getSnackbar('समुदाय बाट तपाइको पृष्ठ हटाइएको छ ।', ms: 1000));
    Get.back();
  });
}

/// Yo Chai User Data Valid Xa Ki nai Check Garna Banako Function Ho. Parameter(String fname, String lname, String email,String phone, String pass, String passconfig, String userType )
void validateRegisterForm(
    TextEditingController fname,
    TextEditingController lname,
    TextEditingController email,
    TextEditingController phone,
    TextEditingController pass,
    TextEditingController passconfig,
    TextEditingController userType,
    TextEditingController pradesh,
    TextEditingController jilla,
    TextEditingController includedBusiness) {
  if (email.text == '' &&
      pass.text == '' &&
      fname.text == '' &&
      lname.text == '' &&
      phone.text == '' &&
      userType.text == '' &&
      jilla.text == '') {
    Get.snackbar('alert', 'All Field Are Required');
  } else if (email.text == '' && pass.text != '') {
    Get.snackbar('alert', 'Please Enter your Email Address.');
  } else if (pass.text == '' && email.text != '') {
    Get.snackbar('alert', 'Please Enter your Password.');
  } else if (email.text != '' &&
      pass.text != '' &&
      fname.text != '' &&
      lname.text != '' &&
      userType.text != '' &&
      phone.text != '') {
    if (GetUtils.isEmail(email.text)) {
      if (pass.text == passconfig.text) {
        Get.find<UserController>().signUp(fname, lname, email, pass, phone,
            userType, pradesh, jilla, includedBusiness);
      } else {
        Get.snackbar(
            'Alert', 'Password and Confirmation Password Doen\'t Match. ');
      }
    } else {
      Get.snackbar('Alert', 'Invalid Email Address.');
    }
  }
}

/// Yo Login Form Validation Garna Banako Function ho Parameter(String email, String pass)
void validateLoginForm(String email, String pass) {
  if (email == '' && pass == '') {
    Get.snackbar('alert', 'Please Enter your Email and Password.');
  } else if (email == '' && pass != '') {
    Get.snackbar('alert', 'Please Enter your Email Address.');
  } else if (pass == '' && email != '') {
    Get.snackbar('alert', 'Please Enter your Password.');
  } else if (email != '' && pass != '') {
    if (GetUtils.isEmail(email)) {
      Get.find<UserController>().signIn(email, pass);
    } else {
      Get.snackbar('Alert', 'Invalid Email Address.');
    }
  }
}

///District List
const districtMap = [
  {'titleEn': 'Panchthar'},
  {'titleEn': 'Ilam'},
  {'titleEn': 'Jhapa'},
  {'titleEn': 'Morang'},
  {'titleEn': 'Sunsari'},
  {'titleEn': 'Terhathum'},
  {'titleEn': 'Bhojpur'},
  {'titleEn': 'Sankhuwasabha'},
  {'titleEn': 'Solukhumbu'},
  {'titleEn': 'Khotang'},
  {'titleEn': 'Okhaldhunga'},
  {'titleEn': 'Udayapur'},
  {'titleEn': 'Saptari'},
  {'titleEn': 'Sindhuli'},
  {'titleEn': 'Dolakha'},
  {'titleEn': 'Rasuwa'},
  {'titleEn': 'Nuwakot'},
  {'titleEn': 'Lalitpur'},
  {'titleEn': 'Makwanpur'},
  {'titleEn': 'Bara'},
  {'titleEn': 'Parsa'},
  {'titleEn': 'Kapilbastu'},
  {'titleEn': 'Arghakhanchi'},
  {'titleEn': 'Syangja'},
  {'titleEn': 'Gorkha'},
  {'titleEn': 'Kaski'},
  {'titleEn': 'Manang'},
  {'titleEn': 'Baglung'},
  {'titleEn': 'Dang'},
  {'titleEn': 'Rolpa'},
  {'titleEn': 'Mugu'},
  {'titleEn': 'Jumla'},
  {'titleEn': 'Dailekh'},
  {'titleEn': 'Surkhet'},
  {'titleEn': 'Banke'},
  {'titleEn': 'Doti'},
  {'titleEn': 'Bajura'},
  {'titleEn': 'Baitadi'},
  {'titleEn': 'Kanchanpur'},
  {'titleEn': 'Rukum East'},
  {'titleEn': 'Taplejung'},
  {'titleEn': 'Dhankuta'},
  {'titleEn': 'Siraha'},
  {'titleEn': 'Dhanusa'},
  {'titleEn': 'Mahottari'},
  {'titleEn': 'Saralahi'},
  {'titleEn': 'Ramechhap'},
  {'titleEn': 'Sindhupalchok'},
  {'titleEn': 'Dhading'},
  {'titleEn': 'Kathmandu'},
  {'titleEn': 'Bhaktapur'},
  {'titleEn': 'Kavrepalanchok'},
  {'titleEn': 'Rautahat'},
  {'titleEn': 'Chitwan'},
  {'titleEn': 'Rupandehi'},
  {'titleEn': 'Palpa'},
  {'titleEn': 'Gulmi'},
  {'titleEn': 'Tanahu'},
  {'titleEn': 'Lamjung'},
  {'titleEn': 'Mustang'},
  {'titleEn': 'Myagdi'},
  {'titleEn': 'Parbat'},
  {'titleEn': 'Pyuthan'},
  {'titleEn': 'Salyan'},
  {'titleEn': 'Dolpa'},
  {'titleEn': 'Humla'},
  {'titleEn': 'Kalikot'},
  {'titleEn': 'Jajarkot'},
  {'titleEn': 'Bardiya'},
  {'titleEn': 'Kailali'},
  {'titleEn': 'Achham'},
  {'titleEn': 'Bajhang'},
  {'titleEn': 'Darchula'},
  {'titleEn': 'Dadeldhura'},
  {'titleEn': 'Nawalparasi East'},
  {'titleEn': 'Nawalparasi West'},
];

const reportList = [
  {'title': 'Harassment'},
  {'title': 'False Information'},
  {'title': 'Hate Speech'},
  {'title': 'Terrorism'},
  {'title': 'Something Else'},
];

/// जिल्ला सुची
const jillaMap = [
  [
    {'title': 'भोजपुर जिल्ला'},
    {'title': 'धनकुटा जिल्ला'},
    {'title': 'इलाम जिल्ला'},
    {'title': 'झापा जिल्ला'},
    {'title': 'खोटाँग जिल्ला'},
    {'title': 'मोरंग जिल्ला'},
    {'title': 'ओखलढुंगा जिल्ला'},
    {'title': 'पांचथर जिल्ला'},
    {'title': 'संखुवासभा जिल्ला'},
    {'title': 'सोलुखुम्बु जिल्ला'},
    {'title': 'सुनसरी जिल्ला'},
    {'title': 'ताप्लेजुंग जिल्ला'},
    {'title': 'तेर्हथुम जिल्ला'},
    {'title': 'उदयपुर जिल्ला'},
  ],
  [
    {'title': 'पर्सा जिल्ला'},
    {'title': 'बारा जिल्ला'},
    {'title': 'रौतहट जिल्ला'},
    {'title': 'सर्लाही जिल्ला'},
    {'title': 'धनुषा जिल्ला'},
    {'title': 'सिराहा जिल्ला'},
    {'title': 'महोत्तरी जिल्ला'},
    {'title': 'सप्तरी जिल्ला'},
  ],
  [
    {'title': 'सिन्धुली जिल्ला'},
    {'title': 'रामेछाप जिल्ला'},
    {'title': 'दोलखा जिल्ला'},
    {'title': 'भक्तपुर जिल्ला'},
    {'title': 'धादिङ जिल्ला'},
    {'title': 'काठमाडौँ जिल्ला'},
    {'title': 'काभ्रेपलान्चोक जिल्ला'},
    {'title': 'ललितपुर जिल्ला'},
    {'title': 'नुवाकोट जिल्ला'},
    {'title': 'रसुवा जिल्ला'},
    {'title': 'सिन्धुपाल्चोक जिल्ला'},
    {'title': 'चितवन जिल्ला'},
    {'title': 'मकवानपुर जिल्ला'},
  ],
  [
    {'title': 'बागलुङ जिल्ला'},
    {'title': 'गोरखा जिल्ला'},
    {'title': 'कास्की जिल्ला'},
    {'title': 'लमजुङ जिल्ला'},
    {'title': 'मनाङ जिल्ला	'},
    {'title': 'मुस्ताङ जिल्ला'},
    {'title': 'म्याग्दी जिल्ला'},
    {'title': 'नवलपुर जिल्ला'},
    {'title': 'पर्वत जिल्ला	'},
    {'title': 'स्याङग्जा जिल्ला'},
    {'title': 'तनहुँ जिल्ला'},
  ],
  [
    {'title': 'कपिलवस्तु जिल्ला'},
    {'title': 'परासी जिल्ला'},
    {'title': 'रुपन्देही जिल्ला'},
    {'title': 'अर्घाखाँची जिल्ला'},
    {'title': 'गुल्मी जिल्ला'},
    {'title': 'पाल्पा जिल्ला'},
    {'title': 'दाङ जिल्ला'},
    {'title': 'प्युठान जिल्ला'},
    {'title': 'रोल्पा जिल्ला'},
    {'title': 'पूर्वी रूकुम जिल्ला'},
    {'title': 'बाँके जिल्ला'},
    {'title': 'बर्दिया जिल्ला'},
  ],
  [
    {'title': 'पश्चिमी रूकुम जिल्ला'},
    {'title': 'सल्यान जिल्ला'},
    {'title': 'डोल्पा जिल्ला'},
    {'title': 'हुम्ला जिल्ला'},
    {'title': 'जुम्ला जिल्ला'},
    {'title': 'कालिकोट जिल्ला'},
    {'title': 'मुगु जिल्ला'},
    {'title': 'सुर्खेत जिल्ला'},
    {'title': 'दैलेख जिल्ला'},
    {'title': 'जाजरकोट जिल्ला'},
  ],
  [
    {'title': 'कैलाली जिल्ला'},
    {'title': 'अछाम जिल्ला	'},
    {'title': 'डोटी जिल्ला	'},
    {'title': 'बझाङ जिल्ला'},
    {'title': 'बाजुरा जिल्ला'},
    {'title': 'कंचनपुर जिल्ला'},
    {'title': 'डडेलधुरा जिल्ला'},
    {'title': 'बैतडी जिल्ला'},
    {'title': 'दार्चुला जिल्ला'}
  ],
];

/// जिल्ला सुची
var jillaList = [
  [
    'भोजपुर जिल्ला',
    'धनकुटा जिल्ला',
    'इलाम जिल्ला',
    'झापा जिल्ला',
    'खोटाँग जिल्ला',
    'मोरंग जिल्ला',
    'ओखलढुंगा जिल्ला',
    'पांचथर जिल्ला',
    'संखुवासभा जिल्ला',
    'सोलुखुम्बु जिल्ला',
    'सुनसरी जिल्ला',
    'ताप्लेजुंग जिल्ला',
    'तेर्हथुम जिल्ला',
    'उदयपुर जिल्ला',
  ],
  [
    'पर्सा जिल्ला',
    'बारा जिल्ला',
    'रौतहट जिल्ला',
    'सर्लाही जिल्ला',
    'धनुषा जिल्ला',
    'सिराहा जिल्ला',
    'महोत्तरी जिल्ला',
    'सप्तरी जिल्ला',
  ],
  [
    'सिन्धुली जिल्ला',
    'रामेछाप जिल्ला',
    'दोलखा जिल्ला',
    'भक्तपुर जिल्ला',
    'धादिङ जिल्ला',
    'काठमाडौँ जिल्ला',
    'काभ्रेपलान्चोक जिल्ला',
    'ललितपुर जिल्ला',
    'नुवाकोट जिल्ला',
    'रसुवा जिल्ला',
    'सिन्धुपाल्चोक जिल्ला',
    'चितवन जिल्ला',
    'मकवानपुर जिल्ला',
  ],
  [
    'बागलुङ जिल्ला',
    'गोरखा जिल्ला',
    'कास्की जिल्ला',
    'लमजुङ जिल्ला',
    'मनाङ जिल्ला	',
    'मुस्ताङ जिल्ला',
    'म्याग्दी जिल्ला',
    'नवलपुर जिल्ला',
    'पर्वत जिल्ला	',
    'स्याङग्जा जिल्ला',
    'तनहुँ जिल्ला',
  ],
  [
    'कपिलवस्तु जिल्ला',
    'परासी जिल्ला',
    'रुपन्देही जिल्ला',
    'अर्घाखाँची जिल्ला',
    'गुल्मी जिल्ला',
    'पाल्पा जिल्ला',
    'दाङ जिल्ला',
    'प्युठान जिल्ला',
    'रोल्पा जिल्ला',
    'पूर्वी रूकुम जिल्ला',
    'बाँके जिल्ला',
    'बर्दिया जिल्ला',
  ],
  [
    'पश्चिमी रूकुम जिल्ला',
    'सल्यान जिल्ला',
    'डोल्पा जिल्ला',
    'हुम्ला जिल्ला',
    'जुम्ला जिल्ला',
    'कालिकोट जिल्ला',
    'मुगु जिल्ला',
    'सुर्खेत जिल्ला',
    'दैलेख जिल्ला',
    'जाजरकोट जिल्ला',
  ],
  [
    'कैलाली जिल्ला',
    'अछाम जिल्ला	',
    'डोटी जिल्ला	',
    'बझाङ जिल्ला',
    'बाजुरा जिल्ला',
    'कंचनपुर जिल्ला',
    'डडेलधुरा जिल्ला',
    'बैतडी जिल्ला',
    'दार्चुला जिल्ला'
  ],
];

///प्रदेश सुची
const pradeshMap = [
  {'title': 'प्रदेश १'},
  {'title': 'प्रदेश २'},
  {'title': 'बागमती प्रदेश'},
  {'title': 'गण्डकी प्रदेश'},
  {'title': 'लुम्बिनी प्रदेश'},
  {'title': 'कर्णाली प्रदेश'},
  {'title': 'सुदूरपश्चिम प्रदेश'},
];

List<String> pradeshList = [
  'प्रदेश १',
  'प्रदेश २',
  'बागमती प्रदेश',
  'गण्डकी प्रदेश',
  'लुम्बिनी प्रदेश',
  'कर्णाली प्रदेश',
  'सुदूरपश्चिम प्रदेश',
];

///KaryaLaya Search Firebase
Stream<QuerySnapshot> getKaryalayaData(String? pradesh, String? jilla) async* {
  if (pradesh != "" && jilla == "") {
    var _snapshot = firebaseFirestore
        .collection('Krishikaryalaya')
        .orderBy('timestamp', descending: true)
        .where('province', isEqualTo: pradesh)
        .snapshots();
    yield* _snapshot;
  } else if (pradesh != "" && jilla != "") {
    var _snapshot = firebaseFirestore
        .collection('Krishikaryalaya')
        .orderBy('timestamp', descending: true)
        .where('province', isEqualTo: pradesh)
        .where('district', isEqualTo: jilla)
        .snapshots();
    yield* _snapshot;
  } else {
    var _snapshot = firebaseFirestore
        .collection('Krishikaryalaya')
        .orderBy('timestamp', descending: true)
        .snapshots();
    yield* _snapshot;
  }
}

///Santhaharu Search Firebase
Stream<QuerySnapshot> getSansthaData(String? pradesh, String? jilla) async* {
  if (pradesh != "" && jilla == "") {
    var _snapshot = firebaseFirestore
        .collection('sansthaharu')
        .orderBy('timestamp', descending: true)
        .where('province', isEqualTo: pradesh)
        .snapshots();
    yield* _snapshot;
  } else if (pradesh != "" && jilla != "") {
    var _snapshot = firebaseFirestore
        .collection('sansthaharu')
        .orderBy('timestamp', descending: true)
        .where('province', isEqualTo: pradesh)
        .where('district', isEqualTo: jilla)
        .snapshots();
    yield* _snapshot;
  } else {
    var _snapshot = firebaseFirestore
        .collection('sansthaharu')
        .orderBy('timestamp', descending: true)
        .snapshots();
    yield* _snapshot;
  }
}

///Suchana Search Firebase
Stream<QuerySnapshot> getSuchanaData(String? pradesh, String? jilla) async* {
  if (pradesh != "" && jilla == "") {
    var _snapshot = firebaseFirestore
        .collection('suchana')
        .orderBy('added_at', descending: true)
        .where('province', isEqualTo: pradesh)
        .snapshots();
    yield* _snapshot;
  } else if (pradesh != "" && jilla != "") {
    var _snapshot = firebaseFirestore
        .collection('suchana')
        .orderBy('added_at', descending: true)
        .where('province', isEqualTo: pradesh)
        .where('district', isEqualTo: jilla)
        .snapshots();
    yield* _snapshot;
  } else {
    var _snapshot = firebaseFirestore
        .collection('suchana')
        .orderBy('added_at', descending: true)
        .snapshots();
    yield* _snapshot;
  }
}

class EmailEditingRegexValidator extends RegexValidator {
  EmailEditingRegexValidator()
      : super(
            regexSource:
                "^[a-zA-Z0-9_.]*(@([a-zA-Z0-9-]*(\\.[a-zA-Z0-9-]*)?)?)?\$");
}

class EmailSubmitRegexValidator extends RegexValidator {
  EmailSubmitRegexValidator()
      : super(
            regexSource: "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]+\$)");
}
