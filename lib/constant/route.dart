import 'package:agromate/screen/authentication/login.dart';
import 'package:agromate/screen/authentication/signup.dart';
import 'package:agromate/screen/main/calendar.dart';
import 'package:agromate/screen/main/grid/calculator/calculatorHome.dart';
import 'package:agromate/screen/main/grid/calculator/list/emicalculator.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/hisabkitabscreen.dart';
import 'package:agromate/screen/main/grid/gridscreen/krishipustakalaya.dart';
import 'package:agromate/screen/main/grid/gridscreen/sanstha.dart';
import 'package:agromate/screen/main/grid/gridscreen/suchana.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/kinmelscreen.dart';
import 'package:agromate/screen/main/grid/gridscreen/krishiguide.dart';
import 'package:agromate/screen/main/grid/krishikaryalay.dart';
import 'package:agromate/screen/main/videos.dart';
import 'package:agromate/screen/users/content/community_post.dart';
import 'package:agromate/screen/users/content/kinmel_post.dart';
import 'package:flutter/material.dart';
import '../main.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // ignore: unused_local_variable
    Object? args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const AgroMate());
      case '/krishiguide':
        return MaterialPageRoute(builder: (_) => const KrishiGuideScreen());
      case '/login':
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case '/register':
        return MaterialPageRoute(builder: (_) => const RegisterAccount());
      case '/videos':
        return MaterialPageRoute(builder: (_) => VideosScreen());
      case '/krishicalculator':
        return MaterialPageRoute(builder: (_) => CalculatorHome());
      case '/kinmel':
        return MaterialPageRoute(builder: (_) => const KinmelScreen());
      case '/krishikaryalaya':
        return MaterialPageRoute(builder: (_) => const KrishiKaryalaya());
      case '/krishisuchana':
        return MaterialPageRoute(builder: (_) => KrishiSuchana());
      case '/sansthaharu':
        return MaterialPageRoute(builder: (_) => const Sansthaharu());
      case '/krishipustakalaya':
        return MaterialPageRoute(builder: (_) => const KrishiPustakalaya());
      case '/hisabkitab':
        return MaterialPageRoute(builder: (_) => const HisabKitabScreen());
      case '/calendar':
        return MaterialPageRoute(builder: (_) => const SDNepaliCalendar());
      case '/emicalculator':
        return MaterialPageRoute(builder: (_) =>  const EmiCalculator());
      case '/usercommpost':
        return MaterialPageRoute(builder: (_) =>  const UserCommunityPost());
      case '/userkinmelpost':
        return MaterialPageRoute(builder: (_) =>  const UserKinmelPost());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          foregroundColor: Colors.black,
          backgroundColor: Colors.white,
          title: const Text('Error'),
          centerTitle: true,
        ),
        body: const Center(
          child: Text("Error Occur"),
        ),
      );
    });
  }
}
