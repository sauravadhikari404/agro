import 'package:agromate/controller/adscontroller.dart';
import 'package:agromate/controller/all_user_controller.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/controller/communitypost_controller.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/controller/kinmel_controller.dart';
import 'package:agromate/controller/like_controller.dart';
import 'package:agromate/controller/news_controller.dart';
import 'package:agromate/controller/slider_ads_controller.dart';
import 'package:agromate/controller/theme_controller.dart';

UserController userController = UserController.instance;
AllUsersController allUsersController = AllUsersController.instance;
CommunityPostController commPostCont = CommunityPostController.instance;
LikeController likeController = LikeController.instance;
KinmelController kinmelController = KinmelController.instance;
HisabKitabExpensesController hisabKitabExpensesController =
    HisabKitabExpensesController.instance;
HisabKitabIncomeController hisabKitabIncomeController =
    HisabKitabIncomeController.instance;
AdsController adsController = AdsController.instance;
NewsController newsController = NewsController.instace;
ThemeController themeController = ThemeController.instance;
SliderAdsController sliderAdsController = SliderAdsController.instance;
