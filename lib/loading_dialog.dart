import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class CustomFullScreenLoadingDialog {

  static void showDialog() {
    Get.dialog(
      WillPopScope(
        child: Material(
          color: const Color(0xff00c853),
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(child: Image.asset('images/agronepal.png',)),
                  Center(
                    child: SizedBox(
                      width: 30,
                      height: 30,
                      child: SleekCircularSlider(
                        min: 0,
                        max: 100,
                        initialValue: 50,
                        appearance: CircularSliderAppearance(
                          customWidths: CustomSliderWidths(handlerSize: 2,
                              trackWidth: 2,progressBarWidth: 5),
                          customColors: CustomSliderColors(
                            gradientStartAngle: 2,
                            gradientEndAngle: 1,
                            dynamicGradient: true,
                            dotColor: Colors.white,
                            hideShadow: true,
                            trackColor: Colors.transparent,
                            progressBarColors: [
                              const Color(0xfffff176),
                              const Color(0xffffb74d),
                            ],
                            shadowStep: 100,
                          ),
                          spinnerDuration: 5000,
                          animationEnabled: true,
                          animDurationMultiplier: 1.2,
                          spinnerMode: true,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 2,
                left: 0,
                right: 0,
                child: Column(
                  children: [
                    SizedBox(width:100,height: 50, child: Image.asset('images/SDlogo.png')),
                    Text('Powered By SD Tech Company',style: TextStyle(
                      color: Colors.white.withOpacity(0.6),
                      fontSize: 10,
                      fontFamily: 'Ubuntu',

                    ),
                    )
                  ],
                ),

              ),
            ],
          ),
        ),
        onWillPop: () => Future.value(false),
      ),
      barrierDismissible: false,
      barrierColor: const Color(0xff141A31).withOpacity(.3),
      useSafeArea: true,
      name: 'AgroMate'
    );
  }

  static void cancelDialog() {
    Get.back();
  }
}
