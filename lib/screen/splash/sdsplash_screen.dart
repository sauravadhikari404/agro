import 'dart:async';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/widget/mainnav.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class SDSplashScreen extends StatefulWidget {
  const SDSplashScreen({Key? key}) : super(key: key);

  @override
  _SDSplashScreenState createState() => _SDSplashScreenState();
}

class _SDSplashScreenState extends State<SDSplashScreen> {
  late final SharedPreferences _prefs;
  getSharedPreference() async {
    _prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    super.initState();
    getSharedPreference();
    startTimer();
  }

  startTimer() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, route);
  }

  final UserController userCtrl = Get.put(UserController());

  route() {
    
    return userCtrl.firebaseUser.value?.isAnonymous == true ||
            userCtrl.firebaseUser.value?.uid != null
        ? Get.offAll(() => const BottomSheetNavSD())
        : Get.offAll(() => const AccountPage());
  }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff00c853),
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(child: Image.asset('images/agronepal.png')),
              Center(
                child: SizedBox(
                  width: 30,
                  height: 30,
                  child: SleekCircularSlider(
                    min: 0,
                    max: 100,
                    initialValue: 50,
                    appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(
                          handlerSize: 2, trackWidth: 2, progressBarWidth: 5),
                      customColors: CustomSliderColors(
                        gradientStartAngle: 2,
                        gradientEndAngle: 1,
                        dynamicGradient: true,
                        dotColor: Colors.white,
                        hideShadow: true,
                        trackColor: Colors.transparent,
                        progressBarColors: [
                          const Color(0xfffff176),
                          const Color(0xffffb74d),
                        ],
                        shadowStep: 100,
                      ),
                      spinnerDuration: 5000,
                      animationEnabled: true,
                      animDurationMultiplier: 1.2,
                      spinnerMode: true,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 2,
            left: 0,
            right: 0,
            child: Column(
              children: [
                SizedBox(
                  width: 150,
                  height: 150,
                  child: Image.asset('images/SDlogo.png'),
                ),
                const Text(
                  'Powered By SD Tech Company',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                    fontFamily: 'Ubuntu',
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
   @override
  void dispose() {
    super.dispose();
  }
}
