import 'dart:async';
import 'dart:convert';

import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/input/inputfield.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';

import '../../../loading_dialog.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final UserController userCtrl = Get.put(UserController());
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController fname = TextEditingController();
  TextEditingController lname = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController province = TextEditingController();
  TextEditingController district = TextEditingController();
  TextEditingController userType = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController includedBusiness = TextEditingController();
  List<String> userTypeList = [
    'किसान',
    'बिज्ञ',
    'विद्यार्थी',
    'बिक्रेता',
    'प्राविधिक',
    'उपभोक्ता',
    'योगदानकर्ता',
    'अन्य'
  ];
  int districtIndex = 0;
  startTimer(route) async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, route);
  }

  @override
  void initState() {
    super.initState();
    fname.text = userCtrl.userModel.value.fname.toString();
    lname.text = userCtrl.userModel.value.lname.toString();
    address.text = userCtrl.userModel.value.address.toString();
    province.text = userCtrl.userModel.value.province.toString();
    district.text = userCtrl.userModel.value.district.toString();
    userType.text = userCtrl.userModel.value.userType.toString();
    phone.text = userCtrl.userModel.value.phone.toString();
    email.text = userCtrl.userModel.value.email.toString();
    includedBusiness.text =
        userCtrl.userModel.value.includedBusiness.toString();
    districtIndex = pradeshList.indexOf(province.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'Edit Profile',
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    userCtrl.firebaseUser.value?.photoURL != null
                        ? CachedNetworkImage(
                            imageUrl:
                                "${userCtrl.firebaseUser.value?.photoURL}",
                            cacheKey:
                                "${userCtrl.firebaseUser.value?.photoURL}",
                            imageBuilder: (context, imgProvider) => Container(
                              width: 80,
                              height: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border:
                                    Border.all(color: Colors.black45, width: 4),
                                image: DecorationImage(
                                  image: imgProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          )
                        : Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border:
                                  Border.all(color: Colors.black45, width: 4),
                              image: const DecorationImage(
                                image: AssetImage('images/avatar.png'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )
                  ],
                ),
              ),
            ),
            Form(
                key: _key,
                child: Column(
                  children: [
                    TextInputField(
                      textEditingController: fname,
                      labelText: 'नाम',
                      filled: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      validator: (val) {
                        if (val == null) {
                          return "तपाइको पहिलो नाम खाली हुन सक्दैन";
                        }
                        return null;
                      },
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    TextInputField(
                      textEditingController: lname,
                      labelText: 'थर',
                      filled: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      validator: (val) {
                        if (val == null) {
                          return "तपाइको थर खाली हुन सक्दैन";
                        }
                        return null;
                      },
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    TextInputField(
                      textEditingController: email,
                      labelText: 'इमेल',
                      filled: true,
                      readOnly: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      validator: (val) {
                        if (val == null) {
                          return "तपाइको इमेल खाली हुन सक्दैन";
                        }
                        return null;
                      },
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    TextInputField(
                      textEditingController: address,
                      labelText: 'ठेगाना',
                      filled: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    TextInputField(
                      textEditingController: phone,
                      labelText: 'फोन नं.',
                      filled: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextInputField(
                            textEditingController: province,
                            labelText: 'प्रदेश',
                            filled: true,
                            readOnly: true,
                            focusColor: Colors.black,
                            focusBorder: const OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            filledColor: const Color(0xffdcedc8),
                            labelStyle: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Color(0xff00c853)),
                            errorStyle: const TextStyle(
                                color: Colors.redAccent, fontFamily: 'Ubuntu'),
                            validator: (val) {
                              if (val == null) {
                                return "तपाइको प्रदेश छान्नुहोस्।";
                              }
                              return null;
                            },
                            textStyle: const TextStyle(fontFamily: 'Poppins'),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            inputBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide.none),
                          ),
                        ),
                        PopupMenuButton<String>(
                          icon: const Icon(Ionicons.arrow_down_circle),
                          onSelected: (String value) {
                            setState(() {
                              province.text = value;
                              district.clear();
                              districtIndex = pradeshList.indexOf(value);
                            });
                          },
                          itemBuilder: (BuildContext context) {
                            return pradeshList
                                .map<PopupMenuItem<String>>((String value) {
                              return PopupMenuItem(
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontFamily: 'poppins',
                                        color: value == province.text
                                            ? Colors.green
                                            : Colors.black,
                                        fontWeight: value == province.text
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  value: value);
                            }).toList();
                          },
                        ),
                      ],
                    ),

                    Row(
                      children: [
                        Expanded(
                          child: TextInputField(
                            textEditingController: district,
                            focusBorder: const OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            labelText: 'जिल्ला',
                            filled: true,
                            readOnly: true,
                            filledColor: const Color(0xffdcedc8),
                            labelStyle: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Color(0xff00c853)),
                            errorStyle: const TextStyle(
                                color: Colors.redAccent, fontFamily: 'Ubuntu'),
                            validator: (val) {
                              if (val == null) {
                                return "तपाइको जिल्ला चुन्नुहोस";
                              }
                              return null;
                            },
                            textStyle: const TextStyle(fontFamily: 'Poppins'),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            inputBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide.none),
                          ),
                        ),
                        PopupMenuButton<String>(
                          icon: const Icon(Ionicons.arrow_down_circle),
                          onSelected: (String value) {
                            setState(() {
                              district.text = value;
                            });
                          },
                          itemBuilder: (BuildContext context) {
                            return jillaList[districtIndex]
                                .map<PopupMenuItem<String>>((String value) {
                              return PopupMenuItem(
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontFamily: 'poppins',
                                        color: value == district.text
                                            ? Colors.green
                                            : Colors.black,
                                        fontWeight: value == district.text
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  value: value);
                            }).toList();
                          },
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextInputField(
                            textEditingController: userType,
                            focusBorder: const OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            labelText: 'प्रयोगकर्ता किसिम',
                            filled: true,
                            readOnly: true,
                            filledColor: const Color(0xffdcedc8),
                            labelStyle: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Color(0xff00c853)),
                            errorStyle: const TextStyle(
                                color: Colors.redAccent, fontFamily: 'Ubuntu'),
                            validator: (val) {
                              if (val == null) {
                                return "प्रयोग कर्ताको किसिम/प्रकार चुन्नुहोस";
                              }
                              return null;
                            },
                            textStyle: const TextStyle(fontFamily: 'Poppins'),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            inputBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide.none),
                          ),
                        ),
                        PopupMenuButton<String>(
                          icon: const Icon(Ionicons.arrow_down_circle),
                          onSelected: (String value) {
                            setState(() {
                              userType.text = value;
                            });
                          },
                          itemBuilder: (BuildContext context) {
                            return userTypeList
                                .map<PopupMenuItem<String>>((String value) {
                              return PopupMenuItem(
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontFamily: 'poppins',
                                        color: value == userType.text
                                            ? Colors.green
                                            : Colors.black,
                                        fontWeight: value == userType.text
                                            ? FontWeight.bold
                                            : FontWeight.normal),
                                  ),
                                  value: value);
                            }).toList();
                          },
                        ),
                      ],
                    ),

                    //configure password to save
                    TextInputField(
                      textEditingController: password,
                      labelText: 'पासवोर्ड',
                      filled: true,
                      isPassword: true,
                      filledColor: const Color(0xffdcedc8),
                      labelStyle: const TextStyle(
                          fontFamily: 'Poppins', color: Color(0xff00c853)),
                      errorStyle: const TextStyle(
                          color: Colors.redAccent, fontFamily: 'Ubuntu'),
                      validator: (val) {
                        if (val == null) {
                          return "तपाइको Profile Update गर्न तपाइको Password प्रदान गर्नुहोस।";
                        }
                        return null;
                      },
                      textStyle: const TextStyle(fontFamily: 'Poppins'),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 20),
                      inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.green, width: 2)),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextButton(
                      onPressed: () async {
                        if (_key.currentState!.validate()) {
                          if (md5
                                  .convert(utf8.encode(password.text))
                                  .toString() ==
                              userCtrl.userModel.value.password.toString()) {
                            CustomFullScreenLoadingDialog.showDialog();
                            await firebaseFirestore
                                .collection('Users')
                                .doc(userCtrl.firebaseUser.value!.uid)
                                .update({
                              'fname': fname.text,
                              'lname': lname.text,
                              'phone': phone.text,
                              'province': province.text,
                              'district': district.text,
                              'userType': userType.text,
                              'includedBusiness': includedBusiness.text,
                              'address': address.text,
                            }).whenComplete(() {
                              startTimer(getBack());
                            });
                          } else {
                            GetSnackBar snack = const GetSnackBar(
                              isDismissible: true,
                              barBlur: 0.6,
                              icon: Icon(Ionicons.alert),
                              duration: Duration(seconds: 5),
                              borderColor: Colors.green,
                              backgroundColor: Color(0xff00c853),
                              messageText: Text(
                                  'कृपया तपाइको पासवोर्ड सहि लेखि पुन् प्रयास गर्नुहोस।'),
                            );
                            Get.showSnackbar(snack);
                          }
                        }
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: const Color(0xff00c853),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8))),
                      child: const Text(
                        'प्रोफाइल अपडेट गर्नुहोस्',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  getBack() {
    CustomFullScreenLoadingDialog.cancelDialog();
    Get.back();
    Get.snackbar('', 'तपाइको प्रोफाइल सफलता पुर्वक अपडेट भएको छ।');
  }

  @override
  void dispose() {
    super.dispose();
  }
}
