import 'dart:io';
import 'dart:math';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/screen/users/profile/editprofile.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:url_launcher/url_launcher.dart';

class MainProfile extends StatefulWidget {
  const MainProfile({Key? key}) : super(key: key);

  @override
  _MainProfileState createState() => _MainProfileState();
}

class _MainProfileState extends State<MainProfile>
    with SingleTickerProviderStateMixin {
  late TabController controller;
  CollectionReference? imgRef;
  firebase_storage.Reference? ref;
  late File _image;
  double val = 0;
  late List<String> _refreshData;
  final _getcountPost = [
    {
      'title': 'समुदाय पृष्ठ',
      'Icon': FontAwesomeIcons.peopleGroup,
      'count': 0,
      'route': '/usercommpost'
    },
    {
      'title': 'किनमेल पृष्ठ',
      'Icon': FontAwesomeIcons.basketShopping,
      'count': 0,
      'route': '/userkinmelpost'
    },
  ].obs;

  _getCount() {
    setState(() {
      commPostCont.userCommModel
          .bindStream(commPostCont.currentUserCommunityPost());
      kinmelController.userkmodel.bindStream(kinmelController.userKinemPost());
    });
    Future.delayed(const Duration(seconds: 2), () {
      RxInt communityLenght = commPostCont.userCommModel.length.obs;
      if (!mounted) return;
      setState(() {
        _getcountPost[0]['count'] = communityLenght.toInt();
        _getcountPost[1]['count'] = kinmelController.userkmodel.length.toInt();
      });
    });
  }

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();

    _refreshData = [
      "Flutter",
      "React Native",
      "Cordova/ PhoneGap",
      "Native Script"
    ];
    _getCount();
    super.initState();
    controller = TabController(length: 2, vsync: this);
  }

  final picker = ImagePicker();
  final UserController userData = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CusAppBar(
          appBarTitle: 'Profile',
        ),
        body: CustomScrollView(slivers: [
          SliverToBoxAdapter(
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(100),
                  bottomRight: Radius.circular(100),
                ),
              ),
              child: Obx(
                () => Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          width: sizeProvider(context).width,
                          height: 200,
                          decoration: const BoxDecoration(
                            color: Colors.black38,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage('images/bg/peakpx.jpg'),
                            ),
                          ),
                          child: Container(
                            width: sizeProvider(context).width,
                            height: 200,
                            color: Colors.white.withOpacity(0.4),
                          ),
                        ),
                        SizedBox(
                          width: sizeProvider(context).width,
                          height: 125,
                          child: Align(
                            alignment: Alignment.center,
                            child: SizedBox(
                              width: 80,
                              height: 80,
                              child: Center(
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl:
                                          '${userData.firebaseUser.value?.photoURL}',
                                      cacheKey:
                                          '${userData.firebaseUser.value?.photoURL}',
                                      placeholder: (context, url) => Container(
                                          width: 80,
                                          height: 80,
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black45,
                                                width: 4),
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(50)),
                                          ),
                                          child: Lottie.asset(
                                              JsonAssets.imgLoading)),
                                      imageBuilder: (context, imgProvider) =>
                                          Container(
                                        width: 80,
                                        height: 80,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.black45, width: 4),
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(50)),
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: imgProvider,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: IconButton(
                                          onPressed: () {
                                            chooseImage();
                                          },
                                          icon: const Icon(
                                            Ionicons.add_circle,
                                            size: 28,
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: sizeProvider(context).width,
                          height: 140,
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Obx(() {
                                return Text(
                                  '${userController.userModel.value.fname} ${userController.userModel.value.lname}',
                                  style: const TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                  ),
                                );
                              }),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: sizeProvider(context).width,
                          height: 195,
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Stack(
                              children: [
                                Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.6),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20))),
                                  child: FittedBox(
                                    child: ButtonBar(
                                      alignment: MainAxisAlignment.center,
                                      children: [
                                        Transform.rotate(
                                          angle: pi / 4,
                                          child: Container(
                                            width: 35,
                                            height: 35,
                                            decoration: const BoxDecoration(
                                                color: Colors.white,
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.green,
                                                      spreadRadius: 2)
                                                ],
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20))),
                                            child: IconButton(
                                              hoverColor: Colors.red,
                                              color: Colors.green,
                                              iconSize: 18,
                                              onPressed: () {
                                                launch(
                                                    'tel:${userData.userModel.value.phone}');
                                              },
                                              icon: const Icon(
                                                  Ionicons.call_sharp),
                                              splashColor:
                                                  Colors.green.shade300,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            Get.to(() => const EditProfile());
                                          },
                                          child: const Text(
                                            'Edit',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          style: TextButton.styleFrom(
                                            backgroundColor:
                                                Colors.indigo.shade300,
                                            splashFactory:
                                                InkRipple.splashFactory,
                                            enableFeedback: true,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(10),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Transform.rotate(
                                          angle: pi / 4,
                                          child: Container(
                                            width: 35,
                                            height: 35,
                                            decoration: const BoxDecoration(
                                                color: Colors.white,
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.orange,
                                                      spreadRadius: 2),
                                                ],
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20))),
                                            child: Transform.rotate(
                                              angle: -120.2,
                                              child: IconButton(
                                                  color: Colors.orange,
                                                  iconSize: 18,
                                                  onPressed: () {
                                                    launch(
                                                        'mailto:${userData.userModel.value.email}');
                                                  },
                                                  icon: const Icon(
                                                      Ionicons.mail_sharp)),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                '${userData.userModel.value.address}',
                style: const TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                '${userData.userModel.value.userType}',
                style: const TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              ListTile(
                leading: const Icon(LineIcons.phone),
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: const Text('फोन नं.'),
                subtitle: Text('${userData.userModel.value.phone}'),
              ),
              ListTile(
                leading: const Icon(Ionicons.mail_outline),
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: const Text('इमेल'),
                subtitle: Text('${userData.userModel.value.email}'),
              ),
              ListTile(
                leading: const Icon(Ionicons.business_outline),
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: const Text('आबद्ध संस्था'),
                subtitle: Text('${userData.userModel.value.includedBusiness}'),
              ),
              ListTile(
                leading: const Icon(LineIcons.user),
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: const Text('प्रयोगकर्ताको प्रकार'),
                subtitle: Text('${userData.userModel.value.userType}'),
              ),
              ListTile(
                leading: const Icon(Ionicons.time_outline),
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                title: const Text('खाता खोलिएको मिति'),
                subtitle: Text(
                    (userData.userModel.value.userRegistered?.toDate() ??
                            DateTime.now())
                        .toNepaliDateTime()
                        .format('EEE MMMM dd, yyyy')),
              ),
              const SizedBox(
                height: 5,
              ),
              const Divider(
                height: 2,
              ),
              const SizedBox(
                height: 5,
              ),
            ],
          )),
          SliverAnimatedList(
            initialItemCount: _getcountPost.length,
            itemBuilder: (context, index, an) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () async {
                    await Get.toNamed(_getcountPost[index]['route'].toString());
                    setState(() {
                      _refreshData.addAll(['C++', 'Global']);
                      _getCount();
                    });
                  },
                  child: Container(
                    width: 100,
                    height: 75,
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.blueGrey.shade300,
                            blurRadius: 0.3,
                            spreadRadius: 0.5,
                            offset: const Offset(0, 0.2)),
                        BoxShadow(
                            color: Colors.green.shade200,
                            blurRadius: 0.5,
                            spreadRadius: 0.6,
                            offset: const Offset(0, 0.2)),
                      ],
                    ),
                    child: Stack(
                      children: [
                        Container(
                          width: double.infinity,
                          height: 75,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            Colors.green.shade500,
                            Colors.green.shade300
                          ])),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                _getcountPost[index]["Icon"] as IconData,
                                color: Colors.white,
                                size: 35,
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    NepaliNumberFormat(
                                      language: Language.nepali,
                                      inWords: true,
                                    )
                                        .format(
                                            _getcountPost[index]['count'] ?? 0)
                                        .toString(),
                                    style: const TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    _getcountPost[index]['title'].toString(),
                                    style: const TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ]));
  }

  chooseImage() async {
    try {
      final pickedFile =
          await picker.getImage(source: ImageSource.gallery, imageQuality: 40);
      setState(() {
        uploadFile(File(pickedFile!.path));
      });
    } catch (e) {
      rethrow;
    }
  }

  Future uploadFile(File _image) async {
    var usrData = userController.userModel.value;
    var ref = firebase_storage.FirebaseStorage.instance
        .ref()
        .child('Profile/${usrData.uid}/$_image');
    await ref.putFile(_image).whenComplete(() async {
      await ref
          .getDownloadURL()
          .then((value) {
            userData.firebaseUser.value
                ?.updatePhotoURL(value)
                .then((value) => null)
                .whenComplete(() => firebaseFirestore
                    .collection('Users')
                    .doc(usrData.uid)
                    .update({'photoUrl': value}));
          })
          .then((value) {})
          .whenComplete(() {
            Future.delayed(const Duration(seconds: 2), () {
              setState(() {
                _refreshData.addAll(["Ionic", "Xamarin"]);
              });
            });
          });
    });
  }
}
