import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/kinmel_controller.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/add_kinmel_product.dart';
import 'package:agromate/screen/users/content/kinmel_widget.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:agromate/screen/widget/kinmelwid.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';

class UserKinmelPost extends StatefulWidget {
  const UserKinmelPost({Key? key}) : super(key: key);

  @override
  State<UserKinmelPost> createState() => _UserKinmelPostState();
}

class _UserKinmelPostState extends State<UserKinmelPost> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        appBarTitle: "किनमेल पृष्ठहरु",
        action: [
          IconButton(
              onPressed: () {
                Get.to(() => const AddItemToKinmel());
              },
              icon: const Icon(Ionicons.add_circle_outline))
        ],
      ),
      body: Obx(
        () => kinmelController.userkmodel.isEmpty
            ? Center(
                child: Lottie.asset(JsonAssets.emptyJson),
              )
            : ListView.builder(
                itemCount: kinmelController.userkmodel.length,
                itemBuilder: (_, index) {
                  return KinmelWidget(
                    productDetails: {
                      'by': kinmelController.userkmodel[index].by??"",
                      'pid': kinmelController.userkmodel[index].pid??"",
                      'pType': kinmelController.userkmodel[index].pType ?? '',
                      'pName': kinmelController.userkmodel[index].pname ?? '',
                      'pdate': kinmelController.userkmodel[index].date ?? 0,
                      'validDate':
                          kinmelController.userkmodel[index].validDate ?? 0,
                      'quantity':
                          kinmelController.userkmodel[index].quantity ?? 0,
                      'quality':
                          kinmelController.userkmodel[index].quality ?? '',
                      'price': kinmelController.userkmodel[index].price ?? 0,
                      'address':
                          kinmelController.userkmodel[index].address ?? '',
                      'image': kinmelController.userkmodel[index].image ?? [],
                      'votes': kinmelController.userkmodel[index].votes ?? 0,
                      'phone': kinmelController.userkmodel[index].phone ?? '',
                      'index': index,
                      'description':
                          kinmelController.userkmodel[index].description ?? '',
                      'perItemTag':
                          kinmelController.userkmodel[index].perItemTag ?? "",
                    },
                  );
                }),
      ),
    );
  }
}
