import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:agromate/screen/widget/comment/post_comment.dart';
import 'package:agromate/screen/widget/formmodal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class UserCommunityPost extends StatefulWidget {
  const UserCommunityPost({Key? key}) : super(key: key);

  @override
  State<UserCommunityPost> createState() => _UserCommunityPostState();
}

class _UserCommunityPostState extends State<UserCommunityPost> {
  final _postOption = ['Edit', 'Delete'];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode f1 = FocusNode();
  @override
  void initState() {
    super.initState();
    didUpdateWidget(this.widget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant UserCommunityPost oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomeAppBar(appBarTitle: 'समुदाय पृष्ठ', action: [
        IconButton(
            onPressed: () {
              showFormModal(context, f1);
            },
            icon: const Icon(Ionicons.add_circle_outline))
      ]),
      body: Obx(
        () => commPostCont.userCommModel.isEmpty
            ? Center(
                child: Lottie.asset(JsonAssets.emptyJson),
              )
            : ListView.builder(
                itemCount: commPostCont.userCommModel.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Wrap(
                        children: [
                          Column(
                            children: [
                              Stack(
                                children: [
                                  Align(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Text(
                                        'प्रश्न',
                                        style: TextStyle(
                                            fontFamily: 'Poppins',
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      title: Text(commPostCont
                                              .userCommModel[index].body ??
                                          ""),
                                      subtitle: Row(
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Icon(
                                              FontAwesomeIcons.clock,
                                              color: Colors.blueGrey,
                                              size: 10,
                                            ),
                                          ),
                                          Text(
                                            NepaliMoment.fromAD(commPostCont
                                                .userCommModel[index].date!
                                                .toDate()),
                                            style:
                                                const TextStyle(fontSize: 12),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      right: 5,
                                      top: 0,
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton<dynamic>(
                                          isExpanded: false,
                                          focusColor: Colors.transparent,
                                          elevation: 0,
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(10)),
                                          autofocus: false,
                                          icon: const Icon(
                                            FontAwesomeIcons.ellipsis,
                                            color: Colors.black54,
                                            size: 10,
                                          ),
                                          items: _postOption.map((e) {
                                            return DropdownMenuItem(
                                                value: e, child: Text(e));
                                          }).toList(),
                                          onChanged: (val) {
                                            if (val == "Edit") {
                                              getAction(
                                                  GetActionTypes.edit,
                                                  commPostCont
                                                      .userCommModel[index].pid,
                                                  _scaffoldKey
                                                      .currentState!.context,
                                                  body: commPostCont
                                                      .userCommModel[index].body
                                                      .toString());
                                            } else if (val.toString() ==
                                                "Delete") {
                                              getAction(
                                                  GetActionTypes.delete,
                                                  commPostCont
                                                      .userCommModel[index].pid,
                                                  _scaffoldKey
                                                      .currentState!.context);
                                            }
                                          },
                                        ),
                                      )),
                                ],
                              ),
                              Obx(
                                () => Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    isCurrentUserLikeIt(commPostCont
                                                .userCommModel[index].votes) ==
                                            false
                                        ? TextButton.icon(
                                            style: ElevatedButton.styleFrom(
                                              splashFactory:
                                                  InkRipple.splashFactory,
                                              primary: Colors.white,
                                              shadowColor: Colors.white,
                                              elevation: 0,
                                              shape:
                                                  const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(50),
                                                ),
                                              ),
                                            ),
                                            icon: const Icon(
                                              Ionicons.thumbs_up,
                                              size: 14,
                                            ),
                                            onPressed: () {
                                              likeController
                                                  .addLikeToCommunityPosts(
                                                      commPostCont
                                                          .userCommModel[index]
                                                          .pid);
                                              setState(() {
                                                commPostCont.commModel.clear();
                                                commPostCont.onInit();
                                              });
                                            },
                                            label: Text(
                                              commPostCont.userCommModel[index]
                                                  .votes!.length
                                                  .toString(),
                                              style: const TextStyle(
                                                  fontFamily: 'ubuntu',
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal),
                                            ),
                                          )
                                        : TextButton.icon(
                                            style: ElevatedButton.styleFrom(
                                              splashFactory:
                                                  InkRipple.splashFactory,
                                              primary: Colors.orange
                                                  .withOpacity(0.2),
                                              shadowColor: Colors.white,
                                              elevation: 0,
                                              shape:
                                                  const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(50),
                                                ),
                                              ),
                                            ),
                                            icon: const Icon(
                                              Ionicons.thumbs_up,
                                              color: Colors.orange,
                                              size: 14,
                                            ),
                                            onPressed: () {
                                              likeController
                                                  .removeLikeFromCommunityPosts(
                                                      commPostCont
                                                          .userCommModel[index]
                                                          .pid);
                                            },
                                            label: Text(
                                              commPostCont.userCommModel[index]
                                                  .votes!.length
                                                  .toString(),
                                              style: const TextStyle(
                                                  fontFamily: 'ubuntu',
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.orange,
                                                  fontStyle: FontStyle.normal),
                                            ),
                                          ),

                                    //Comment Button
                                    StreamBuilder(
                                        stream: firebaseFirestore
                                            .collection('CummunityPosts')
                                            .doc(commPostCont
                                                .userCommModel[index].pid)
                                            .collection('Comments')
                                            .snapshots(),
                                        builder: (context,
                                            AsyncSnapshot<QuerySnapshot>
                                                snapshot) {
                                          return TextButton.icon(
                                            style: ElevatedButton.styleFrom(
                                              splashFactory:
                                                  InkSplash.splashFactory,
                                              primary: Colors.white,
                                              shadowColor: Colors.white,
                                              elevation: 0,
                                              shape:
                                                  const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(50),
                                                ),
                                              ),
                                            ),
                                            onPressed: () {
                                              Get.to(() => PostCommentScreen(
                                                    usrDetails: storeIt(
                                                        userController.userModel
                                                            .value.uid),
                                                    postData: {
                                                      'pid': commPostCont
                                                              .userCommModel[
                                                                  index]
                                                              .pid ??
                                                          '',
                                                      'body': commPostCont
                                                              .userCommModel[
                                                                  index]
                                                              .body ??
                                                          '',
                                                      'pdate': commPostCont
                                                          .userCommModel[index]
                                                          .date,
                                                      'by': commPostCont
                                                          .userCommModel[index]
                                                          .by,
                                                      'votes': commPostCont
                                                          .userCommModel[index]
                                                          .votes
                                                          ?.length,
                                                      'isCurrentUserLikedIt':
                                                          isCurrentUserLikeIt(
                                                              commPostCont
                                                                  .userCommModel[
                                                                      index]
                                                                  .votes),
                                                      'index': index
                                                    },
                                                  ));
                                            },
                                            icon: const Icon(
                                              Icons.comment,
                                              size: 14,
                                            ),
                                            label: FittedBox(
                                              child: snapshot.connectionState ==
                                                      ConnectionState.waiting
                                                  ? const CircularProgressIndicator
                                                      .adaptive()
                                                  : Text(
                                                      '${snapshot.data?.docs.length ?? 0}',
                                                      style: const TextStyle(
                                                          fontFamily: 'ubuntu',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontStyle:
                                                              FontStyle.normal),
                                                    ),
                                            ),
                                          );
                                        })
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }),
      ),
    );
  }

  dynamic getAction(GetActionTypes getActionType, pid, BuildContext context,
      {String body = ""}) {
    switch (getActionType) {
      case GetActionTypes.edit:
        return showCommunityEditBottomModal(context, f1, pid, body);
      case GetActionTypes.delete:
        return showDialog(
            context: context,
            builder: (context) {
              return getDialogCommunity(deleteCommunityPost,
                  pid: pid,
                  message: "के यो समुदाय बाट पृष्ठ हटाउन चाहनुहुन्छ।",
                  context: context);
            });
    }
  }

  @override
  void dispose() {
    f1.dispose();
    super.dispose();
  }
}

enum GetActionTypes { edit, delete }
