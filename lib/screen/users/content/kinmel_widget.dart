import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/kinmelprofile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:progressive_image/progressive_image.dart';

class KinmelWidget extends StatefulWidget {
  final Map<String, dynamic>? productDetails;
  const KinmelWidget({Key? key, this.productDetails}) : super(key: key);

  @override
  _KinmelWidgetState createState() => _KinmelWidgetState();
}

class _KinmelWidgetState extends State<KinmelWidget> {
  var postModify = [
    // 'edit',
    'delete',
  ];

  var postReport = [
    'report',
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        type: MaterialType.card,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            Get.to(() => KinmelProfile(
                  dtsource: widget.productDetails,
                ));
          },
          child: Container(
            width: sizeProvider(context).width,
            height: 265,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              const BoxShadow(
                  color: Colors.blueGrey, spreadRadius: 0.5, blurRadius: 0.4),
              BoxShadow(
                  color: Colors.green.shade300,
                  spreadRadius: 0.4,
                  blurRadius: 0.5)
            ]),
            child: Stack(
              children: [
                Container(
                  width: sizeProvider(context).width,
                  height: 180,
                  decoration: BoxDecoration(
                    color: Colors.green.shade200,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    boxShadow: const [
                      BoxShadow(color: Colors.black, spreadRadius: 0.4),
                      BoxShadow(color: Colors.white, spreadRadius: 0.4),
                    ],
                  ),
                  child: Stack(
                    children: [
                      ProgressiveImage(
                          placeholder: const AssetImage('images/noimg.png'),
                          thumbnail: NetworkImage(
                            widget.productDetails?['image'][0],
                          ),
                          image: NetworkImage(
                            widget.productDetails?['image'][0],
                          ),
                          width: sizeProvider(context).width,
                          fit: BoxFit.cover,
                          fadeDuration: const Duration(milliseconds: 200),
                          height: 180),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: sizeProvider(context).width,
                          height: 60,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.6),
                          ),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  widget.productDetails?['pName'] ?? 'NA',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'poppins',
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                              const Divider(
                                thickness: 0.5,
                                color: Colors.white,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Obx(
                                    () => Text(
                                      kinmelController
                                              .kmodel
                                              .value[widget
                                                  .productDetails?['index']]
                                              .votes!
                                              .length
                                              .toString() +
                                          ' लाइक'
                                              '',
                                      style: const TextStyle(
                                          fontFamily: 'poppins',
                                          color: Colors.white),
                                    ),
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: Text(
                                      '|',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  StreamBuilder<QuerySnapshot>(
                                      stream: firebaseFirestore
                                          .collection('Kinmel')
                                          .doc(widget.productDetails?['pid'])
                                          .collection('Comments')
                                          .snapshots(),
                                      builder: (context,
                                          AsyncSnapshot<QuerySnapshot>
                                              snapshot) {
                                        if (!snapshot.hasData) {
                                          return const Text(
                                            '...',
                                            style:
                                                TextStyle(color: Colors.white),
                                          );
                                        }
                                        return FittedBox(
                                          child: snapshot.data!.docs.length <= 1
                                              ? Text(
                                                  '${snapshot.data?.docs.length} टिप्पणी',
                                                  style: const TextStyle(
                                                      fontFamily: 'poppins',
                                                      color: Colors.white),
                                                )
                                              : Text(
                                                  '${snapshot.data?.docs.length} टिप्पणीहरु',
                                                  style: const TextStyle(
                                                      fontFamily: 'poppins',
                                                      color: Colors.white),
                                                ),
                                        );
                                      }),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  child: Container(
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                        borderRadius: const BorderRadius.only(
                            bottomRight: Radius.circular(10))),
                    child: FittedBox(
                      child: Text(
                        'परिमाण: ${widget.productDetails?['quantity']}',
                        style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'ubuntu',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(10))),
                    child: FittedBox(
                      child: Text(
                        'रू. ${widget.productDetails?['price']}/ ${widget.productDetails?['perItemTag']}',
                        style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'ubuntu',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 0,
                  bottom: 0,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: sizeProvider(context).width,
                        height: 80,
                        child: ListTile(
                          minVerticalPadding: 0,
                          visualDensity:
                              const VisualDensity(horizontal: 0, vertical: -4),
                          leading: storeIt(widget.productDetails?['by'])[
                                      'photoUrl'] ==
                                  ''
                              ? const CircleAvatar(
                                  maxRadius: 22,
                                  backgroundImage:
                                      AssetImage('images/avatar.png'),
                                )
                              : CircleAvatar(
                                  maxRadius: 20,
                                  backgroundImage: NetworkImage(
                                      '${storeIt(widget.productDetails?['by'])['photoUrl']}'),
                                ),
                          title: Text(
                            "${storeIt(widget.productDetails?['by'])['fname']}  ${storeIt(widget.productDetails?['by'])['lname']}",
                            style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                          ),
                          trailing: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<dynamic>(
                                isExpanded: false,
                                focusColor: Colors.transparent,
                                elevation: 0,
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                autofocus: false,
                                icon: const Icon(
                                  FontAwesomeIcons.ellipsis,
                                  color: Colors.black54,
                                ),
                                items:
                                    isCurrentUser(widget.productDetails?['by'])
                                        ? postModify.map((e) {
                                            return DropdownMenuItem(
                                                value: e, child: Text(e));
                                          }).toList()
                                        : postReport.map((e) {
                                            return DropdownMenuItem(
                                                value: e, child: Text(e));
                                          }).toList(),
                                onChanged: (val) async {
                                  if (val == "edit") {
                                    getAction(GetActionType.edit,
                                        docId: widget.productDetails?['pid']);
                                  } else if (val == "delete") {
                                    getAction(GetActionType.delete,
                                        cntxt: context,
                                        docId: widget.productDetails?["pid"],
                                        by: widget.productDetails?['by']);
                                  } else if (val == "report") {
                                    getAction(GetActionType.report);
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  dynamic getAction(GetActionType getActionType,
      {String by = "", String docId = "", BuildContext? cntxt}) {
    switch (getActionType) {
      case GetActionType.edit:
        break;
      case GetActionType.delete:
        return showDialog(
            context: cntxt!,
            builder: (cntxt) {
              return getDialog(deleteKinemlPost,
                  context: cntxt,
                  pid: docId,
                  img: widget.productDetails?['image']);
            });
      case GetActionType.report:
        return showDialog(
            context: context,
            builder: (_) {
              return getReportDialog(context);
            });
    }
  }
}

enum GetActionType { edit, delete, report }
