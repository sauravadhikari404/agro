// ignore_for_file: avoid_unnecessary_containers
import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/main/grid/gridopt.dart';
import 'package:agromate/screen/widget/adsslider.dart';
import 'package:agromate/screen/widget/main/newswidget.dart';
import 'package:agromate/screen/widget/main/videowidget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart' as picker;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  RxString timeNow = picker.NepaliDateTime.now().format('aa h:mm').obs;
  late AdWithView _adWithView;
  int response = 0;
  double adHeight = 0.0;

  _getAds() {
    _adWithView = adsController.getBannerAd();
    if (_adWithView.responseInfo == null) {
      response = 0;
    } else {
      response = 1;
      adHeight = 70.0;
    }
  }

  @override
  void initState() {
    if (!mounted) return;
    _getAds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime prebackpress = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(prebackpress);
        final cantExit = timegap >= const Duration(seconds: 2);
        if (cantExit) {
          const snack = SnackBar(
            content: Text(
              'Press Back button again to Exit',
              style: TextStyle(fontFamily: 'Ubuntu'),
            ),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        }
        Get.back();
        return true;
      },
      child: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          children: [
            const AdsSliderScreen(),
            Container(
              constraints: const BoxConstraints(minHeight: 360),
              clipBehavior: Clip.antiAlias,
              width: double.infinity,
              height: 420,
              decoration: const BoxDecoration(),
              padding: const EdgeInsets.all(10.0),
              child: Flex(
                direction: Axis.horizontal,
                children: const [
                  Flexible(child: LimitedBox(child: GridCatOptions())),
                ],
              ),
            ),
            const NewsWidget(),
            // SizedBox(
            //   width: double.infinity,
            //   height: adHeight,
            //   child: AdWidget(
            //     ad: _adWithView,
            //   ),
            // ),
            const VideoWidget(),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (!mounted) return;
    adsController.getBannerAd().dispose();
    super.dispose();
  }
}
