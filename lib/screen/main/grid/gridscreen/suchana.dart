import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/widget/helpers/pdfviewer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:image_viewer/image_viewer.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class KrishiSuchana extends StatefulWidget {
  KrishiSuchana({Key? key}) : super(key: key);

  @override
  _KrishiSuchanaState createState() => _KrishiSuchanaState();
}

class _KrishiSuchanaState extends State<KrishiSuchana> {
  var cusdate = NepaliDateFormat.yMEd();
  final _key = GlobalKey<FormBuilderState>();
  late AdWithView _adWithView;
  bool isResponsed = false;
  _getAds() {
    _adWithView = adsController.getBannerAd2();
    if (_adWithView.responseInfo == null) {
      isResponsed = false;
    } else {
      isResponsed = true;
    }
  }

  final TextEditingController _districtTextController =
      TextEditingController(text: '');
  final TextEditingController _provinceTextController =
      TextEditingController(text: '');
  var jindex = 0;
  @override
  void initState() {
    _getAds();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant KrishiSuchana oldWidget) {
    super.didUpdateWidget(oldWidget);
    activate();
    setState(() {});
  }

  var colour = Colors.black;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: StreamBuilder<QuerySnapshot>(
            stream: getSuchanaData(
                _provinceTextController.text, _districtTextController.text),
            builder: (context, snapshot) {
              return CustomScrollView(
                shrinkWrap: true,
                clipBehavior: Clip.antiAlias,
                primary: true,
                slivers: [
                  SliverAppBar(
                    leading: IconButton(
                        icon: const Icon(
                          LineIcons.arrowLeft,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                    backgroundColor: Colors.white,
                    title: const Text(
                      'सुचनाहरु',
                      style:
                          TextStyle(color: Colors.black, fontFamily: 'Poppins'),
                    ),
                    pinned: true,
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(color: Colors.white),
                      child: FormBuilder(
                        key: _key,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Wrap(
                                alignment: WrapAlignment.center,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      onChanged: (value) {
                                        if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश न.१") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 0;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश न.२") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 1;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "बागमती प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 2;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "गण्डकी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 3;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "लुम्बिनी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 4;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "कर्णाली प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 5;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "सुदूरपश्चिम प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 6;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        }
                                      },
                                      focusColor: Colors.blue,
                                      name: 'pradesh',
                                      items: pradeshMap
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'pradesh']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                    .currentState!
                                                                    .fields[
                                                                        'pradesh']!
                                                                    .value['title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Colors.black
                                                        : Colors.black,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'प्रदेश',
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontFamily: 'Poppins',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      enabled: _key.currentState
                                                  ?.fields['pradesh']?.value !=
                                              null
                                          ? true
                                          : false,
                                      onChanged: (value) {
                                        if (_key.currentState!.fields['jilla']!
                                                .value !=
                                            null) {
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        }
                                      },
                                      name: 'jilla',
                                      items: jillaMap[jindex]
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'jilla']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                    .currentState!
                                                                    .fields[
                                                                        'jilla']!
                                                                    .value['title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Colors.black
                                                        : Colors.black,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: const InputDecoration(
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'जिल्ला',
                                          labelStyle: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontFamily: 'Poppins',
                                          )),
                                    ),
                                  ),
                                  IconButton(
                                    hoverColor: Colors.green,
                                    onPressed: () {
                                      if (_key.currentState?.fields['pradesh']
                                                  ?.value !=
                                              null &&
                                          _key.currentState?.fields['jilla']
                                                  ?.value !=
                                              null) {
                                        setState(() {
                                          _provinceTextController.text = _key
                                              .currentState!
                                              .fields['pradesh']!
                                              .value['title']
                                              .toString();
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        });
                                      } else if (_key.currentState!
                                              .fields['pradesh']!.value !=
                                          null) {
                                        _provinceTextController.text = _key
                                            .currentState!
                                            .fields['pradesh']!
                                            .value['title']
                                            .toString();
                                      } else {
                                        Get.snackbar('सूचना',
                                            'पहिले प्रदेश वा जिल्ला छान्नुहोस्');
                                      }
                                    },
                                    icon: const Icon(
                                      Ionicons.search_circle,
                                      size: 28,
                                    ),
                                    splashColor: Colors.green,
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  !snapshot.hasData
                      ? const SliverToBoxAdapter(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : snapshot.data!.docs.isEmpty
                          ? SliverToBoxAdapter(
                              child: Center(
                                child: Lottie.asset(JsonAssets.emptyJson),
                              ),
                            )
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                                (context, index) {
                                  if (index == 2 && isResponsed) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SizedBox(
                                          height: 300,
                                          width: double.infinity,
                                          child: AdWidget(
                                            ad: _adWithView,
                                          )),
                                    );
                                  }
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        if (snapshot.data!.docs[index]
                                                ['filetype'] ==
                                            "pdf") {
                                          Get.to(
                                            PdfViewerSD(
                                              appBarTitle: snapshot.data
                                                      ?.docs[index]['title'] ??
                                                  '',
                                              data: {
                                                'url': snapshot.data
                                                        ?.docs[index]['url'] ??
                                                    '',
                                              },
                                            ),
                                          );
                                        } else {
                                          ImageViewer.showImageSlider(images: [
                                            '${snapshot.data!.docs[index]['url']}'
                                          ]);
                                        }
                                      },
                                      child: Container(
                                        width: sizeProvider(context).width,
                                        height: 100,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10),
                                            ),
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.black87,
                                                  blurRadius: 0.2,
                                                  spreadRadius: 0.1),
                                              BoxShadow(
                                                  color: Colors.white,
                                                  blurRadius: 0.4,
                                                  spreadRadius: 0.4)
                                            ]),
                                        child: Stack(
                                          children: [
                                            ListTile(
                                              leading: const CircleAvatar(
                                                backgroundImage: AssetImage(
                                                    'images/main/folder.png'),
                                                radius: 20,
                                              ),
                                              isThreeLine: true,
                                              title: Text(
                                                snapshot
                                                    .data!.docs[index]['title']
                                                    .toString(),
                                                softWrap: true,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: 'Poppins',
                                                    fontSize: 12),
                                              ),
                                              subtitle: Text(
                                                snapshot
                                                    .data!.docs[index]['from']
                                                    .toString(),
                                                softWrap: true,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                  fontFamily: 'Ubuntu',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              left: 0,
                                              right: 0,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Row(
                                                      children: [
                                                        const Icon(
                                                          Ionicons.person,
                                                          size: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 8.0),
                                                          child: Text(
                                                            snapshot
                                                                .data!
                                                                .docs[index]
                                                                    ['by']
                                                                .toString(),
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  'Ubuntu',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Row(
                                                      children: [
                                                        const Icon(
                                                          Ionicons.time,
                                                          size: 10,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 8.0),
                                                          child: Text(
                                                            NepaliDateTime.fromDateTime(snapshot
                                                                    .data!
                                                                    .docs[index]
                                                                        [
                                                                        'added_at']
                                                                    .toDate())
                                                                .format(
                                                                    'yyyy-MMMM-dd, EEE'),
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  'Ubuntu',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                childCount: snapshot.data!.docs.length,
                              ),
                            ),
                ],
              );
            }),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
