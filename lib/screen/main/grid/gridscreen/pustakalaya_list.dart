import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/helpers/pdfviewer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_viewer/image_viewer.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';

class PustakalayaDataList extends StatefulWidget {
  const PustakalayaDataList(
      {Key? key, required this.title, required this.cTitle})
      : super(key: key);
  final String title;
  final String cTitle;

  @override
  _PustakalayaDataListState createState() => _PustakalayaDataListState();
}

class _PustakalayaDataListState extends State<PustakalayaDataList> {
  var cusdate = NepaliDateFormat.yMEd();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: widget.title,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: firebaseFirestore
            .collection('KrishiPustakalaya')
            .doc(widget.cTitle)
            .collection('data')
            .snapshots(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SizedBox(
              width: sizeProvider(context).width,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.data!.docs.isEmpty) {
            return SizedBox(
              width: sizeProvider(context).width,
              child: Center(child: Lottie.asset(JsonAssets.emptyJson)),
            );
          }
          return AnimatedList(
              initialItemCount: snapshot.data?.docs.length ?? 0,
              itemBuilder:
                  (_, index, Animation<double> AlwaysStoppedAnimation) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      if (snapshot.data!.docs[index]['filetype'] == "pdf") {
                        Get.to(
                          PdfViewerSD(
                            appBarTitle:
                                snapshot.data?.docs[index]['title'] ?? '',
                            data: {
                              'url': snapshot.data?.docs[index]['url'] ?? '',
                            },
                          ),
                        );
                      } else {
                        ImageViewer.showImageSlider(
                            images: ['${snapshot.data!.docs[index]['url']}']);
                      }
                    },
                    child: Container(
                      width: sizeProvider(context).width,
                      height: 100,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black87,
                                blurRadius: 0.2,
                                spreadRadius: 0.1),
                            BoxShadow(
                                color: Colors.white,
                                blurRadius: 0.4,
                                spreadRadius: 0.4)
                          ]),
                      child: Stack(
                        children: [
                          ListTile(
                            leading: const CircleAvatar(
                              backgroundImage:
                                  AssetImage('images/main/folder.png'),
                              radius: 20,
                            ),
                            isThreeLine: true,
                            title: Text(
                              snapshot.data!.docs[index]['title'].toString(),
                              softWrap: true,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Poppins',
                                  fontSize: 15),
                            ),
                            subtitle: Text(
                              snapshot.data!.docs[index]['from'].toString(),
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            left: 0,
                            right: 0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Ionicons.person,
                                        size: 10,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          snapshot.data!.docs[index]['by']
                                              .toString(),
                                          style: const TextStyle(
                                            fontFamily: 'Ubuntu',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Ionicons.time,
                                        size: 10,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          cusdate
                                              .format(
                                                  NepaliDateTime.fromDateTime(
                                                      snapshot
                                                          .data!
                                                          .docs[index]
                                                              ['timestamp']
                                                          .toDate()))
                                              .toString(),
                                          style: const TextStyle(
                                            fontFamily: 'Ubuntu',
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              });
        },
      ),
    );
  }
}
