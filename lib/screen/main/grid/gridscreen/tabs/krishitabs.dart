import 'package:agromate/screen/main/grid/gridscreen/tabs/guidedata.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gradient_widgets/gradient_widgets.dart';

class KrishiTabsDataScreen extends StatefulWidget {
  final RxInt index;
  const KrishiTabsDataScreen({Key? key,required this.index}) : super(key: key);

  @override
  _KrishiTabsDataScreenState createState() => _KrishiTabsDataScreenState();
}

class _KrishiTabsDataScreenState extends State<KrishiTabsDataScreen> {
  var guide = [
    {
      List: [
        {'title': 'गाई',
          'collectionName':'gaai',
          'thumb': 'images/KrishiGuide/pasupalan/cow.jpg',
          'type':'Animals'
        },
        {'title': 'भैसी',
          'collectionName':'bhaisi',
          'thumb': 'images/KrishiGuide/pasupalan/buffalo2.jpg',
          'type':'Animals'},
        {'title': 'बाख्रा',
          'collectionName':'bakhra',
          'thumb': 'images/KrishiGuide/pasupalan/goat1.jpg',
          'type':'Animals'},
        {
          'title': 'लेयर्स कुखुरा',
          'collectionName':'localkukhura',
          'thumb': 'images/KrishiGuide/pasupalan/localhen.jpg','type':'Animals'
        },
        {
          'title': 'बोइलर कुखुरा',
          'collectionName':'boilerkukhura',
          'thumb': 'images/KrishiGuide/pasupalan/boilerhen.jpg',
          'type':'Animals'
        },
        {'title': 'बंगुर',
          'collectionName':'bangur',
          'thumb': 'images/KrishiGuide/pasupalan/pig.jpg',
          'type':'Animals'},
        {'title': 'मौरी','collectionName':'mauri',
          'thumb': 'images/KrishiGuide/pasupalan/bees.png',
          'type':'Animals'},
        {'title': 'बट्टाई','collectionName':'battai',
          'thumb': 'images/KrishiGuide/pasupalan/battai.jpg',
          'type':'Animals'},
        {'title': 'माछा','collectionName':'machha',
          'thumb': 'images/KrishiGuide/pasupalan/fish1.jpg',
          'type':'Animals'},
      ]
    },
    {
      List: [
        {
          'title': 'टमाटर','collectionName':'tomato',
          'thumb': 'images/KrishiGuide/vegitables/tomatoes.jpg','type':'Vegetables'
        },
        {'title': 'बोडी',
          'collectionName':'bodi',
          'thumb': 'images/KrishiGuide/vegitables/bodi.jpg',
          'type':'Vegetables'},
        {'title': 'आलु',
          'collectionName':'aalu',
          'thumb': 'images/KrishiGuide/vegitables/potato.jpg',
          'type':'Vegetables'},
        {
          'title': 'काँक्रो',
          'collectionName':'kakro',
          'thumb': 'images/KrishiGuide/vegitables/cucumber.jpg','type':'Vegetables'
        },
        {'title': 'बन्दा/काउली',
          'collectionName':'kaauli',
          'thumb': 'images/KrishiGuide/vegitables/banada.jpg',
          'type':'Vegetables'},

        {
          'title': 'खुर्सानी',
          'collectionName':'khursani',
          'thumb': 'images/KrishiGuide/vegitables/khursani.jpg','type':'Vegetables'
        },
        {'title': 'मुला', 'thumb': 'images/KrishiGuide/vegitables/mulaaa.jpg','type':'Vegetables'},

        {
          'title': 'गाँजर',
          'collectionName':'gajar',
          'thumb': 'images/KrishiGuide/vegitables/carratos.jpg','type':'Vegetables'
        },
        {
          'title': 'फर्सी',
          'collectionName':'pharsi',
          'thumb': 'images/KrishiGuide/vegitables/pumpkin.jpg','type':'Vegetables'
        },
        {
          'title': 'करेला',
          'collectionName':'karela',
          'thumb': 'images/KrishiGuide/vegitables/bitterkarela.jpg','type':'Vegetables'
        },
        {
          'title': 'च्याउ',
          'collectionName':'chyau',
          'thumb': 'images/KrishiGuide/vegitables/mushrooms.jpg','type':'Vegetables'
        },



      ]
    },
    {
      List: [
        {
          'title': 'केरा',
          'collectionName':'kera',
          'thumb': 'images/KrishiGuide/fruites/banana.jpg',
          'type':'Fruits'
        },
        {'title': 'स्याउ','collectionName':'shyau',
          'thumb': 'images/KrishiGuide/fruites/apple.jpg','type':'Fruits'},
        {'title': 'ड्रागन फल','collectionName':'dragonfal','thumb': 'images/KrishiGuide/fruites/dragon.jpg','type':'Fruits'},
        {'title': 'एभोकाडो','collectionName':'avocado','thumb': 'images/KrishiGuide/fruites/avocado.jpg','type':'Fruits'},
        {'title': 'किवी','collectionName':'kiwi', 'thumb': 'images/KrishiGuide/fruites/kiwi.jpg','type':'Fruits'},
        {'title': 'अंगुर','collectionName':'angur', 'thumb': 'images/KrishiGuide/fruites/angoor.jpg','type':'Fruits'},
        {'title': 'आँप','collectionName':'aanp', 'thumb': 'images/KrishiGuide/fruites/mango.jpg','type':'Fruits'},
        {'title': 'लिच्ची','collectionName':'litchi', 'thumb': 'images/KrishiGuide/fruites/lichhi.jpg','type':'Fruits'},
        {'title': 'इस्ट्रोबेरी','collectionName':'strawberry','thumb': 'images/KrishiGuide/fruites/strawberries.jpg','type':'Fruits'},
      ]
    },
    {
      List: [
        {'title': 'मकै','collectionName':'makai', 'thumb': 'images/KrishiGuide/cashcrop/maize.jpg','type':'Annabali'},
        {'title': 'धान','collectionName':'dhaan', 'thumb': 'images/KrishiGuide/cashcrop/dhan.jpg','type':'Annabali'},
        {'title': 'गंहू','collectionName':'gahun', 'thumb': 'images/KrishiGuide/cashcrop/gahun.jpg','type':'Annabali'},
        {'title': 'कोदो','collectionName':'kodo','thumb': 'images/KrishiGuide/cashcrop/fingermillet.jpg','type':'Annabali'},
        {'title': 'तोरी','collectionName':'tori', 'thumb': 'images/KrishiGuide/cashcrop/tori.jpg','type':'Annabali'},

      ]
    },
    {
      List: [
        {'title': 'अलैची',
          'collectionName':'alaichi',
          'thumb': 'images/KrishiGuide/annabali/alaichi.png','type':'Nagadebali'},
        {'title': 'कफी','collectionName':'kafi', 'thumb': 'images/KrishiGuide/annabali/coffee.jpg','type':'Nagadebali'},
        {'title': 'सूर्यमुखी','collectionName':'suryamukhi','thumb': 'images/KrishiGuide/annabali/sunflower.jpg','type':'Nagadebali'},
        {'title': 'जिरा','collectionName':'jira', 'thumb': 'images/KrishiGuide/annabali/jira.jpg','type':'Nagadebali'}

      ]
    },
    {
      List: [
        {'title': 'टिमुर',
          'collectionName':'timur',
          'thumb': 'images/KrishiGuide/jadibuti/timur.jpg','type':'Jadibuti'},
      ]
    },
    {
      List: [
        {
          'title': 'सयपत्री',
          'collectionName':'sayepatri',
          'thumb': 'images/KrishiGuide/flowers/saypatri.jpg','type':'Flowers'
        },
        {'title': 'मखमली',
          'collectionName':'makhamali',
          'thumb': 'images/KrishiGuide/flowers/makhamali.jpg','type':'Flowers'},
      ]
    },
    {
      List: [
        {
          'title': 'सुपर नेपियर',
          'collectionName':'supernapier',
          'thumb': 'images/KrishiGuide/grass/supernapier.jpg','type':'Grass'
        },
      ]
    },
    {
      List: [
        {
          'title': 'पाउलोनीया',
          'collectionName':'pauloniya',
          'thumb': 'images/KrishiGuide/tree/pawolinia.jpg','type':'Trees'
        },
        {'title': 'अगरउड','collectionName':'agarwood', 'thumb': 'images/KrishiGuide/tree/Agarwood.jpg','type':'Trees'},
      ]
    },
    {
      List: [
        {
          'title': 'रासायनिक मल','collectionName':'rasayanikmal',
          'thumb': 'images/KrishiGuide/fertilizer/chemical-fertilizer.jpg','type':'Mal'
        },
        {
          'title': 'प्रांगारिक मल',
          'collectionName':'prangarikmal',
          'thumb': 'images/KrishiGuide/fertilizer/organicfertilizer.jpg','type':'Mal'
        },
        {
          'title': 'गडयौला मल',
          'collectionName':'gadeulamal',
          'thumb': 'images/KrishiGuide/fertilizer/earthworm.jpg','type':'Mal'
        },
      ]
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
            itemCount: guide[widget.index.toInt()][List]!.length,
            shrinkWrap: true,
            itemBuilder: (_, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: double.infinity,
                  height: 200,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(25),
                    ),
                  ),
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        height: 200,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  '${guide[widget.index.toInt()][List]![index]['thumb']}'),
                              fit: BoxFit.cover),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.4)),
                          width: double.infinity,
                          height: 50,
                          child: Center(
                            child: Text(
                              '${guide[widget.index.toInt()][List]![index]['title']}',
                              style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                      GradientCard(
                        gradient: LinearGradient(colors: [
                          Colors.yellow.withOpacity(0.1),
                          Colors.green.withOpacity(0.2)
                        ]),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Get.to(()=>KrishiGuideData(collectionName: guide[widget.index.toInt()][List]![index]['collectionName'].toString(),
                                  type: guide[widget.index.toInt()][List]![index]['type'].toString(), title: guide[widget.index.toInt()][List]![index]['title'].toString()));
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            }));
  }
}
