import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/addincome.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/incomelist.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class IncomeHisab extends StatefulWidget {
  const IncomeHisab({Key? key}) : super(key: key);

  @override
  _IncomeHisabState createState() => _IncomeHisabState();
}

class _IncomeHisabState extends State<IncomeHisab> {
  final HisabKitabIncomeController incomeHisab =
      Get.put(HisabKitabIncomeController());

  RxList<dynamic> title = [].obs;

  @override
  void initState() {
    super.initState();
    getTitleData();
  }

  void getTitleData() {
    RxList preType = [].obs;
    for (var element in incomeHisab.hisabkitabIncomeData) {
      RxList listThis = RxList.filled(incomeHisab.hisabkitabIncomeData.length, {
        'title': element.title.toString(),
      });
      preType.addAll(listThis);
    }
    RxList<dynamic> stackDuplicates(title) {
      Map<String, dynamic> uniqueItems = {};
      for (Map item in title) {
        final key = '$item["title"]';
        (uniqueItems[key] == null) ? uniqueItems[key] = item : '';
      }
      return uniqueItems.values.toList().obs;
    }

    title.addAll(stackDuplicates(preType));
  }

  int countTitleStored(String titleTy) {
    int count = 0;
    for (var element in incomeHisab.hisabkitabIncomeData) {
      if (element.title == titleTy) {
        count++;
      }
    }
    return count;
  }

  double countTotalAmountType(String titleTy) {
    double count = 0;
    for (var element in incomeHisab.hisabkitabIncomeData) {
      if (element.title == titleTy) {
        count = count + element.amount;
      }
    }
    return count;
  }

  Color colorTy = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomeAppBar(
        appBarTitle: 'आम्दानी हिसाबकिताब',
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () async {
            await Get.to(() => const AddIncome());
            setState(() {
              colorTy == Colors.white
                  ? colorTy = Colors.black
                  : colorTy = Colors.white;
              title.clear();
              getTitleData();
            });
          },
          label: const Icon(Ionicons.add_circle)),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () async {
            setState(() {
              colorTy == Colors.white
                  ? colorTy = Colors.black
                  : colorTy = Colors.white;
              title.clear();
              getTitleData();
            });
          },
          child: title.isEmpty
              ? Center(
                  child: Lottie.asset(JsonAssets.emptyJson),
                )
              : ListView.builder(
                  itemCount: title.length,
                  itemBuilder: (_, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: const BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Color(0xff00c853),
                              Color(0xff64dd17),
                            ]),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: InkWell(
                          splashColor: Colors.redAccent,
                          onTap: () async {
                            await Get.to(() => IncomeDataList(
                                  title: title[index]['title'],
                                  count:
                                      countTitleStored(title[index]['title']),
                                ));
                            setState(() {
                              colorTy == Colors.white
                                  ? colorTy = Colors.black
                                  : colorTy = Colors.white;

                              title.clear();
                              getTitleData();
                            });
                          },
                          child: ListTile(
                            title: Text(
                              title[index]['title'].toString().toUpperCase(),
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 29,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle: Text(
                              'रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(countTotalAmountType(title[index]['title']))}',
                              style: const TextStyle(
                                fontFamily: 'ubuntu',
                                fontSize: 22,
                                color: Color(0xff263238),
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            trailing: Text(
                              NepaliNumberFormat(
                                      language: Language.nepali,
                                      decimalDigits: 0)
                                  .format(
                                      countTitleStored(title[index]['title'])),
                              style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 26,
                                color: Color(0xfff1f8e9),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
        );
      }),
    );
  }
}
