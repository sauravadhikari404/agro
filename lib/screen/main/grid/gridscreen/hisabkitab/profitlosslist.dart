import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:timelines/timelines.dart';

class ProfitLossListDetails extends StatefulWidget {
  final String title;
  const ProfitLossListDetails({Key? key, required this.title})
      : super(key: key);

  @override
  _ProfitLossListDetailsState createState() => _ProfitLossListDetailsState();
}

class _ProfitLossListDetailsState extends State<ProfitLossListDetails> {
  var expList = [].obs;
  var expListFinal = [].obs;
  var incList = [].obs;
  var incListFinal = [].obs;
  final HisabKitabExpensesController expensesData =
      Get.put(HisabKitabExpensesController());
  final HisabKitabIncomeController incomeData =
      Get.put(HisabKitabIncomeController());
  var combineData = [].obs;
  @override
  void initState() {
    super.initState();
    expList.addAll(getData(expensesData.hisabkitabExpensesData, widget.title)
        .toSet()
        .toList());
    incList.addAll(getData(incomeData.hisabkitabIncomeData, widget.title)
        .toSet()
        .toList());
    debugPrint('Expenses List Data ' + expList.toString());
    debugPrint('Income List Data ' + incList.toString());
    expList.sort((a, b) => DateTime.parse(a['date'].toString())
        .compareTo(DateTime.parse(b['date'].toString())));
    expListFinal.addAll(expList);
    incListFinal.addAll(incList);
    debugPrint('Expenses List Data ' + expList.toString());
    debugPrint('Expenses List Final Data ' + expListFinal.toString());
    combineData.addAll(expList + incList);
  }

  RxList<dynamic> getData(RxList<dynamic> list, String title) {
    RxList<dynamic> returnList = [].obs;
    for (var data in list) {
      if (data.title.toString() == title) {
        RxList thisList = RxList.filled(list.length, {
          'title': data.title.toString(),
          'amount': data.amount.toString(),
          'date': NepaliDateTime.parse(data.date).toDateTime(),
        });
        returnList.addAll(thisList);
      }
    }
    return returnList;
  }

  double sumListData(RxList<dynamic> data) {
    double totalAmount = 0;
    for (var items in data) {
      totalAmount = totalAmount + double.parse(items['amount']);
    }
    return totalAmount;
  }

  // double sumListDataByTitle(RxList<dynamic> data, String title) {
  //   double totalAmount = 0;
  //   for (var items in data) {
  //     if (items['title'] == "title") {
  //       totalAmount = totalAmount + double.parse(items['amount']);
  //     }
  //   }
  //   return totalAmount;
  // }

  String twoSummedValue(RxList<dynamic> inc, RxList<dynamic> exp) {
    double incAmount = sumListData(inc);
    double expAmount = sumListData(exp);
    debugPrint("This is total Expenses Amount" + expAmount.toString());
    debugPrint("This is total Income Amount" + incAmount.toString());
    if (incAmount != 0.0 && expAmount != 0.0) {
      if (incAmount > expAmount) {
        double calc = incAmount - expAmount;
        return "तपाइको आम्दानी ${widget.title}मा रू.${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(calc)} रहेको छ।";
      } else if (incAmount == expAmount) {
        return "तपाइको आम्दानी ${widget.title}मा रू.० रहेको छ।";
      } else {
        double calc = incAmount - expAmount;
        return "तपाइको नुक्सानी ${widget.title}मा रू.${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(calc.abs())} रहेको छ।";
      }
    } else if (incAmount != 0.0 && expAmount == 0.0) {
      return "तपाइको आम्दानी ${widget.title}मा रू.${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(incAmount)} रहेको छ।";
    } else if (incAmount == 0.0 && expAmount == 0.0) {
      return "तपाइको नाफा/नुक्सानी ${widget.title}मा रू.० रहेको छ।";
    } else {
      double calc = incAmount - expAmount;
      return "तपाइको नुक्सानी ${widget.title}मा रू.${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(calc.abs())} रहेको छ।";
    }
  }

  // double getLostOnTitledItem(
  //     RxList<dynamic> inc, RxList<dynamic> exp, String title) {
  //   double incAmount = sumListDataByTitle(inc, title);
  //   double expAmount = sumListDataByTitle(exp, title);
  //   return incAmount - expAmount;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: widget.title,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: const EdgeInsets.all(4.0),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadiusDirectional.horizontal(
                      start: Radius.circular(6), end: Radius.circular(10)),
                  color: const Color(0xffff5722),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.shade400,
                        spreadRadius: 0.5,
                        blurRadius: 0.3,
                        blurStyle: BlurStyle.solid),
                    const BoxShadow(
                        color: Colors.white,
                        spreadRadius: 0.5,
                        blurRadius: 0.4,
                        blurStyle: BlurStyle.outer),
                  ]),
              child: ListTile(
                title: const Text(
                  'आज सम्मको नाफा/नुक्सान',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Poppins',
                  ),
                ),
                subtitle: RichText(
                  text: TextSpan(
                      text: NepaliDateTime.now()
                              .format('EEE MMMM, yyyy')
                              .toString() +
                          '\n',
                      children: [
                        TextSpan(
                            text: twoSummedValue(incListFinal, expListFinal)),
                      ]),
                  softWrap: true,
                ),
              ),
            ),
          ),
          FixedTimeline.tileBuilder(
              theme: TimelineThemeData(
                  color: Colors.green.shade300,
                  connectorTheme: ConnectorThemeData(
                    color: Colors.green.shade300,
                    thickness: 4,
                    space: 5,
                    indent: 0,
                  ),
                  indicatorTheme:
                      const IndicatorThemeData(color: Colors.green)),
              builder: TimelineTileBuilder.connectedFromStyle(
                  indicatorStyleBuilder: (context, index) {
                    return IndicatorStyle.outlined;
                  },
                  itemCount: combineData.length,
                  contentsAlign: ContentsAlign.alternating,
                  connectionDirection: ConnectionDirection.before,
                  contentsBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          padding: const EdgeInsets.only(bottom: 5),
                          decoration: const BoxDecoration(
                              color: Color(0xff00c853),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          child: ListTile(
                            title: Text(
                              combineData[index]['title']
                                  .toString()
                                  .toUpperCase(),
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                              ),
                            ),
                            subtitle: RichText(
                              text: TextSpan(
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade900,
                                      fontSize: 10,
                                      fontFamily: 'Ubuntu'),
                                  text: NepaliDateTime.parse(combineData[index]
                                                  ['date']
                                              .toString())
                                          .format('EE MMM,yyyy h:m a') +
                                      '\n',
                                  children: [
                                    TextSpan(
                                        text: 'रू. ' +
                                            NepaliNumberFormat(
                                                    language: Language.nepali,
                                                    decimalDigits: 2)
                                                .format(combineData[index]
                                                        ['amount']
                                                    .toString()),
                                        style: const TextStyle(
                                          fontFamily: 'Ubuntu',
                                          fontSize: 16,
                                          color: Color(0xffffebee),
                                        ))
                                  ]),
                            ),
                          ),
                        ),
                      )))
        ],
      ),
    );
  }
}
