import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/addexpenses.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/expenseslist.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class ExpensesHisab extends StatefulWidget {
  const ExpensesHisab({Key? key}) : super(key: key);

  @override
  _ExpensesHisabState createState() => _ExpensesHisabState();
}

class _ExpensesHisabState extends State<ExpensesHisab> {
  final HisabKitabExpensesController expensesData =
      Get.put(HisabKitabExpensesController());

  RxList<dynamic> type = [].obs;

  @override
  void initState() {
    super.initState();

    getTypeData();
  }

  void getTypeData() {
    RxList preType = [].obs;
    for (var element in expensesData.hisabkitabExpensesData) {
      RxList listThis =
          RxList.filled(expensesData.hisabkitabExpensesData.length, {
        'title': element.title.toString(),
      });
      preType.addAll(listThis);
    }
    RxList<dynamic> stackDuplicates(title) {
      Map<String, dynamic> uniqueItems = {};
      for (Map item in title) {
        final key = '$item["title"]';
        (uniqueItems[key] == null) ? uniqueItems[key] = item : '';
      }
      return uniqueItems.values.toList().obs;
    }

    type.addAll(stackDuplicates(preType));
  }

  int countTitleStored(String titleTy) {
    int count = 0;
    for (var element in expensesData.hisabkitabExpensesData) {
      if (element.title == titleTy) {
        count++;
      }
    }
    return count;
  }

  double countTotalAmountTitle(String titleTy) {
    double count = 0;
    for (var element in expensesData.hisabkitabExpensesData) {
      if (element.title == titleTy) {
        count = count + element.amount;
      }
    }
    return count;
  }

  Color colorTy = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomeAppBar(
        appBarTitle: 'खर्च हिसाबकिताब',
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          await Get.to(() => const AddExpenses());
          setState(() {
            colorTy == Colors.white
                ? colorTy = Colors.black
                : colorTy = Colors.white;
            type.clear();

            getTypeData();
          });
        },
        label: const Icon(Ionicons.add_circle),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50))),
      ),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () async {
            setState(() {
              colorTy == Colors.white
                  ? colorTy = Colors.black
                  : colorTy = Colors.white;
              type.clear();

              getTypeData();
            });
          },
          child: type.isEmpty
              ? Center(
                  child: Lottie.asset(JsonAssets.emptyJson),
                )
              : ListView.builder(
                  itemCount: type.length,
                  itemBuilder: (_, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: const BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Color(0xff00c853),
                              Color(0xff64dd17),
                            ]),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: InkWell(
                          splashColor: Colors.redAccent,
                          onTap: () async {
                            await Get.to(() => ExpensesDataList(
                                  title: type[index]['title'],
                                  count: countTitleStored(type[index]['title']),
                                ));
                            setState(() {
                              colorTy == Colors.white
                                  ? colorTy = Colors.black
                                  : colorTy = Colors.white;
                              type.clear();

                              getTypeData();
                            });
                          },
                          child: ListTile(
                            title: Text(
                              type[index]['title'].toString().toUpperCase(),
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 29,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle: Text(
                              'रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(countTotalAmountTitle(type[index]['title']))}',
                              style: const TextStyle(
                                fontFamily: 'ubuntu',
                                fontSize: 22,
                                color: Color(0xff263238),
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            trailing: Text(
                              NepaliNumberFormat(
                                      language: Language.nepali,
                                      decimalDigits: 0)
                                  .format(
                                      countTitleStored(type[index]['title'])),
                              style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 26,
                                color: Color(0xfff1f8e9),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
        );
      }),
    );
  }
}
