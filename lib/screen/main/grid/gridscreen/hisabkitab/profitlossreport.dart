import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/profitlosslist.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:intl/intl.dart';

class ProfitLossReport extends StatefulWidget {
  const ProfitLossReport({Key? key}) : super(key: key);

  @override
  _ProfitLossReportState createState() => _ProfitLossReportState();
}

class _ProfitLossReportState extends State<ProfitLossReport> {
  final HisabKitabIncomeController incomeDataCtrl =
      Get.put(HisabKitabIncomeController());
  final HisabKitabExpensesController expensesDataCtrl =
      Get.put(HisabKitabExpensesController());
  final _expensesData = [];
  final _incomeData = [];
  final _title = [];
  late var _titlecombine = [];
  final _title2 = [];

  @override
  void initState() {
    super.initState();
    expAndIncData();
    extractTitles();
    _titlecombine = _title + _title2;
    adsController.loadAds();
  }

  void expAndIncData() {
    var getExpData = [].obs;
    var getIncData = [].obs;
    for (var element in expensesDataCtrl.hisabkitabExpensesData) {
      List expList =
          List.filled(expensesDataCtrl.hisabkitabExpensesData.length, {
        'title': element.title,
        'amount': element.amount,
        'date': NepaliDateTime.parse(element.date).toDateTime(),
        'dateYearMonth': DateFormat('yyyy-MM')
            .format(NepaliDateTime.parse(element.date).toDateTime()),
        'type': element.type,
      });
      getExpData.addAll(expList);
    }
    RxList<dynamic> stackDuplicates(title) {
      Map<String, dynamic> uniqueItems = {};
      for (Map item in title) {
        final key = '$item["date"]';
        (uniqueItems[key] == null) ? uniqueItems[key] = item : '';
      }
      return uniqueItems.values.toList().obs;
    }

    _expensesData.addAll(stackDuplicates(getExpData));
    for (var element in incomeDataCtrl.hisabkitabIncomeData) {
      List incList = List.filled(incomeDataCtrl.hisabkitabIncomeData.length, {
        'title': element.title,
        'amount': element.amount,
        'date': NepaliDateTime.parse(element.date).toDateTime(),
        'dateYearMonth': DateFormat('yyyy-MM')
            .format(NepaliDateTime.parse(element.date).toDateTime()),
      });
      getIncData.addAll(incList);
    }
    _incomeData.addAll(stackDuplicates(getIncData));
  }

  void extractTitles() {
    //This is Income Titles
    var getTitle = [].obs;
    for (var element in incomeDataCtrl.hisabkitabIncomeData) {
      RxList listThis = RxList.filled(
          incomeDataCtrl.hisabkitabIncomeData.length,
          {'title': element.title.toString()});
      getTitle.addAll(listThis);
    }
//Income Title Stored

    RxList<dynamic> stackDuplicates(title) {
      Map<String, dynamic> uniqueItems = {};
      for (Map item in title) {
        final key = '$item["title"]';
        (uniqueItems[key] == null) ? uniqueItems[key] = item : '';
      }
      return uniqueItems.values.toList().obs;
    }

    var getCombineTitle = [].obs;

    var getTitle2 = [].obs;
    for (var element in expensesDataCtrl.hisabkitabExpensesData) {
      RxList listThis = RxList.filled(
          expensesDataCtrl.hisabkitabExpensesData.length,
          {'title': element.title.toString()});
      getTitle2.addAll(listThis);
    }
    getCombineTitle = getTitle + getTitle2;
    _title.addAll(stackDuplicates(getCombineTitle));
  }

  int countAvailableData(data, title) {
    int count = 0;
    for (var element in data) {
      if (element['title'] == title) {
        count++;
      }
    }
    return count;
  }

  double sumArray(List<dynamic> data) {
    double val = 0.0;
    for (var element in data) {
      val = val + double.parse(element['amount'].toString());
    }
    return val;
  }

  double sumArrayAcToTitle(List<dynamic> data, title) {
    double val = 0.0;
    for (var element in data) {
      if (element['title'] == title) {
        val = val + double.parse(element['amount'].toString());
      }
    }
    return val;
  }

  int countTitleStored(String titleTy, RxList data) {
    int count = 0;
    for (var element in data) {
      if (element.title == titleTy) {
        count++;
      }
    }
    return count;
  }

  String profitLoss(List<dynamic> exp, List<dynamic> inc) {
    double expVal = sumArray(_expensesData);
    double incVal = sumArray(_incomeData);
    double clc = incVal - expVal;
    if (clc.isNegative) {
      clc = clc.abs();
      String last = NepaliNumberFormat(
              language: Language.nepali, symbol: 'रू.', isMonetory: true)
          .format('$clc');
      return "जम्मा घाटा रकम $last";
    }
    String last = NepaliNumberFormat(
            language: Language.nepali, symbol: 'रू.', isMonetory: true)
        .format('$clc');
    return "जम्मा नाफा रकम $last";
  }

  Color colorTy = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'नाफा/घाटा विवरण',
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: sizeProvider(context).width,
                clipBehavior: Clip.antiAlias,
                height: 150,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    image: const DecorationImage(
                        image:
                            AssetImage('images/KrishiGuide/cashcrop/maize.jpg'),
                        fit: BoxFit.cover,
                        colorFilter:
                            ColorFilter.mode(Colors.green, BlendMode.colorBurn),
                        opacity: 0.2),
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.centerLeft,
                        colors: [
                          Colors.greenAccent.shade400,
                          Colors.greenAccent.shade700,
                        ])),
                child: Stack(
                  children: [
                    Positioned(
                      top: 2,
                      left: 5,
                      child: Text(
                        NepaliDateTime.now().format('dd EEE,MMMM,yyyy'),
                        style: const TextStyle(
                          fontFamily: 'Ubuntu',
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 35,
                      left: 20,
                      child: Container(
                        padding:
                            const EdgeInsets.only(left: 5, top: 5, bottom: 5),
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FittedBox(
                              child: Text(
                                'जम्मा आम्दानी रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(sumArray(_incomeData))}',
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Ubuntu',
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            FittedBox(
                              child: Text(
                                'जम्मा खर्च रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(sumArray(_expensesData))}',
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Ubuntu',
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 20,
                      bottom: 10,
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: FittedBox(
                          child: Text(
                            profitLoss(_expensesData, _incomeData),
                            style: const TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            _titlecombine.isEmpty
                ? Center(
                    child: Lottie.asset(JsonAssets.emptyJson),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    itemCount: _titlecombine.length,
                    itemBuilder: (_, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () async {
                            await Get.to(() => ProfitLossListDetails(
                                title: _titlecombine[index]['title']));
                            setState(() {
                              colorTy == Colors.white
                                  ? colorTy = Colors.black
                                  : colorTy = Colors.white;
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.all(8.0),
                            decoration: const BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff00c853),
                                  Color(0xff64dd17),
                                ]),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: ListTile(
                              title: Text(
                                _titlecombine[index]['title']
                                    .toString()
                                    .toUpperCase(),
                                style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 29,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              subtitle: Text(
                                'रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format((sumArrayAcToTitle(_incomeData, _titlecombine[index]['title']) - sumArrayAcToTitle(_expensesData, _titlecombine[index]['title'])).abs())}',
                                style: const TextStyle(
                                  fontFamily: 'ubuntu',
                                  fontSize: 22,
                                  color: Color(0xff263238),
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              trailing: Text(
                                NepaliNumberFormat(
                                        language: Language.nepali,
                                        decimalDigits: 0)
                                    .format(countTitleStored(
                                            _titlecombine[index]['title'],
                                            incomeDataCtrl
                                                .hisabkitabIncomeData) +
                                        countTitleStored(
                                            _titlecombine[index]['title'],
                                            expensesDataCtrl
                                                .hisabkitabExpensesData)),
                                style: const TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 26,
                                  color: Color(0xfff1f8e9),
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
          ],
        ),
      ),
    );
  }
}
