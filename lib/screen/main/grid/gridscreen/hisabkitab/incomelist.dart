import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/addincome.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class IncomeDataList extends StatefulWidget {
  const IncomeDataList({Key? key, required this.title, required this.count})
      : super(key: key);
  final String title;
  final int count;
  @override
  _IncomeDataListState createState() => _IncomeDataListState();
}

class _IncomeDataListState extends State<IncomeDataList> {
  final HisabKitabIncomeController incomeData =
      Get.put(HisabKitabIncomeController());

  @override
  void initState() {
    super.initState();
  }

  int countTypeStored(String type) {
    int count = 0;
    for (var element in incomeData.hisabkitabIncomeData) {
      if (element.type == type) {
        count++;
      }
    }
    return count;
  }

  @override
  void didUpdateWidget(covariant IncomeDataList oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  Color colorTy = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: widget.title,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          await Get.to(() => const AddIncome());
          setState(() {
            colorTy == Colors.white
                ? colorTy = Colors.black
                : colorTy = Colors.white;
          });
        },
        label: const Icon(Ionicons.add_circle),
      ),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () async {
            setState(() {
              colorTy == Colors.white
                  ? colorTy = Colors.black
                  : colorTy = Colors.white;
            });
          },
          child: incomeData.hisabkitabIncomeData.isEmpty
              ? Center(
                  child: Lottie.asset(JsonAssets.emptyJson),
                )
              : ListView.builder(
                  itemCount: incomeData.hisabkitabIncomeData.length,
                  itemBuilder: (_, index) {
                    if (incomeData.hisabkitabIncomeData[index].title ==
                        widget.title) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(4.0),
                          decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              gradient: LinearGradient(colors: [
                                Color(0xff00c853),
                                Color(0xff64dd17),
                              ])),
                          child: ListTile(
                            title: Text(
                              'रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(incomeData.hisabkitabIncomeData[index].amount)}',
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Poppins',
                                fontSize: 26,
                                color: Colors.white,
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: RichText(
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: NepaliDateTime.parse(incomeData
                                          .hisabkitabIncomeData[index].date)
                                      .format('dd EEE,MMMM,yyyy, h:m aa'),
                                  style: TextStyle(
                                    color: Colors.blueGrey.shade800,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                            trailing: InkWell(
                              onTap: () async {
                                incomeData.deleteIncomeData(
                                    incomeData.hisabkitabIncomeData[index].id);
                                setState(() {
                                  colorTy == Colors.white
                                      ? colorTy = Colors.black
                                      : colorTy = Colors.white;
                                });
                              },
                              child: const Icon(Ionicons.trash),
                            ),
                          ),
                        ),
                      );
                    }
                    return Container();
                  }),
        );
      }),
    );
  }
}
