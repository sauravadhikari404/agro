import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'addexpenses.dart';

class ExpensesDataList extends StatefulWidget {
  const ExpensesDataList({Key? key, required this.title, required this.count})
      : super(key: key);
  final String title;
  final int count;
  @override
  _ExpensesDataListState createState() => _ExpensesDataListState();
}

class _ExpensesDataListState extends State<ExpensesDataList> {
  final HisabKitabExpensesController expensesData =
      Get.put(HisabKitabExpensesController());

  @override
  void initState() {
    super.initState();
  }

  int countTypeStored(String type) {
    int count = 0;
    for (var element in expensesData.hisabkitabExpensesData) {
      if (element.type == type) {
        count++;
      }
    }
    return count;
  }

  @override
  void didUpdateWidget(covariant ExpensesDataList oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  Color colorTy = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: widget.title,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          await Get.to(() => const AddExpenses());
          setState(() {
            colorTy == Colors.white
                ? colorTy = Colors.black
                : colorTy = Colors.white;
          });
        },
        label: const Icon(Ionicons.add_circle),
      ),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () async {
            setState(() {
              colorTy == Colors.white
                  ? colorTy = Colors.black
                  : colorTy = Colors.white;
            });
          },
          child: expensesData.hisabkitabExpensesData.isEmpty
              ? Center(
                  child: Lottie.asset(JsonAssets.emptyJson),
                )
              : ListView.builder(
                  itemCount: expensesData.hisabkitabExpensesData.length,
                  itemBuilder: (_, index) {
                    if (expensesData.hisabkitabExpensesData[index].title ==
                        widget.title) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(4.0),
                          decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              gradient: LinearGradient(colors: [
                                Color(0xff00c853),
                                Color(0xff64dd17),
                              ])),
                          child: ListTile(
                            title: Text(
                              expensesData.hisabkitabExpensesData[index].type
                                  .toString()
                                  .toUpperCase(),
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Poppins',
                                fontSize: 26,
                                color: Colors.white,
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: RichText(
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                    text:
                                        '${NepaliDateTime.parse(expensesData.hisabkitabExpensesData[index].date).format('dd EEE,MMMM,yyyy, h:m aa')} \n \n',
                                    style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontSize: 16,
                                    ),
                                    children: [
                                      TextSpan(
                                        text:
                                            'रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(expensesData.hisabkitabExpensesData[index].amount)}',
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 26,
                                          fontFamily: 'Ubuntu',
                                          fontWeight: FontWeight.w700,
                                        ),
                                        spellOut: true,
                                      ),
                                    ]),
                              ),
                            ),
                            trailing: InkWell(
                              onTap: () async {
                                expensesData.deleteExpensesData(expensesData
                                    .hisabkitabExpensesData[index].id);
                                setState(() {
                                  colorTy == Colors.white
                                      ? colorTy = Colors.black
                                      : colorTy = Colors.white;
                                });
                              },
                              child: Icon(Ionicons.trash),
                            ),
                          ),
                        ),
                      );
                    }
                    return Container();
                  }),
        );
      }),
    );
  }
}
