import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/expenses.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/income.dart';
import 'package:agromate/screen/main/grid/gridscreen/hisabkitab/profitlossreport.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:intl/intl.dart';

class HisabKitabScreen extends StatefulWidget {
  const HisabKitabScreen({Key? key}) : super(key: key);

  @override
  _HisabKitabScreenState createState() => _HisabKitabScreenState();
}

class _HisabKitabScreenState extends State<HisabKitabScreen> {
  final HisabKitabIncomeController incomeDataCtrl =
      Get.put(HisabKitabIncomeController());
  final HisabKitabExpensesController expensesDataCtrl =
      Get.put(HisabKitabExpensesController());
  var _expensesData = [];
  var _incomeData = [];
  var _compareByDate = [];

  @override
  void initState() {
    super.initState();
    expAndIncData();
    dataByDate();
    adsController.loadAds();
  }

  void expAndIncData() {
    var getExpData = [].obs;
    var getIncData = [].obs;
    for (var element in expensesDataCtrl.hisabkitabExpensesData) {
      List expList =
          List.filled(expensesDataCtrl.hisabkitabExpensesData.length, {
        'title': element.title,
        'amount': element.amount,
        'date': NepaliDateTime.parse(element.date).toDateTime(),
        'dateYearMonth': DateFormat('yyyy-MM')
            .format(NepaliDateTime.parse(element.date).toDateTime()),
        'type': element.type,
      });
      getExpData.addAll(expList);
    }
    RxList<dynamic> stackDuplicates(title) {
      Map<String, dynamic> uniqueItems = {};
      for (Map item in title) {
        final key = '$item["date"]';
        (uniqueItems[key] == null) ? uniqueItems[key] = item : '';
      }
      return uniqueItems.values.toList().obs;
    }

    _expensesData.addAll(stackDuplicates(getExpData));
    for (var element in incomeDataCtrl.hisabkitabIncomeData) {
      List incList = List.filled(incomeDataCtrl.hisabkitabIncomeData.length, {
        'title': element.title,
        'amount': element.amount,
        'date': NepaliDateTime.parse(element.date).toDateTime(),
        'dateYearMonth': DateFormat('yyyy-MM')
            .format(NepaliDateTime.parse(element.date).toDateTime()),
      });
      getIncData.addAll(incList);
    }
    _incomeData.addAll(stackDuplicates(getIncData));
  }

  void dataByDate() {
    int lengthOfExpenses = _expensesData.length;
    int lengthOfIncome = _incomeData.length;
    for (int i = 0; i < lengthOfExpenses; i++) {
      for (int j = 0; j < lengthOfIncome; j++) {
        if (_expensesData[i]['date'] == _incomeData[j]['date']) {
          List cmpList = List.filled(
              lengthOfExpenses > lengthOfIncome
                  ? lengthOfExpenses
                  : lengthOfIncome,
              {
                'expensesTitle': _expensesData[i]['title'],
                'incomeTitle': _incomeData[j]['title'],
                'expensesAmount': _expensesData[i]['amount'],
                'incomeAmount': _incomeData[j]['amount'],
                'expensesType': _expensesData[i]['type'],
                'incomeDate': _incomeData[j]['date'],
                'expensesDate': _expensesData[i]['date'],
              });
          _compareByDate.addAll(cmpList);
        }
      }
    }
  }

  double sumArray(List<dynamic> data) {
    double val = 0.0;
    for (var element in data) {
      val = val + double.parse(element['amount'].toString());
    }
    return val;
  }

  String profitLoss(List<dynamic> exp, List<dynamic> inc) {
    double expVal = sumArray(_expensesData);
    double incVal = sumArray(_incomeData);
    double clc = incVal - expVal;
    if (clc.isNegative) {
      clc = clc.abs();
      String last = NepaliNumberFormat(
              language: Language.nepali, symbol: 'रू.', isMonetory: true)
          .format('$clc');
      return "जम्मा घाटा रकम $last";
    }
    String last = NepaliNumberFormat(
            language: Language.nepali, symbol: 'रू.', isMonetory: true)
        .format('$clc');
    return "जम्मा नाफा रकम $last";
  }

  Color colorTy = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'हिसाबकिताब',
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: sizeProvider(context).width,
              clipBehavior: Clip.antiAlias,
              height: 150,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  image: const DecorationImage(
                      image:
                          AssetImage('images/KrishiGuide/cashcrop/maize.jpg'),
                      fit: BoxFit.cover,
                      colorFilter:
                          ColorFilter.mode(Colors.green, BlendMode.colorBurn),
                      opacity: 0.2),
                  gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.centerLeft,
                      colors: [
                        Colors.greenAccent.shade400,
                        Colors.greenAccent.shade700,
                      ])),
              child: Stack(
                children: [
                  Positioned(
                    top: 2,
                    left: 5,
                    child: Text(
                      NepaliDateTime.now().format('dd EEE,MMMM,yyyy'),
                      style: const TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 35,
                    left: 20,
                    child: Container(
                      padding:
                          const EdgeInsets.only(left: 5, top: 5, bottom: 5),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FittedBox(
                            child: Text(
                              'जम्मा आम्दानी रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(sumArray(_incomeData))}',
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                              ),
                            ),
                          ),
                          FittedBox(
                            child: Text(
                              'जम्मा खर्च रू. ${NepaliNumberFormat(language: Language.nepali, decimalDigits: 2).format(sumArray(_expensesData))}',
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: FittedBox(
                        child: Text(
                          profitLoss(_expensesData, _incomeData),
                          style: const TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          ListTile(
            onTap: () async {
              await Get.to(() => const ExpensesHisab());
              setState(() {
                colorTy == Colors.white
                    ? colorTy = Colors.black
                    : colorTy = Colors.white;
                _expensesData.clear();
                _incomeData.clear();
                _compareByDate.clear();
                expAndIncData();
                dataByDate();
              });
            },
            leading: const Icon(Ionicons.cash_outline),
            title: const Text('खर्च'),
          ),
          ListTile(
            onTap: () async {
              await Get.to(() => const IncomeHisab());
              setState(() {
                colorTy == Colors.white
                    ? colorTy = Colors.black
                    : colorTy = Colors.white;
                _expensesData.clear();
                _incomeData.clear();
                _compareByDate.clear();
                expAndIncData();
                dataByDate();
              });
            },
            leading: const Icon(LineIcons.handHoldingUsDollar),
            title: const Text('आम्दानी '),
          ),
          ListTile(
            onTap: () async {
              await Get.to(() => const ProfitLossReport());
              setState(() {
                colorTy == Colors.white
                    ? colorTy = Colors.black
                    : colorTy = Colors.white;
                _expensesData.clear();
                _incomeData.clear();
                _compareByDate.clear();
                expAndIncData();
                dataByDate();
              });
            },
            leading: const Icon(Ionicons.bar_chart_outline),
            title: const Text('नाफा/घाटा रिपोर्ट'),
          ),
        ],
      ),
    );
  }
}
