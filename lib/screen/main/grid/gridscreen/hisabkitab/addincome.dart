import 'dart:async';

import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/input/datepicker.dart';
import 'package:agromate/screen/widget/input/inputfield.dart';
import 'package:agromate/screen/widget/input/stringAutoComplete.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class AddIncome extends StatefulWidget {
  const AddIncome({Key? key}) : super(key: key);

  @override
  _AddIncomeState createState() => _AddIncomeState();
}

class _AddIncomeState extends State<AddIncome> {
  final _key = GlobalKey<FormState>();

  TextEditingController dateTextEditController = TextEditingController(
      text: NepaliDateTime.now().format('yyyy-MM-dd').toString());
  final HisabKitabIncomeController incomeData =
      Get.put(HisabKitabIncomeController());
  String? _selectedText;
  String pickedDates = '';
  Timer? _timer;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    incomeData.addHisabIncomeDateController?.text =
        NepaliDateTime.now().toString();
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'आम्दानी रेकर्ड थप्नुहोस',
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _key,
          child: Column(
            children: [
              StringAutoComplete(
                labelText: "शिर्षक",
                hintText: "जस्तै मकै बाली, टमाटर खेति आदि",
                controller: incomeData.addHisabIncomeTitleController!,
              ),
              TextInputField(
                textEditingController:
                    incomeData.addHisabIncomeAmountController!,
                labelText: 'आम्दानी  रकम',
                textInputType: TextInputType.number,
                hintText: '0',
                validator: (val) {
                  if (val == null || val.toString() == '') {
                    return 'कृपया आम्दानी रकम लेख्नुहोस';
                  }
                  return null;
                },
                inputBorder: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.grey[300]!)),
              ),
              NepaliDatePicker(
                hiddenDateTextEditController:
                    incomeData.addHisabIncomeDateController!,
                onChange: (value) {
                  debugPrint(value.toString());
                },
                datePickerEdit: dateTextEditController,
                labelText: 'मिति राख्नुहोस',
                padding: EdgeInsets.all(8.0),
                validator: (value) {
                  if (value == null) {
                    return 'कृपया मिति राख्नुहोस';
                  }
                  return null;
                },
                hintText: 'मिति चुन्नुहोस',
                inputBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey[300]!,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
              ),
              MaterialButton(
                onPressed: () {
                  if (_key.currentState?.validate() ?? false) {
                    incomeData.addIncomeData();
                  }
                },
                splashColor: Colors.red.shade300,
                materialTapTargetSize: MaterialTapTargetSize.padded,
                color: Colors.green.shade300,
                shape: const RoundedRectangleBorder(
                    side: BorderSide(color: Colors.green),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    )),
                child: const Text(
                  'सुरक्षित गर्नुहोस',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
