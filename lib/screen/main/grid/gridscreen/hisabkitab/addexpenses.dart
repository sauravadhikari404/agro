import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/input/datepicker.dart';
import 'package:agromate/screen/widget/input/dropdown.dart';
import 'package:agromate/screen/widget/input/inputfield.dart';
import 'package:agromate/screen/widget/input/stringAutoComplete.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class AddExpenses extends StatefulWidget {
  const AddExpenses({Key? key}) : super(key: key);

  @override
  _AddExpensesState createState() => _AddExpensesState();
}

class _AddExpensesState extends State<AddExpenses> {
  final _key = GlobalKey<FormState>();

  TextEditingController dateTextEditController = TextEditingController(
      text: NepaliDateTime.now().format('yyyy-MM-dd').toString());
  final HisabKitabExpensesController expensesData =
      Get.put(HisabKitabExpensesController());
  String? _selectedText;
  String pickedDates = '';
  final List<String> _type = [
    'ज्याला',
    'सिचाई',
    'बिजुली',
    'इन्धन',
    'बिउबिजन',
    'मल',
    'मेसिनरी',
    'भाडा',
    'औषधि',
    'तलब',
    'दाना/चोकर',
    'घाँस/पराल',
    'प्लास्टिक',
    'अन्य'
  ];

  @override
  void initState() {
    super.initState();
    expensesData.addHisabExpensesTypeController = TextEditingController();
    expensesData.addHisabExpensesDateController = TextEditingController();
    expensesData.addHisabExpensesTitleController = TextEditingController();
    expensesData.addHisabExpensesAmountController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    expensesData.addHisabExpensesDateController?.text =
        NepaliDateTime.now().toString();
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'खर्च रेकर्ड थप्नुहोस',
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _key,
          child: Column(
            children: [
              StringAutoComplete(
                labelText: "शिर्षक",
                hintText: "जस्तै मकै बाली, टमाटर खेति आदि",
                controller: expensesData.addHisabExpensesTitleController!,
              ),
              DropDownField(
                textEditingController:
                    expensesData.addHisabExpensesTypeController!,
                dropdownValue:
                    expensesData.addHisabExpensesTypeController?.text == ''
                        ? _selectedText
                        : expensesData.addHisabExpensesTypeController?.text,
                labelText: 'प्रकार',
                hintText: 'मल,औषधी आदि',
                dropdownList: _type,
                onChange: (value) {
                  setState(() {
                    expensesData.addHisabExpensesTypeController?.text =
                        value.toString();
                  });
                },
                inputBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  borderSide: BorderSide(
                    color: Colors.grey[300]!,
                  ),
                ),
                padding: const EdgeInsets.only(
                    top: 10, left: 8, right: 8, bottom: 10),
                validator: (value) {
                  if (value == null) {
                    return 'कृपया प्रकार छान्नुहोस्';
                  }
                  return null;
                },
              ),
              NepaliDatePicker(
                hiddenDateTextEditController:
                    expensesData.addHisabExpensesDateController!,
                onChange: (value) {
                  debugPrint(value.toString());
                },
                padding: const EdgeInsets.all(8.0),
                datePickerEdit: dateTextEditController,
                labelText: 'मिति राख्नुहोस',
                validator: (value) {
                  if (value == null) {
                    return 'कृपया मिति राख्नुहोस';
                  }
                  return null;
                },
                hintText: 'मिति चुन्नुहोस',
                inputBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey[300]!,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(8))),
              ),
              TextInputField(
                textEditingController:
                    expensesData.addHisabExpensesAmountController!,
                labelText: 'खर्च रकम',
                textInputType: TextInputType.number,
                hintText: '0',
                validator: (val) {
                  if (val == null || val.toString() == '') {
                    return 'कृपया खर्च रकम राख्नुहोस';
                  }
                  return null;
                },
                inputBorder: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.grey[300]!)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MaterialButton(
                  onPressed: () {
                    if (_key.currentState?.validate() ?? false) {
                      expensesData.addExpensesData();
                      _selectedText = '';
                    }
                  },
                  splashColor: Colors.red.shade300,
                  materialTapTargetSize: MaterialTapTargetSize.padded,
                  color: Colors.green.shade300,
                  shape: const RoundedRectangleBorder(
                      side: BorderSide(color: Colors.green),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      )),
                  child: const Text(
                    'सुरक्षित गर्नुहोस',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _selectedText = '';
    expensesData.addHisabExpensesTypeController!.dispose();
    expensesData.addHisabExpensesDateController!.dispose();
    expensesData.addHisabExpensesTitleController!.dispose();
    expensesData.addHisabExpensesAmountController!.dispose();
  }
}
