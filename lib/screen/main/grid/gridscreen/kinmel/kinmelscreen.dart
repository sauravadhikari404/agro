import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/kinmel_controller.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/add_kinmel_product.dart';
import 'package:agromate/screen/widget/kinmelwid.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';

class KinmelScreen extends StatefulWidget {
  const KinmelScreen({Key? key}) : super(key: key);

  @override
  _KinmelScreenState createState() => _KinmelScreenState();
}

class _KinmelScreenState extends State<KinmelScreen> {
  @override
  Widget build(BuildContext context) {
    int? index = kinmelController.kmodel.length;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'कृषि बजार',
          style: TextStyle(
              fontFamily: 'poppins',
              fontWeight: FontWeight.w600,
              color: Colors.white),
        ),
        actions: [
          IconButton(
              onPressed: () {
                if (isLoginUser()) {
                  Get.to(() => const AddItemToKinmel());
                } else {
                  Get.defaultDialog(
                      title: 'Login Required',
                      middleText: 'पहिले login गर्नुहोला',
                      cancel: MaterialButton(
                        onPressed: () {
                          Get.back();
                        },
                        child: const Text(
                          'Cancel',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      confirm: MaterialButton(
                        onPressed: () {
                          Get.back();
                          Get.to(const AccountPage());
                        },
                        child: const Text(
                          'Login',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600),
                        ),
                      ));
                }
              },
              icon: Icon(
                Ionicons.add_circle_outline,
                color: Colors.grey.shade300,
              )),
        ],
        backgroundColor: Colors.green,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: RefreshIndicator(
        backgroundColor: Colors.black.withOpacity(0.4),
        strokeWidth: 3,
        triggerMode: RefreshIndicatorTriggerMode.onEdge,
        onRefresh: () async {
          setState(() {
            build(context);
            kinmelController.allKinmelData();
          });
        },
        child: GetBuilder<KinmelController>(
          init: KinmelController(),
          initState: (_) {},
          builder: (_) {
            return kinmelController.kmodel.isEmpty
                ? Center(
                    child: Lottie.asset(JsonAssets.emptyJson),
                  )
                : ListView.builder(
                    itemCount: index,
                    itemBuilder: (context, index) {
                      var data = kinmelController
                          .kmodel; // yo chai model list bata data ma store gareko
                      var by = data[index].by ?? '';
                      return Obx(
                        () => KinmelWid(
                          productDetails: {
                            'by': by,
                            'pid': data[index].pid,
                            'pType': data[index].pType ?? '',
                            'pName': data[index].pname ?? '',
                            'pdate': data[index].date ?? 0,
                            'validDate': data[index].validDate ?? 0,
                            'quantity': data[index].quantity ?? 0,
                            'quality': data[index].quality ?? '',
                            'price': data[index].price ?? 0,
                            'address': data[index].address ?? '',
                            'image': data[index].image ?? [],
                            'votes': data[index].votes ?? 0,
                            'phone': data[index].phone ?? '',
                            'index': index,
                            'description': data[index].description ?? '',
                            'perItemTag': data[index].perItemTag ?? "",
                          },
                        ),
                      );
                    });
          },
        ),
      ),
    );
  }
}
