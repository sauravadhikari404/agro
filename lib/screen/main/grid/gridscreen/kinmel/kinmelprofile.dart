// ignore_for_file: avoid_unnecessary_containers
import 'dart:io';
import 'dart:typed_data';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/widget/comment/comments.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_viewer/image_viewer.dart';
import 'package:ionicons/ionicons.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:ui' as ui;
import 'package:path_provider/path_provider.dart';

class KinmelProfile extends StatefulWidget {
  final Map<String, dynamic>? dtsource;
  const KinmelProfile({Key? key, this.dtsource}) : super(key: key);

  @override
  _KinmelProfileState createState() => _KinmelProfileState();
}

class ImageList {
  final String image;
  ImageList({required this.image});
}

class _KinmelProfileState extends State<KinmelProfile> {
  TextEditingController commentTexts = TextEditingController(text: '');
  FocusNode fnode = FocusNode();
  var scrollController = ScrollController();
  final _key = GlobalKey<FormFieldState>();
  GlobalKey previewContainer = GlobalKey();
  var commnetModify = [
    'edit',
    'delete',
    'copy',
  ];

  var commnetReport = [
    'report',
    'copy',
  ];
  @override
  void initState() {
    super.initState();
    kinmelController.addViewsToKinmel(
        widget.dtsource?['pid'], widget.dtsource?['index']);
  }

  @override
  void dispose() {
    commentTexts.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = (widget.dtsource?['pdate'] ?? Timestamp.now()).toDate();
    DateTime validDate =
        (widget.dtsource?['validDate'] ?? Timestamp.now()).toDate();
    var pdDateNepali = date.toNepaliDateTime();
    var pdValidDateNepali = validDate.toNepaliDateTime();
    var date1 = NepaliDateFormat.yMEd();
    var date2 = NepaliDateFormat.yMEd();

    return Scaffold(
      body: StreamBuilder(
          stream: firebaseFirestore
              .collection('Kinmel')
              .doc(widget.dtsource?['pid'])
              .collection('Comments')
              .snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Container(
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            return RepaintBoundary(
              key: previewContainer,
              child: CustomScrollView(
                  clipBehavior: Clip.antiAlias,
                  physics: const AlwaysScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: scrollController,
                  slivers: [
                    SliverAppBar(
                      pinned: true,
                      expandedHeight: 200,
                      iconTheme: const IconThemeData(
                        color: Colors.black,
                      ),
                      forceElevated: true,
                      backgroundColor: Colors.white,
                      floating: true,
                      leading: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50))),
                          child: const Icon(Ionicons.arrow_back),
                        ),
                      ),
                      actions: [
                        Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50))),
                          child: InkWell(
                            onTap: () {
                              _captureSocialPng();
                            },
                            child: const Icon(
                              Ionicons.share_social,
                              color: Colors.black,
                            ),
                          ),
                        )
                      ],
                      snap: false,
                      flexibleSpace: FlexibleSpaceBar(
                        collapseMode: CollapseMode.pin,
                        background: Stack(
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: CachedNetworkImage(
                                imageUrl: widget.dtsource?['image'][0],
                                cacheKey: widget.dtsource?['image'][0],
                                imageBuilder: (context, imgProvider) =>
                                    ProgressiveImage(
                                  width: double.infinity,
                                  image: widget.dtsource?['image'].isNotEmpty
                                      ? imgProvider
                                      : const NetworkImage(''),
                                  fit: BoxFit.cover,
                                  blur: 0.9,
                                  placeholder: const AssetImage(
                                    'images/pngagro.png',
                                  ),
                                  thumbnail:
                                      const AssetImage('images/pngagro.png'),
                                  height: double.infinity,
                                ),
                              ),
                            ),
                            Positioned(
                                bottom: 0,
                                right: 10,
                                child: TextButton.icon(
                                    style: ElevatedButton.styleFrom(
                                        primary: Colors.black.withOpacity(0.5)),
                                    onPressed: () {
                                      ImageViewer.showImageSlider(
                                          images: List<String>.from(
                                              widget.dtsource?['image']),
                                          startingPosition: 0);
                                    },
                                    icon: const Icon(
                                      Ionicons.image,
                                      color: Colors.white,
                                    ),
                                    label: Text(
                                      '${widget.dtsource?['image'].length}',
                                      style: const TextStyle(
                                          fontFamily: 'Ubuntu',
                                          color: Colors.white),
                                    )))
                          ],
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Column(
                            children: [
                              ListTile(
                                trailing: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Icon(
                                      Ionicons.eye,
                                    ),
                                    Text(
                                      kinmelController
                                              .kmodel[widget.dtsource?['index']]
                                              .views
                                              ?.length
                                              .toString() ??
                                          '',
                                      style: const TextStyle(
                                        fontFamily: 'ubuntu',
                                        fontWeight: FontWeight.w500,
                                        color: Colors.blueGrey,
                                      ),
                                    )
                                  ],
                                ),
                                leading: storeIt(widget.dtsource?['by'])[
                                            'photoUrl'] ==
                                        ''
                                    ? const CircleAvatar(
                                        maxRadius: 18,
                                        backgroundImage:
                                            AssetImage('images/avatar.png'),
                                      )
                                    : CircleAvatar(
                                        maxRadius: 18,
                                        backgroundImage: NetworkImage(
                                            storeIt(widget.dtsource?['by'])[
                                                'photoUrl']),
                                      ),
                                title: Text(
                                  storeIt(widget.dtsource?['by'])['fname']
                                          .toString() +
                                      ' ' +
                                      storeIt(widget.dtsource?['by'])['lname']
                                          .toString(),
                                  style: const TextStyle(
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600),
                                ),
                                subtitle: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    TextButton.icon(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.green.shade400,
                                          splashFactory:
                                              InkRipple.splashFactory,
                                          shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(50)))),
                                      onPressed: () {
                                        launch(
                                            'tel:+977${widget.dtsource?['phone']}');
                                      },
                                      icon: const Icon(
                                        Ionicons.call,
                                        color: Colors.white,
                                        size: 14,
                                      ),
                                      label: const Text(
                                        'CALL',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: 'ubuntu',
                                            color: Colors.white),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    TextButton.icon(
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.green.shade400,
                                          splashFactory:
                                              InkRipple.splashFactory,
                                          shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(50)))),
                                      onPressed: () {
                                        launch(
                                            'sms:+977${widget.dtsource?['phone']}');
                                      },
                                      icon: const Icon(
                                        Ionicons.chatbubble,
                                        color: Colors.white,
                                        size: 14,
                                      ),
                                      label: const Text(
                                        'MESSAGE',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: 'ubuntu',
                                            color: Colors.white),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Obx(
                            () => LimitedBox(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  isCurrentUserLikeIt(kinmelController
                                              .kmodel[widget.dtsource?['index']]
                                              .votes) ==
                                          false
                                      ? TextButton.icon(
                                          style: ElevatedButton.styleFrom(
                                            splashFactory:
                                                InkRipple.splashFactory,
                                            primary:
                                                Colors.white.withOpacity(0.2),
                                            shadowColor: Colors.white,
                                            elevation: 0,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(50),
                                              ),
                                            ),
                                          ),
                                          icon: const Icon(
                                            Ionicons.thumbs_up,
                                            size: 14,
                                          ),
                                          onPressed: () {
                                            if (isLoginUser()) {
                                              likeController.addLikeToKinmel(
                                                  widget.dtsource?['pid'] ??
                                                      '');
                                            } else {
                                              Get.defaultDialog(
                                                title: 'सूचना',
                                                middleText:
                                                    'कृपया लगइन गर्नुहोस',
                                                textCancel: 'रद्दगर्नुहोस',
                                                textConfirm: 'लगइनगर्नुहोस',
                                                confirmTextColor: Colors.white,
                                                onConfirm: () {
                                                  Get.back();
                                                  Get.to(const AccountPage());
                                                },
                                              );
                                            }
                                          },
                                          label: Text(
                                              widget.dtsource?['votes'].length
                                                      .toString() ??
                                                  '',
                                              style: const TextStyle(
                                                  fontFamily: 'ubuntu',
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal)),
                                        )
                                      : TextButton.icon(
                                          style: ElevatedButton.styleFrom(
                                            splashFactory:
                                                InkRipple.splashFactory,
                                            primary:
                                                Colors.orange.withOpacity(0.2),
                                            shadowColor: Colors.white,
                                            elevation: 0,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(50),
                                              ),
                                            ),
                                          ),
                                          icon: const Icon(
                                            Ionicons.thumbs_up,
                                            color: Colors.orange,
                                            size: 14,
                                          ),
                                          onPressed: () {
                                            if (isLoginUser()) {
                                              likeController
                                                  .removeLikeFromKinmel(
                                                      widget.dtsource?['pid'] ??
                                                          '');
                                            } else {
                                              Get.defaultDialog(
                                                title: 'सूचना',
                                                middleText:
                                                    'कृपया लगइन गर्नुहोस',
                                                textCancel: 'रद्दगर्नुहोस',
                                                textConfirm: 'लगइनगर्नुहोस',
                                                confirmTextColor: Colors.white,
                                                onConfirm: () {
                                                  Get.back();
                                                  Get.to(AccountPage());
                                                },
                                              );
                                            }
                                          },
                                          label: Text(
                                              kinmelController
                                                  .kmodel[
                                                      widget.dtsource?['index']]
                                                  .votes!
                                                  .length
                                                  .toString(),
                                              style: const TextStyle(
                                                  fontFamily: 'ubuntu',
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.orange,
                                                  fontStyle: FontStyle.normal)),
                                        ),
                                  TextButton.icon(
                                    style: ElevatedButton.styleFrom(
                                      splashFactory: InkRipple.splashFactory,
                                      primary: Colors.white.withOpacity(0.2),
                                      shadowColor: Colors.white,
                                      elevation: 0,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(50),
                                        ),
                                      ),
                                    ),
                                    icon: const Icon(
                                      Ionicons.chatbubble,
                                      size: 14,
                                    ),
                                    onPressed: () {
                                      fnode.requestFocus();
                                    },
                                    label: Text(
                                        '${snapshot.data?.docs.length} प्रतिक्रियाहरु',
                                        style: const TextStyle(
                                            fontFamily: 'ubuntu',
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            child: ExpansionTile(
                              initiallyExpanded: true,
                              title: Text(
                                widget.dtsource?['pName'],
                                style: const TextStyle(
                                  fontFamily: 'poppins',
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  fontSize: 20,
                                ),
                              ),
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, right: 10),
                                  child: ListBody(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'बिक्रेता:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            storeIt(widget.dtsource?['by'])[
                                                    'fname'] +
                                                ' ' +
                                                storeIt(widget.dtsource?['by'])[
                                                    'lname'],
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'ठेगाना:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            storeIt(widget.dtsource?['by'])[
                                                'address'],
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'प्रकार:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            widget.dtsource?['pType'],
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'मूल्य:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            'रू. ' +
                                                widget.dtsource?['price'] +
                                                '/' +
                                                widget.dtsource?['perItemTag'],
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'परिमाण:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            widget.dtsource?['quantity'] +
                                                ' ' +
                                                widget.dtsource?['perItemTag'],
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            'गुणस्तर:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          ExpandableText(
                                            widget.dtsource?['quality'],
                                            onUrlTap: (url) {
                                              launch('url');
                                            },
                                            collapseText: 'लुकाउनु होस्',
                                            expandText: 'पुरा पद्नुहोस',
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            'बिवरण:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          ExpandableText(
                                            widget.dtsource?['description'],
                                            onUrlTap: (url) {
                                              launch('url');
                                            },
                                            collapseText: 'लुकाउनु होस्',
                                            expandText: 'पुरा पद्नुहोस',
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'राखिएको मिति:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            date1
                                                .format(pdDateNepali)
                                                .toString(),
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(
                                            'कायम रहेने मिति:',
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            date2
                                                .format(pdValidDateNepali)
                                                .toString(),
                                            style: const TextStyle(
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          const Text(
                            'प्रतिक्रियाहरु',
                            style: TextStyle(
                                fontFamily: 'poppins',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                          Container(
                            padding: const EdgeInsets.all(8.0),
                            width: sizeProvider(context).width,
                            height: 80,
                            child: Stack(
                              children: [
                                TextFormField(
                                  focusNode: fnode,
                                  key: _key,
                                  controller: commentTexts,
                                  enableSuggestions: true,
                                  validator: (val) {
                                    if (val?.length == 0) {
                                      return '';
                                    }
                                  },
                                  decoration: const InputDecoration(
                                    errorBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.red)),
                                    labelText: 'REPLY',
                                    labelStyle: TextStyle(
                                        fontFamily: 'poppins',
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                                Positioned(
                                  right: 0,
                                  top: 0,
                                  bottom: 0,
                                  child: Material(
                                    type: MaterialType.transparency,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                    child: IconButton(
                                      color: Colors.green.shade400,
                                      splashColor: Colors.greenAccent,
                                      icon: const Icon(Ionicons.send),
                                      onPressed: () {
                                        if (_key.currentState?.isValid ??
                                            false) {
                                          if (isLoginUser()) {
                                            kinmelController
                                                .addCommentOnKinmelPost(
                                                    widget.dtsource?['pid'],
                                                    commentTexts.text);
                                            fnode.unfocus();
                                            commentTexts.text = "";
                                          } else {
                                            Get.defaultDialog(
                                              title: 'सूचना',
                                              middleText: 'कृपया लगइन गर्नुहोस',
                                              textCancel: 'रद्दगर्नुहोस',
                                              textConfirm: 'लगइनगर्नुहोस',
                                              confirmTextColor: Colors.white,
                                              onConfirm: () {
                                                Get.back();
                                                Get.to(const AccountPage());
                                              },
                                            );
                                          }
                                        }
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    //This Is the List Area for Comments
                    SliverToBoxAdapter(
                      child: ListView.builder(
                          shrinkWrap: true,
                          controller: scrollController,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: snapshot.data?.docs.length ?? 0,
                          itemBuilder: (context, index) {
                            List votersData = [];
                            snapshot.data?.docs[index]
                                .get('votes')
                                .forEach((element) {
                              votersData = [element];
                            });

                            int votesCount = votersData.length;
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: LimitedBox(
                                child: Container(
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(15),
                                      ),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black,
                                            spreadRadius: 0.2),
                                        BoxShadow(
                                            color: Colors.white,
                                            spreadRadius: 0.5)
                                      ]),
                                  child: Material(
                                    type: MaterialType.transparency,
                                    shadowColor: Colors.black,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    clipBehavior: Clip.antiAlias,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  storeIt(snapshot.data?.docs[
                                                                      index]
                                                                  ['by'] ??
                                                              '')['photoUrl'] ==
                                                          ''
                                                      ? const CircleAvatar(
                                                          radius: 14,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  'images/KrishiGuide/pasupalan/cow.jpg'),
                                                        )
                                                      : CircleAvatar(
                                                          radius: 14,
                                                          backgroundImage: NetworkImage(
                                                              storeIt(snapshot
                                                                          .data
                                                                          ?.docs[
                                                                      index]['by'] ??
                                                                  '')['photoUrl']),
                                                        ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 2),
                                                    child: Text(
                                                        storeIt(snapshot.data?.docs[index][
                                                                        'by'] ??
                                                                    '')['fname']
                                                                .toString() +
                                                            ' ' +
                                                            storeIt(snapshot.data
                                                                            ?.docs[index]
                                                                        [
                                                                        'by'] ??
                                                                    '')['lname']
                                                                .toString(),
                                                        style: const TextStyle(
                                                            fontFamily:
                                                                'poppins',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  )
                                                ],
                                              ),
                                              Text(
                                                NepaliMoment.fromAD(
                                                        (snapshot.data?.docs[
                                                                        index]
                                                                    ['date'] ??
                                                                Timestamp.now())
                                                            .toDate())
                                                    .toString(),
                                                style: const TextStyle(
                                                  color: Colors.blueGrey,
                                                  fontFamily: 'ubuntu',
                                                  fontSize: 10,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left: sizeProvider(context)
                                                        .width -
                                                    (sizeProvider(context)
                                                            .width *
                                                        0.8),
                                                right: sizeProvider(context)
                                                        .width -
                                                    (sizeProvider(context)
                                                            .width *
                                                        0.99),
                                              ),
                                              child: ExpandableText(
                                                snapshot.data?.docs[index]
                                                        ['body'] ??
                                                    '',
                                                maxLines: 1,
                                                linkColor: Colors.blueAccent,
                                                collapseOnTextTap: true,
                                                expandOnTextTap: true,
                                                expandText: 'पुरा हेर्नुहोस',
                                                collapseText: 'लुकाउनु होस्',
                                                animation: true,
                                                style: const TextStyle(
                                                    color: Colors.black54,
                                                    fontFamily: 'poppins',
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            )),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              decoration: const BoxDecoration(),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: FittedBox(
                                                  child: Row(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        child: isCurrentUserLikeIt(snapshot
                                                                        .data
                                                                        ?.docs[
                                                                            index]
                                                                            [
                                                                            'votes']
                                                                        .toList() ??
                                                                    []) ==
                                                                false
                                                            ? TextButton.icon(
                                                                onPressed: () {
                                                                  if (isLoginUser()) {
                                                                    likeController.addLikeToCommentKinmel(
                                                                        widget.dtsource?[
                                                                            'pid'],
                                                                        snapshot.data?.docs[index].id.toString() ??
                                                                            '');
                                                                  } else {
                                                                    Get.defaultDialog(
                                                                      title:
                                                                          'सूचना',
                                                                      middleText:
                                                                          'कृपया लगइन गर्नुहोस',
                                                                      textCancel:
                                                                          'रद्दगर्नुहोस',
                                                                      textConfirm:
                                                                          'लगइनगर्नुहोस',
                                                                      confirmTextColor:
                                                                          Colors
                                                                              .white,
                                                                      onConfirm:
                                                                          () {
                                                                        Get.back();
                                                                        Get.to(
                                                                            const AccountPage());
                                                                      },
                                                                    );
                                                                  }
                                                                },
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  splashFactory:
                                                                      InkRipple
                                                                          .splashFactory,
                                                                  primary: Colors
                                                                      .white,
                                                                  shadowColor:
                                                                      Colors
                                                                          .white,
                                                                  elevation: 0,
                                                                  shape:
                                                                      const RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .all(
                                                                      Radius.circular(
                                                                          50),
                                                                    ),
                                                                  ),
                                                                ),
                                                                icon:
                                                                    const Icon(
                                                                  Ionicons
                                                                      .thumbs_up,
                                                                  size: 15,
                                                                ),
                                                                label: Text(
                                                                  snapshot
                                                                          .data
                                                                          ?.docs[
                                                                              index]
                                                                              [
                                                                              'votes']
                                                                          .length
                                                                          .toString() ??
                                                                      '0',
                                                                  style: const TextStyle(
                                                                      fontFamily:
                                                                          'ubuntu'),
                                                                ),
                                                              )
                                                            : TextButton.icon(
                                                                onPressed: () {
                                                                  if (isLoginUser()) {
                                                                    likeController.removeLikeFromCommentKinmel(
                                                                        widget.dtsource?[
                                                                            'pid'],
                                                                        snapshot.data?.docs[index].id.toString() ??
                                                                            '');
                                                                  } else {
                                                                    Get.defaultDialog(
                                                                      title:
                                                                          'सूचना',
                                                                      middleText:
                                                                          'कृपया लगइन गर्नुहोस',
                                                                      textCancel:
                                                                          'रद्दगर्नुहोस',
                                                                      textConfirm:
                                                                          'लगइनगर्नुहोस',
                                                                      confirmTextColor:
                                                                          Colors
                                                                              .white,
                                                                      onConfirm:
                                                                          () {
                                                                        Get.back();
                                                                        Get.to(
                                                                            const AccountPage());
                                                                      },
                                                                    );
                                                                  }
                                                                },
                                                                style: ElevatedButton
                                                                    .styleFrom(
                                                                  splashFactory:
                                                                      InkRipple
                                                                          .splashFactory,
                                                                  primary: Colors
                                                                      .orange
                                                                      .withOpacity(
                                                                          0.2),
                                                                  shadowColor:
                                                                      Colors
                                                                          .white,
                                                                  elevation: 0,
                                                                  shape:
                                                                      const RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .all(
                                                                      Radius.circular(
                                                                          50),
                                                                    ),
                                                                  ),
                                                                ),
                                                                icon:
                                                                    const Icon(
                                                                  Ionicons
                                                                      .thumbs_up,
                                                                  color: Colors
                                                                      .orange,
                                                                  size: 15,
                                                                ),
                                                                label: Text(
                                                                    snapshot
                                                                            .data
                                                                            ?.docs[index][
                                                                                'votes']
                                                                            .length
                                                                            .toString() ??
                                                                        '0',
                                                                    style: const TextStyle(
                                                                        fontFamily:
                                                                            'ubuntu',
                                                                        color: Colors
                                                                            .orange)),
                                                              ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: [
                                                  DropdownButtonHideUnderline(
                                                    child:
                                                        DropdownButton<dynamic>(
                                                      isExpanded: false,
                                                      focusColor:
                                                          Colors.transparent,
                                                      elevation: 0,
                                                      borderRadius:
                                                          const BorderRadius
                                                                  .all(
                                                              Radius.circular(
                                                                  10)),
                                                      autofocus: false,
                                                      icon: const Icon(
                                                        FontAwesomeIcons
                                                            .ellipsis,
                                                        color: Colors.black54,
                                                      ),
                                                      items: isCurrentUser(
                                                              snapshot.data
                                                                      ?.docs[
                                                                  index]['by'])
                                                          ? commnetModify
                                                              .map((e) {
                                                              return DropdownMenuItem(
                                                                  value: e,
                                                                  child:
                                                                      Text(e));
                                                            }).toList()
                                                          : commnetReport
                                                              .map((e) {
                                                              return DropdownMenuItem(
                                                                  value: e,
                                                                  child:
                                                                      Text(e));
                                                            }).toList(),
                                                      onChanged: (val) async {
                                                        if (val == "copy") {
                                                          getAction(
                                                              GetActionType
                                                                  .copy,
                                                              body: snapshot.data
                                                                              ?.docs[
                                                                          index]
                                                                      [
                                                                      'body'] ??
                                                                  "");
                                                        } else if (val ==
                                                            "edit") {
                                                          getAction(
                                                            GetActionType.edit,
                                                            body: snapshot.data
                                                                            ?.docs[
                                                                        index]
                                                                    ['body'] ??
                                                                "",
                                                          );
                                                        } else if (val ==
                                                            "delete") {
                                                          getAction(
                                                              GetActionType
                                                                  .delete,
                                                              pid: widget
                                                                      .dtsource![
                                                                  "pid"],
                                                              cid: snapshot
                                                                  .data!
                                                                  .docs[index]
                                                                  .id);
                                                        } else if (val ==
                                                            "report") {
                                                          getAction(
                                                              GetActionType
                                                                  .report);
                                                        }
                                                      },
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                  ]),
            );
          }),
    );
  }

  Future<void> _captureSocialPng() {
    List<String> imagePaths = [];
    final RenderBox box = context.findRenderObject() as RenderBox;
    return Future.delayed(const Duration(milliseconds: 20), () async {
      RenderRepaintBoundary? boundary = previewContainer.currentContext!
          .findRenderObject() as RenderRepaintBoundary?;
      ui.Image image = await boundary!.toImage();
      final directory = (await getApplicationDocumentsDirectory()).path;
      ByteData? byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData!.buffer.asUint8List();
      File imgFile = File('$directory/screenshot.png');
      imagePaths.add(imgFile.path);
      imgFile.writeAsBytes(pngBytes).then((value) async {
        await Share.shareFiles(imagePaths,
            subject: 'Kinmel',
            text: 'Check this Out!->#AgroMate',
            sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
      }).catchError((onError) {
        debugPrint(onError);
      });
    });
  }

  dynamic getAction(GetActionType getActionType,
      {String body = "",
      String by = "",
      String cid = "",
      String pid = ""}) async {
    switch (getActionType) {
      case GetActionType.copy:
        return await Clipboard.setData(ClipboardData(text: body))
            .then((value) =>
                ScaffoldMessenger.of(context).removeCurrentSnackBar())
            .whenComplete(() => ScaffoldMessenger.of(context).showSnackBar(
                  (getSnackbar('Text Copied to Clipboard')),
                ));
      case GetActionType.edit:
        break;
      case GetActionType.delete:
        return showDialog(
            context: context,
            builder: (_) {
              return getDialog(deleteComment, pid: pid, cid: cid);
            });
      case GetActionType.report:
        return showDialog(
            context: context,
            builder: (_) {
              return getReportDialog(context);
            });
    }
  }

  Future<void> deleteComment(pid, cid) async {
    await firebaseFirestore
        .collection('Kinmel')
        .doc(pid)
        .collection('Comments')
        .doc(cid)
        .delete()
        .then((value) => ScaffoldMessenger.of(context).removeCurrentSnackBar())
        .whenComplete(() {
      ScaffoldMessenger.of(context)
          .showSnackBar(getSnackbar('Your Comment has been deleted', ms: 600));
      Get.back();
    });
  }
}
