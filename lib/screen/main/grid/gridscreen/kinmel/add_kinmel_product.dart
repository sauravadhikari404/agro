import 'dart:async';
import 'dart:io';

import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/loading_dialog.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart' as datePicker;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class AddItemToKinmel extends StatefulWidget {
  const AddItemToKinmel({Key? key}) : super(key: key);

  @override
  _AddItemToKinmelState createState() => _AddItemToKinmelState();
}

class _AddItemToKinmelState extends State<AddItemToKinmel> {
  final formKey = GlobalKey<FormState>();
  List<File> _image = [];
  var listController = ScrollController();
  bool uploading = false;
  double val = 0;
  CollectionReference? imgRef;
  firebase_storage.Reference? ref;
  var _valueController;
  var _pertiController;

  TextEditingController bname = TextEditingController(text: '');
  TextEditingController pname = TextEditingController(text: '');
  TextEditingController price = TextEditingController(text: '');
  TextEditingController quantity = TextEditingController(text: '');
  TextEditingController quality = TextEditingController(text: '');
  TextEditingController validDate = TextEditingController(text: '');
  String validDates = '';
  DateTime? valDates;
  TextEditingController address = TextEditingController(text: '');
  TextEditingController conact = TextEditingController(text: '');
  TextEditingController itemType = TextEditingController(text: '');
  TextEditingController itemPer = TextEditingController(text: '');
  TextEditingController description = TextEditingController();

  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CusAppBar(
          appBarTitle: 'सुचिकृत गर्नुहोस',
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                    key: formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: bname,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'बिक्रेताको नाम लेख्नुहोस';
                              }
                            },
                            decoration: const InputDecoration(
                                labelText: 'बिक्रेताको नाम:',
                                labelStyle: TextStyle(
                                  fontFamily: 'poppins',
                                ),
                                helperText:
                                    'एदी तपाई नै बिक्रेता हो भने नाम लेखन पर्दैन '),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: pname,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'सामानको नाम लेख्नुहोस';
                              }
                            },
                            decoration: InputDecoration(
                              labelText: 'सामानको नाम:',
                              labelStyle: const TextStyle(
                                fontFamily: 'poppins',
                              ),
                              suffixIcon: LineIcon(
                                LineIcons.asterisk,
                                size: 10,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 1.5,
                                height: 60,
                                child: TextFormField(
                                  expands: false,
                                  controller: price,
                                  validator: (val) {
                                    if (val?.isEmpty ?? false) {
                                      return 'सामानको मूल्य लेख्नुहोस';
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                    labelText: 'मूल्य:',
                                    labelStyle: TextStyle(
                                      fontFamily: 'poppins',
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: sizeProvider(context).width / 4,
                                height: 70,
                                child: DropdownButtonFormField<String>(
                                  decoration: const InputDecoration(
                                      labelText: 'प्रति:',
                                      labelStyle: TextStyle(
                                        fontFamily: 'Poppins',
                                      )),
                                  validator: (val) {
                                    if (val?.isEmpty ?? false) {
                                      return 'सामानको पमूल्य प्रति छानुहोस';
                                    }
                                  },
                                  value: itemPer.text == ''
                                      ? _pertiController
                                      : itemPer.text,
                                  items: [
                                    'किलो',
                                    'मुठा',
                                    'मन',
                                    'गोटा',
                                  ]
                                      .map((label) => DropdownMenuItem(
                                            child: Text(label.toString(),
                                                style: TextStyle(
                                                  fontFamily: 'poppins',
                                                  color: itemPer.text ==
                                                          label.toString()
                                                      ? Colors.green
                                                      : Colors.black,
                                                )),
                                            value: label,
                                          ))
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      itemPer.text = value ?? '';
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          DropdownButtonFormField<String>(
                            decoration: const InputDecoration(
                                labelText: 'प्रकार छान्नुहोस्:',
                                labelStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                )),
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'सामानको प्रकार छानुहोस';
                              }
                            },
                            value: itemType.text == ''
                                ? _valueController
                                : itemType.text,
                            items: [
                              'बिरुवा',
                              'पशु',
                              'कच्चापदार्थ',
                              'दाना/चोक्कर',
                              'मेसिनरी सामान',
                              'कृषि उत्पादन',
                              'मल',
                              'औषधि',
                              'अन्य'
                            ]
                                .map((label) => DropdownMenuItem(
                                      child: Text(label.toString(),
                                          style: TextStyle(
                                            fontFamily: 'poppins',
                                            color: itemType.text ==
                                                    label.toString()
                                                ? Colors.green
                                                : Colors.black,
                                          )),
                                      value: label,
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                itemType.text = value ?? '';
                              });
                            },
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: quantity,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'सामानको परिमाण लेख्नुहोस';
                              }
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: 'परिमाण:',
                              labelStyle: const TextStyle(
                                fontFamily: 'poppins',
                              ),
                              suffixIcon: LineIcon(
                                LineIcons.asterisk,
                                size: 10,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: quality,
                            decoration: const InputDecoration(
                                labelText: 'गुणस्तर:',
                                labelStyle: TextStyle(
                                  fontFamily: 'poppins',
                                ),
                                hintText: 'राम्रो,पर्योग गरि सकिएको अत्त्याधि'),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: description,
                            maxLines: 3,
                            maxLength: 250,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'सामानको विवरण लेख्नुहोस';
                              } else if (val!.length < 10) {
                                return 'कम्तिमा १० अक्षर लामो सब्द लेख्नुहोस';
                              }
                            },
                            decoration: InputDecoration(
                              hintMaxLines: 250,
                              labelText: 'विवरण:',
                              labelStyle: const TextStyle(
                                fontFamily: 'poppins',
                              ),
                              helperText: 'सामानको विवरण लेख्नुहोस्',
                              suffixIcon: LineIcon(
                                LineIcons.asterisk,
                                size: 10,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Material(
                            child: InkWell(
                              onTap: () async {
                                NepaliDateTime? _selectedDateTime =
                                    await datePicker.showAdaptiveDatePicker(
                                  context: context,
                                  initialDate: validDates == ''
                                      ? NepaliDateTime.now()
                                      : NepaliDateTime.parse(validDates),
                                  firstDate: NepaliDateTime(2000),
                                  lastDate: NepaliDateTime(2090),
                                  language: Language.nepali,

                                  initialDatePickerMode: DatePickerMode
                                      .day, // for platform except iOS
                                );
                                var date1 = NepaliDateFormat.yMMMEd(
                                    datePicker.Language.nepali);

                                if (_selectedDateTime != null) {
                                  validDates = _selectedDateTime.toString();
                                  valDates = _selectedDateTime.toDateTime();
                                  validDate.text =
                                      date1.format(_selectedDateTime);
                                }
                              },
                              child: TextFormField(
                                readOnly: true,
                                enabled: false,
                                controller: validDate,
                                validator: (val) {
                                  if (val?.isEmpty ?? false) {
                                    return ' कायम रहने मिति ';
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: 'कायम रहने मिति',
                                  hintText: 'कायम रहने मिति छान्नुहोस्',
                                  labelStyle: const TextStyle(
                                      fontFamily: 'poppins',
                                      color: Colors.black54),
                                  suffixIcon: LineIcon(
                                    LineIcons.asterisk,
                                    size: 10,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: address,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'बिक्रेताको ठेगाना लेख्नुहोस';
                              }
                            },
                            decoration: InputDecoration(
                              labelText: 'ठेगाना:',
                              labelStyle: const TextStyle(
                                fontFamily: 'poppins',
                              ),
                              suffixIcon: LineIcon(
                                LineIcons.asterisk,
                                size: 10,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: conact,
                            validator: (val) {
                              if (val?.isEmpty ?? false) {
                                return 'बिक्रेताको सम्पर्क नम्बर लेख्नुहोस';
                              } else if (val!.length < 9) {
                                return 'फोन नं मिलेन १० अंकको फोन नुम्बर लेख्नुहोस';
                              }
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: 'सम्पर्क नं.:',
                              labelStyle: const TextStyle(
                                fontFamily: 'poppins',
                              ),
                              suffixIcon: LineIcon(
                                LineIcons.asterisk,
                                size: 10,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Text(
                            'बस्तुको तस्बिर',
                            style: TextStyle(
                              fontFamily: 'poppins',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: sizeProvider(context).width,
                            height: 120,
                            child: GridView.builder(
                                primary: false,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: _image.length + 1,
                                padding: const EdgeInsets.all(8.0),
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 5),
                                itemBuilder: (_, index) {
                                  return Column(
                                    children: [
                                      index == 0
                                          ? Flexible(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(2.0),
                                                child: Material(
                                                  child: InkWell(
                                                    onTap: () {
                                                      _image.length == 4
                                                          ? uploadFile('meroid')
                                                          : chooseImage();
                                                    },
                                                    child: Container(
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      width: 120,
                                                      height: 70,
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Colors
                                                                .blueGrey),
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(10),
                                                        ),
                                                      ),
                                                      child: const Icon(
                                                          Ionicons.add_sharp),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Flexible(
                                              child: Stack(
                                                fit: StackFit.loose,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            2.0),
                                                    child: Container(
                                                      width: 120,
                                                      height: 70,
                                                      clipBehavior:
                                                          Clip.antiAlias,
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Colors
                                                                .blueGrey),
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(10),
                                                        ),
                                                        image: DecorationImage(
                                                          fit: BoxFit.fill,
                                                          image: FileImage(
                                                              _image[
                                                                  index - 1]),
                                                        ),
                                                      ),
                                                      child: Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Material(
                                                          type:
                                                              MaterialType.card,
                                                          borderRadius:
                                                              const BorderRadius
                                                                  .all(
                                                            Radius.circular(50),
                                                          ),
                                                          child: InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                _image.removeAt(
                                                                    index - 1);
                                                              });
                                                            },
                                                            child: const Icon(
                                                              Ionicons
                                                                  .remove_circle,
                                                              color:
                                                                  Colors.green,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                    ],
                                  );
                                }),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextButton(
                                onPressed: () {},
                                child: const Text('रद्दगर्नुहोस',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'poppins',
                                      fontWeight: FontWeight.bold,
                                    )),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.orange.shade300,
                                  splashFactory: InkRipple.splashFactory,
                                ),
                              ),
                              const Spacer(),
                              TextButton(
                                onPressed: () async {
                                  if (formKey.currentState?.validate() ??
                                      false) {
                                    await addProductToKinmel(
                                        itemType,
                                        pname,
                                        price,
                                        valDates,
                                        quantity,
                                        quality,
                                        description,
                                        address,
                                        conact,
                                        itemPer,
                                        '');
                                    Future.delayed(const Duration(seconds: 3),
                                        () {
                                      clearAllTextField();
                                      Get.back();
                                    });
                                  } else {
                                    Get.snackbar(
                                        'सूचना', 'सबै फिल्डहरु मा लेख्नुहोस्');
                                  }
                                },
                                child: const Text('पेशगर्नुहोस',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'poppins',
                                      fontWeight: FontWeight.bold,
                                    )),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.green.shade300,
                                  splashFactory: InkRipple.splashFactory,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                    )),
              ),
            ),
          ],
        ));
  }

  chooseImage() async {
    try {
      final pickedFile =
          await picker.getImage(source: ImageSource.gallery, imageQuality: 50);
      setState(() {
        _image.add(File(pickedFile!.path));
      });

      if (pickedFile?.path == null) retrieveLostData();
    } catch (e) {
      rethrow;
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _image.add(File(response.file?.path ?? ''));
      });
    } else {
      debugPrint(response.file.toString());
    }
  }

  ///Yesle Kinmel ma Data Rakhne kam garxa Parameter(pType, pName, price, validDate, quantity, quality,description,address,phone,image)
  Future addProductToKinmel(pType, pName, price, validDate, quantity, quality,
      description, address, phone, perItem, image) async {
    var usrData = userController.userModel.value;
    var timeStamp = Timestamp.fromDate(validDate);

    var isImg = _image.isNotEmpty;
    if (isImg) {
      await firebaseFirestore.collection('Kinmel').add({
        'by': '${usrData.uid}',
        'pdate': FieldValue.serverTimestamp(),
        'pType': '${pType.text}',
        'pName': '${pName.text}',
        'price': '${price.text}',
        'validDate': timeStamp,
        'quantity': '${quantity.text}',
        'quality': '${quality.text}',
        'description': '${description.text}',
        'address': '${address.text}',
        'phone': '${phone.text}',
        'image': [],
        'perItemTag': '${perItem.text}',
        'votes': [],
        'views': [],
      }).then((value) {
        uploadFile(value.id);
      });
    } else {
      Get.snackbar('सूचना', 'कृपया बस्तुको तस्बिर Upload गर्नुहोस ');
    }
  }

  Future uploadFile(pid) async {
    try {
      var usrData = userController.userModel.value;
      int i = 1;
      for (var img in _image) {
        setState(() {
          debugPrint(_image.length.toString());
          val = i / _image.length;
        });
        var ref = firebase_storage.FirebaseStorage.instance
            .ref()
            .child('Kinmel/${usrData.uid}/$pid/$i');

        CustomFullScreenLoadingDialog.showDialog();

        await ref.putFile(img).whenComplete(() async {
          await ref.getDownloadURL().then((value) {
            firebaseFirestore.collection('Kinmel').doc(pid).update({
              'image': FieldValue.arrayUnion([value])
            });
          }).then((value) {});
        });
        if (_image.isEmpty) {
          Future.delayed(const Duration(seconds: 3), () {
            CustomFullScreenLoadingDialog.cancelDialog();
            clearAllTextField();
          });
        }
        i++;
      }
    } on Exception {
      if (kDebugMode) throw Exception();
    }
  }

  void clearAllTextField() {
    bname.text = '';
    pname.text = '';
    itemType.text = '';
    price.text = '';
    itemPer.text = '';
    quantity.text = '';
    quality.text = '';
    description.text = '';
    validDate.text = '';
    valDates = DateTime.now();
    validDates = '';
    address.text = '';
    conact.text = '';
    _image.clear();
  }
}
