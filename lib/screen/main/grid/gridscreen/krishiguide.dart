import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/main/grid/gridscreen/tabs/krishitabs.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class KrishiGuideScreen extends StatefulWidget {
  const KrishiGuideScreen({Key? key}) : super(key: key);

  @override
  _KrishiGuideScreenState createState() => _KrishiGuideScreenState();
}

class _KrishiGuideScreenState extends State<KrishiGuideScreen>
    with SingleTickerProviderStateMixin {
  TabController? controller;

  int index = 0;

  var appBarTitle = [
    {'title': 'पशुपन्छिपालन'},
    {'title': 'तरकारी'},
    {'title': 'फलफुल'},
    {'title': 'अन्नबाली'},
    {'title': 'नगदेबाली'},
    {'title': 'जडिबुटी'},
    {'title': 'फुल'},
    {'title': 'घासँ'},
    {'title': 'रुख'},
    {'title': 'मल बनाउने बिधि'},
  ];

  @override
  void initState() {
    controller = TabController(vsync: this, length: 10);
    controller!.addListener(() {
      setState(() {
        if (controller!.indexIsChanging) {
          index = controller!.index;
        }
      });
    });
    super.initState();
    adsController.loadAds();
  }

  final TextStyle _textStyle = const TextStyle(
      color: Colors.white, fontFamily: 'poppins', fontWeight: FontWeight.bold);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: FittedBox(
          child: Text('कृषि गाइड: ${appBarTitle[controller!.index]['title']} ',
              style: const TextStyle(
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.w500,
                  color: Colors.white)),
        ),
        foregroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: Colors.green,
        bottom: TabBar(
          isScrollable: true,
          controller: controller,
          indicatorColor: Colors.yellow,
          tabs: [
            Text(
              'पशुपन्छिपालन',
              style: _textStyle,
            ),
            Text(
              'तरकारी',
              style: _textStyle,
            ),
            Text(
              'फलफुल',
              style: _textStyle,
            ),

            Text(
              'अन्नबाली',
              style: _textStyle,
            ),
            Text(
              'नगदेबाली',
              style: _textStyle,
            ),
            Text(
              'जडिबुटी',
              style: _textStyle,
            ),
            Text(
              'फुल',
              style: _textStyle,
            ),
            Text(
              'घासँ',
              style: _textStyle,
            ),
            Text(
              'रुख',
              style: _textStyle,
            ),
            Text(
              'मल',
              style: _textStyle,
            ),
          ],
        ),
      ),
      body: DefaultTabController(
          initialIndex: 0,
          length: 10,
          child: TabBarView(
            controller: controller,
            children: [
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
              KrishiTabsDataScreen(index: controller!.index.obs),
            ],
          )),
    );
  }
}
