import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/main/grid/gridscreen/pustakalaya_list.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nepali_utils/nepali_utils.dart';

class KrishiPustakalaya extends StatefulWidget {
  const KrishiPustakalaya({Key? key}) : super(key: key);

  @override
  _KrishiPustakalayaState createState() => _KrishiPustakalayaState();
}

class _KrishiPustakalayaState extends State<KrishiPustakalaya> {
  var booklist = [
    {'title': 'ऐन नियम', 'ctitle': 'yenkanun'},
    {'title': 'कार्यबिधि', 'ctitle': 'karyabidhi'},
    {'title': 'निदेशिका', 'ctitle': 'prakashan'},
    {'title': 'प्रकाशन', 'ctitle': 'nidershan'},
    {'title': 'नीति तथा योजना', 'ctitle': 'nititathayojana'},
    {'title': 'नियमावली', 'ctitle': 'niyamawoli'},
    {'title': 'व्यवसायिक योजना', 'ctitle': 'byawosayikyojana'},
    {'title': 'महत्त्वपूर्ण डकुमेन्ट', 'ctitle': 'mahattopurnadocument'},
  ];
  var inNepaliNumber = NepaliNumberFormat(
    inWords: false,
    language: Language.nepali,
  );

  @override
  void initState() {
    super.initState();
    adsController.loadAds();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'कृषि पुस्तकालय',
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: ListView.builder(
            itemCount: booklist.length,
            itemBuilder: (_, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  shadowColor: Colors.green,
                  clipBehavior: Clip.antiAlias,
                  borderOnForeground: true,
                  type: MaterialType.card,
                  child: InkWell(
                    onTap: () => Get.to(() => PustakalayaDataList(
                        title: booklist[index]['title'] ?? '',
                        cTitle: booklist[index]['ctitle'] ?? '')),
                    child: Container(
                      width: sizeProvider(context).width,
                      height: 60,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              Colors.green.shade700,
                              Colors.green.shade400,
                              Colors.white,
                            ]),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(8),
                        ),
                      ),
                      child: ListTile(
                        leading: CircleAvatar(
                          child: Image.asset('images/main/book.png'),
                        ),
                        trailing: StreamBuilder<QuerySnapshot>(
                            stream: firebaseFirestore
                                .collection('KrishiPustakalaya')
                                .doc('${booklist[index]['ctitle']}')
                                .collection('data')
                                .snapshots(),
                            builder: (context, snapshot) {
                              return Text(
                                '${inNepaliNumber.format(snapshot.data?.docs.length)} पुस्तक',
                                style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              );
                            }),
                        selectedTileColor: Colors.white,
                        title: Text(
                          '${booklist[index]['title']}',
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
