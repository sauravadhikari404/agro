import 'package:agromate/config/themes/sub_theme_data_mixin.dart';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:lottie/lottie.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:url_launcher/url_launcher.dart';

class Sansthaharu extends StatefulWidget {
  const Sansthaharu({Key? key}) : super(key: key);

  @override
  _SansthaharuState createState() => _SansthaharuState();
}

class _SansthaharuState extends State<Sansthaharu> {
  final _key = GlobalKey<FormBuilderState>();

  final TextEditingController _districtTextController =
      TextEditingController(text: '');
  final TextEditingController _provinceTextController =
      TextEditingController(text: '');
  var jindex = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(covariant Sansthaharu oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    activate();
    setState(() {});
  }

  var colour = Colors.black;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: StreamBuilder<QuerySnapshot>(
            stream: getSansthaData(
                _provinceTextController.text, _districtTextController.text),
            builder: (context, snapshot) {
              return CustomScrollView(
                shrinkWrap: true,
                clipBehavior: Clip.antiAlias,
                primary: true,
                slivers: [
                  SliverAppBar(
                    elevation: 2,
                    leading: IconButton(
                        icon: Icon(
                          LineIcons.arrowLeft,
                          color: Theme.of(context).textTheme.bodyMedium!.color,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                    title: Text(
                      'संस्थाहरु',
                      style: TextStyle(
                          color: Theme.of(context).textTheme.bodyMedium!.color,
                          fontFamily: 'Poppins'),
                    ),
                    pinned: true,
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(),
                      child: FormBuilder(
                        key: _key,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Wrap(
                                alignment: WrapAlignment.center,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      onChanged: (value) {
                                        print(_key.currentState!
                                            .fields['pradesh']!.value['title']);
                                        print('Answer is' +
                                            (_key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null)
                                                .toString());
                                        if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश १") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value
                                                        .toString() !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 0;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश २") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 1;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "बागमती प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 2;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "गण्डकी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 3;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "लुम्बिनी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 4;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "कर्णाली प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 5;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "सुदूरपश्चिम प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 6;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        }
                                      },
                                      focusColor: Colors.blue,
                                      name: 'pradesh',
                                      items: pradeshMap
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'pradesh']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                        .currentState!
                                                                        .fields[
                                                                            'pradesh']!
                                                                        .value[
                                                                    'title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Theme.of(context)
                                                                .textTheme
                                                                .bodyMedium!
                                                                .color
                                                        : Theme.of(context)
                                                            .textTheme
                                                            .bodyMedium!
                                                            .color,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'प्रदेश',
                                          labelStyle: TextStyle(
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium!
                                                  .color,
                                              fontFamily: 'Poppins',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      enabled: _key.currentState
                                                  ?.fields['pradesh']?.value !=
                                              null
                                          ? true
                                          : false,
                                      onChanged: (value) {
                                        if (_key.currentState!.fields['jilla']!
                                                .value !=
                                            null) {
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        }
                                      },
                                      name: 'jilla',
                                      items: jillaMap[jindex]
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'jilla']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                    .currentState!
                                                                    .fields[
                                                                        'jilla']!
                                                                    .value['title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Colors.black
                                                        : Colors.black,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'जिल्ला',
                                          labelStyle: TextStyle(
                                            fontSize: 14,
                                            color: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .color,
                                            fontFamily: 'Poppins',
                                          )),
                                    ),
                                  ),
                                  IconButton(
                                    hoverColor: Colors.green,
                                    onPressed: () {
                                      if (_key.currentState?.fields['pradesh']
                                                  ?.value !=
                                              null &&
                                          _key.currentState?.fields['jilla']
                                                  ?.value !=
                                              null) {
                                        setState(() {
                                          _provinceTextController.text = _key
                                              .currentState!
                                              .fields['pradesh']!
                                              .value['title']
                                              .toString();
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        });
                                      } else if (_key.currentState!
                                              .fields['pradesh']!.value !=
                                          null) {
                                        _provinceTextController.text = _key
                                            .currentState!
                                            .fields['pradesh']!
                                            .value['title']
                                            .toString();
                                      } else {
                                        Get.snackbar('सूचना',
                                            'पहिले प्रदेश वा जिल्ला छान्नुहोस्');
                                      }
                                    },
                                    icon: Icon(Ionicons.search_circle,
                                        size: 28,
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .color),
                                    splashColor: Colors.green,
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  !snapshot.hasData
                      ? const SliverToBoxAdapter(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : snapshot.data!.docs.isEmpty
                          ? SliverToBoxAdapter(
                              child: Center(
                                child: Lottie.asset(JsonAssets.emptyJson),
                              ),
                            )
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                                (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            width: sizeProvider(context).width,
                                            decoration: BoxDecoration(
                                              color:
                                                  Theme.of(context).cardColor,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(8),
                                              ),
                                              boxShadow: SubThemeData
                                                  .getListItemBoxShadow(
                                                      context),
                                            ),
                                            child: Column(
                                              children: [
                                                ListTile(
                                                  leading: Container(
                                                    width: 50,
                                                    height: 50,
                                                    clipBehavior:
                                                        Clip.antiAlias,
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Colors.transparent,
                                                        border: Border.all(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.2)),
                                                        borderRadius:
                                                            const BorderRadius
                                                                    .all(
                                                                Radius.circular(
                                                                    50))),
                                                    child: CachedNetworkImage(
                                                      cacheKey:
                                                          "${snapshot.data!.docs[index]['image']}",
                                                      imageUrl:
                                                          "${snapshot.data!.docs[index]['image']}",
                                                      errorWidget: (context,
                                                              data, datas) =>
                                                          SizedBox(
                                                              width: 50,
                                                              height: 50,
                                                              child: Lottie.asset(
                                                                  JsonAssets
                                                                      .errorExplamation)),
                                                      imageBuilder:
                                                          (context, img) =>
                                                              ProgressiveImage(
                                                        width: 50,
                                                        height: 50,
                                                        fadeDuration:
                                                            const Duration(
                                                                seconds: 1),
                                                        fit: BoxFit.contain,
                                                        blur: 20,
                                                        placeholder:
                                                            const AssetImage(
                                                                'images/avatar.png'),
                                                        thumbnail: const AssetImage(
                                                            'images/avatar.png'),
                                                        image: img,
                                                      ),
                                                    ),
                                                  ),
                                                  title: Text(
                                                    '${snapshot.data!.docs[index]['name']}',
                                                    style: const TextStyle(
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  subtitle: Text(
                                                    '${snapshot.data!.docs[index]['address']}',
                                                    style: const TextStyle(
                                                      fontFamily: 'Ubuntu',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                  isThreeLine: true,
                                                ),
                                                ButtonBar(
                                                  alignment: MainAxisAlignment
                                                      .spaceBetween,
                                                  children: [
                                                    TextButton.icon(
                                                      onPressed: () async {
                                                        await launch(
                                                            'tel:${snapshot.data!.docs[index]['phone']}');
                                                      },
                                                      icon: const Icon(
                                                          Ionicons.call,
                                                          size: 12,
                                                          color: Colors.white),
                                                      label: const Text(
                                                        'सम्पर्क',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                      style:
                                                          TextButton.styleFrom(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8.5),
                                                              backgroundColor:
                                                                  Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                              shape:
                                                                  const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          15),
                                                                ),
                                                              ),
                                                              primary:
                                                                  Colors.green,
                                                              elevation: 2),
                                                    ),
                                                    TextButton.icon(
                                                      onPressed: () async {
                                                        await launch(
                                                            'http:${snapshot.data!.docs[index]['website']}');
                                                      },
                                                      icon: const Icon(
                                                          Ionicons.link,
                                                          size: 12,
                                                          color: Colors.white),
                                                      label: const Text(
                                                        'वेबसाइट',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                      style:
                                                          TextButton.styleFrom(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8.5),
                                                              backgroundColor:
                                                                  Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                              shape:
                                                                  const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          15),
                                                                ),
                                                              ),
                                                              primary:
                                                                  Colors.green,
                                                              elevation: 2),
                                                    ),
                                                    TextButton.icon(
                                                      onPressed: () async {
                                                        // String googleMapslocationUrl = "https://www.google.com/maps/search/?api=1&query=${.homeLat},${TextStrings.homeLng}";
                                                        String encodedURl = Uri
                                                            .encodeFull(snapshot
                                                                    .data!
                                                                    .docs[index]
                                                                ['map']);

                                                        await launch(
                                                            encodedURl);
                                                      },
                                                      icon: const Icon(
                                                          Ionicons.locate,
                                                          size: 12,
                                                          color: Colors.white),
                                                      label: const Text(
                                                        'म्याप',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                      style:
                                                          TextButton.styleFrom(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8.5),
                                                              backgroundColor:
                                                                  Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                              shape:
                                                                  const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          15),
                                                                ),
                                                              ),
                                                              primary:
                                                                  Colors.green,
                                                              elevation: 2),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                childCount: snapshot.data!.docs.length,
                              ),
                            ),
                ],
              );
            }),
      ),
    );
  }
}
