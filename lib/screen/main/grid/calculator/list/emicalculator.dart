import "package:flutter/material.dart";
import "dart:math";

import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:nepali_utils/nepali_utils.dart';



class EmiCalculator extends StatefulWidget {
  const EmiCalculator({Key? key}) : super(key: key);
  @override
  EmiCalculatorState createState() => EmiCalculatorState();
}

class EmiCalculatorState extends State<EmiCalculator> {

  final List _tenureTypes = [ 'महिना(M)', 'बर्ष(Y)' ];
  String _tenureType = 'बर्ष(Y)';
  String _emiResult = "";

  final TextEditingController _principalAmount = TextEditingController();
  final TextEditingController _interestRate = TextEditingController();
  final TextEditingController _tenure = TextEditingController();

  bool _switchValue = true;


  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
          slivers:[
            SliverToBoxAdapter(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.all(20.0),
                      child: TextField(
                        controller: _principalAmount,
                        decoration: const InputDecoration(
                            labelText: "साँवा रकम",
                        ),
                        keyboardType: TextInputType.number,

                      )
                  ),
                  Container(
                      padding: const EdgeInsets.all(20.0),
                      child: TextField(
                        controller: _interestRate,
                        decoration: const InputDecoration(
                            labelText: "ब्याजदर(%)"
                        ),
                        keyboardType: TextInputType.number,
                      )
                  ),

                  Container(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                              flex: 4,
                              fit: FlexFit.tight,
                              child: TextField(
                                controller: _tenure,
                                decoration: const InputDecoration(
                                    labelText: "अबधि(समय)"
                                ),
                                keyboardType: TextInputType.number,
                              )
                          ),

                          Flexible(
                              flex: 1,
                              child: Column(
                                  children: [
                                    Text(
                                        _tenureType,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold
                                        )
                                    ),
                                    Switch(
                                        value: _switchValue,
                                        onChanged: (bool value) {
                                          print(value);
                                          if( value ) {
                                            _tenureType = _tenureTypes[1];
                                          } else {
                                            _tenureType = _tenureTypes[0];
                                          }

                                          setState(() {
                                            _switchValue = value;
                                          });
                                        }

                                    )
                                  ]
                              )
                          )
                        ],
                      )

                  ),

                  Flexible(
                      fit: FlexFit.loose,
                      child: ElevatedButton(
                          onPressed: _handleCalculation,
                          child: const Text("गणना गर्नुहोस्"),

                      )
                  ),

                  emiResultsWidget(_emiResult)

                ],
              ),
            )
]);
  }

  void _handleCalculation() {

    //  Amortization
    //  A = Payemtn amount per period
    //  P = Initial Printical (loan amount)
    //  r = interest rate
    //  n = total number of payments or periods

    if(_principalAmount.text=="" || _interestRate.text==''){

      GetSnackBar snackBar = const GetSnackBar(
        messageText: Text('कृपया सबै ठाउँ मा डाटा भरि फेरी प्रयास गर्नुहोस',style: TextStyle(
        fontFamily: 'Poppins',
      ),
        ),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.green,
        icon: Icon(Ionicons.alert),
      );

      Get.showSnackbar(snackBar);
    }else {
      double A = 0.0;
      double P = double.parse(_principalAmount.text);
      double r = double.parse(_interestRate.text) / 12 / 100;
      double n = _tenureType == "बर्ष(Y)" ? double.parse(_tenure.text) * 12 : double
          .parse(_tenure.text);
      A = (P * r * pow((1 + r), n) / (pow((1 + r), n) - 1));

      _emiResult = A.toStringAsFixed(2);

      setState(() {

      });
    }
  }


  Widget emiResultsWidget(emiResult) {

    bool canShow = false;
    String _emiResult = emiResult;

    if( _emiResult.isNotEmpty ) {
      canShow = true;
    }
    return
      Container(
          margin: const EdgeInsets.only(top: 40.0),
          child: canShow ? Column(
              children: [
                const Text("तपाईको मासिक EMI रहेको छ",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                    )
                ),
                Text(NepaliNumberFormat(language: Language.nepali).format(_emiResult),
                    style: const TextStyle(
                        fontSize: 50.0,
                        fontWeight: FontWeight.bold
                    ))
              ]
          ) : Container()
      );
  }
}