import 'package:agromate/constant/constant.dart';
import 'package:flutter/material.dart';

class PrangarikMal extends StatefulWidget {
  PrangarikMal({Key? key}) : super(key: key);

  @override
  _PrangarikMalState createState() => _PrangarikMalState();
}

class _PrangarikMalState extends State<PrangarikMal> {
  final _prangarikKey = new GlobalKey<FormState>();
  String baliSelect = "आलु";
  String pranaliSelect = "रोपनी/आना";
  TextEditingController val1 = TextEditingController(text: "");
  TextEditingController val2 = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        height: sizeProvider(context).height,
        child: Form(
          key: _prangarikKey,
          child: Column(
            children: [
              Text(
                'प्रांगारिक मलखाद डोज',
                style: TextStyle(
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.w800,
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                        labelText: 'बाली छान्नुहोस्',
                        labelStyle: TextStyle(
                          fontFamily: 'poppins',
                          fontWeight: FontWeight.w800,
                        ),
                        border: OutlineInputBorder()),
                    onChanged: (val) {
                      setState(() {
                        baliSelect = val.toString();
                      });
                    },
                    value: baliSelect,
                    items: [
                      'आलु',
                      'गोल्भेदा',
                      'प्याज',
                      'सिमि',
                      'बन्दा',
                      'खुर्सानी',
                      'काउली',
                      'केराउ',
                      'भेन्टा',
                      'करेला',
                      'मुला',
                      'अदुवा',
                      'काक्रो',
                      'गाजर',
                    ]
                        .map(
                          (label) => DropdownMenuItem(
                            child: Text(
                              "$label",
                              style: TextStyle(
                                fontFamily: 'poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            value: label,
                          ),
                        )
                        .toList()),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                        labelText: 'प्रणाली छान्नुहोस्',
                        labelStyle: TextStyle(
                          fontFamily: 'poppins',
                          fontWeight: FontWeight.w800,
                        ),
                        border: OutlineInputBorder()),
                    onChanged: (val) {
                      setState(() {
                        pranaliSelect = val.toString();
                      });
                    },
                    value: pranaliSelect,
                    items: [
                      'रोपनी/आना',
                      'बिघा/कठ्ठा',
                    ]
                        .map(
                          (label) => DropdownMenuItem(
                            child: Text(
                              "$label",
                              style: TextStyle(
                                fontFamily: 'poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            value: label,
                          ),
                        )
                        .toList()),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: val1,
                  decoration: InputDecoration(
                    labelText: pranaliSelect.split("/")[0].toString(),
                    labelStyle: TextStyle(
                      fontFamily: 'poppins',
                      fontWeight: FontWeight.w700,
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: val2,
                  decoration: InputDecoration(
                    labelText: pranaliSelect.split("/")[1].toString(),
                    labelStyle: TextStyle(
                      fontFamily: 'poppins',
                      fontWeight: FontWeight.w700,
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  if (_prangarikKey.currentState?.validate() ?? false) {}
                },
                child: Text(
                  'हिसाब गर्नुस्',
                  style: TextStyle(color: Colors.white, fontFamily: 'poppins'),
                ),
                style: ElevatedButton.styleFrom(
                  splashFactory: InkRipple.splashFactory,
                  primary: Colors.green.withOpacity(0.9),
                  onPrimary: Colors.orange.withOpacity(0.4),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
