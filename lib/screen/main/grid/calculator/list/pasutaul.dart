
import 'package:agromate/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PasuTaul extends StatefulWidget {
  PasuTaul({Key? key}) : super(key: key);

  @override
  _PasuTaulState createState() => _PasuTaulState();
}

class _PasuTaulState extends State<PasuTaul> {
  TextEditingController pasuSelect =
      TextEditingController(text: 'पशु छान्नुहोस्');
  TextEditingController girthController =
      TextEditingController(text: '');
  TextEditingController lenghtController =
      TextEditingController(text: '');
  final _pasuKey = new GlobalKey<FormState>();

  String result='';

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Material(
        type: MaterialType.card,
        child: Form(
          key: _pasuKey,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                        labelText: 'पशु',
                        labelStyle: TextStyle(
                          fontFamily: 'poppins',
                          fontWeight: FontWeight.w800,
                        ),
                        border: OutlineInputBorder()),
                    onChanged: (val) {
                      setState(() {
                        pasuSelect.text = val.toString();

                      });

                        if(val!='पशु छान्नुहोस्' && girthController.text!='' && lenghtController.text!=''){
                          setState(() {
                            var get=calcWeight(girthController.text, lenghtController.text, pasuSelect.text);
                            result="${lenghtController.text} से.मि लम्बाई र ${girthController.text} से.मि चौडाई ${pasuSelect.text}को तौल "+get.toStringAsFixed(2)+" किलो हुन्छ ।";
                          });
                        }

                    },
                    value: pasuSelect.text,
                    items: ['पशु छान्नुहोस्', 'गाइ/भैसी', 'खसी/बाख्रा', 'भेडा','सुँगुर','घोडा']
                        .map(
                          (label) => DropdownMenuItem(
                            child: Text(
                              "$label",
                              style: TextStyle(
                                fontFamily: 'poppins',
                                fontWeight: FontWeight.w500,
                                color: label.toString() == "पशु छान्नुहोस्"
                                    ? Colors.blueGrey
                                    : label.toString() == pasuSelect.text
                                        ? Colors.green
                                        : Colors.black,
                              ),
                            ),
                            value: label,
                          ),
                        )
                        .toList()),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: sizeProvider(context).width / 1.8,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        controller: lenghtController,
                        decoration: InputDecoration(
                          labelText: '${pasuSelect.text}को लम्बाई',
                          border: OutlineInputBorder(),
                          suffixText: 'से.मी',
                          labelStyle: TextStyle(
                            fontFamily: 'poppins',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: sizeProvider(context).width / 2.8,
                      height: 100,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'images/calculator/measure/widthmeasure.png'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: sizeProvider(context).width / 1.8,
                      child: TextFormField(
                        controller: girthController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: '${pasuSelect.text}को चौडाई',
                            border: OutlineInputBorder(),
                            suffixText: 'से.मी',
                            labelStyle: TextStyle(
                              fontFamily: 'poppins',
                              fontWeight: FontWeight.w500,
                            )),
                      ),
                    ),
                    Container(
                      width: sizeProvider(context).width / 2.8,
                      height: 100,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'images/calculator/measure/breathmeasure.png'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextButton(
                onPressed: () {
                  if (pasuSelect.text!='पशु छान्नुहोस्' && girthController.text!='' && lenghtController.text!='') {
                    setState(() {
                      var get=calcWeight(girthController.text, lenghtController.text, pasuSelect.text);
                      result="${lenghtController.text} से.मि लम्बाई र ${girthController.text} से.मि चौडाई ${pasuSelect.text}को तौल "+get.toStringAsFixed(2)+" किलो हुन्छ ।";
                      FocusScope.of(context).requestFocus(new FocusNode());
                    });
                  }
                  else if(pasuSelect.text!='पशु छान्नुहोस्' && (girthController.text=='' || lenghtController.text=='')){
                    Get.snackbar('साबधान', '${pasuSelect.text}को लम्बाई/चौडाई लेख्नुहोस');
                    FocusScope.of(context).requestFocus(new FocusNode());
                  }else{
                    Get.snackbar('साबधान', 'पशु छान्नुहोस्');
                    FocusScope.of(context).requestFocus(new FocusNode());
                  }
                },
                child: Text(
                  'नाप्नुहोस',
                  style: TextStyle(color: Colors.white, fontFamily: 'poppins'),
                ),
                style: ElevatedButton.styleFrom(
                  splashFactory: InkRipple.splashFactory,
                  primary: Colors.green.withOpacity(0.9),
                  onPrimary: Colors.orange.withOpacity(0.4),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('$result'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  double calcWeight(String girth,String body,String animal){
    if(animal=='गाइ/भैसी'){
      double lenght= double.parse(body);
      double height= double.parse(girth);
      double calc=(lenght*lenght)*height/330;

      return calc/2.205;
    }
    else if(animal=='खसी/बाख्रा'){
      double lenght= double.parse(body);
      double height= double.parse(girth);
      double calc=(lenght*lenght)*height/300;
      return calc/2.205;
    }
    else if(animal=='भेडा'){
      double lenght= double.parse(body);
      double height= double.parse(girth);

      double calc=(lenght*lenght)*height/330;
      return calc/2.205;
    }
    else if(animal=='सुँगुर'){
      double lenght= double.parse(body);
      double height= double.parse(girth);

      double calc=(lenght*lenght)*height/400;

      return calc/2.205;
    }
    else if(animal=='घोडा'){
      double lenght= double.parse(body);
      double height= double.parse(girth);

      double calc=(lenght*lenght)*height/330;

      return calc/2.205;
    }
    else{
      Get.snackbar('साबधान', 'कृपया पूर्ण प्रयास गर्नुहोला');
      return 0;
    }
  }
}
