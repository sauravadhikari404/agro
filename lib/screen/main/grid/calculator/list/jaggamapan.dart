import 'package:agromate/constant/controller.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class JaggaMapan extends StatefulWidget {
  JaggaMapan({Key? key}) : super(key: key);

  @override
  _JaggaMapanState createState() => _JaggaMapanState();
}

class _JaggaMapanState extends State<JaggaMapan> {
  final _jaggaKey = GlobalKey<FormState>();
  String mapan1 = 'दाम';
  String mapan2 = 'रोपनी';
  TextEditingController mapanField1 = TextEditingController(text: '0');
  TextEditingController mapanField2 = TextEditingController(text: '');
  @override
  void initState() {
    convertIt();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
          key: _jaggaKey,
          child: Material(
            type: MaterialType.card,
            child: SizedBox(
              width: double.infinity,
              child: Column(
                children: [
                  SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: AdWidget(
                        ad: adsController.getBannerAd(),
                      )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButtonFormField<String>(
                      onChanged: (val) {
                        setState(() {
                          mapan1 = val.toString();
                          if (_jaggaKey.currentState?.validate() ?? false) {
                            convertIt();
                          }
                        });
                      },
                      value: mapan1,
                      validator: (val) {},
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(gapPadding: 10)),
                      items: [
                        "दाम",
                        "पैसा",
                        "आना",
                        "रोपनी",
                        "धुर",
                        "कठ्ठा",
                        "बिघा",
                        "वर्ग फिट",
                        "वर्ग यार्ड",
                        "हेक्टर",
                        "एकड",
                        "वर्ग मिटर",
                        "वर्ग किलोमीटर",
                        "वर्ग मिल",
                        "सेन्ट"
                      ]
                          .map((label) => DropdownMenuItem(
                                child: Text(label.toString(),
                                    style: TextStyle(
                                      fontFamily: 'poppins',
                                      color: mapan1 == label.toString()
                                          ? Colors.orange.shade400
                                          : Colors.black,
                                    )),
                                value: label,
                              ))
                          .toList(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: mapanField1,
                      keyboardType: TextInputType.number,
                      onTap: () {
                        if (mapanField1.text == "0") {
                          mapanField1.text = "";
                        }
                      },
                      onChanged: (val) {
                        if (val.isNotEmpty || val.length != 0) {
                          if (_jaggaKey.currentState?.validate() ?? false) {
                            convertIt();
                          }
                        }
                      },
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "कति $mapan1 हो लेख्नुहोस?";
                        }
                      },
                      decoration: InputDecoration(
                          labelText: 'कति $mapan1',
                          border: const OutlineInputBorder(),
                          labelStyle: const TextStyle(
                            fontFamily: 'poppins',
                          )),
                    ),
                  ),
                  const Text(
                    '- बाट -',
                    style: TextStyle(
                      fontFamily: 'poppins',
                      color: Colors.blueGrey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButtonFormField<String>(
                      onChanged: (val) {
                        setState(() {
                          mapan2 = val.toString();
                          if (_jaggaKey.currentState?.validate() ?? false) {
                            convertIt();
                          }
                        });
                      },
                      value: mapan2,
                      validator: (val) {},
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(gapPadding: 10)),
                      items: [
                        "दाम",
                        "पैसा",
                        "आना",
                        "रोपनी",
                        "धुर",
                        "कठ्ठा",
                        "बिघा",
                        "वर्ग फिट",
                        "वर्ग यार्ड",
                        "हेक्टर",
                        "एकड",
                        "वर्ग मिटर",
                        "वर्ग किलोमीटर",
                        "वर्ग मिल",
                        "सेन्ट"
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(label.toString(),
                                  style: TextStyle(
                                    fontFamily: 'poppins',
                                    color: mapan2 == label.toString()
                                        ? Colors.green.shade400
                                        : Colors.black,
                                  )),
                              value: label,
                            ),
                          )
                          .toList(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextFormField(
                      readOnly: true,
                      controller: mapanField2,
                      decoration: InputDecoration(
                          labelText: 'कति $mapan2',
                          border: OutlineInputBorder(),
                          labelStyle: TextStyle(
                            fontFamily: 'poppins',
                          )),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      if (_jaggaKey.currentState?.validate() ?? false) {
                        convertIt();
                        FocusScope.of(context).requestFocus(new FocusNode());
                      }
                    },
                    child: Text(
                      'मापन परिवर्तन गर्नुहोस',
                      style:
                          TextStyle(color: Colors.white, fontFamily: 'poppins'),
                    ),
                    style: ElevatedButton.styleFrom(
                      splashFactory: InkRipple.splashFactory,
                      primary: Colors.green.withOpacity(0.9),
                      onPrimary: Colors.orange.withOpacity(0.4),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void convertIt() {
    if (mapan1 == "आना") {
      if (mapan2 == "आना") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 16;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 4;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0625;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 1.8779;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.0939;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0047;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 342.25;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 38.0278;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0032;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.0079;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 31.7961;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0.0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0.0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.7857;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
      //end
    } // if आना
    else if (mapan1 == "दाम") {
      //दाम ------------------------

      if (mapan2 == "आना") {
        final double value2 = 0.0625;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 0.25;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0039;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 0.1174;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.0059;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0003;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 21.3906;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 2.3767;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0002;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.0005;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 1.9873;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.0491;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else दाम
    else if (mapan1 == "पैसा") {
      // पैसा --------------------------
      if (mapan2 == "आना") {
        final double value2 = 0.25;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 4;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0156;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 0.4695;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.0235;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0012;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 85.5625;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 9.5069;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0008;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.002;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 7.949;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.1964;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else पैसा
    else if (mapan1 == "रोपनी") {
      // रोपनी -----------------------
      if (mapan2 == "आना") {
        final double value2 = 16;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 256;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 64;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 30.0466;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 1.5023;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0751;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 5476;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 608.4444;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0509;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.1257;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 508.737;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0.0005;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0.0002;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 12.5712;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } else if (mapan1 == "धुर") {
      //धुर -----------------------

      if (mapan2 == "आना") {
        final double value2 = 0.5325;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 8.5201;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 2.13;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0333;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.05;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0025;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 182.25;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 20.25;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0017;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.0042;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 16.9316;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.4184;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else धुर
    else if (mapan1 == "कठ्ठा") {
      //कठ्ठा -----------------------
      if (mapan2 == "आना") {
        final double value2 = 10.6501;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 170.4018;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 42.6004;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.6656;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 20;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.05;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 3645;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 405;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0339;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.0837;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 338.6316;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0.003;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0.0001;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 8.3678;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else कठ्ठा
    else if (mapan1 == "बिघा") {
      //बिघा --------------------

      if (mapan2 == "आना") {
        final double value2 = 213.0022;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 3408.0351;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 852.0088;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 13.3126;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 400;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 20;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 72900;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 8100;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.6773;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 1.6736;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 6772.6316;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0.0068;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0.0026;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 167.3554;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //ele बिघा--------
    else if (mapan1 == "वर्ग फिट") {
      // वर्ग फिट -------------------

      if (mapan2 == "आना") {
        final double value2 = 0.0029;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 0.0467;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 0.0117;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0002;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 0.0055;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.0003;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 0.1111;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 0.0929;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.0023;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else वर्ग फिट
    else if (mapan1 == "वर्ग यार्ड") {
      // वर्ग यार्ड ------------------------------
      if (mapan2 == "आना") {
        final double value2 = 0.0263;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 0.4207;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 0.1052;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 0.0016;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 0.0494;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 0.0025;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.0001;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 9;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.0001;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 0.0002;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 0.8361;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 0.0207;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      }
    } //else वर्ग यार्ड
    else if (mapan1 == "एकड") {
      //एकड------------
      if (mapan2 == "आना") {
        final double value2 = 127.2754;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "दाम") {
        final double value2 = 2036.4061;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "पैसा") {
        final double value2 = 509.1015;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "रोपनी") {
        final double value2 = 7.9547;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "धुर") {
        final double value2 = 239.0123;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "कठ्ठा") {
        final double value2 = 11.9506;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "बिघा") {
        final double value2 = 0.5975;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग फिट") {
        final double value2 = 43560;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग यार्ड") {
        final double value2 = 4840;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "हेक्टर") {
        final double value2 = 0.4047;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "एकड") {
        final double value2 = 1;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिटर") {
        final double value2 = 4046.8564;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग किलोमीटर") {
        final double value2 = 0.004;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "वर्ग मिल") {
        final double value2 = 0.0016;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } else if (mapan2 == "सेन्ट") {
        final double value2 = 100;
        var calc = value2 * double.parse(mapanField1.text);
        setState(() {
          mapanField2.text = "$calc $mapan2";
        });
      } // else if एकड
      else if (mapan1 == "एकड") {
        //हेक्टर---------------
        if (mapan2 == "आना") {
          final double value2 = 314.5043;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "दाम") {
          final double value2 = 5032.0692;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "पैसा") {
          final double value2 = 1258.0173;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "रोपनी") {
          final double value2 = 19.6565;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "धुर") {
          final double value2 = 590.6124;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "कठ्ठा") {
          final double value2 = 29.5306;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "बिघा") {
          final double value2 = 1.4765;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग फिट") {
          final double value2 = 107639.1042;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग यार्ड") {
          final double value2 = 11959.9005;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "हेक्टर") {
          final double value2 = 1;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "एकड") {
          final double value2 = 2.4711;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिटर") {
          final double value2 = 10000;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग किलोमीटर") {
          final double value2 = 0.01;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिल") {
          final double value2 = 0.0039;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "सेन्ट") {
          final double value2 = 247.1054;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        }
      } //else if end हेक्टर-------------------------
      else if (mapan1 == "वर्ग मिटर") {
        // वर्ग मिटर----------------------
        if (mapan2 == "आना") {
          final double value2 = 0.0315;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "दाम") {
          final double value2 = 0.5032;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "पैसा") {
          final double value2 = 0.1258;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "रोपनी") {
          final double value2 = 0.002;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "धुर") {
          final double value2 = 0.0591;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "कठ्ठा") {
          final double value2 = 0.003;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "बिघा") {
          final double value2 = 0.0001;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग फिट") {
          final double value2 = 10.7639;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग यार्ड") {
          final double value2 = 1.196;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "हेक्टर") {
          final double value2 = 0.0001;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "एकड") {
          final double value2 = 0.0002;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिटर") {
          final double value2 = 1;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग किलोमीटर") {
          final double value2 = 0;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिल") {
          final double value2 = 0;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "सेन्ट") {
          final double value2 = 0.0247;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        }
      } //else if end वर्ग मिटर
      else if (mapan1 == "वर्ग किलोमीटर") {
        // वर्ग किलोमीटर-----------------------
        if (mapan2 == "आना") {
          final double value2 = 31450.4322;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "दाम") {
          final double value2 = 503206.915;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "पैसा") {
          final double value2 = 125801.7288;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "रोपनी") {
          final double value2 = 1965.652;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "धुर") {
          final double value2 = 59061.2369;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "कठ्ठा") {
          final double value2 = 2953.0618;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "बिघा") {
          final double value2 = 147.6531;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग फिट") {
          final double value2 = 10763910.4167;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग यार्ड") {
          final double value2 = 1195990.0463;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "हेक्टर") {
          final double value2 = 100;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "एकड") {
          final double value2 = 247.1054;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिटर") {
          final double value2 = 1000000;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग किलोमीटर") {
          final double value2 = 1;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिल") {
          final double value2 = 0.3861;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "सेन्ट") {
          final double value2 = 24710.5381;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        }
      } //else if end वर्ग किलोमीटर
      else if (mapan1 == "वर्ग मिल") {
        //वर्ग मिल---------------------------------
        if (mapan2 == "आना") {
          final double value2 = 81456.2454;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "दाम") {
          final double value2 = 1303299.927;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "पैसा") {
          final double value2 = 325824.9817;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "रोपनी") {
          final double value2 = 5091.0153;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "धुर") {
          final double value2 = 152967.9012;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "कठ्ठा") {
          final double value2 = 7648.3951;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "बिघा") {
          final double value2 = 382.4198;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग फिट") {
          final double value2 = 27878400;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग यार्ड") {
          final double value2 = 3097600;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "हेक्टर") {
          final double value2 = 258.9988;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "एकड") {
          final double value2 = 640;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिटर") {
          final double value2 = 2589988.1103;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग किलोमीटर") {
          final double value2 = 2.59;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिल") {
          final double value2 = 1;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "सेन्ट") {
          final double value2 = 64000;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        }
      } //else if end वर्ग मिल
      else if (mapan1 == "सेन्ट") {
        // सेन्ट--------------------
        if (mapan2 == "आना") {
          final double value2 = 1.2728;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "दाम") {
          final double value2 = 20.3641;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "पैसा") {
          final double value2 = 5.091;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "रोपनी") {
          final double value2 = 0.0795;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "धुर") {
          final double value2 = 2.3901;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "कठ्ठा") {
          final double value2 = 0.1195;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "बिघा") {
          final double value2 = 0.006;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग फिट") {
          final double value2 = 435.6;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग यार्ड") {
          final double value2 = 48.4;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "हेक्टर") {
          final double value2 = 0.004;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "एकड") {
          final double value2 = 0.01;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिटर") {
          final double value2 = 40.4686;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग किलोमीटर") {
          final double value2 = 0;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "वर्ग मिल") {
          final double value2 = 0;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        } else if (mapan2 == "सेन्ट") {
          final double value2 = 1;
          var calc = value2 * double.parse(mapanField1.text);
          setState(() {
            mapanField2.text = "$calc $mapan2";
          });
        }
      } //nested condition
    } //main condition ended
  }
}
