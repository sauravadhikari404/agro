import 'package:agromate/constant/constant.dart';
import 'package:flutter/material.dart';

class BiruwaSankhya extends StatefulWidget {
  BiruwaSankhya({Key? key}) : super(key: key);

  @override
  _BiruwaSankhyaState createState() => _BiruwaSankhyaState();
}

class _BiruwaSankhyaState extends State<BiruwaSankhya> {
  final _biruwaKey = new GlobalKey<FormState>();
  TextEditingController biruwKisimSelect =
      new TextEditingController(text: 'तरकारी');
  TextEditingController biruwNameSelect =
      new TextEditingController(text: 'काँक्रो');
  TextEditingController biruwCastSelect =
      new TextEditingController(text: '');
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Material(
        type: MaterialType.card,
        child: Form(
          key: _biruwaKey,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                        labelText: 'बिरुवाको किसिम',
                        labelStyle: TextStyle(
                          fontFamily: 'poppins',
                          fontWeight: FontWeight.w800,
                        ),
                        border: OutlineInputBorder()),
                    onChanged: (val) {
                      setState(() {
                        biruwKisimSelect.text = val.toString();
                      });
                    },
                    value: biruwKisimSelect.text,
                    items: ['तरकारी', 'फलफुल']
                        .map(
                          (label) => DropdownMenuItem(
                            child: Text(
                              "$label",
                              style: TextStyle(
                                fontFamily: 'poppins',
                                fontWeight: FontWeight.w500,
                                color: label.toString() == biruwKisimSelect.text
                                    ? Colors.green
                                    : Colors.black,
                              ),
                            ),
                            value: label,
                          ),
                        )
                        .toList()),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                        labelText: 'बिरुवाको नाम',
                        labelStyle: TextStyle(
                          fontFamily: 'poppins',
                          fontWeight: FontWeight.w800,
                        ),
                        border: OutlineInputBorder()),
                    onChanged: (val) {
                      setState(() {
                        biruwNameSelect.text = val.toString();
                      });
                    },
                    value: biruwNameSelect.text,
                    items: [
                      'काँक्रो',
                      'काउली अगौटे',
                      'काउली पछौटे',
                      'काउली मध्य मौसमी',
                      'कुरिलो',
                      'केराउ',
                      'खुर्सानी',
                      'गाँजर',
                      'टमाटर अग्लो',
                      'टमाटर मध्यम',
                      'टमाटर होचो',
                      'ग्याँठ गोपी',
                      'घिरौला',
                      'चम्सुर',
                      'चुकुंदर',
                      'जिरीको साग',
                      'तरकारी भटमास',
                      'तरबुजा',
                      'पाकचोई साग',
                      'पालुंग',
                      'प्याज',
                      'फर्सी',
                      'बन्दा',
                      'बोडी',
                      'ब्रोकाउली',
                      'भेन्टा',
                      'भेडे खुर्सानी',
                      'मुला',
                      'मेथी',
                      'रामतोरिया',
                      'रायो',
                      'लसुन',
                      'लौका',
                      'बकुल्ला',
                      'सखरखण्ड',
                      'सलगम',
                      'सिमि',
                      'स्विसचार्ड'
                    ]
                        .map(
                          (label) => DropdownMenuItem(
                            child: Text(
                              "$label",
                              style: TextStyle(
                                fontFamily: 'poppins',
                                fontWeight: FontWeight.w500,
                                color: label.toString() == biruwNameSelect.text
                                    ? Colors.green
                                    : Colors.black,
                              ),
                            ),
                            value: label,
                          ),
                        )
                        .toList()),
              ),
              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Container(
              //         width: sizeProvider(context).width / 1.8,
              //         child: TextFormField(
              //           keyboardType: TextInputType.number,
              //           decoration: InputDecoration(
              //             labelText: '${pasuSelect.text}को लम्बाई',
              //             border: OutlineInputBorder(),
              //             suffixText: 'से.मी',
              //             labelStyle: TextStyle(
              //               fontFamily: 'poppins',
              //               fontWeight: FontWeight.w500,
              //             ),
              //           ),
              //         ),
              //       ),
              //       Container(
              //         width: sizeProvider(context).width / 2.8,
              //         height: 100,
              //         decoration: BoxDecoration(
              //           image: DecorationImage(
              //             fit: BoxFit.cover,
              //             image: AssetImage(
              //                 'images/calculator/measure/widthmeasure.png'),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Container(
              //         width: sizeProvider(context).width / 1.8,
              //         child: TextFormField(
              //           keyboardType: TextInputType.number,
              //           decoration: InputDecoration(
              //               labelText: '${pasuSelect.text}को चौडाई',
              //               border: OutlineInputBorder(),
              //               suffixText: 'से.मी',
              //               labelStyle: TextStyle(
              //                 fontFamily: 'poppins',
              //                 fontWeight: FontWeight.w500,
              //               )),
              //         ),
              //       ),
              //       Container(
              //         width: sizeProvider(context).width / 2.8,
              //         height: 100,
              //         decoration: BoxDecoration(
              //           image: DecorationImage(
              //             fit: BoxFit.cover,
              //             image: AssetImage(
              //                 'images/calculator/measure/breathmeasure.png'),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              SizedBox(
                height: 20,
              ),
              TextButton(
                onPressed: () {
                  if (_biruwaKey.currentState?.validate() ?? false) {}
                },
                child: Text(
                  'नाप्नुहोस',
                  style: TextStyle(color: Colors.white, fontFamily: 'poppins'),
                ),
                style: ElevatedButton.styleFrom(
                  splashFactory: InkRipple.splashFactory,
                  primary: Colors.green.withOpacity(0.9),
                  onPrimary: Colors.orange.withOpacity(0.4),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
