import 'package:agromate/constant/constant.dart';
import 'package:agromate/screen/main/grid/calculator/list/mallist/prangarikmal.dart';
import 'package:agromate/screen/main/grid/calculator/list/mallist/rasayanikmal.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class MalKhad extends StatefulWidget {
  MalKhad({Key? key}) : super(key: key);

  @override
  _MalKhadState createState() => _MalKhadState();
}

class _MalKhadState extends State<MalKhad> with SingleTickerProviderStateMixin {
  TabController? controllerTab;
  var controller = new ScrollController();

  @override
  void initState() {
    controllerTab = new TabController(length: 2, vsync: this);
    super.initState();
  }
   var isFalse=true;

  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      controller: controller,
      child: DefaultTabController(
        initialIndex: 0,
        length: 2,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                width: double.infinity,
                height: 50,
                child: TabBar(
                    dragStartBehavior: DragStartBehavior.down,
                    isScrollable: true,
                    unselectedLabelColor: Colors.black,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: Colors.white,
                    indicator: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.green, Colors.greenAccent]),
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.redAccent),
                    controller: controllerTab,
                    physics: AlwaysScrollableScrollPhysics(),
                    tabs: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Tab(
                            child: Text(
                          'रासायनिक मल',
                          style: TextStyle(
                            fontFamily: 'poppins',
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Tab(
                          child: Text(
                            'प्रांगारिक मलखाद',
                            style: TextStyle(
                              fontFamily: 'poppins',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      )
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                controller: controller,
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: sizeProvider(context).height / 1.2,
                      child: TabBarView(controller: controllerTab, children: [
                        RasayanikMal(),
                        PrangarikMal(),
                      ]),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
