import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import '../../../../../controller/adscontroller.dart';

class BaliPatro extends StatefulWidget {
  const BaliPatro({Key? key}) : super(key: key);

  @override
  _BaliPatroState createState() => _BaliPatroState();
}

class _BaliPatroState extends State<BaliPatro> {
  String global = 'तराई(६०-३०० मिटर)';
  String month = 'बैशाख';

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Form(
      child: Material(
        type: MaterialType.card,
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Text(
                'तपाईले हेर्न खोज्नु भएको भूगोल कुन हो?',
                style: TextStyle(
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                  onChanged: (val) {
                    setState(() {
                      global = val.toString();
                    });
                  },
                  value: global,
                  validator: (val) {},
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(gapPadding: 10)),
                  items: [
                    'तराई(६०-३०० मिटर)',
                    'मध्य पहाड(३००-९०० मिटर)',
                    'उच्च पहाड(९००- १८०० मिटर)'
                  ]
                      .map((label) => DropdownMenuItem(
                            child: Text(label.toString(),
                                style: const TextStyle(
                                  fontFamily: 'poppins',
                                  color: Colors.black,
                                )),
                            value: label,
                          ))
                      .toList(),
                ),
              ),
              const Text(
                'कुन महिनाको लागि हेर्न खोज्नु भएको हो?',
                style: TextStyle(
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonFormField<String>(
                  onChanged: (val) {
                    setState(() {
                      month = val.toString();
                    });
                  },
                  value: month,
                  validator: (val) {},
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(gapPadding: 10)),
                  items: [
                    'बैशाख',
                    'जेठ',
                    'असार',
                    'श्रावण',
                    'भदौ',
                    'असोज',
                    'कार्तिक',
                    'मंसिर',
                    'पुष',
                    'माघ',
                    'फागुन',
                    'चैत',
                  ]
                      .map((label) => DropdownMenuItem(
                            child: Text(label.toString(),
                                style: const TextStyle(
                                  fontFamily: 'poppins',
                                  color: Colors.black,
                                )),
                            value: label,
                          ))
                      .toList(),
                ),
              ),
              TextButton(
                onPressed: () {},
                child: const Text(
                  'बलि पात्रो हेर्नुहोस',
                  style: TextStyle(color: Colors.white, fontFamily: 'poppins'),
                ),
                style: ElevatedButton.styleFrom(
                  splashFactory: InkRipple.splashFactory,
                  primary: Colors.green.withOpacity(0.9),
                  onPrimary: Colors.orange.withOpacity(0.4),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}