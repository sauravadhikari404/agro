import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/main/grid/calculator/list/emicalculator.dart';
import 'package:agromate/screen/main/grid/calculator/list/pasutaul.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:lottie/lottie.dart';

import 'list/jaggamapan.dart';

class CalculatorScreen extends StatefulWidget {
  final Map<String, dynamic>? data;
  const CalculatorScreen({Key? key, this.data}) : super(key: key);

  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    adsController.loadAds();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: '${widget.data?['title']}',
      ),
      bottomNavigationBar: SizedBox(
          width: double.infinity,
          height: 50,
          child: AdWidget(
            ad: adsController.getBannerAd(),
          )),
      body: widget.data?['name'] == "jaggamapan"
          ? JaggaMapan()
          : widget.data?['name'] == "balipatro"
              ? Center(
                  child: Lottie.asset(JsonAssets.emptyJson),
                )
              : widget.data?['name'] == "malkhad"
                  ? Center(
                      child: Lottie.asset(JsonAssets.emptyJson),
                    )
                  : widget.data?['name'] == "pasutaul"
                      ? PasuTaul()
                      : widget.data?['name'] == "biruwasankhya"
                          ? Center(
                              child: Lottie.asset(JsonAssets.emptyJson),
                            )
                          : const EmiCalculator(),
    );
  }
}
