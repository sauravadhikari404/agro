import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/main/grid/calculator/calculatorscreens.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get/state_manager.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class CalculatorHome extends GetWidget {
  CalculatorHome({Key? key}) : super(key: key);

  final calculatorList = [
    {
      'name': 'पशु तौल मापन',
      'thumb': 'images/calculator/measurement.png',
      'route': 'pasutaul'
    },
    {
      'name': 'जग्गा मापन',
      'thumb': 'images/calculator/measuring.png',
      'route': 'jaggamapan',
    },
    {
      'name': 'बाली पात्रो',
      'thumb': 'images/calculator/calendar.png',
      'route': 'balipatro'
    },
    {
      'name': 'मलखाद',
      'thumb': 'images/calculator/planting.png',
      'route': 'malkhad'
    },


    {
      'name': 'बिरुवा संख्या',
      'thumb': 'images/calculator/biruwa.png',
      'route': 'biruwasankhya'
    },
    {
      'name': 'EMI',
      'thumb': 'images/calculator/emi.png',
      'route': 'emicalculator'
    },
  ].obs;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text('कृषि क्याल्कुलेटर',style: TextStyle(
          fontFamily: 'Poppins',
        ),
        ),
      ),
      bottomNavigationBar: SizedBox( width:double.infinity,
          height: 50,
          child: AdWidget(ad: adsController.getBannerAd(),)),
      body: SingleChildScrollView(
        child: Obx(
          () => GridView.builder(
              gridDelegate:
                  const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              shrinkWrap: true,
              cacheExtent: 10,
              itemCount: calculatorList.length,
              itemBuilder: (_, index) {
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Material(
                    clipBehavior: Clip.antiAlias,
                    type: MaterialType.card,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    child: InkWell(
                      onTap: () {
                        Get.to(() => CalculatorScreen(
                              data: {
                                'title': '${calculatorList[index]["name"]}',
                                'name': '${calculatorList[index]["route"]}'
                              },
                            ));
                      },
                      customBorder: Border.all(color: Colors.green.shade300),
                      child: Container(
                        height: 200,
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                              color: Colors.white.withOpacity(0.4),
                              spreadRadius: 4,
                              blurRadius: 4),
                          BoxShadow(
                              color: Colors.green.withOpacity(0.1),
                              spreadRadius: 4,
                              blurRadius: 4),
                        ]),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: double.infinity,
                              height: 75,
                              decoration: BoxDecoration(
                                  color: Colors.accents[index].withAlpha(1),
                                  image: DecorationImage(
                                      scale: index == 4 ? 4 : 2,
                                      image: AssetImage(
                                          '${calculatorList[index]['thumb']}'))),
                            ),
                            FittedBox(
                              child: Text(
                                '${calculatorList[index]['name']}',
                                style: const TextStyle(
                                  fontFamily: 'poppins',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }
}
