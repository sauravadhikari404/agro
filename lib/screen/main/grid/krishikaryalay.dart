import 'package:agromate/config/themes/sub_theme_data_mixin.dart';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:lottie/lottie.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:url_launcher/url_launcher.dart';

class KrishiKaryalaya extends StatefulWidget {
  const KrishiKaryalaya({Key? key}) : super(key: key);

  @override
  _KrishiKaryalayaState createState() => _KrishiKaryalayaState();
}

class _KrishiKaryalayaState extends State<KrishiKaryalaya> {
  late AdWithView _adWithView;
  bool isResponsed = false;
  _getAds() {
    _adWithView = adsController.getBannerAd2();
    if (_adWithView.responseInfo == null) {
      isResponsed = false;
    } else {
      isResponsed = true;
    }
  }

  final _key = GlobalKey<FormBuilderState>();

  final TextEditingController _districtTextController =
      TextEditingController(text: '');
  final TextEditingController _provinceTextController =
      TextEditingController(text: '');
  var jindex = 0;

  @override
  void initState() {
    if (!mounted) return;
    _getAds();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant KrishiKaryalaya oldWidget) {
    super.didUpdateWidget(oldWidget);
    activate();
    setState(() {});
  }

  var colour = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: StreamBuilder<QuerySnapshot>(
            stream: getKaryalayaData(
                _provinceTextController.text, _districtTextController.text),
            builder: (context, snapshot) {
              return CustomScrollView(
                shrinkWrap: true,
                clipBehavior: Clip.antiAlias,
                primary: true,
                slivers: [
                  SliverAppBar(
                    leading: IconButton(
                        icon: const Icon(
                          LineIcons.arrowLeft,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                    backgroundColor: Theme.of(context).primaryColor,
                    title: const Text(
                      'कार्यालयहरु',
                      style:
                          TextStyle(color: Colors.white, fontFamily: 'Poppins'),
                    ),
                    pinned: true,
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(),
                      child: FormBuilder(
                        key: _key,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Wrap(
                                alignment: WrapAlignment.center,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      onChanged: (value) {
                                        print(_key.currentState!
                                            .fields['pradesh']!.value['title']);
                                        print('Answer is' +
                                            (_key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null)
                                                .toString());
                                        if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश १") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value
                                                        .toString() !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 0;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "प्रदेश २") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 1;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "बागमती प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 2;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "गण्डकी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 3;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "लुम्बिनी प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 4;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "कर्णाली प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 5;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        } else if (_key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString() ==
                                            "सुदूरपश्चिम प्रदेश") {
                                          setState(() {
                                            _key.currentState!.fields['jilla']!
                                                        .value !=
                                                    null
                                                ? _key.currentState!
                                                    .fields['jilla']!
                                                    .reset()
                                                : '';
                                            _districtTextController.text = '';
                                            jindex = 6;
                                            _provinceTextController.text = _key
                                                .currentState!
                                                .fields['pradesh']!
                                                .value['title']
                                                .toString();
                                          });
                                        }
                                      },
                                      focusColor: Colors.blue,
                                      name: 'pradesh',
                                      items: pradeshMap
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'pradesh']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                        .currentState!
                                                                        .fields[
                                                                            'pradesh']!
                                                                        .value[
                                                                    'title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Theme.of(context)
                                                                .textTheme
                                                                .bodyMedium!
                                                                .color
                                                        : Theme.of(context)
                                                            .textTheme
                                                            .bodyMedium!
                                                            .color,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'प्रदेश',
                                          labelStyle: TextStyle(
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .bodyMedium!
                                                  .color,
                                              fontFamily: 'Poppins',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    child: FormBuilderDropdown(
                                      enabled: _key.currentState
                                                  ?.fields['pradesh']?.value !=
                                              null
                                          ? true
                                          : false,
                                      onChanged: (value) {
                                        if (_key.currentState!.fields['jilla']!
                                                .value !=
                                            null) {
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        }
                                      },
                                      name: 'jilla',
                                      items: jillaMap[jindex]
                                          .map(
                                            (value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(
                                                value['title'] ?? '',
                                                style: TextStyle(
                                                    color: _key
                                                                .currentState
                                                                ?.fields[
                                                                    'jilla']
                                                                ?.value !=
                                                            null
                                                        ? _key
                                                                        .currentState!
                                                                        .fields[
                                                                            'jilla']!
                                                                        .value[
                                                                    'title'] ==
                                                                value['title']
                                                            ? Colors.green
                                                            : Theme.of(context)
                                                                .textTheme
                                                                .bodyMedium!
                                                                .color
                                                        : Theme.of(context)
                                                            .textTheme
                                                            .bodyMedium!
                                                            .color,
                                                    fontFamily: 'Robot',
                                                    fontSize: 12),
                                              ),
                                            ),
                                          )
                                          .toList(),
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          labelText: 'जिल्ला',
                                          labelStyle: TextStyle(
                                            fontSize: 14,
                                            color: Theme.of(context)
                                                .textTheme
                                                .bodyMedium!
                                                .color,
                                            fontFamily: 'Poppins',
                                          )),
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      if (_key.currentState?.fields['pradesh']
                                                  ?.value !=
                                              null &&
                                          _key.currentState?.fields['jilla']
                                                  ?.value !=
                                              null) {
                                        setState(() {
                                          _provinceTextController.text = _key
                                              .currentState!
                                              .fields['pradesh']!
                                              .value['title']
                                              .toString();
                                          _districtTextController.text = _key
                                              .currentState!
                                              .fields['jilla']!
                                              .value['title']
                                              .toString();
                                        });
                                      } else if (_key.currentState!
                                              .fields['pradesh']!.value !=
                                          null) {
                                        _provinceTextController.text = _key
                                            .currentState!
                                            .fields['pradesh']!
                                            .value['title']
                                            .toString();
                                      } else {
                                        Get.snackbar('सूचना',
                                            'पहिले प्रदेश वा जिल्ला छान्नुहोस्');
                                      }
                                    },
                                    icon: const Icon(
                                      Ionicons.search_circle,
                                      size: 28,
                                    ),
                                    splashColor: Colors.green,
                                    splashRadius: 20,
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  !snapshot.hasData
                      ? const SliverToBoxAdapter(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : snapshot.data!.docs.isEmpty
                          ? SliverToBoxAdapter(
                              child: Center(
                                child: Lottie.asset(JsonAssets.emptyJson),
                              ),
                            )
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                                (context, index) {
                                  if (index == 2 && isResponsed) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SizedBox(
                                          height: 300,
                                          width: double.infinity,
                                          child: AdWidget(
                                            ad: _adWithView,
                                          )),
                                    );
                                  }
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            width: sizeProvider(context).width,
                                            decoration: BoxDecoration(
                                                color:
                                                    Theme.of(context).cardColor,
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(8)),
                                                boxShadow: SubThemeData
                                                    .getListItemBoxShadow(
                                                        context)),
                                            child: Column(
                                              children: [
                                                ListTile(
                                                  leading: Container(
                                                    width: 50,
                                                    height: 50,
                                                    clipBehavior:
                                                        Clip.antiAlias,
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Colors.transparent,
                                                        border: Border.all(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.2)),
                                                        borderRadius:
                                                            const BorderRadius
                                                                    .all(
                                                                Radius.circular(
                                                                    50))),
                                                    child: ProgressiveImage(
                                                      width: 50,
                                                      height: 50,
                                                      fit: BoxFit.contain,
                                                      placeholder:
                                                          const AssetImage(
                                                              'images/avatar.png'),
                                                      thumbnail: const AssetImage(
                                                          'images/avatar.png'),
                                                      image: NetworkImage(
                                                        '${snapshot.data!.docs[index]['image']}',
                                                      ),
                                                    ),
                                                  ),
                                                  title: Text(
                                                    '${snapshot.data!.docs[index]['name']}',
                                                    style: const TextStyle(
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  subtitle: Text(
                                                    '${snapshot.data!.docs[index]['address']}',
                                                    style: const TextStyle(
                                                      fontFamily: 'Ubuntu',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                  isThreeLine: true,
                                                ),
                                                ListTile(
                                                  leading: Text(
                                                    'बारे:',
                                                    style: TextStyle(
                                                        fontFamily: 'Ubuntu',
                                                        color: Theme.of(context)
                                                            .textTheme
                                                            .labelSmall!
                                                            .color),
                                                  ),
                                                  subtitle: ExpandableText(
                                                    snapshot.data!.docs[index]
                                                            ['description'] ??
                                                        '',
                                                    maxLines: 3,
                                                    linkColor:
                                                        Colors.blueAccent,
                                                    collapseOnTextTap: true,
                                                    expandOnTextTap: true,
                                                    expandText:
                                                        'पुरा हेर्नुहोस',
                                                    collapseText:
                                                        'लुकाउनु होस्',
                                                    linkStyle: const TextStyle(
                                                      fontFamily: 'Ubuntu',
                                                      color: Colors.blue,
                                                    ),
                                                    onUrlTap: (url) {
                                                      Get.to(() => CoWebViewer(
                                                            url: url,
                                                            title: snapshot
                                                                    .data!
                                                                    .docs[index]
                                                                ['name'],
                                                            link: url,
                                                          ));
                                                    },
                                                    animation: true,
                                                    style: const TextStyle(),
                                                  ),
                                                ),
                                                ButtonBar(
                                                  alignment: MainAxisAlignment
                                                      .spaceBetween,
                                                  children: [
                                                    IconButton(
                                                      onPressed: () async {
                                                        await launch(
                                                            'tel:${snapshot.data!.docs[index]['phone']}');
                                                      },
                                                      icon: const Icon(
                                                        Ionicons.call_outline,
                                                        color: Colors.green,
                                                      ),
                                                    ),
                                                    IconButton(
                                                      onPressed: () async {
                                                        await launch(
                                                            'http:${snapshot.data!.docs[index]['website']}');
                                                      },
                                                      icon: const Icon(
                                                        Ionicons.link_outline,
                                                        color: Colors.green,
                                                      ),
                                                    ),
                                                    IconButton(
                                                      onPressed: () async {
                                                        await launch(
                                                            'mailto:${snapshot.data!.docs[index]['email']}');
                                                      },
                                                      icon: const Icon(
                                                        Ionicons.mail_outline,
                                                        color: Colors.green,
                                                      ),
                                                    ),
                                                    IconButton(
                                                      onPressed: () async {
                                                        var url = Uri
                                                            .encodeFull(snapshot
                                                                    .data!
                                                                    .docs[index]
                                                                ['map']);
                                                        await launch(url);
                                                      },
                                                      icon: const Icon(
                                                        Ionicons
                                                            .location_outline,
                                                        color: Colors.green,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                childCount: snapshot.data!.docs.length,
                              ),
                            ),
                ],
              );
            }),
      ),
    );
  }
}
