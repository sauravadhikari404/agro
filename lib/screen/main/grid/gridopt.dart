// ignore_for_file: avoid_unnecessary_containers
import 'package:agromate/config/themes/sub_theme_data_mixin.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GridCatOptions extends StatefulWidget {
  const GridCatOptions({Key? key}) : super(key: key);

  @override
  _GridCatOptionsState createState() => _GridCatOptionsState();
}

class _GridCatOptionsState extends State<GridCatOptions> {
  var categorie = [
    {
      'name': 'कृषि गाइड',
      'thum': 'images/main/guide.png',
      'route': '/krishiguide'
    },
    {
      'name': 'सूचनाहरु',
      'thum': 'images/main/alert.png',
      'route': '/krishisuchana'
    },
    {'name': 'कृषि बजार', 'thum': 'images/main/kinmel.png', 'route': '/kinmel'},
    {
      'name': 'कृषि पुस्तकालय',
      'thum': 'images/main/book.png',
      'route': '/krishipustakalaya'
    },
    {
      'name': 'संस्थाहरु',
      'thum': 'images/main/sanstha.png',
      'route': '/sansthaharu'
    },
    {
      'name': 'कार्यालयहरु',
      'thum': 'images/main/sahakari.png',
      'route': '/krishikaryalaya'
    },
    {
      'name': 'कृषि क्यालकुलेटर',
      'thum': 'images/main/calculator.png',
      'route': '/krishicalculator'
    },
    {
      'name': 'हिसाबकिताब',
      'thum': 'images/main/ledger.png',
      'route': '/hisabkitab'
    },
    {'name': 'भिडियोहरु', 'thum': 'images/main/video.png', 'route': '/videos'},
  ].obs;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GridView.builder(
          primary: false,
          padding: const EdgeInsets.all(5),
          itemCount: categorie.length,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3),
          itemBuilder: (_, index) {
            return GestureDetector(
              onTap: () {
                if (mounted) {
                  setState(() {
                    Get.toNamed('${categorie[index]["route"]}');
                  });
                }
              },
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: LimitedBox(
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Theme.of(context).cardColor,
                        boxShadow: SubThemeData.getGridItemBoxShadow(context),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 4, top: 8.0, right: 4),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  alignment: Alignment.center,
                                  scale: 0.8,
                                  image: AssetImage(
                                      categorie[index]['thum'].toString()),
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              color: Colors.transparent,
                              child: Align(
                                alignment: Alignment.center,
                                child: FittedBox(
                                  child: Text(
                                    categorie[index]['name'].toString(),
                                    softWrap: true,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .color,
                                        fontSize: 10,
                                        fontFamily: 'poppins',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
