import 'package:agromate/controller/notificationController.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final GlobalKey<ScaffoldState> scaffoldkey = GlobalKey();

  String data = 'No Data';
  String title = 'No title';
  String body = 'No body';
  final NotificationController notyData = Get.put(NotificationController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldkey,
        appBar: CustomeAppBar(
          appBarTitle: 'Notification',
          action: [
            IconButton(
                onPressed: () {
                  notyData.deleteAllNoty();
                },
                icon: const Icon(LineIcons.alternateTrash))
          ],
        ),
        body: Obx(() {
          return notyData.notificationData.isNotEmpty? ListView.builder(
              itemCount: notyData.notificationData.length,
              itemBuilder: (_, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    child: notyData.notificationData[index].image == '' ||
                            notyData.notificationData[index].image == null
                        ? ListTile(
                            contentPadding: const EdgeInsets.all(4.0),
                            title: Text(
                              '${notyData.notificationData[index].title}',
                              style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600),
                            ),
                            subtitle: RichText(
                              text: TextSpan(
                                  style: TextStyle(
                                    fontFamily: 'Ubuntu',
                                    fontWeight: FontWeight.w700,
                                    color: Colors.blueGrey.shade600,
                                    fontSize: 10,
                                  ),
                                  text: NepaliDateTime.parse(notyData
                                              .notificationData[index].date)
                                          .format('EEE MMMM dd,yyyy')
                                          .toString() +
                                      '\n',
                                  children: [
                                    TextSpan(
                                      text:
                                          notyData.notificationData[index].body,
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        color: Colors.grey.shade600,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ]),
                            ),
                          )
                        : ListTile(
                            contentPadding: const EdgeInsets.all(4.0),
                            leading: Image.network(
                                notyData.notificationData[index].image),
                            title: Text(
                              '${notyData.notificationData[index].title}',
                              style: const TextStyle(
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600),
                            ),
                            subtitle: RichText(
                              text: TextSpan(
                                  style: TextStyle(
                                    fontFamily: 'Ubuntu',
                                    fontWeight: FontWeight.w700,
                                    color: Colors.blueGrey.shade600,
                                    fontSize: 10,
                                  ),
                                  text: NepaliDateTime.parse(notyData
                                              .notificationData[index].date)
                                          .format('EEE MMMM dd,yyyy')
                                          .toString() +
                                      '\n',
                                  children: [
                                    TextSpan(
                                      text:
                                          notyData.notificationData[index].body,
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        color: Colors.grey.shade600,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ]),
                            ),
                          ),
                  ),
                );
              }) :const Center(child: Text('No Notification'),
          );
        }));
  }
}
