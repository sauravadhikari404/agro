import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/helpers/linkpreviewer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class VideosScreen extends StatefulWidget {
  const VideosScreen({Key? key}) : super(key: key);
  @override
  _VideosScreenState createState() => _VideosScreenState();
}

class _VideosScreenState extends State<VideosScreen> {
  late AdWithView _adWithView;
  bool isResponsed = false;
  _getAds() {
    _adWithView = adsController.getBannerAd2();
    if (_adWithView.responseInfo == null) {
      isResponsed = false;
    } else {
      isResponsed = true;
    }
  }

  @override
  void initState() {
    if (!mounted) return;
    adsController.loadAds();
    _getAds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CusAppBar(
          appBarTitle: 'भिडियोहरु',
        ),
        body: Obx(
          () => ListView.builder(
              itemCount: newsController.videoModelClk.length,
              itemBuilder: (context, index) {
                if (index == 2 && isResponsed) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                        height: 300,
                        width: double.infinity,
                        child: AdWidget(
                          ad: _adWithView,
                        )),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CoLinkPreviewer(
                    url: '${newsController.videoModelClk[index].url}',
                    isVideo: true,
                  ),
                );
              }),
        ));
  }
}
