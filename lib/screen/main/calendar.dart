import 'package:agromate/constant/constant.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart' as picker;

class SDNepaliCalendar extends StatefulWidget {
  const SDNepaliCalendar({Key? key}) : super(key: key);

  @override
  _SDNepaliCalendarState createState() => _SDNepaliCalendarState();
}

class _SDNepaliCalendarState extends State<SDNepaliCalendar> {

  RxString timeNow = picker.NepaliDateTime
      .now()
      .format('aa h:mm')
      .obs;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'नेपाली पात्रो',
      ),
      body: RefreshIndicator(
        semanticsValue: 'Refresh',
        onRefresh: ()async{
          setState(() {
            timeNow=picker.NepaliDateTime
                .now()
                .format('aa h:mm')
                .obs;
          });
        },
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FittedBox(
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    width: sizeProvider(context).width,
                    height: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        gradient: LinearGradient(
                            colors: [
                              Colors.green.shade300,
                              Colors.green.shade300,
                              Colors.green.shade400,
                            ]
                        )
                    ),
                    child: Stack(
                      children: [
                      Positioned(
                        top: 10,
                        left: 10,
                        child: FittedBox(
                          child: Text('${picker.NepaliDateTime.now().format(
                              'dd MMMM yyyy, EEE')}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontFamily: 'Ubuntu',
                              fontWeight: FontWeight.bold,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 40,
                        left: 10,
                        child: Obx(() {
                          return FittedBox(
                            child: Text('$timeNow',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Ubuntu',
                                fontSize: 18,
                                color: Colors.white,
                              ),
                            ),
                          );
                        }),
                      ),

                      Positioned(
                        top: 65,
                        left: 10,
                        child:FittedBox(
                            child: Text('${DateFormat('dd MM,yyyy').format(DateTime.now())}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ),
                      ),
                    ],),
                  ),
                ),
              ),
              picker.CalendarDatePicker(
                onDateChanged: (value) {},
                initialDate: picker.NepaliDateTime.now(),
                firstDate: picker.NepaliDateTime(1970),
                lastDate: picker.NepaliDateTime(2100),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
