import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/controller/communitypost_controller.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/widget/comment/comments.dart';
import 'package:agromate/screen/widget/formmodal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_utils/nepali_utils.dart';

class CommunityScreen extends StatefulWidget {
  const CommunityScreen({Key? key}) : super(key: key);

  @override
  _CommunityScreenState createState() => _CommunityScreenState();
}

class _CommunityScreenState extends State<CommunityScreen>
    with SingleTickerProviderStateMixin {
  FocusNode f1 = FocusNode();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    super.dispose();
    f1.dispose();
    adsController.loadAds();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant CommunityScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    DateTime prebackpress = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(prebackpress);
        final cantExit = timegap >= const Duration(seconds: 2);
        if (cantExit) {
          const snack = SnackBar(
            content: Text(
              'Press Back button again to Exit',
              style: TextStyle(fontFamily: 'Ubuntu'),
            ),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        }
        Get.back();
        return true;
      },
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (!mounted) return;
            setState(() {
              if (isLoginUser()) {
                showFormModal(context, f1);
              } else {
                getNotLoggedDialog(context);
              }
            });
          },
          child: Icon(Ionicons.add_circle,
              color: Theme.of(context).iconTheme.color),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            if (!mounted) return;
            setState(() {
              commPostCont.getData();
            });
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: GetX<CommunityPostController>(
                    init: CommunityPostController(),
                    initState: (_) {},
                    builder: (_) {
                      return commPostCont.commModel.isEmpty
                          ? Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Lottie.asset(JsonAssets.emptyJson),
                                  TextButton.icon(
                                    onPressed: () {
                                      if (!mounted) return;
                                      setState(() {
                                        commPostCont.commModel.clear();
                                        commPostCont.onInit();
                                      });
                                    },
                                    icon: const Icon(Ionicons.refresh_circle),
                                    label: const Text(
                                      'रिफ्रेस गर्नुहोस',
                                      style: TextStyle(fontFamily: 'Poppins'),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : ListView.builder(
                              shrinkWrap: true,
                              itemCount: commPostCont.commModel.length,
                              itemBuilder: (_, index) {
                                var data = commPostCont
                                    .commModel; // yo chai model list bata data ma store gareko
                                var by = data[index].by ?? '';
                                //uid lege ani com post ko users ko ho bhanerw
                                debugPrint(commPostCont.commModel[index].body
                                    .toString());
                                return Hero(
                                  tag: data[index].pid ?? '',
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Card(
                                      child: Stack(
                                        clipBehavior: Clip.antiAlias,
                                        children: [
                                          Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Flexible(
                                                fit: FlexFit.loose,
                                                child: ListTile(
                                                  isThreeLine: true,
                                                  minVerticalPadding: 0,
                                                  visualDensity:
                                                      const VisualDensity(
                                                          horizontal: 0,
                                                          vertical: -4),
                                                  leading:
                                                      storeIt(by)['photoUrl'] ==
                                                              ''
                                                          ? const CircleAvatar(
                                                              radius: 18,
                                                              backgroundImage:
                                                                  AssetImage(
                                                                      'images/avatar.png'),
                                                            )
                                                          : CircleAvatar(
                                                              radius: 18,
                                                              backgroundImage:
                                                                  NetworkImage(
                                                                      '${storeIt(by)['photoUrl']}'),
                                                            ),
                                                  title: storeIt(
                                                              by)['farmName'] ==
                                                          ''
                                                      ? Text(
                                                          storeIt(by)['fname'] +
                                                              ' ' +
                                                              storeIt(
                                                                  by)['lname'],
                                                          style:
                                                              const TextStyle(
                                                            fontFamily:
                                                                'poppins',
                                                            fontWeight:
                                                                FontWeight.w500,
                                                          ),
                                                        )
                                                      : Text(
                                                          storeIt(by)[
                                                                  'farmName']
                                                              .toString(),
                                                          style: const TextStyle(
                                                              fontFamily:
                                                                  'poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                  subtitle: Row(
                                                    children: [
                                                      LineIcon(
                                                        LineIcons.mapMarked,
                                                        size: 16,
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        storeIt(by)[
                                                                'address'] ??
                                                            '',
                                                        style: const TextStyle(
                                                          fontFamily: 'ubuntu',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 10,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 25,
                                                    right: 25,
                                                    top: 5,
                                                    bottom: 5),
                                                child: Column(
                                                  children: [
                                                    ListBody(
                                                      children: [
                                                        storeIt(by)['farmName'] !=
                                                                ''
                                                            ? RichText(
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                text: TextSpan(
                                                                    children: [
                                                                      TextSpan(
                                                                          text:
                                                                              'नाम: \n',
                                                                          style: const TextStyle(
                                                                              color: Colors.green,
                                                                              fontFamily: 'poppins',
                                                                              fontWeight: FontWeight.bold),
                                                                          children: [
                                                                            TextSpan(
                                                                              text: storeIt(by)['fname'] + ' ' + storeIt(by)['lname'],
                                                                              style: const TextStyle(color: Colors.black, fontFamily: 'poppins', fontWeight: FontWeight.w600),
                                                                            )
                                                                          ])
                                                                    ]))
                                                            : Container(),
                                                        storeIt(by)['farmName'] ==
                                                                null
                                                            ? const Divider()
                                                            : Container(),
                                                        RichText(
                                                            textAlign:
                                                                TextAlign.left,
                                                            text: TextSpan(
                                                                children: [
                                                                  TextSpan(
                                                                      text:
                                                                          'प्रश्न: \n',
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .green,
                                                                          fontFamily:
                                                                              'poppins',
                                                                          fontWeight:
                                                                              FontWeight.bold),
                                                                      children: [
                                                                        TextSpan(
                                                                          text:
                                                                              data[index].body,
                                                                          style: const TextStyle(
                                                                              color: Colors.orange,
                                                                              fontFamily: 'poppins',
                                                                              fontWeight: FontWeight.w600),
                                                                        )
                                                                      ])
                                                                ])),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Divider(
                                                thickness: 1,
                                                color: Colors.green.shade200,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 8, right: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      NepaliMoment.fromAD((data[
                                                                      index]
                                                                  .date
                                                                  ?.toDate() ??
                                                              DateTime.now()))
                                                          .toString(),
                                                      style: TextStyle(
                                                          fontFamily: 'ubuntu',
                                                          color: Get.isDarkMode
                                                              ? const Color(
                                                                  0xFFD8D5D5)
                                                              : Colors.blueGrey,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 10,
                                                          fontStyle:
                                                              FontStyle.normal),
                                                    ),
                                                    StreamBuilder<
                                                            QuerySnapshot>(
                                                        stream: firebaseFirestore
                                                            .collection(
                                                                'CummunityPosts')
                                                            .doc(
                                                                data[index].pid)
                                                            .collection(
                                                                'Comments')
                                                            .snapshots(),
                                                        builder: (context,
                                                            AsyncSnapshot<
                                                                    QuerySnapshot>
                                                                snap) {
                                                          if (!snap.hasData) {
                                                            return const Text(
                                                              '..',
                                                              style: TextStyle(
                                                                fontSize: 10,
                                                              ),
                                                            );
                                                          } else if (snap.data!
                                                              .docs.isEmpty) {
                                                            return Text(
                                                              '0 टिप्पणी',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'ubuntu',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 10,
                                                                  color: Get
                                                                          .isDarkMode
                                                                      ? const Color(
                                                                          0xFFD8D5D5)
                                                                      : Colors
                                                                          .blueGrey,
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal),
                                                            );
                                                          }
                                                          return Text(
                                                            snap.data!.docs
                                                                    .length
                                                                    .toString() +
                                                                ' टिप्पणीहरु',
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'ubuntu',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 10,
                                                              color: Get
                                                                      .isDarkMode
                                                                  ? const Color(
                                                                      0xFFD8D5D5)
                                                                  : Colors
                                                                      .blueGrey,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            ),
                                                          );
                                                        }),
                                                  ],
                                                ),
                                              ),
                                              Divider(
                                                thickness: 1,
                                                color: Colors.green.shade200,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 8, right: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    isCurrentUserLikeIt(
                                                                data[index]
                                                                    .votes) ==
                                                            false
                                                        ? TextButton.icon(
                                                            style:
                                                                ElevatedButton
                                                                    .styleFrom(
                                                              splashFactory:
                                                                  InkRipple
                                                                      .splashFactory,
                                                              primary: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                              shadowColor:
                                                                  Colors.white,
                                                              elevation: 0,
                                                              shape:
                                                                  const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          50),
                                                                ),
                                                              ),
                                                            ),
                                                            icon: const Icon(
                                                              Ionicons
                                                                  .thumbs_up,
                                                              size: 14,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            onPressed: () {
                                                              if (isLoginUser()) {
                                                                likeController
                                                                    .addLikeToCommunityPosts(
                                                                        data[index]
                                                                            .pid);
                                                              } else {
                                                                getNotLoggedDialog(
                                                                    context);
                                                              }
                                                            },
                                                            label: Text(
                                                              data[index]
                                                                  .votes!
                                                                  .length
                                                                  .toString(),
                                                              style: const TextStyle(
                                                                  fontFamily:
                                                                      'ubuntu',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: Colors
                                                                      .white,
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal),
                                                            ),
                                                          )
                                                        : TextButton.icon(
                                                            style:
                                                                ElevatedButton
                                                                    .styleFrom(
                                                              splashFactory:
                                                                  InkRipple
                                                                      .splashFactory,
                                                              primary: Colors
                                                                  .orange
                                                                  .withOpacity(
                                                                      0.2),
                                                              shadowColor:
                                                                  Colors.white,
                                                              elevation: 0,
                                                              shape:
                                                                  const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          50),
                                                                ),
                                                              ),
                                                            ),
                                                            icon: const Icon(
                                                              Ionicons
                                                                  .thumbs_up,
                                                              color:
                                                                  Colors.orange,
                                                              size: 14,
                                                            ),
                                                            onPressed: () {
                                                              if (isLoginUser()) {
                                                                likeController
                                                                    .removeLikeFromCommunityPosts(
                                                                        data[index]
                                                                            .pid);
                                                              } else {
                                                                Get
                                                                    .defaultDialog(
                                                                        title:
                                                                            'Login Required',
                                                                        middleText:
                                                                            'पहिले login गर्नुहोला',
                                                                        cancel:
                                                                            MaterialButton(
                                                                          onPressed:
                                                                              () {
                                                                            Get.back();
                                                                          },
                                                                          child:
                                                                              const Text(
                                                                            'Cancel',
                                                                            style:
                                                                                TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                                          ),
                                                                        ),
                                                                        confirm:
                                                                            MaterialButton(
                                                                          onPressed:
                                                                              () {
                                                                            Get.back();
                                                                            Get.to(const AccountPage());
                                                                          },
                                                                          child:
                                                                              const Text(
                                                                            'Login',
                                                                            style:
                                                                                TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                                          ),
                                                                        ));
                                                              }
                                                            },
                                                            label: Text(
                                                                data[index]
                                                                    .votes!
                                                                    .length
                                                                    .toString(),
                                                                style: const TextStyle(
                                                                    fontFamily:
                                                                        'ubuntu',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    color: Colors
                                                                        .orange,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal)),
                                                          ),
                                                    TextButton.icon(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        splashFactory: InkSplash
                                                            .splashFactory,
                                                        primary:
                                                            Theme.of(context)
                                                                .primaryColor,
                                                        shadowColor:
                                                            Colors.white,
                                                        elevation: 0,
                                                        shape:
                                                            const RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(50),
                                                          ),
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Get.to(() =>
                                                            CommentScreen(
                                                              usrDetails:
                                                                  storeIt(by),
                                                              postData: {
                                                                'pid': data[index]
                                                                        .pid ??
                                                                    '',
                                                                'body': data[
                                                                            index]
                                                                        .body ??
                                                                    '',
                                                                'pdate':
                                                                    data[index]
                                                                        .date,
                                                                'by':
                                                                    data[index]
                                                                        .by,
                                                                'votes': data[
                                                                        index]
                                                                    .votes
                                                                    ?.length,
                                                                'isCurrentUserLikedIt':
                                                                    isCurrentUserLikeIt(
                                                                        data[index]
                                                                            .votes),
                                                                'index': index
                                                              },
                                                            ));
                                                      },
                                                      icon: const Icon(
                                                        Icons.comment,
                                                        color: Colors.white,
                                                        size: 14,
                                                      ),
                                                      label: const FittedBox(
                                                        child: Text(
                                                          'प्रतिक्रिया',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'ubuntu',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              color:
                                                                  Colors.white,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              });
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
