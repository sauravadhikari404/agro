import 'package:agromate/controller/tipotcontroller.dart';
import 'package:agromate/model/tipot.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TipotScreen extends StatelessWidget {
  final TipotController controller = Get.put(TipotController());

  TipotScreen({Key? key}) : super(key: key);

  void addTipot() {
    if (controller.titleController.text.isEmpty ||
        controller.bodyController.text.isEmpty) return;
    var tipot = TipotModel(
      id: UniqueKey().toString(),
      title: controller.titleController.text,
      body: controller.bodyController.text,
      created: DateTime.now(),
      category: '',
      color: 0,
      isComplete: false,
    );
    controller.titleController.text = '';
    controller.bodyController.text = '';
    controller.addToTipot(tipot);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: CusAppBar(
        appBarTitle: "टिपोट थप्नुहोस",
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                top: 40,
                left: 20,
                right: 20,
              ),
              child: Column(
                children: [
                  const Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "तपाइको कार्य टिपोट गर्नुहोस",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    borderRadius: BorderRadius.circular(25),
                    controller: controller.titleController,
                    height: 50.0,
                    hintText: "सिर्शक ",
                    nextFocus: controller.bodyFocus,
                  ),
                  CustomTextFormField(
                    padding: const EdgeInsets.fromLTRB(25, 10, 10, 10),
                    focus: controller.bodyFocus,
                    borderRadius: BorderRadius.circular(10),
                    controller: controller.bodyController,
                    height: 100.0,
                    hintText: "सन्छिप्त बिबरण लेख्नुहोस",
                    maxLines: 10,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            CustomButton(
              title: "राख्नुहोस",
              icon: Icons.done,
              onPressed: addTipot,
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.onPressed,
    required this.title,
    required this.icon,
    this.height = 40.0,
    this.color,
  }) : super(key: key);
  final VoidCallback onPressed;
  final String title;
  final IconData icon;
  final double height;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: height,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: color ?? Theme.of(context).primaryColor,
              ),
              child: TextButton.icon(
                onPressed: onPressed,
                icon: Icon(
                  icon,
                  color: Colors.white,
                ),
                label: Text(
                  title,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    Key? key,
    required this.controller,
    required this.height,
    required this.hintText,
    this.borderRadius,
    this.nextFocus,
    this.focus,
    this.maxLines,
    this.padding,
  }) : super(key: key);

  final TextEditingController controller;
  final String hintText;
  final double height;
  final BorderRadius? borderRadius;
  final FocusNode? nextFocus;
  final FocusNode? focus;
  final int? maxLines;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: padding,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: borderRadius,
      ),
      child: TextFormField(
        autofocus: true,
        focusNode: focus,
        maxLines: maxLines,
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
        ),
        controller: controller,
        onEditingComplete: () {
          nextFocus?.requestFocus();
        },
      ),
    );
  }
}
