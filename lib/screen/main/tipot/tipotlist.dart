import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/controller/tipotcontroller.dart';
import 'package:agromate/model/tipot.dart';
import 'package:agromate/screen/main/tipot/add_tipot.dart';
import 'package:agromate/screen/main/tipot/tipotscreen.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';

class MyTipotsScreen extends StatelessWidget {
  final controller = Get.put(TipotController());

  MyTipotsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'मेरो टिपोटहरु',
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Get.to(() => TipotScreen());
        },
        label: Row(
          children: const [
            Icon(Icons.add),
            SizedBox(width: 10),
            Text("टिपोट थप्नुहोस"),
          ],
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 45),
          Expanded(
            child: _buildCompleted(),
          ),
        ],
      ),
    );
  }

  Widget _buildCompleted() {
    return Obx(
      () => controller.tipots.isEmpty
          ? Center(
              child: Lottie.asset(JsonAssets.emptyJson),
            )
          : ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: controller.tipots.length,
              itemBuilder: (context, index) {
                TipotModel tipotModel = controller.tipots[index];
                return GestureDetector(
                  onTap: () {
                    Get.to(() => ViewTipotScreen(tipot: tipotModel));
                  },
                  onLongPress: () {
                    controller.toggleTipots(tipotModel);
                  },
                  child: TipotCard(
                    isComplete: tipotModel.isComplete!,
                    title: tipotModel.title!,
                    date: tipotModel.created!,
                  ),
                );
              },
            ),
    );
  }
}

class TipotCard extends StatelessWidget {
  const TipotCard({
    Key? key,
    required this.title,
    required this.date,
    required this.isComplete,
  }) : super(key: key);
  final String title;
  final DateTime date;
  final bool isComplete;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      margin: const EdgeInsets.fromLTRB(15, 5, 15, 5),
      height: 100,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300]!,
            blurRadius: 20,
            spreadRadius: 1,
          )
        ],
        color: isComplete ? Colors.green.shade300 : Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Row(
              children: [
                Icon(
                  Icons.task_alt,
                  color: isComplete ? Colors.green : Colors.grey,
                ),
                const SizedBox(width: 5),
                Text(
                  title,
                  style: const TextStyle(
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
          // ignore: deprecated_member_use
          Text(NepaliDateTime.fromDateTime(date).format('EEE MMMM dd, yyyy')),
        ],
      ),
    );
  }
}
