import 'package:agromate/model/tipot.dart';
import 'package:flutter/material.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';

class ViewTipotScreen extends StatelessWidget {
  const ViewTipotScreen({
    Key? key,
    required this.tipot,
  }) : super(key: key);
  final TipotModel tipot;

  @override
  Widget build(BuildContext context) {
    debugPrint(tipot.body.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text(
          tipot.title!,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 40),
          const Icon(
            Icons.task_outlined,
            size: 60,
          ),
          const SizedBox(height: 10),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Row(
                  children: [
                    const Text("शिर्षक: "),
                    Text(tipot.title!),
                  ],
                ),
                Row(
                  children: [
                    const Text("विवरण: "),
                    Text(tipot.body!),
                  ],
                ),
                Row(
                  children: [
                    Text("राखिएको मिति: "),
                    Text(
                      NepaliDateTime.fromDateTime(tipot.created!)
                          .format('EEE yyyy-MM-dd'),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
