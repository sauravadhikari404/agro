import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/widget/helpers/linkpreviewer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class NewsPaperScreen extends StatefulWidget {
  const NewsPaperScreen({Key? key}) : super(key: key);

  @override
  _NewsPaperScreenState createState() => _NewsPaperScreenState();
}

class _NewsPaperScreenState extends State<NewsPaperScreen> {
  late AdWithView _adWithView;
  bool isResponsed = false;
  _getAds() {
    _adWithView = adsController.getBannerAd2();
    if (_adWithView.responseInfo == null) {
      isResponsed = false;
    } else {
      isResponsed = true;
    }
  }

  @override
  void initState() {
    _getAds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime prebackpress = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(prebackpress);
        final cantExit = timegap >= const Duration(seconds: 2);
        if (cantExit) {
          const snack = SnackBar(
            content: Text(
              'Press Back button again to Exit',
              style: TextStyle(fontFamily: 'Ubuntu'),
            ),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        }
        Get.back();
        return true;
      },
      child: Scaffold(
          body: Column(
        children: [
          Expanded(
              child: Obx(
            () => ListView.builder(
                clipBehavior: Clip.antiAlias,
                primary: true,
                padding: const EdgeInsets.all(10),
                itemCount: newsController.newsModelClk.length,
                itemBuilder: (_, index) {
                  if (index == 2 && isResponsed) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                          height: 300,
                          width: double.infinity,
                          child: AdWidget(
                            ad: _adWithView,
                          )),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: CoLinkPreviewer(
                      url: newsController.newsModelClk[index].url.toString(),
                    ),
                  );
                }),
          )),
        ],
      )),
    );
  }
}
