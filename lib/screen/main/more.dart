// ignore_for_file: avoid_unnecessary_containers
import 'package:agromate/config/configs.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/controller/theme_controller.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/users/profile/editprofile.dart';
import 'package:agromate/screen/users/profile/mainprofile.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:launch_review/launch_review.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({Key? key}) : super(key: key);

  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    DateTime prebackpress = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(prebackpress);
        final cantExit = timegap >= const Duration(seconds: 2);
        if (cantExit) {
          const snack = SnackBar(
            content: Text(
              'Press Back button again to Exit',
              style: TextStyle(fontFamily: 'Ubuntu'),
            ),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        }
        Get.back();
        return true;
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Container(
                    width: sizeProvider(context).width,
                    height: 150,
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8)),
                        image: DecorationImage(
                          image: const AssetImage('images/bg/peakpx.jpg'),
                          fit: BoxFit.cover,
                          filterQuality: FilterQuality.high,
                          colorFilter: ColorFilter.mode(
                              Colors.white.withOpacity(0.4), BlendMode.lighten),
                        )),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: 2,
                          left: 0,
                          right: 0,
                          child: GestureDetector(
                            onTap: () {
                              if (isLoginUser()) {
                                Get.to(const MainProfile());
                              }
                            },
                            child: Center(
                              child: userController
                                          .firebaseUser.value?.photoURL !=
                                      null
                                  ? CachedNetworkImage(
                                      imageUrl:
                                          "${userController.firebaseUser.value?.photoURL}",
                                      cacheKey:
                                          "${userController.firebaseUser.value?.photoURL}",
                                      imageBuilder: (context, imgProvider) =>
                                          CircleAvatar(
                                        radius: 45,
                                        backgroundImage: imgProvider,
                                      ),
                                    )
                                  : const CircleAvatar(
                                      radius: 45,
                                      backgroundImage:
                                          AssetImage('images/avatar.png'),
                                    ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Obx(
                  () => FittedBox(
                    clipBehavior: Clip.antiAlias,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: (userController.firebaseUser.value!.isAnonymous) ==
                              true
                          ? TextButton(
                              onPressed: () {
                                Get.to(() => const AccountPage());
                              },
                              child: const Text(
                                'LOGIN',
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Colors.black,
                                ),
                              ),
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.green.shade700, width: 2),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                              ),
                            )
                          : Text(
                              '${userController.userModel.value.fname} ${userController.userModel.value.lname}',
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                    ),
                  ),
                ),
                isLoginUser()
                    ? ListTile(
                        onTap: () {
                          Get.to(() => const EditProfile());
                        },
                        leading: LineIcon(LineIcons.edit),
                        title: const Text('प्रोफाइल सम्पादन गर्नुहोस'),
                        trailing: const Icon(Ionicons.chevron_forward),
                      )
                    : const SizedBox(
                        height: 0,
                      ),
                const Divider(
                  thickness: 0.5,
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        Get.find<ThemeController>().changeTheme();
                      });
                    },
                    icon: Icon(themeChangedIcon(context))),
                ListTile(
                  onTap: () {
                    LaunchReview.launch(
                      androidAppId: 'com.sdtechcompany.agromate',
                      writeReview: true,
                    );
                  },
                  leading: const Icon(LineIcons.star),
                  title: const Text('एपलाइ रेटिंग गर्नुहोस'),
                  trailing: const Icon(Ionicons.chevron_forward),
                ),
                const Divider(
                  thickness: 0.5,
                ),
                ListTile(
                  onTap: () {
                    Share.share(
                        'https://play.google.com/store/apps/details?id=com.sdtechcompany.agromate');
                  },
                  leading: const Icon(Ionicons.share_social_outline),
                  title: const Text('एपलाई सेयरगर्नुहोस'),
                  trailing: const Icon(Ionicons.chevron_forward),
                ),
                const Divider(
                  thickness: 0.5,
                ),
                ListTile(
                  onTap: () {
                    // mailto:<email address>?subject=<subject>&body=<body>
                    launch("mailto:care@agromateapp.com?subject=Bug Report&'");
                  },
                  leading: LineIcon(Ionicons.bug),
                  title: const Text('बग रिपोर्ट गर्नुहोस'),
                  trailing: const Icon(Ionicons.chevron_forward),
                ),
                const Divider(
                  thickness: 0.5,
                ),
                ListTile(
                  onTap: () {
                    Get.to(() => const CoWebViewer(
                          link: 'https://agromateapp.com/about',
                          url: 'https://agromateapp.com/about',
                          title: "हाम्रो बारेमा",
                        ));
                  },
                  leading: LineIcon(Ionicons.alert),
                  title: const Text('हाम्रो बारेमा'),
                  trailing: const Icon(Ionicons.chevron_forward),
                ),
                const Divider(
                  thickness: 0.5,
                ),
                (userController.firebaseUser.value!.isAnonymous) == true
                    ? Container()
                    : ListTile(
                        onTap: () => Get.find<UserController>().signOut(),
                        leading: const Icon(Ionicons.exit_outline),
                        title: const Text('लग आउट'),
                        trailing: const Icon(Ionicons.chevron_forward),
                      ),
                (userController.firebaseUser.value!.isAnonymous) == true
                    ? Container()
                    : const Divider(
                        thickness: 0.5,
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
