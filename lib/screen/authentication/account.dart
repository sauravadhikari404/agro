// ignore_for_file: avoid_unnecessary_containers

import 'package:agromate/constant/constant.dart';
import 'package:agromate/screen/authentication/signup.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login.dart';
import 'package:flutter/services.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage>
    with SingleTickerProviderStateMixin {
  var tabController;
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  final DateTime prebackpress = DateTime.now();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.green, // navigation bar color
      statusBarColor: Colors.green.shade800, // status bar color
    ));
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          final timegap = DateTime.now().difference(prebackpress);
          final cantExit = timegap >= const Duration(seconds: 2);
          if (cantExit) {
            const snack = SnackBar(
              content: Text(
                'Press Back button again to Exit',
                style: TextStyle(fontFamily: 'Ubuntu'),
              ),
              duration: Duration(seconds: 2),
            );
            ScaffoldMessenger.of(context).showSnackBar(snack);
            return false;
          }
          Get.back();
          return true;
        },
        child: SingleChildScrollView(
          primary: false,
          physics: const NeverScrollableScrollPhysics(),
          child: SafeArea(
            child: DefaultTabController(
              length: 2,
              initialIndex: 0,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TabBar(
                        unselectedLabelColor: Colors.green.shade300,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: Colors.white,
                        indicator: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.green.shade300,
                              Colors.green.shade400,
                            ]),
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.redAccent),
                        controller: tabController,
                        tabs: [
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                      color: Colors.greenAccent, width: 1)),
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Login",
                                  style: TextStyle(fontFamily: 'Poppins'),
                                ),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                      color: Colors.greenAccent, width: 1)),
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Register",
                                  style: TextStyle(fontFamily: 'Poppins'),
                                ),
                              ),
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(
                    width: sizeProvider(context).width,
                    height: sizeProvider(context).height,
                    child: TabBarView(
                        controller: tabController,
                        physics: const AlwaysScrollableScrollPhysics(),
                        dragStartBehavior: DragStartBehavior.down,
                        children: const [LoginPage(), RegisterAccount()]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
