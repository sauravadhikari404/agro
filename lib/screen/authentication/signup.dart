import 'dart:core';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/screen/widget/helpers/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ionicons/ionicons.dart';

class RegisterAccount extends StatefulWidget {
  const RegisterAccount({Key? key}) : super(key: key);
  @override
  _RegisterAccountState createState() => _RegisterAccountState();
}

class _RegisterAccountState extends State<RegisterAccount> {
  TextEditingController email = TextEditingController(text: '');
  TextEditingController pass = TextEditingController(text: '');
  TextEditingController passconfig = TextEditingController(text: '');
  TextEditingController fname = TextEditingController(text: '');
  TextEditingController lname = TextEditingController(text: '');
  TextEditingController phone = TextEditingController(text: '');
  TextEditingController userType = TextEditingController(text: 'किसान');
  TextEditingController pradesh = TextEditingController(text: 'बागमती प्रदेश');
  TextEditingController district = TextEditingController(text: '');
  TextEditingController includedBusiness = TextEditingController(text: '');

  FocusNode emailNode = FocusNode();
  FocusNode passNode = FocusNode();
  FocusNode passconfigNode = FocusNode();
  FocusNode fnameNode = FocusNode();
  FocusNode lnameNode = FocusNode();
  FocusNode phoneNode = FocusNode();
  FocusNode userTypeNode = FocusNode();
  FocusNode pradeshNode = FocusNode();
  FocusNode districtNode = FocusNode();
  FocusNode includedBusinessNode = FocusNode();
  final GlobalKey<FormState> _key = GlobalKey<FormState>();

  List<String> userTypeList = [
    'किसान',
    'बिज्ञ',
    'विद्यार्थी',
    'बिक्रेता',
    'प्राविधिक',
    'उपभोक्ता',
    'योगदानकर्ता',
    'अन्य'
  ];

  int districtIndex = 2;
  var passEye = const Icon(
    Ionicons.eye_off,
  );
  bool isScrubText = true;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      primary: true,
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              width: sizeProvider(context).width,
              child: const Align(
                alignment: Alignment.topRight,
                child: Text(
                  'Register',
                  style: TextStyle(
                    fontFamily: 'poppins',
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          const Divider(
            thickness: 5,
            indent: 150,
            color: Colors.greenAccent,
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: fname,
                        autofocus: true,
                        validator: (val) {},
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(labelText: 'नाम'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: lname,
                        autofocus: true,
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(labelText: 'थर'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 1000),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextFormField(
                              decoration: const InputDecoration(
                                labelText: 'प्रयोगकर्ता किसिम',
                              ),
                              readOnly: true,
                              controller: userType,
                            )),
                            PopupMenuButton<String>(
                              icon: const Icon(Ionicons.arrow_down_circle),
                              onSelected: (String value) {
                                setState(() {
                                  userType.text = value;
                                });
                              },
                              itemBuilder: (BuildContext context) {
                                return userTypeList
                                    .map<PopupMenuItem<String>>((String value) {
                                  return PopupMenuItem(
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontFamily: 'poppins',
                                            color: value == userType.text
                                                ? Colors.green
                                                : Colors.black,
                                            fontWeight: value == userType.text
                                                ? FontWeight.bold
                                                : FontWeight.normal),
                                      ),
                                      value: value);
                                }).toList();
                              },
                            ),
                          ],
                        )),
                    AnimatedContainer(
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 1000),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextFormField(
                              decoration: const InputDecoration(
                                labelText: 'प्रदेश',
                              ),
                              readOnly: true,
                              controller: pradesh,
                            )),
                            PopupMenuButton<String>(
                              icon: const Icon(Ionicons.arrow_down_circle),
                              onSelected: (String value) {
                                setState(() {
                                  pradesh.text = value;
                                  district.clear();
                                  districtIndex = pradeshList.indexOf(value);
                                });
                              },
                              itemBuilder: (BuildContext context) {
                                return pradeshList
                                    .map<PopupMenuItem<String>>((String value) {
                                  return PopupMenuItem(
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontFamily: 'poppins',
                                            color: value == pradesh.text
                                                ? Colors.green
                                                : Colors.black,
                                            fontWeight: value == pradesh.text
                                                ? FontWeight.bold
                                                : FontWeight.normal),
                                      ),
                                      value: value);
                                }).toList();
                              },
                            ),
                          ],
                        )),
                    AnimatedContainer(
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 1000),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextFormField(
                              decoration: const InputDecoration(
                                labelText: 'जिल्ला',
                              ),
                              readOnly: true,
                              controller: district,
                            )),
                            PopupMenuButton<String>(
                              icon: const Icon(Ionicons.arrow_down_circle),
                              onSelected: (String value) {
                                setState(() {
                                  district.text = value;
                                });
                              },
                              itemBuilder: (BuildContext context) {
                                return jillaList[districtIndex]
                                    .map<PopupMenuItem<String>>((String value) {
                                  return PopupMenuItem(
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontFamily: 'poppins',
                                            color: value == district.text
                                                ? Colors.green
                                                : Colors.black,
                                            fontWeight: value == district.text
                                                ? FontWeight.bold
                                                : FontWeight.normal),
                                      ),
                                      value: value);
                                }).toList();
                              },
                            ),
                          ],
                        )),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        focusNode: includedBusinessNode,
                        enableSuggestions: true,
                        controller: includedBusiness,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                            labelText: 'आबद्ध संस्था',
                            hintText: 'eg.सहकारी संस्था,फाम,कृषि उद्योग'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: phone,
                        keyboardType: TextInputType.phone,
                        maxLength: 10,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                            counterText: '', labelText: 'फोन'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: email,
                        inputFormatters: <TextInputFormatter>[
                          ValidatorInputFormatter(
                              editingValidator: EmailEditingRegexValidator())
                        ],
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(labelText: 'इमेल'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: pass,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: isScrubText,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            suffix: IconButton(
                              onPressed: () {
                                setState(() {
                                  if (isScrubText) {
                                    isScrubText = false;
                                    passEye = const Icon(Ionicons.eye);
                                  } else if (!isScrubText) {
                                    isScrubText = true;
                                    passEye = const Icon(Ionicons.eye_off);
                                  }
                                });
                              },
                              icon: passEye,
                            ),
                            labelText: 'पासवोर्ड'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: passconfig,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: isScrubText,
                        textInputAction: TextInputAction.go,
                        onFieldSubmitted: (val) {},
                        decoration: InputDecoration(
                            suffix: IconButton(
                              hoverColor: Colors.greenAccent,
                              splashColor: Colors.greenAccent,
                              splashRadius: 50,
                              onPressed: () {
                                setState(() {
                                  if (isScrubText) {
                                    isScrubText = false;
                                    passEye = const Icon(
                                      Ionicons.eye,
                                    );
                                  } else if (!isScrubText) {
                                    isScrubText = true;
                                    passEye = const Icon(
                                      Ionicons.eye_off,
                                    );
                                  }
                                });
                              },
                              icon: passEye,
                            ),
                            labelText: 'पुनः पासवोर्ड राख्नुहोस'),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: InkWell(
                        onTap: () {
                          validateRegisterForm(
                              fname,
                              lname,
                              email,
                              phone,
                              pass,
                              passconfig,
                              userType,
                              pradesh,
                              district,
                              includedBusiness);
                        },
                        child: AnimatedContainer(
                          padding: const EdgeInsets.all(10),
                          duration: const Duration(milliseconds: 20),
                          curve: Curves.bounceInOut,
                          decoration: BoxDecoration(
                              color: Colors.green.shade300,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(12))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: const [
                              Text(
                                'दर्ता गर्नुहोस्',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'poppins',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25.5,
                    ),
                    // const FittedBox(
                    //     child: Text(
                    //   '---------------   OR   ---------------',
                    //   style: TextStyle(
                    //       color: Colors.black12,
                    //       fontFamily: 'poppins',
                    //       letterSpacing: -1.8),
                    // )),
                    // const SizedBox(
                    //   height: 20,
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(left: 50, right: 50),
                    //   child: InkWell(
                    //     onTap: () {
                    //       // UserController().google_signIn();
                    //       userController.signInWithGoogle();
                    //     },
                    //     child: AnimatedContainer(
                    //       padding: const EdgeInsets.all(10),
                    //       duration: const Duration(milliseconds: 20),
                    //       curve: Curves.bounceInOut,
                    //       decoration: BoxDecoration(
                    //           color: Colors.orange.shade300,
                    //           borderRadius: const BorderRadius.all(
                    //               Radius.circular(12))),
                    //       child: Row(
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: const [
                    //           Text(
                    //             'Join With Google',
                    //             style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontFamily: 'poppins',
                    //                 fontWeight: FontWeight.bold),
                    //           ),
                    //           Icon(
                    //             Ionicons.logo_google,
                    //             color: Colors.white,
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                )),
          ),
          const SizedBox(
            height: 300,
          )
        ],
      ),
    );
  }
}
