import 'package:agromate/constant/constant.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/screen/widget/helpers/validator.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  TextEditingController email = TextEditingController(text: '');
  TextEditingController pass = TextEditingController(text: '');
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              transformAlignment: Alignment.center,
              child: const Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Login',
                  style: TextStyle(
                    fontFamily: 'poppins',
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          const Divider(
            thickness: 5,
            endIndent: 150,
            color: Colors.greenAccent,
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        controller: email,
                        inputFormatters: [
                          ValidatorInputFormatter(
                              editingValidator: EmailEditingRegexValidator())
                        ],
                        enableSuggestions: true,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                          labelText: 'Email',
                          hintText: 'ram@email.com',
                          labelStyle: TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    AnimatedContainer(
                      curve: Curves.easeOut,
                      duration: const Duration(milliseconds: 1000),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        controller: pass,
                        obscureText: true,
                        keyboardType: TextInputType.visiblePassword,
                        decoration: const InputDecoration(
                          labelText: 'Password',
                          hintText: 'Password',
                          labelStyle: TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        cursorRadius: const Radius.circular(50),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: InkWell(
                        onTap: () {
                          validateLoginForm(email.text, pass.text);
                        },
                        child: AnimatedContainer(
                          padding: const EdgeInsets.all(10),
                          duration: const Duration(milliseconds: 20),
                          curve: Curves.bounceInOut,
                          decoration: BoxDecoration(
                              color: Colors.green.shade300,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(12))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: const [
                              Text(
                                'Login',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'poppins',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25.5,
                    ),
                    const FittedBox(
                        child: Text(
                      '---------------   OR   ---------------',
                      style: TextStyle(
                          color: Colors.black12,
                          fontFamily: 'poppins',
                          letterSpacing: -1.8),
                    )),
                    const SizedBox(
                      height: 10.5,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: InkWell(
                        onTap: () {
                          UserController().signInAnonymously();
                        },
                        child: AnimatedContainer(
                          padding: const EdgeInsets.all(10),
                          duration: const Duration(milliseconds: 20),
                          curve: Curves.bounceInOut,
                          decoration: BoxDecoration(
                              color: Colors.blue.shade300,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(12))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                'Login Anonymously',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'poppins',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
