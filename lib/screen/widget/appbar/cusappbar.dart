import 'package:flutter/material.dart';

class CusAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String? appBarTitle;

  @override
  final Size preferredSize;
  CusAppBar({Key? key, this.appBarTitle})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  // ignore: no_logic_in_create_state
  _CusAppBarState createState() => _CusAppBarState(
        appBarTitle!,
      );
}

class _CusAppBarState extends State<CusAppBar> {
  String appBarTitle;

  _CusAppBarState(this.appBarTitle);
  var titles = [
    'AgroMate',
    'समाचार',
    'समुदाय',
    'खोजि गर्नुहोस्',
    'थप',
  ];
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: LimitedBox(
        child: Text(
          appBarTitle,
          maxLines: 1,
          softWrap: true,
          style: const TextStyle(
            color: Colors.white,
            fontFamily: 'poppins',
          ),
        ),
      ),
      elevation: 2,
      iconTheme: const IconThemeData(color: Colors.white),
      foregroundColor: Colors.white,
    );
  }
}
