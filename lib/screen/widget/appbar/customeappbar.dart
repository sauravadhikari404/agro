import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomeAppBar extends StatefulWidget implements PreferredSizeWidget {
  final GlobalKey<ScaffoldState>? scaffoldkey;
  final String appBarTitle;
  final TextStyle? textStyle;
  final int? maxLines;
  final bool? softWrap;
  final TextOverflow? textOverFlow;
  final double? textScaleFactor;
  final List<Widget>? action;
  final Widget? leading;
  @override
  final Size preferredSize;
  const CustomeAppBar({
    Key? key,
    this.scaffoldkey,
    required this.appBarTitle,
    this.textStyle,
    this.maxLines,
    this.textOverFlow,
    this.textScaleFactor,
    this.leading,
    this.softWrap,
    this.action,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  // ignore: no_logic_in_create_state
  _CustomeAppBarState createState() => _CustomeAppBarState();
}

class _CustomeAppBarState extends State<CustomeAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: widget.leading,
      title: Text(
        widget.appBarTitle,
        style: widget.textStyle.isNull
            ? const TextStyle(color: Colors.white, fontFamily: 'poppins')
            : widget.textStyle,
        maxLines: widget.maxLines.isNull ? widget.maxLines : 1,
        softWrap: widget.softWrap,
        overflow: widget.textOverFlow,
        textScaleFactor: widget.textScaleFactor,
      ),
      elevation: 0,
      iconTheme: const IconThemeData(color: Colors.white),
      foregroundColor: Colors.white,
      actions: widget.action.isNull ? [] : widget.action,
    );
  }
}
