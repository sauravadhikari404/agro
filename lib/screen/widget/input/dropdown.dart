import 'package:flutter/material.dart';

class DropDownField extends StatefulWidget {
  final TextEditingController textEditingController;
  final FormFieldValidator? validator;
  final TextStyle? textStyle;
  final String labelText;
  final String? hintText;
  final InputBorder? inputBorder;
  final EdgeInsetsGeometry? padding;
  final TextStyle? errorStyle;
  final TextInputType? textInputType;
  final dynamic dropdownValue;
  final void Function(String?) onChange;
  final List<String> dropdownList;

   DropDownField({Key? key,required this.textEditingController,
   this.textInputType,this.errorStyle,
     this.padding,this.inputBorder,
     required this.dropdownValue,
     required this.dropdownList,
     required this.onChange,
     this.hintText,required this.labelText,
     this.validator,this.textStyle}) : super(key: key);

  @override
  State<DropDownField> createState() => _DropDownFieldState();
}

class _DropDownFieldState extends State<DropDownField> {
  String? _selectedText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding??EdgeInsets.only(left: 8.0,right: 8.0),
      child: DropdownButtonFormField<String>(
        decoration: InputDecoration(
          border: widget.inputBorder,
            hintText: widget.hintText,
            labelText: widget.labelText,
            labelStyle: widget.textStyle,
        ),
        validator:widget.validator,
        value: widget.dropdownValue,
        items: widget.dropdownList
            .map((label) => DropdownMenuItem(
          child: Text(label.toString(),
              style: TextStyle(
                fontFamily: 'poppins',
                color: widget.textEditingController.text ==
                    label.toString()
                    ? Colors.green
                    : Colors.black,
              )),
          value: label,
        ))
            .toList(),
        onChanged: widget.onChange,
    ),
    );
  }
}
