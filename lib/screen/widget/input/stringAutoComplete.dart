import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/controller/hisabkitabincomecontroller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TitleList{

  Map<String,String> title;
  // Map<String,String> id;
  TitleList({required this.title});
  @override
  String toString() {
    return '{ $title }';
  }
}

class StringAutoComplete extends StatefulWidget {
   const StringAutoComplete({Key? key,required this.labelText,this.hintText,required this.controller}) : super(key: key);
   final String labelText;
   final String? hintText;
   final TextEditingController controller;



  @override
  State<StringAutoComplete> createState() => _StringAutoCompleteState();
}

class _StringAutoCompleteState extends State<StringAutoComplete> {

      List<String> _options=<String>[];
     final HisabKitabExpensesController expensesData= Get.put(HisabKitabExpensesController());
     final HisabKitabIncomeController incomeData= Get.put(HisabKitabIncomeController());

  RxList<dynamic> getTitles(RxList<dynamic> data){
    var listThat=[].obs;
    for (var element in data) {
      RxList<dynamic> listThis= RxList.filled(data.length, {
        'title':element.title.toString(),
      });
      listThat.addAll(listThis);
  }
    debugPrint('This is ListThat'+listThat.toString()+'From $data');
    return listThat.toSet().toList().obs;
}


@override
  void initState() {
    super.initState();
    //this is First Data Adding process
    var expData=[];
    var incData=[];
    getTitles(expensesData.hisabkitabExpensesData).asMap().forEach((key, value)=> expData.add(TitleList(title: value)));
    getTitles(incomeData.hisabkitabIncomeData).asMap().forEach((key, value)=> incData.add(TitleList(title: value)));
   //first data adding process end
//this is second data Adding Process
    List data=[];
    List data2=[];
   for (var element in expData) {
       data.addAll({element.title});
   }
    for (var element in incData) {
       data2.addAll({element.title});
    }
   //second data adding process end
     var expData2=List.generate(data.length+2, (index) => '$index');
     var incData2=List.generate(data2.length+2, (index) => '$index');
    expData2.length=data.length;
    incData2.length=data2.length;
    for(int i=0;i<data.length;i++){
      expData2[i]=data[i]['title'].toString();
    }
  for(int i=0;i<data2.length;i++){
    incData2[i]=data2[i]['title'].toString();
  }
    _options=(expData2+incData2).toSet().toList();
  }
     TextEditingController textEditingController=TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return RawAutocomplete<String>(
      optionsBuilder: (TextEditingValue textEditingValue) {
        return _options.where((String option) {
          return option.contains(textEditingValue.text.toLowerCase());
        });
      },
      fieldViewBuilder: (BuildContext context,
          TextEditingController controller,
          FocusNode focusNode,
          VoidCallback onFieldSubmitted) {
        textEditingController=controller;
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            onFieldSubmitted: (value){
              FocusManager.instance.primaryFocus?.unfocus();
            },
            controller: controller,
            focusNode: focusNode,
            validator: (val){
              if(val==null){
                return "कृपया खर्च शिर्षक लेख्नुहोस्";
              }
              return null;
            },
            onEditingComplete: onFieldSubmitted,
            onChanged: (value){
              widget.controller.text=controller.text.toString();
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey[300]!),
              ),
              hintText: widget.hintText,
              label: Text(widget.labelText),
            ),
          ),
        );
      },
      optionsViewBuilder: (BuildContext context,
          AutocompleteOnSelected<String> onSelected, Iterable<String> options) {
        return Align(
          alignment: Alignment.topLeft,
          child: Material(
            elevation: 4.0,
            child: ListView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8.0),
              itemCount: options.length,
              itemBuilder: (BuildContext context, int index) {
                final String option = options.elementAt(index);
                return GestureDetector(
                  onTap: () {
                    onSelected(option);
                  },
                  child: ListTile(
                    title: Text(option),
                  ),
                );
              },
            ),
          ),
        );
      },
      onSelected: (String selection){
        widget.controller.text=selection;
        FocusManager.instance.primaryFocus?.unfocus();
        debugPrint('You just Selected $selection');
      },
    );
  }
}
