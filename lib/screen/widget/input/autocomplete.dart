import 'package:agromate/controller/hisabkitabexpensescontroller.dart';
import 'package:agromate/model/hisabkitab.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
@immutable
class AutoCompleteTitle extends StatefulWidget {
  final String labelText;
  final String? hintText;
  final TextEditingController controller;
  const AutoCompleteTitle({Key? key,required this.controller,required this.labelText,this.hintText}) : super(key: key);

  @override
  State<AutoCompleteTitle> createState() => _AutoCompleteTitleState();
}

class _AutoCompleteTitleState extends State<AutoCompleteTitle> {
  final HisabKitabExpensesController expensesData=Get.put(HisabKitabExpensesController());
  late var _titleOption;

  String _displayStringForOption(HisabKitabExpensesModel option) => option.title;

  @override
  void initState() {
    _titleOption=  expensesData.hisabkitabExpensesData;
    super.initState();
  }
  TextEditingController textEditingController=TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {

    return Autocomplete<HisabKitabExpensesModel>(
      displayStringForOption: _displayStringForOption,
      fieldViewBuilder:
          (context, controller, focusNode, onEditingComplete) {
        textEditingController=controller;
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            controller: controller,
            focusNode: focusNode,
            validator: (val){
              if(val==null){
                return "कृपया खर्च शिर्षक लेख्नुहोस्";
              }
              return null;
            },
            onEditingComplete: onEditingComplete,
            onChanged: (value){
              widget.controller.text=controller.text.toString();
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey[300]!),
              ),
              hintText: widget.hintText,
              label: Text(widget.labelText),
            ),
          ),
        );
      },
      optionsBuilder: (TextEditingValue textEditingValue){
        if(textEditingValue.text==''){
          return const Iterable<HisabKitabExpensesModel>.empty();
        }
        return _titleOption.where((HisabKitabExpensesModel option){
          return option.toString().contains(textEditingValue.text.toLowerCase());
        } );
      },
      onSelected: (HisabKitabExpensesModel selection){
        widget.controller.text=_displayStringForOption(selection);
        debugPrint('You just Selected ${_displayStringForOption(selection)}');
      },
    );
  }
}



