import 'package:flutter/material.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart' as datePicker;
import 'package:nepali_utils/nepali_utils.dart';

class NepaliDatePicker extends StatefulWidget {
   NepaliDatePicker({Key? key,
     required this.datePickerEdit,
     required this.hiddenDateTextEditController,
     this.validator,this.textStyle,
     this.padding,this.inputBorder,
     required this.labelText,
     this.hintText,this.errorStyle,
     required this.onChange,
     this.textInputType}) : super(key: key);

   final TextEditingController datePickerEdit;
   final FormFieldValidator? validator;
   final TextStyle? textStyle;
   final String labelText;
   final void Function(String?) onChange;
   final String? hintText;
   final InputBorder? inputBorder;
   final EdgeInsetsGeometry? padding;
   final TextStyle? errorStyle;
   final TextInputType? textInputType;
   final  TextEditingController hiddenDateTextEditController;

  @override
  _NepaliDatePickerState createState() => _NepaliDatePickerState();
}

class _NepaliDatePickerState extends State<NepaliDatePicker> {
  var pickedDates='';
  DateTime? pickDates;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding??EdgeInsets.only(left: 8.0 , right: 8.0),
      child: Material(
        child: InkWell(
          onTap: () async {
            NepaliDateTime? _selectedDateTime =
            await datePicker.showAdaptiveDatePicker(
              context: context,
              initialDate: pickedDates == ''
                  ? NepaliDateTime.now()
                  : NepaliDateTime.parse(pickedDates),
              firstDate: NepaliDateTime(2053),
              lastDate: NepaliDateTime(2090),
              language: Language.nepali,

              initialDatePickerMode: DatePickerMode
                  .day, // for platform except iOS
            );
            var date1=datePicker.NepaliDateFormat('yyyy-MM-dd');
            if (_selectedDateTime != null) {
              pickedDates = _selectedDateTime.toString();
              pickDates = _selectedDateTime.toDateTime();
              widget.hiddenDateTextEditController.text=_selectedDateTime.toString();
              widget.datePickerEdit.text =
                date1.format(_selectedDateTime).toString();
            }
          },
          child: Stack(
            children: [
              TextFormField(
                style: TextStyle(
                  color: Colors.transparent,
                  fontSize: 0,
                ),
                readOnly: true,
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
                controller: widget.hiddenDateTextEditController,
              ),
              TextFormField(
                readOnly: true,
                enabled: false,
                onChanged: widget.onChange,
                controller: widget.datePickerEdit,
                validator: widget.validator,
                decoration: InputDecoration(
                  labelText: widget.labelText,
                  hintText: widget.hintText,
                  border: widget.inputBorder,
                  errorStyle: widget.errorStyle,
                  labelStyle: widget.textStyle,
                  suffixIcon: LineIcon(
                    LineIcons.asterisk,
                    size: 10,
                    color: Colors.red,
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
