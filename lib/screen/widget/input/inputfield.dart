import 'package:flutter/material.dart';

class TextInputField extends StatelessWidget {
  const TextInputField(
      {Key? key,
      required this.textEditingController,
      this.validator,
      this.textStyle,
      required this.labelText,
      this.hintText,
      this.inputBorder,
      this.padding,
      this.errorStyle,
      this.textInputType,
      this.focusBorder,
      this.filled,
      this.filledColor,
      this.labelStyle,
      this.focusColor,
      this.isPassword,
      this.scrollPadding,
      this.enabled,
      this.readOnly,
      this.disableBorder})
      : super(key: key);

  final TextEditingController textEditingController;
  final FormFieldValidator? validator;
  final TextStyle? textStyle;
  final String labelText;
  final String? hintText;
  final InputBorder? inputBorder;
  final EdgeInsetsGeometry? padding;
  final TextStyle? errorStyle;
  final TextStyle? labelStyle;
  final TextInputType? textInputType;
  final InputBorder? focusBorder;
  final InputBorder? disableBorder;
  final bool? filled;
  final bool? enabled;
  final bool? readOnly;
  final bool? isPassword;
  final Color? filledColor;
  final Color? focusColor;
  final EdgeInsets? scrollPadding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.only(left: 8.0, right: 8.0),
      child: TextFormField(
        controller: textEditingController,
        validator: validator,
        readOnly: readOnly ?? false,
        keyboardType: textInputType,
        obscureText: isPassword??false,
        scrollPadding: scrollPadding ?? const EdgeInsets.all(20.0),
        decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
          border: inputBorder,
          errorStyle: errorStyle,
          focusedBorder: focusBorder,
          filled: filled,
          fillColor: filledColor,
          labelStyle: labelStyle,
          focusColor: focusColor,
          disabledBorder: disableBorder,
          enabled: enabled ?? true,
        ),
      ),
    );
  }
}
