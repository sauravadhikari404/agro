import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/kinmelprofile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:url_launcher/url_launcher.dart';

class KinmelWid extends StatefulWidget {
  final Map<String, dynamic>? productDetails;
  const KinmelWid({Key? key, this.productDetails}) : super(key: key);

  @override
  _KinmelWidState createState() => _KinmelWidState();
}

class _KinmelWidState extends State<KinmelWid> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        type: MaterialType.card,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            Get.to(() => KinmelProfile(
                  dtsource: widget.productDetails,
                ));
          },
          child: Container(
            width: sizeProvider(context).width,
            height: 260,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              const BoxShadow(
                  color: Colors.blueGrey, spreadRadius: 0.5, blurRadius: 0.4),
              BoxShadow(
                  color: Colors.green.shade300,
                  spreadRadius: 0.4,
                  blurRadius: 0.5)
            ]),
            child: Stack(
              children: [
                Container(
                  width: sizeProvider(context).width,
                  height: 180,
                  decoration: BoxDecoration(
                    color: Colors.green.shade200,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    boxShadow: const [
                      BoxShadow(color: Colors.black, spreadRadius: 0.4),
                      BoxShadow(color: Colors.white, spreadRadius: 0.4),
                    ],
                  ),
                  child: Stack(
                    children: [
                      CachedNetworkImage(
                        imageUrl: widget.productDetails?['image'][0],
                        cacheKey: widget.productDetails?['image'][0],
                        placeholder: (context, url) => Container(
                          height: 180,
                          width: sizeProvider(context).width,
                          color: const Color(0xff0AF290),
                          child: Lottie.asset(JsonAssets.imgLoading),
                        ),
                        imageBuilder: (context, imgProvider) => Container(
                          width: sizeProvider(context).width,
                          height: 180,
                          decoration: BoxDecoration(
                              color: const Color(0xff0AF290),
                              image: DecorationImage(
                                  image: imgProvider,
                                  fit: BoxFit.cover,
                                  filterQuality: FilterQuality.low)),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: sizeProvider(context).width,
                          height: 60,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.6),
                          ),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  widget.productDetails?['pName'] ?? 'NA',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'poppins',
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                              const Divider(
                                thickness: 0.5,
                                color: Colors.white,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Obx(
                                    () => Text(
                                      kinmelController
                                              .kmodel[widget
                                                  .productDetails?['index']]
                                              .votes!
                                              .length
                                              .toString() +
                                          ' लाइक'
                                              '',
                                      style: const TextStyle(
                                          fontFamily: 'poppins',
                                          color: Colors.white),
                                    ),
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: Text(
                                      '|',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  StreamBuilder<QuerySnapshot>(
                                      stream: firebaseFirestore
                                          .collection('Kinmel')
                                          .doc(widget.productDetails?['pid'])
                                          .collection('Comments')
                                          .snapshots(),
                                      builder: (context,
                                          AsyncSnapshot<QuerySnapshot>
                                              snapshot) {
                                        if (!snapshot.hasData) {
                                          return const Text(
                                            '...',
                                            style:
                                                TextStyle(color: Colors.white),
                                          );
                                        }
                                        return FittedBox(
                                          child: snapshot.data!.docs.length <= 1
                                              ? Text(
                                                  '${snapshot.data?.docs.length} टिप्पणी',
                                                  style: const TextStyle(
                                                      fontFamily: 'poppins',
                                                      color: Colors.white),
                                                )
                                              : Text(
                                                  '${snapshot.data?.docs.length} टिप्पणीहरु',
                                                  style: const TextStyle(
                                                      fontFamily: 'poppins',
                                                      color: Colors.white),
                                                ),
                                        );
                                      }),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  child: Container(
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                        borderRadius: const BorderRadius.only(
                            bottomRight: Radius.circular(10))),
                    child: FittedBox(
                      child: Text(
                        'परिमाण: ${widget.productDetails?['quantity']}',
                        style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'ubuntu',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(10))),
                    child: FittedBox(
                      child: Text(
                        'रू. ${widget.productDetails?['price']}/ ${widget.productDetails?['perItemTag']}',
                        style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'ubuntu',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 0,
                  bottom: 0,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: sizeProvider(context).width,
                        height: 80,
                        child: ListTile(
                          minVerticalPadding: 0,
                          visualDensity:
                              const VisualDensity(horizontal: 0, vertical: -4),
                          isThreeLine: true,
                          leading: storeIt(widget.productDetails?['by'])[
                                      'photoUrl'] ==
                                  ''
                              ? const CircleAvatar(
                                  maxRadius: 22,
                                  backgroundImage:
                                      AssetImage('images/avatar.png'),
                                )
                              : CircleAvatar(
                                  maxRadius: 20,
                                  backgroundImage: NetworkImage(
                                      '${storeIt(widget.productDetails?['by'])['photoUrl']}'),
                                ),
                          title: Text(
                            "${storeIt(widget.productDetails?['by'])['fname']}  ${storeIt(widget.productDetails?['by'])['lname']}",
                            style: const TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                          ),
                          trailing: TextButton.icon(
                            onPressed: () {
                              Get.to(() => KinmelProfile(
                                    dtsource: widget.productDetails,
                                  ));
                            },
                            icon: const Icon(
                              Ionicons.open,
                              size: 14,
                              color: Colors.white,
                            ),
                            style: ElevatedButton.styleFrom(
                                primary: Colors.green.shade300,
                                onPrimary: Colors.orange,
                                splashFactory: InkRipple.splashFactory,
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50)))),
                            label: const Text(
                              'हेर्नुहोस',
                              style: TextStyle(
                                fontFamily: 'ubuntu',
                                color: Colors.white,
                              ),
                            ),
                          ),
                          subtitle: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              TextButton.icon(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    splashFactory: InkRipple.splashFactory,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50)))),
                                onPressed: () {
                                  launch(
                                      'tel:+977${widget.productDetails!['phone']}');
                                },
                                icon: const Icon(
                                  Ionicons.call,
                                  size: 14,
                                ),
                                label: const Text(
                                  'CALL',
                                  style: TextStyle(
                                      fontSize: 14, fontFamily: 'ubuntu'),
                                ),
                              ),
                              TextButton.icon(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    splashFactory: InkRipple.splashFactory,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50)))),
                                onPressed: () {
                                  launch(
                                      'sms:+977${widget.productDetails!['phone']}');
                                },
                                icon: const Icon(
                                  Ionicons.chatbubble,
                                  size: 14,
                                ),
                                label: const Text(
                                  'MESSAGE',
                                  style: TextStyle(
                                      fontSize: 14, fontFamily: 'ubuntu'),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
