import 'dart:convert';
import 'package:agromate/config/configs.dart';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/screen/widget/videoplayerwidget.dart';
import 'package:agromate/services/fetchpreview.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CoLinkPreviewer extends StatefulWidget {
  final String url;
  final bool isVideo;

  const CoLinkPreviewer({Key? key, required this.url, bool? isVideo})
      : isVideo = isVideo ?? false,
        super(key: key);

  @override
  _CoLinkPreviewerState createState() => _CoLinkPreviewerState();
}

class _CoLinkPreviewerState extends State<CoLinkPreviewer> {
  var data;
  bool isLoading = true;
  Future getPreview() async {
    var prefs = await SharedPreferences.getInstance();
    String retrievedEncodedMap = prefs.getString(widget.url) ?? "";
    if (retrievedEncodedMap != "") {
      Map<String, dynamic> decodedMap = json.decode(retrievedEncodedMap);
      data = decodedMap;
    } else {
      await FetchUrlPreview().fetch(widget.url).then((value) async {
        String enCodedMap = json.encode(value);
        debugPrint(enCodedMap);
        prefs.setString(widget.url, enCodedMap);

        data = value;
      }).whenComplete(() => {});
    }
  }

  _tryIt() {
    try {
      WidgetsBinding.instance!
          .addPostFrameCallback((_) => getPreview().then((value) {
                if (value != null) {
                  isLoading = false;
                }
              }).whenComplete(() {
                setState(() {
                  isLoading = false;
                });
              }));
      // ignore: empty_catches
    } catch (e) {}
  }

  @override
  void initState() {
    if (!mounted) return;
    _tryIt();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        child: Lottie.asset(JsonAssets.loadingDot),
      );
    }
    return data == null
        ? const SizedBox(
            height: 0,
          )
        : Container(
            width: MediaQuery.of(context).size.width,
            height: 280,
            decoration: BoxDecoration(
              border: Border.all(color: ColorManager.kPrimaryColor),
            ),
            child: InkWell(
              onTap: () {
                if (widget.isVideo != false && data != null) {
                  Get.to(() => SDVideoPlayer(
                        videoUrl: widget.url,
                        title: data == null ? "" : data['title'],
                      ));
                } else {
                  Get.to(() => CoWebViewer(
                        url: widget.url,
                        title: data == null ? 'नेटवर्क छैन' : data['title'],
                      ));
                }
              },
              child: Column(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      data['image'] == null
                          ? Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              height: 180,
                              decoration: const BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.white, blurRadius: 0.9)
                                ],
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(ImageAssets.noImg),
                                ),
                              ),
                              child: widget.isVideo == false
                                  ? Container()
                                  : const Icon(
                                      Ionicons.play_circle,
                                      color: Colors.green,
                                    ),
                            )
                          : CachedNetworkImage(
                              imageUrl: data['image'] ?? "",
                              cacheKey: data['image'] ?? "",
                              errorWidget: (context, url, data) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                height: 180,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: widget.isVideo
                                            ? const Color.fromARGB(
                                                255, 242, 10, 87)
                                            : const Color.fromARGB(
                                                255, 13, 201, 122),
                                        blurRadius: 0.9)
                                  ],
                                ),
                                child: Lottie.asset(
                                  JsonAssets.errorExplamation,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              placeholder: (context, url) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                height: 180,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: widget.isVideo
                                            ? const Color.fromARGB(
                                                255, 242, 10, 87)
                                            : Colors.green.shade300,
                                        blurRadius: 0.9)
                                  ],
                                ),
                                child: Lottie.asset(
                                  JsonAssets.imgLoading,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              imageBuilder: (context, img) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                height: 180,
                                decoration: BoxDecoration(
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Colors.white, blurRadius: 0.9)
                                  ],
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: img,
                                  ),
                                ),
                                child: widget.isVideo == false
                                    ? Container()
                                    : const Icon(
                                        Ionicons.play_circle,
                                        color: Color.fromARGB(255, 242, 10, 87),
                                      ),
                              ),
                            ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 180,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 50,
                            decoration: BoxDecoration(
                              color: widget.isVideo
                                  ? const Color.fromARGB(255, 242, 10, 87)
                                  : const Color(0xff4CB051),
                            ),
                            child: Center(
                              child: Text(
                                data == null
                                    ? ' नेटवर्क छैन'
                                    : ' ' + data['title'],
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'poppins',
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        right: 0,
                        top: 0,
                        child: Container(
                            width: 40,
                            height: 40,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: IconButton(
                                onPressed: () {
                                  Share.share(data == null ? '' : data['link'],
                                      subject: '${data!["title"]}');
                                },
                                icon: Icon(
                                  Ionicons.share_social,
                                  color: Colors.green.shade400,
                                ))),
                      ),
                    ],
                  ),
                  ListTile(
                    subtitle: Text(
                      data == null
                          ? 'तपाईको नेटवर्क छैन, कृपया नेटवर्क जडान गर्नुहोस'
                          : data['description'],
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(),
                    ),
                  ),
                ],
              ),
            ),
          );
  }

  @override
  void dispose() {
    if (!mounted) return;
    super.dispose();
  }
}
