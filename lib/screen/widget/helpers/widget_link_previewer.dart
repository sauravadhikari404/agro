import 'dart:convert';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/screen/widget/videoplayerwidget.dart';
import 'package:agromate/services/fetchpreview.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:share_plus/share_plus.dart';

import 'package:shared_preferences/shared_preferences.dart';

class CoWidgetLinkPreviewer extends StatefulWidget {
  final String url;
  final bool? isVideo;

  const CoWidgetLinkPreviewer({Key? key, required this.url, this.isVideo})
      : super(key: key);

  @override
  _CoWidgetLinkPreviewerState createState() => _CoWidgetLinkPreviewerState();
}

class _CoWidgetLinkPreviewerState extends State<CoWidgetLinkPreviewer> {
  var data;
  bool isLoading = true;
  Future getPreview() async {
    var prefs = await SharedPreferences.getInstance();
    String retrievedEncodedMap = prefs.getString(widget.url) ?? "";
    if (retrievedEncodedMap != "") {
      Map<String, dynamic> decodedMap = json.decode(retrievedEncodedMap);
      data = decodedMap;
    } else {
      await FetchUrlPreview().fetch(widget.url).then((value) async {
        String enCodedMap = json.encode(value);
        debugPrint(enCodedMap);
        prefs.setString(widget.url, enCodedMap);

        data = value;
      }).whenComplete(() => {});
    }
  }

  _tryIt() {
    try {
      WidgetsBinding.instance!
          .addPostFrameCallback((_) => getPreview().then((value) {
                if (value != null) {
                  isLoading = false;
                }
              }).whenComplete(() {
                setState(() {
                  isLoading = false;
                });
              }));
      // ignore: empty_catches
    } catch (e) {}
  }

  @override
  void initState() {
    if (!mounted) return;
    _tryIt();
    super.initState();
  }

  Color titleBarColor = Colors.green.shade300.withOpacity(0.9);
  Color titleBarColorVideo = Colors.red.shade300.withOpacity(0.9);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isVideo ?? false) {
      return _videoWidget(context);
    }

    return _newsWidgetTry(context);
  }

  _newsWidgetTry(BuildContext context) {
    if (isLoading) {
      return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 100,
        child: Center(child: Lottie.asset(JsonAssets.loadingDot)),
      );
    }
    return data == null
        ? const SizedBox(
            height: 0,
          )
        : GestureDetector(
            onTap: () {
              Get.to(() => CoWebViewer(
                    title: data['title'],
                    link: data['link'],
                    url: data['link'],
                  ));
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 100,
              child: Row(
                children: [
                  data['image'] == null
                      ? Container(
                          clipBehavior: Clip.antiAlias,
                          width: MediaQuery.of(context).size.width * 0.30,
                          height: 100,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  bottomLeft: Radius.circular(8)),
                              image: DecorationImage(
                                  image: AssetImage(ImageAssets.noImg))),
                        )
                      : CachedNetworkImage(
                          imageUrl: data['image'] ?? "",
                          cacheKey: data['image'] ?? "",
                          errorWidget: (context, url, data) => Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            height: 150,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  bottomLeft: Radius.circular(8)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff0AF290), blurRadius: 0.9)
                              ],
                            ),
                            child: Lottie.asset(
                              JsonAssets.errorExplamation,
                              fit: BoxFit.fill,
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.30,
                            height: 100,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  bottomLeft: Radius.circular(8)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff0AF290), blurRadius: 0.9)
                              ],
                            ),
                            child: Lottie.asset(
                              JsonAssets.imgLoading,
                              fit: BoxFit.fill,
                            ),
                          ),
                          imageBuilder: (context, img) => Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.30,
                            height: 100,
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  bottomLeft: Radius.circular(8)),
                              boxShadow: const [
                                BoxShadow(color: Colors.white, blurRadius: 0.9)
                              ],
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: img,
                              ),
                            ),
                            child: widget.isVideo == false
                                ? Container()
                                : const Icon(
                                    Ionicons.play_circle,
                                    color: Colors.green,
                                  ),
                          ),
                        ),
                  Flexible(
                    child: ListTile(
                      title: Text(
                        '${data['title']}',
                        maxLines: 4,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }

  _videoWidget(BuildContext context) {
    if (isLoading) {
      return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 100,
        child: Center(child: Lottie.asset(JsonAssets.loadingDot)),
      );
    }
    return data == null
        ? const SizedBox(
            height: 0,
          )
        : GestureDetector(
            onTap: () {
              Get.to(() => SDVideoPlayer(
                    title: data['title'],
                    videoUrl: data['link'],
                  ));
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 100,
              child: Row(
                children: [
                  Stack(
                    children: [
                      data['image'] == null
                          ? Container(
                              clipBehavior: Clip.antiAlias,
                              width: MediaQuery.of(context).size.width * 0.30,
                              height: 100,
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8)),
                                  image: DecorationImage(
                                      image: AssetImage(ImageAssets.noImg))),
                            )
                          : CachedNetworkImage(
                              imageUrl: data['image'] ?? "",
                              cacheKey: data['image'] ?? "",
                              errorWidget: (context, url, data) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                height: 150,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff0AF290),
                                        blurRadius: 0.9)
                                  ],
                                ),
                                child: Lottie.asset(
                                  JsonAssets.errorExplamation,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              placeholder: (context, url) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width * 0.30,
                                height: 100,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff0AF290),
                                        blurRadius: 0.9)
                                  ],
                                ),
                                child: Lottie.asset(
                                  JsonAssets.imgLoading,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              imageBuilder: (context, img) => Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width * 0.30,
                                height: 100,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(8),
                                      bottomLeft: Radius.circular(8)),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Colors.white, blurRadius: 0.9)
                                  ],
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: img,
                                  ),
                                ),
                                child: widget.isVideo == false
                                    ? Container()
                                    : const Icon(
                                        Ionicons.play_circle,
                                        color: Colors.green,
                                      ),
                              ),
                            ),
                    ],
                  ),
                  Flexible(
                    child: ListTile(
                      title: Text(
                        '${data['title']}',
                        maxLines: 4,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }

  @override
  void dispose() {
    if (!mounted) return;
    super.dispose();
  }
}
