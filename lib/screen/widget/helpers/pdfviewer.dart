import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:ionicons/ionicons.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
class PdfViewerSD extends StatefulWidget {
  final String appBarTitle;
  final Map<dynamic,String>? data;
  const PdfViewerSD({Key? key,required this.appBarTitle,this.data}) : super(key: key);

  @override
  _PdfViewerSDState createState() => _PdfViewerSDState();
}

class _PdfViewerSDState extends State<PdfViewerSD> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  late PdfViewerController _pdfViewerController;

  @override
  void initState() {
    _pdfViewerController=PdfViewerController();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: CustomeAppBar(appBarTitle: widget.appBarTitle,
      textOverFlow: TextOverflow.ellipsis,
      softWrap: true,
      maxLines: 2,
      textStyle: const TextStyle(
        fontSize: 12,
        fontFamily: 'Poppins'
      ),
      ),
      bottomNavigationBar: Container(width:double.infinity,
        color: Colors.transparent,
        height: 50,child: AdWidget(ad:adsController.getBannerAd()),),
      body: SfPdfViewer.network('${widget.data!['url']}',
        key:_pdfViewerKey,
        controller: _pdfViewerController,
        canShowPaginationDialog: true,
        onDocumentLoaded: (details) => details.document,
        canShowScrollHead: true,
        enableDocumentLinkAnnotation: true,
        pageLayoutMode: PdfPageLayoutMode.continuous,
        enableTextSelection: true,
        canShowScrollStatus: true,
        pageSpacing: 2,
        searchTextHighlightColor: Colors.green,
        onDocumentLoadFailed: (details) =>Get.snackbar('Error', details.error.toString()),
      ),
    );
  }
}
