import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/utils.dart';
import 'package:ionicons/ionicons.dart';

InputDecoration? dec(String? label, Icon icon) {
  return InputDecoration(
      labelStyle: const TextStyle(fontFamily: 'poppins', color: Colors.green),
      focusColor: Colors.green.shade200,
      focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: Colors.green)),
      border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.green.shade200),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      labelText: "$label",
      prefixIcon: icon);
}

InputDecoration? comm(String? label, Icon icon) {
  return InputDecoration(
      labelStyle: const TextStyle(fontFamily: 'poppins', color: Colors.green),
      focusColor: Colors.green.shade200,
      focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: Colors.green)),
      border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.green.shade200),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      labelText: "$label",
      suffix: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.send,
            color: Colors.black38,
            size: 18,
          )),
      prefixIcon: icon);
}

void showFormModal(BuildContext context, FocusNode f1) {
  TextEditingController problem = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  showModalBottomSheet(
      elevation: 5,
      isScrollControlled: true,
      barrierColor: Colors.green.shade300.withOpacity(0.2),
      clipBehavior: Clip.antiAlias,
      context: context,
      useRootNavigator: true,
      enableDrag: true,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return SingleChildScrollView(
            primary: true,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Stack(
                        children: [
                          Container(
                            width: sizeProvider(context).width,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Colors.green.shade300,
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    topRight: Radius.circular(8))),
                            child: const Center(
                              child: Text(
                                'तपाइको समस्या दर्ता गर्नुहोस?',
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: TextButton(
                                onPressed: () {
                                  Get.back();
                                },
                                child: const Text(
                                  'X',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Poppins',
                                    color: Colors.white,
                                  ),
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                    child: SizedBox(
                      width: context.width,
                      height: context.height / 1.5,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          TextFormField(
                            maxLines: 2,
                            keyboardType: TextInputType.multiline,
                            controller: problem,
                            autofocus: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'कृपया समय लेख्नुहोला';
                              }
                              return null;
                            },
                            focusNode: f1,
                            decoration:
                                dec('समस्या', const Icon(Icons.report_problem)),
                          ),
                          MaterialButton(
                            color: Colors.green.shade300,
                            onPressed: () {
                              if (_formKey.currentState?.validate() ?? false) {
                                commPostCont.addPosts(problem.text,
                                    userController.firebaseUser.value!.uid);
                                Get.back();
                              } else {
                                Get.snackbar('साबधान',
                                    'तपाइले समस्या लेख्नु भएको छैन..\n कृपया समस्या लेखि फेरी प्रयास गर्नुहोस्');
                              }
                            },
                            child: const Text(
                              'दर्ता गर्नुहोस',
                              style: TextStyle(
                                  fontFamily: 'ubuntu', color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
      });
}

void showCommunityEditBottomModal(
    BuildContext context, FocusNode f1, pid, body) {
  TextEditingController problem = TextEditingController(text: body.toString());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  showModalBottomSheet(
      elevation: 5,
      isScrollControlled: true,
      barrierColor: Colors.green.shade300.withOpacity(0.2),
      clipBehavior: Clip.antiAlias,
      context: context,
      useRootNavigator: true,
      enableDrag: true,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return SingleChildScrollView(
            primary: true,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: <Widget>[
                    Flexible(
                      child: Stack(
                        children: [
                          Container(
                            width: sizeProvider(context).width,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Colors.green.shade300,
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    topRight: Radius.circular(8))),
                            child: const Center(
                              child: Text(
                                'तपाइको समस्या अपडेट गर्नुहोस?',
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: TextButton(
                                onPressed: () {
                                  Get.back();
                                  f1.unfocus();
                                },
                                child: const Text(
                                  'X',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Poppins',
                                    color: Colors.white,
                                  ),
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                    child: SizedBox(
                      width: context.width,
                      height: context.height / 1.5,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          TextFormField(
                            maxLines: 2,
                            keyboardType: TextInputType.multiline,
                            controller: problem,
                            autofocus: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'कृपया समय लेख्नुहोला';
                              }
                              return null;
                            },
                            focusNode: f1,
                            decoration:
                                dec('समस्या', const Icon(Icons.report_problem)),
                          ),
                          MaterialButton(
                            color: Colors.green.shade300,
                            onPressed: () {
                              if (_formKey.currentState?.validate() ?? false) {
                                commPostCont.updatePosts(problem.text, pid);
                                Get.back();
                                f1.unfocus();
                              } else {
                                if (body == problem.text) {
                                  Get.snackbar('साबधान',
                                      'तपाइको समस्या परिबर्तन गर्नु भएको छैन , कृपया केहि परि वर्तन गरि पूर्ण प्रयास गर्नुहोस।');
                                }
                                Get.snackbar('साबधान',
                                    'तपाइले समस्या लेख्नु भएको छैन..\n कृपया समस्या लेखि फेरी प्रयास गर्नुहोस् ।');
                              }
                            },
                            child: const Text(
                              'दर्ता गर्नुहोस',
                              style: TextStyle(
                                  fontFamily: 'ubuntu', color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
      });
}
