import 'package:agromate/constant/controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class IntroWidget extends StatelessWidget {
  const IntroWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          const BoxShadow(color: Colors.white, spreadRadius: 2),
          BoxShadow(color: Colors.black.withOpacity(0.05), spreadRadius: 2.5),
        ],
        borderRadius: const BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Intro',
              style: TextStyle(
                fontFamily: 'poppins',
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Ionicons.map),
            title: Text('${userController.userModel.value.userType}'),
          )
        ],
      ),
    );
  }
}
