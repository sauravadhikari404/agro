import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/widget/helpers/widget_link_previewer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VideoWidget extends StatefulWidget {
  const VideoWidget({Key? key}) : super(key: key);

  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 12, left: 5),
                  child: Text(
                    'कृषि भिडियो',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.toNamed('/videos');
                  },
                  child: const Text('थप भिडियोहरु'),
                  clipBehavior: Clip.antiAlias,
                )
              ],
            )),
            Divider(
              height: 2,
              color: Colors.green.shade200,
            ),
            Flexible(
              flex: 3,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: newsController.videoModelClk.length > 4
                        ? 4 * 220
                        : newsController.videoModelClk.length * 200 +
                            (newsController.newsModelClk.length * 2)),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: newsController.videoModelClk.length > 4
                        ? 4
                        : newsController.videoModelClk.length,
                    itemBuilder: (_, index) {
                      return Card(
                        child: CoWidgetLinkPreviewer(
                          url: newsController.videoModelClk[index].url
                              .toString(),
                          isVideo: true,
                        ),
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
