import 'package:agromate/constant/controller.dart';
import 'package:agromate/screen/widget/helpers/widget_link_previewer.dart';
import 'package:agromate/screen/widget/mainnav.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsWidget extends StatefulWidget {
  const NewsWidget({Key? key}) : super(key: key);

  @override
  _NewsWidgetState createState() => _NewsWidgetState();
}

class _NewsWidgetState extends State<NewsWidget> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 12, left: 5),
                  child: Text(
                    'कृषि समाचार',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.offAll(const BottomSheetNavSD(
                      indexofTab: 1,
                    ));
                  },
                  child: const Text('थप समाचारहरु'),
                  clipBehavior: Clip.antiAlias,
                )
              ],
            )),
            Divider(
              height: 2,
              color: Colors.green.shade200,
            ),
            Flexible(
              flex: 3,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: newsController.newsModelClk.length > 4
                      ? 4 * 220
                      : newsController.newsModelClk.length * 200 +
                          (newsController.newsModelClk.length * 2),
                ),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: newsController.newsModelClk.length > 4
                        ? 4
                        : newsController.newsModelClk.length,
                    itemBuilder: (_, index) {
                      return Card(
                        child: CoWidgetLinkPreviewer(
                          url:
                              newsController.newsModelClk[index].url.toString(),
                          isVideo: false,
                        ),
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
