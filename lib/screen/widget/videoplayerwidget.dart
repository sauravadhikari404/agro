import 'dart:convert';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:http/http.dart' as http;

class SDVideoPlayer extends StatefulWidget {
  const SDVideoPlayer({Key? key, required this.videoUrl, required this.title})
      : super(key: key);
  final String videoUrl;
  final String title;
  @override
  _SDVideoPlayerState createState() => _SDVideoPlayerState();
}

class _SDVideoPlayerState extends State<SDVideoPlayer> {
  late YoutubePlayerController _controller;
  var videoId;
  Future<dynamic> getDetail(String userUrl) async {
    String embedUrl = "https://www.youtube.com/oembed?url=$userUrl&format=json";

    //store http request response to res variable
    var res = await http.get(Uri.parse(embedUrl));
    debugPrint("get youtube detail status code: " + res.statusCode.toString());
    try {
      if (res.statusCode == 200) {
        //return the json from the response
        return json.decode(res.body);
      } else {
        //return null if status code other than 200
        return null;
      }
    } on FormatException catch (e) {
      print('invalid JSON' + e.toString());
      //return null if error
      return null;
    }
  }

  var videoData;
  @override
  void initState() {
    if (!mounted) return;
    super.initState();
    videoId = YoutubePlayer.convertUrlToId(widget.videoUrl);
    _controller = YoutubePlayerController(
        initialVideoId: '$videoId',
        flags: const YoutubePlayerFlags(
            forceHD: true,
            controlsVisibleAtStart: false,
            autoPlay: true,
            mute: false));

    getDataNow();
  }

  getDataNow() async {
    videoData = await getDetail(widget.videoUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        appBarTitle: widget.title,
        textStyle: const TextStyle(
          fontSize: 14,
          fontFamily: 'Poppins',
        ),
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            YoutubePlayer(
              controller: _controller,
              showVideoProgressIndicator: true,
              liveUIColor: Colors.redAccent,
              bottomActions: [],
              progressColors: const ProgressBarColors(
                  backgroundColor: Colors.blueGrey,
                  bufferedColor: Colors.greenAccent,
                  playedColor: Colors.deepOrange,
                  handleColor: Colors.deepOrange),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: ListTile(
                    title: Text(
                      widget.title,
                      style: const TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (!mounted) return;
    _controller.dispose();
    super.dispose();
  }
}
