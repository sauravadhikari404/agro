import 'dart:core';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/widget/appbar/cusappbar.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nepali_utils/nepali_utils.dart';

class PostCommentScreen extends StatefulWidget {
  final Map<String, dynamic>? usrDetails;
  final Map<String, dynamic>? postData;
  const PostCommentScreen({Key? key, this.usrDetails, this.postData})
      : super(key: key);

  @override
  _PostCommentScreenState createState() => _PostCommentScreenState();
}

class _PostCommentScreenState extends State<PostCommentScreen> {
  TextEditingController commentText = TextEditingController(text: '');
  final FocusNode _focusNode = FocusNode();
  final _key = GlobalKey<FormFieldState>();
  var commnetModify = [
    'edit',
    'delete',
    'copy',
  ];

  var commnetReport = [
    'report',
    'copy',
  ];

  @override
  void didUpdateWidget(covariant PostCommentScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusAppBar(
        appBarTitle: 'Comments',
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Hero(
              tag: widget.postData?['pid'],
              placeholderBuilder: (context, size, widget) {
                return SizedBox(
                  width: sizeProvider(context).width,
                  height: 50,
                  child: const CircularProgressIndicator(),
                );
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: Stack(
                    children: [
                      Container(
                        color: Colors.white,
                        child: Column(
                          children: [
                            ListTile(
                              leading: const CircleAvatar(
                                backgroundImage:
                                    AssetImage('images/avatar.png'),
                              ),
                              title: widget.usrDetails?['farmName'] == ''
                                  ? Text(
                                      widget.usrDetails?['fname'] +
                                          ' ' +
                                          widget.usrDetails?['lname'],
                                      style: const TextStyle(
                                          fontFamily: 'poppins',
                                          fontWeight: FontWeight.w500),
                                    )
                                  : Text(
                                      widget.usrDetails?['farmName'],
                                      style: const TextStyle(
                                          fontFamily: 'poppins',
                                          fontWeight: FontWeight.w500),
                                    ),
                              subtitle: Row(
                                children: [
                                  LineIcon(
                                    LineIcons.mapMarked,
                                    size: 16,
                                  ),
                                  Text(
                                    widget.usrDetails?['address'] ?? '',
                                    style: const TextStyle(
                                      fontFamily: 'poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 25, right: 25, top: 5, bottom: 5),
                              child: Container(
                                color: Colors.white,
                                child: Column(
                                  children: [
                                    ListBody(
                                      children: [
                                        widget.usrDetails?['farmName'] != ''
                                            ? RichText(
                                                textAlign: TextAlign.left,
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                      text: 'नाम: \n',
                                                      style: const TextStyle(
                                                          color: Colors.green,
                                                          fontFamily: 'poppins',
                                                          fontWeight:
                                                              FontWeight.bold),
                                                      children: [
                                                        TextSpan(
                                                          text: widget.usrDetails?[
                                                                  'fname'] +
                                                              ' ' +
                                                              widget.usrDetails?[
                                                                  'lname'],
                                                          style: const TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  'poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        )
                                                      ])
                                                ]))
                                            : Container(),
                                        widget.usrDetails?['farmName'] != ''
                                            ? const Divider()
                                            : Container(),
                                        RichText(
                                            textAlign: TextAlign.left,
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: 'प्रश्न: \n',
                                                  style: const TextStyle(
                                                      color: Colors.green,
                                                      fontFamily: 'poppins',
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  children: [
                                                    TextSpan(
                                                      text: widget
                                                          .postData?['body'],
                                                      style: const TextStyle(
                                                          color: Colors.orange,
                                                          fontFamily: 'poppins',
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    )
                                                  ])
                                            ])),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Divider(
                              thickness: 1,
                              color: Colors.green.shade200,
                            ),
                            Obx(() => Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8, right: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      isCurrentUserLikeIt(commPostCont
                                                  .userCommModel
                                                  // ignore: invalid_use_of_protected_member
                                                  .value[
                                                      widget.postData?['index']]
                                                  .votes) ==
                                              false
                                          ? TextButton.icon(
                                              onPressed: () {
                                                if (isLoginUser()) {
                                                  likeController
                                                      .addLikeToCommunityPosts(
                                                    widget.postData?['pid'],
                                                  );
                                                } else {
                                                  Get.defaultDialog(
                                                      title: 'Login Required',
                                                      middleText:
                                                          'पहिले login गर्नुहोला',
                                                      cancel: MaterialButton(
                                                        onPressed: () {
                                                          Get.back();
                                                        },
                                                        child: const Text(
                                                          'Cancel',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ),
                                                      confirm: MaterialButton(
                                                        onPressed: () {
                                                          Get.back();
                                                          Get.to(
                                                              const AccountPage());
                                                        },
                                                        child: const Text(
                                                          'Login',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ));
                                                }
                                              },
                                              icon: const Icon(
                                                Ionicons.thumbs_up,
                                                size: 14,
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                primary: Colors.white,
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    50))),
                                                splashFactory:
                                                    InkRipple.splashFactory,
                                              ),
                                              label: Text(
                                                commPostCont
                                                    .userCommModel[widget
                                                        .postData?['index']]
                                                    .votes!
                                                    .length
                                                    .toString(),
                                                style: const TextStyle(
                                                    fontFamily: 'ubuntu'),
                                              ),
                                            )
                                          : TextButton.icon(
                                              onPressed: () {
                                                if (isLoginUser()) {
                                                  likeController
                                                      .removeLikeFromCommunityPosts(
                                                          widget.postData?[
                                                              'pid']);
                                                } else {
                                                  Get.defaultDialog(
                                                      title: 'Login Required',
                                                      middleText:
                                                          'पहिले login गर्नुहोला',
                                                      cancel: MaterialButton(
                                                        onPressed: () {
                                                          Get.back();
                                                        },
                                                        child: const Text(
                                                          'Cancel',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ),
                                                      confirm: MaterialButton(
                                                        onPressed: () {
                                                          Get.back();
                                                          Get.to(
                                                              const AccountPage());
                                                        },
                                                        child: const Text(
                                                          'Login',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ));
                                                }
                                              },
                                              icon: const Icon(
                                                Ionicons.thumbs_up,
                                                size: 14,
                                                color: Colors.orange,
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                  primary: Colors.orange
                                                      .withOpacity(0.2),
                                                  shape:
                                                      const RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(50),
                                                    ),
                                                  ),
                                                  splashFactory:
                                                      InkRipple.splashFactory),
                                              label: Text(
                                                commPostCont
                                                    .userCommModel
                                                    .value[widget
                                                        .postData?['index']]
                                                    .votes!
                                                    .length
                                                    .toString(),
                                                style: const TextStyle(
                                                    color: Colors.orange,
                                                    fontFamily: 'ubuntu'),
                                              ),
                                            ),
                                      Text(
                                        NepaliMoment.fromAD(
                                                (widget.postData?['pdate'] ??
                                                        Timestamp.now())
                                                    .toDate())
                                            .toString(),
                                        style: const TextStyle(
                                            fontFamily: 'ubuntu',
                                            fontWeight: FontWeight.w500,
                                            color: Colors.blueGrey,
                                            fontSize: 12,
                                            fontStyle: FontStyle.normal),
                                      ),
                                    ],
                                  ),
                                )),
                            const SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              width: sizeProvider(context).width,
              height: 100,
              child: Stack(
                children: [
                  TextFormField(
                    focusNode: _focusNode,
                    controller: commentText,
                    keyboardType: TextInputType.multiline,
                    maxLines: (commentText.text.length >= 15) ? 2 : 1,
                    key: _key,
                    enableSuggestions: true,
                    validator: (val) {
                      if (val?.length == 0) {
                        return;
                      }
                    },
                    decoration: const InputDecoration(
                      errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.red)),
                      labelText: 'प्रतिक्रिया',
                      labelStyle: TextStyle(
                          fontFamily: 'poppins', fontWeight: FontWeight.w700),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    bottom: 0,
                    child: Material(
                      type: MaterialType.transparency,
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      child: IconButton(
                        color: Colors.green.shade400,
                        splashColor: Colors.greenAccent,
                        icon: const Icon(Ionicons.send),
                        onPressed: () {
                          if (_key.currentState?.isValid ?? false) {
                            if (isLoginUser()) {
                              commPostCont.addCommentOnPost(
                                  widget.postData?['pid'], commentText.text);

                              commentText.text = '';
                              _focusNode.unfocus();
                            } else {
                              Get.defaultDialog(
                                  title: 'Login Required',
                                  middleText: 'पहिले login गर्नुहोला',
                                  cancel: MaterialButton(
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: const Text(
                                      'Cancel',
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  confirm: MaterialButton(
                                    onPressed: () {
                                      Get.back();
                                      Get.to(const AccountPage());
                                    },
                                    child: const Text(
                                      'Login',
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ));
                            }
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Flexible(
                child: StreamBuilder(
              stream: commPostCont.getCommentData(widget.postData?['pid']),
              builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return const Text('...');
                }

                return ListView.builder(
                    primary: true,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data?.docs.length ?? 0,
                    itemBuilder: (_, index) {
                      List votersData = [];

                      snapshot.data?.docs[index]
                          .get('votes')
                          .forEach((element) {
                        votersData = [element];
                      });

                      int votesCount = votersData.length;

                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black, spreadRadius: 0.2),
                                BoxShadow(
                                    color: Colors.white, spreadRadius: 0.5)
                              ]),
                          child: Material(
                            type: MaterialType.transparency,
                            shadowColor: Colors.black,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10),
                            ),
                            clipBehavior: Clip.antiAlias,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const CircleAvatar(
                                            radius: 14,
                                            backgroundImage:
                                                AssetImage('images/avatar.png'),
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 2),
                                            child: Text(
                                                storeIt(snapshot.data
                                                                ?.docs[index]
                                                            ['by'])['fname']
                                                        .toString() +
                                                    ' ' +
                                                    storeIt(snapshot.data
                                                                ?.docs[index]
                                                            ['by'])['lname']
                                                        .toString(),
                                                style: const TextStyle(
                                                    fontFamily: 'poppins',
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          )
                                        ],
                                      ),
                                      Text(
                                        NepaliMoment.fromAD((snapshot.data
                                                        ?.docs[index]['date'] ??
                                                    Timestamp.now())
                                                .toDate())
                                            .toString(),
                                        style: const TextStyle(
                                          color: Colors.blueGrey,
                                          fontFamily: 'ubuntu',
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: sizeProvider(context).width -
                                            (sizeProvider(context).width * 0.8),
                                        right: sizeProvider(context).width -
                                            (sizeProvider(context).width *
                                                0.99),
                                      ),
                                      child: ExpandableText(
                                        snapshot.data?.docs[index]['body'],
                                        maxLines: (snapshot.data?.docs[index]
                                                            ['body'] ??
                                                        "")
                                                    .toString()
                                                    .length >=
                                                10.0
                                            ? 2
                                            : 1,
                                        linkEllipsis: true,
                                        urlStyle: const TextStyle(
                                            fontFamily: 'Ubuntu',
                                            color: Colors.blueAccent),
                                        linkStyle: const TextStyle(
                                            fontSize: 10, color: Colors.amber),
                                        onUrlTap: (url) {
                                          Get.to(() => CoWebViewer(
                                                title: url,
                                                url: url,
                                              ));
                                        },
                                        collapseOnTextTap: true,
                                        expandOnTextTap: true,
                                        expandText: 'पुरा हेर्नुहोस',
                                        collapseText: 'लुकाउनु होस्',
                                        animation: true,
                                        style: const TextStyle(
                                            color: Colors.black54,
                                            fontFamily: 'poppins',
                                            fontWeight: FontWeight.w600),
                                      ),
                                    )),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FittedBox(
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              child: isCurrentUserLikeIt(
                                                          snapshot
                                                              .data!
                                                              .docs[index]
                                                                  ['votes']
                                                              .toList()) ==
                                                      false
                                                  ? TextButton.icon(
                                                      onPressed: () {
                                                        if (isLoginUser()) {
                                                          likeController
                                                              .addLikeToComment(
                                                            widget.postData?[
                                                                'pid'],
                                                            snapshot
                                                                .data
                                                                ?.docs[index]
                                                                .id,
                                                          );
                                                        } else {
                                                          Get.defaultDialog(
                                                              title:
                                                                  'Login Required',
                                                              middleText:
                                                                  'पहिले login गर्नुहोला',
                                                              cancel:
                                                                  MaterialButton(
                                                                onPressed: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Text(
                                                                  'Cancel',
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ),
                                                              confirm:
                                                                  MaterialButton(
                                                                onPressed: () {
                                                                  Get.back();
                                                                  Get.to(
                                                                      const AccountPage());
                                                                },
                                                                child:
                                                                    const Text(
                                                                  'Login',
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ));
                                                        }
                                                      },
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        splashFactory: InkRipple
                                                            .splashFactory,
                                                        primary: Colors.white,
                                                        shadowColor:
                                                            Colors.white,
                                                        elevation: 0,
                                                        shape:
                                                            const RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(50),
                                                          ),
                                                        ),
                                                      ),
                                                      icon: const Icon(
                                                        Ionicons.thumbs_up,
                                                        size: 15,
                                                      ),
                                                      label: Text(
                                                        snapshot
                                                            .data!
                                                            .docs[index]
                                                                ['votes']
                                                            .length
                                                            .toString(),
                                                        style: const TextStyle(
                                                            fontFamily:
                                                                'ubuntu'),
                                                      ),
                                                    )
                                                  : TextButton.icon(
                                                      onPressed: () {
                                                        if (isLoginUser()) {
                                                          likeController
                                                              .removeLikeFromComment(
                                                                  widget.postData?[
                                                                      'pid'],
                                                                  snapshot
                                                                      .data
                                                                      ?.docs[
                                                                          index]
                                                                      .id);
                                                        } else {
                                                          Get.defaultDialog(
                                                              title:
                                                                  'Login Required',
                                                              middleText:
                                                                  'पहिले login गर्नुहोला',
                                                              cancel:
                                                                  MaterialButton(
                                                                onPressed: () {
                                                                  Get.back();
                                                                },
                                                                child:
                                                                    const Text(
                                                                  'Cancel',
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ),
                                                              confirm:
                                                                  MaterialButton(
                                                                onPressed: () {
                                                                  Get.back();
                                                                  Get.to(
                                                                      const AccountPage());
                                                                },
                                                                child:
                                                                    const Text(
                                                                  'Login',
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ));
                                                        }
                                                      },
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        splashFactory: InkRipple
                                                            .splashFactory,
                                                        primary: Colors.orange
                                                            .withOpacity(0.2),
                                                        shadowColor:
                                                            Colors.white,
                                                        elevation: 0,
                                                        shape:
                                                            const RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(50),
                                                          ),
                                                        ),
                                                      ),
                                                      icon: const Icon(
                                                        Ionicons.thumbs_up,
                                                        color: Colors.orange,
                                                        size: 15,
                                                      ),
                                                      label: Text(
                                                          snapshot
                                                              .data!
                                                              .docs[index]
                                                                  ['votes']
                                                              .length
                                                              .toString(),
                                                          style:
                                                              const TextStyle(
                                                                  fontFamily:
                                                                      'ubuntu',
                                                                  color: Colors
                                                                      .orange)),
                                                    ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: [
                                          DropdownButtonHideUnderline(
                                            child: DropdownButton<dynamic>(
                                              isExpanded: false,
                                              focusColor: Colors.transparent,
                                              elevation: 0,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(10)),
                                              autofocus: false,
                                              icon: const Icon(
                                                FontAwesomeIcons.ellipsis,
                                                color: Colors.black54,
                                              ),
                                              items: isCurrentUser(snapshot
                                                      .data?.docs[index]['by'])
                                                  ? commnetModify.map((e) {
                                                      return DropdownMenuItem(
                                                          value: e,
                                                          child: Text(e));
                                                    }).toList()
                                                  : commnetReport.map((e) {
                                                      return DropdownMenuItem(
                                                          value: e,
                                                          child: Text(e));
                                                    }).toList(),
                                              onChanged: (val) async {
                                                if (val == "copy") {
                                                  getAction(GetActionType.copy,
                                                      body: snapshot.data
                                                                  ?.docs[index]
                                                              ['body'] ??
                                                          "");
                                                } else if (val == "edit") {
                                                  getAction(GetActionType.edit,
                                                      body: snapshot.data
                                                                  ?.docs[index]
                                                              ['body'] ??
                                                          "",
                                                      pid: widget
                                                          .postData!['pid'],
                                                      cid: snapshot.data!
                                                          .docs[index].id);
                                                } else if (val == "delete") {
                                                  getAction(
                                                      GetActionType.delete,
                                                      pid: widget
                                                          .postData!["pid"],
                                                      cid: snapshot.data!
                                                          .docs[index].id);
                                                } else if (val == "report") {
                                                  getAction(
                                                      GetActionType.report);
                                                }
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              },
            ))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    commentText.dispose();
    _key.currentState?.dispose();
  }

  ///This Function take Action Type* ,body of comment, id of Commenter, Comment id and Post id
  dynamic getAction(GetActionType getActionType,
      {String body = "",
      String by = "",
      String cid = "",
      String pid = ""}) async {
    switch (getActionType) {
      case GetActionType.copy:
        return await Clipboard.setData(ClipboardData(text: body))
            .then((value) =>
                ScaffoldMessenger.of(context).removeCurrentSnackBar())
            .whenComplete(() => ScaffoldMessenger.of(context).showSnackBar(
                  (getSnackbar('Text Copied to Clipboard')),
                ));
      case GetActionType.edit:
        return showDialog(
            context: context,
            builder: (_) {
              return getEditCommentSnackBar(pid, cid, body);
            });
      case GetActionType.delete:
        return showDialog(
            context: context,
            builder: (_) {
              return getDialog(deleteComment, pid: pid, cid: cid);
            });
      case GetActionType.report:
        return showDialog(
            context: context,
            builder: (_) {
              return getReportDialog(context);
            });
    }
  }

  Future<void> deleteComment(pid, cid) async {
    await firebaseFirestore
        .collection('CummunityPosts')
        .doc(pid)
        .collection('Comments')
        .doc(cid)
        .delete()
        .then((value) => ScaffoldMessenger.of(context).removeCurrentSnackBar())
        .whenComplete(() {
      ScaffoldMessenger.of(context)
          .showSnackBar(getSnackbar('Your Comment has been deleted', ms: 600));
      Get.back();
    });
  }

  Dialog getEditCommentSnackBar(pid, cid, body) {
    TextEditingController textEditingController =
        TextEditingController(text: body);
    return Dialog(
        child: SizedBox(
      width: 300,
      height: 200,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Text(
                'Edit Your Comment:',
                style: TextStyle(
                    fontFamily: 'ubuntu',
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    FontAwesomeIcons.close,
                    size: 14,
                    color: Colors.black54,
                  )),
            ],
          ),
          Align(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        maxLines: 3,
                        controller: textEditingController)),
                IconButton(
                    onPressed: () {
                      if (textEditingController.text == "" ||
                          textEditingController.text == body) {
                        if (textEditingController.text == "") {
                          ScaffoldMessenger.of(context).removeCurrentSnackBar();
                          ScaffoldMessenger.of(context).showSnackBar(
                              getSnackbar('Comment is Empty ', ms: 2000));
                        } else if (textEditingController.text == body) {
                          ScaffoldMessenger.of(context).removeCurrentSnackBar();
                          ScaffoldMessenger.of(context).showSnackBar(
                              getSnackbar('No changed detect on Comment ',
                                  ms: 2000));
                        }
                      } else {
                        editComment(pid, cid, textEditingController.text,
                            context, "CummunityPosts");
                      }
                    },
                    icon: const Icon(Ionicons.send))
              ],
            ),
          ),
        ],
      ),
    ));
  }
}

enum GetActionType { copy, edit, delete, report }
