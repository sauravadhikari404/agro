import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/constant/firebasecons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:image_viewer/image_viewer.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class AdsSliderScreen extends StatefulWidget {
  const AdsSliderScreen({Key? key}) : super(key: key);

  @override
  _AdsSliderScreenState createState() => _AdsSliderScreenState();
}

class _AdsSliderScreenState extends State<AdsSliderScreen> {
  @override
  void initState() {
    if (!mounted) return;
    didUpdateWidget(const AdsSliderScreen());
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AdsSliderScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return CarouselSlider(
        options: CarouselOptions(
          scrollPhysics: const AlwaysScrollableScrollPhysics(),
          autoPlay: true,
          autoPlayAnimationDuration: const Duration(milliseconds: 500),
          autoPlayCurve: Curves.easeInOut,
          pauseAutoPlayOnTouch: true,
          autoPlayInterval: const Duration(seconds: 5),
          pauseAutoPlayOnManualNavigate: true,
          pauseAutoPlayInFiniteScroll: true,
          viewportFraction: 1,
          aspectRatio: 1.8,
          height: 200,
          pageSnapping: true,
        ),
        // snapshot.data!.docs
        items: sliderAdsController.sliderAdsData
            .map(
              (items) => InkWell(
                onTap: () {
                  if (!mounted) return;
                  setState(() {
                    if (items.redirect == true) {
                      launch(items.url.toString());
                    } else {
                      ImageViewer.showImageSlider(images: [items.img]);
                    }
                  });
                },
                child: Center(
                  child: Container(
                    clipBehavior: Clip.antiAlias,
                    width: MediaQuery.of(context).size.width,
                    height: 190,
                    decoration: BoxDecoration(
                        color: Colors.green.shade100,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CachedNetworkImage(
                          imageUrl: items.img,
                          cacheKey: items.img,
                          imageBuilder: (context, imageProvider) => Container(
                            width: MediaQuery.of(context).size.width,
                            height: 190,
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      );
    });
  }

  @override
  void dispose() {
    if (!mounted) return;

    super.dispose();
  }
}
