import 'package:agromate/config/configs.dart';
import 'package:agromate/controller/notificationController.dart';
import 'package:agromate/screen/main/community.dart';
import 'package:agromate/screen/main/home.dart';
import 'package:agromate/screen/main/more.dart';
import 'package:agromate/screen/main/news.dart';
import 'package:agromate/screen/main/notifications.dart';
import 'package:agromate/screen/widget/appbar/customeappbar.dart';
import 'package:agromate/screen/widget/drawer/customdrawer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';

class BottomSheetNavSD extends StatefulWidget {
  final int indexofTab;

  const BottomSheetNavSD({Key? key, int? indexofTab})
      : indexofTab = indexofTab ?? 0,
        super(key: key);

  @override
  _BottomSheetNavSDState createState() => _BottomSheetNavSDState();
}

var titles = [
  'AgroMate',
  'समाचार',
  'समुदाय',
  'सेटिंग',
];

class _BottomSheetNavSDState extends State<BottomSheetNavSD> {
  Color color = Colors.white;
  PageController controller = PageController();
  var padding = const EdgeInsets.symmetric(horizontal: 15, vertical: 10);
  double gap = 6;
  int _selectedIndex = 0;

  var screen = [
    const HomeScreen(),
    const NewsPaperScreen(),
    const CommunityScreen(),
    const MoreScreen(),
  ];
  final GlobalKey<ScaffoldState> scaffoldkey = GlobalKey();
  final NotificationController notyData = Get.put(NotificationController());

  @override
  void initState() {
    if (!mounted) return;
    _selectedIndex = widget.indexofTab;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime prebackpress = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        final timegap = DateTime.now().difference(prebackpress);
        final cantExit = timegap >= const Duration(seconds: 2);
        if (cantExit) {
          const snack = SnackBar(
            content: Text(
              'Press Back button again to Exit',
              style: TextStyle(fontFamily: 'Ubuntu'),
            ),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).showSnackBar(snack);
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: scaffoldkey,
        appBar: CustomeAppBar(
          appBarTitle: titles[_selectedIndex],
          scaffoldkey: scaffoldkey,
          action: [
            Obx(() {
              return Stack(
                children: [
                  notyData.notificationData.isEmpty
                      ? Container()
                      : Positioned(
                          top: 5,
                          right: 2,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 12,
                              height: 12,
                              decoration: const BoxDecoration(
                                  color: Colors.redAccent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50))),
                            ),
                          ),
                        ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: IconButton(
                        onPressed: () {
                          Get.to(() => const NotificationScreen());
                        },
                        icon: Icon(
                          Ionicons.notifications,
                          color: Colors.grey.shade200,
                        )),
                  ),
                ],
              );
            })
          ],
        ),
        drawer: const CustomDrawer(),
        body: PageView.builder(
            itemCount: 4,
            controller: controller,
            onPageChanged: (page) {
              if (!mounted) return;
              setState(() {
                _selectedIndex = page;
              });
            },
            itemBuilder: (context, position) {
              position = _selectedIndex;
              return screen[position];
            }),
        bottomNavigationBar: SafeArea(
            maintainBottomViewPadding: true,
            child: Container(
              clipBehavior: Clip.antiAlias,
              height: 55,
              margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 0),
              decoration: BoxDecoration(
                  color: customGnavContainerColor(context),
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 50,
                        spreadRadius: -10,
                        color: customGnavBoxShadowColor(context),
                        offset: const Offset(0, 30))
                  ]),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: GNav(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      tabBackgroundColor: customGNavBackGroundColor(context),
                      curve: Curves.fastOutSlowIn,
                      duration: const Duration(milliseconds: 200),
                      selectedIndex: _selectedIndex,
                      haptic: false,
                      rippleColor: customGNavRippleBackGroundColor(context),
                      onTabChange: (index) {
                        if (mounted) {
                          setState(() {
                            _selectedIndex = index;
                            controller.jumpToPage(index);
                          });
                        }
                      },
                      tabs: [
                        GButton(
                          active: true,
                          icon: LineIcons.home,
                          iconColor: customGNavIconColorUnselected(context),
                          iconActiveColor: customGNavIconColorSelected(context),
                          text: 'होम',
                          textStyle: customGNavTextStyle(context),
                          backgroundColor: customGNavBackGroundColor(context),
                          iconSize: 24,
                          padding: padding,
                          gap: gap,
                        ),
                        GButton(
                          icon: LineIcons.newspaper,
                          iconColor: customGNavIconColorUnselected(context),
                          iconActiveColor: customGNavIconColorSelected(context),
                          text: 'समाचार',
                          textStyle: customGNavTextStyle(context),
                          backgroundColor: customGNavBackGroundColor(context),
                          iconSize: 24,
                          padding: padding,
                          gap: gap,
                        ),
                        GButton(
                          icon: Ionicons.people_outline,
                          iconColor: customGNavIconColorUnselected(context),
                          iconActiveColor: customGNavIconColorSelected(context),
                          text: 'समुदाय',
                          textStyle: customGNavTextStyle(context),
                          backgroundColor: customGNavBackGroundColor(context),
                          iconSize: 24,
                          padding: padding,
                          gap: gap,
                        ),
                        GButton(
                          icon: Icons.settings,
                          iconColor: customGNavIconColorUnselected(context),
                          iconActiveColor: customGNavIconColorSelected(context),
                          text: 'सेटिंग',
                          textStyle: customGNavTextStyle(context),
                          backgroundColor: customGNavBackGroundColor(context),
                          iconSize: 24,
                          padding: padding,
                          gap: gap,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )),
      ),
    );
  }

  @override
  void dispose() {
    if (!mounted) return;
    controller.dispose();
    notyData.onClose();
    super.dispose();
  }
}
