import 'dart:math';

import 'package:agromate/config/configs.dart';
import 'package:agromate/constant/manager/assets_manager.dart';
import 'package:agromate/constant/constant.dart';
import 'package:agromate/constant/controller.dart';
import 'package:agromate/controller/authcontroller.dart';
import 'package:agromate/screen/authentication/account.dart';
import 'package:agromate/screen/main/grid/gridscreen/kinmel/kinmelscreen.dart';
import 'package:agromate/screen/main/grid/gridscreen/suchana.dart';
import 'package:agromate/screen/main/grid/krishikaryalay.dart';
import 'package:agromate/screen/main/tipot/tipotlist.dart';
import 'package:agromate/screen/main/videos.dart';
import 'package:agromate/screen/users/profile/mainprofile.dart';
import 'package:agromate/screen/widget/mainnav.dart';
import 'package:agromate/services/webviewer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:share_plus/share_plus.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  var userDb;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color iconColor = customDrawerIconColorChanged(context);
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(right: 150),
        child: Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
              color: getDrawerBgColor(context),
              borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(10),
                  topRight: Radius.circular(10))),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Obx(
                () => userController.firebaseUser.value?.isAnonymous ??
                        false || userController.firebaseUser.value?.email == ''
                    ? Container(
                        width: double.infinity,
                        height: 200,
                        decoration: BoxDecoration(
                          color: Colors.green.shade300,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const CircleAvatar(
                              backgroundImage: AssetImage('images/avatar.png'),
                              radius: 45,
                            ),
                            TextButton(
                              onPressed: () {
                                Get.to(() => const AccountPage());
                              },
                              child: const Text(
                                'LOGIN',
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Colors.white,
                                ),
                              ),
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.green.shade700, width: 2),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Obx(
                        () => UserAccountsDrawerHeader(
                          arrowColor: Colors.red,
                          currentAccountPicture: InkWell(
                            onTap: () {
                              Get.back();
                              Get.to(() => const MainProfile());
                            },
                            child: userController
                                        .firebaseUser.value?.photoURL ==
                                    null
                                ? const CircleAvatar(
                                    backgroundImage:
                                        AssetImage('images/avatar.png'),
                                  )
                                : CachedNetworkImage(
                                    imageUrl:
                                        "${userController.firebaseUser.value?.photoURL}",
                                    placeholder: (context, url) => CircleAvatar(
                                      child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          width: 50,
                                          height: 50,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50)),
                                          child: Lottie.asset(
                                              JsonAssets.imgLoading)),
                                    ),
                                    cacheKey:
                                        "${userController.firebaseUser.value?.photoURL}",
                                    imageBuilder: (context, imgProvider) =>
                                        CircleAvatar(
                                      backgroundImage: imgProvider,
                                    ),
                                  ),
                          ),
                          accountName: Text(
                            '${userController.userModel.value.fname} ${userController.userModel.value.lname}',
                            style: const TextStyle(
                                color: Colors.black,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold),
                          ),
                          accountEmail: Text(
                            userController.firebaseUser.value?.email ?? '',
                            style: const TextStyle(
                                color: Colors.black, fontFamily: 'ubuntu'),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.green.shade100,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  colorFilter: ColorFilter.mode(
                                      Colors.white.withOpacity(0.6),
                                      BlendMode.lighten),
                                  image: const AssetImage(
                                      'images/KrishiGuide/cashcrop/maize.jpg'))),
                        ),
                      ),
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/groceries.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('कृषि बजार'),
                onTap: () {
                  Get.back();
                  Get.to(() => const KinmelScreen());
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/group.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('समुदाय'),
                onTap: () {
                  Get.back();
                  Get.offAll(() => const BottomSheetNavSD(
                        indexofTab: 2,
                      ));
                },
              ),
              const Divider(
                thickness: 1.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/guide-book.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('गाइड'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/krishiguide'));
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/book.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('पुस्तकालय'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/krishipustakalaya'));
                },
              ),
              const Divider(
                thickness: 1.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/alert.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('सुचना'),
                onTap: () {
                  Get.back();
                  Get.to(() => KrishiSuchana());
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/news.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('समाचार'),
                onTap: () {
                  Get.back();
                  Get.offAll(() => const BottomSheetNavSD(
                        indexofTab: 1,
                      ));
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/play.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('भिडियो'),
                onTap: () {
                  Get.back();
                  Get.to(() => const VideosScreen());
                },
              ),
              const Divider(
                thickness: 1.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/team.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('कार्यालय'),
                onTap: () {
                  Get.back();
                  Get.to(() => const KrishiKaryalaya());
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/meeting.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('संस्था'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/sansthaharu'));
                },
              ),
              const Divider(
                thickness: 1.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/calculator.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('क्यालकुलेटर'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/krishicalculator'));
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/Outlined/budget.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('हिसाबकिताब'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/hisabkitab'));
                },
              ),
              const Divider(
                thickness: 0.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/calculator/calendar.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('पात्रो'),
                onTap: () {
                  Get.back();
                  Get.toNamed(('/calendar'));
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/main/tarkaribazzar.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('तरकारी बजार'),
                onTap: () {
                  Get.back();
                  Get.to(() => const CoWebViewer(
                        link: 'https://kalimatimarket.gov.np/',
                        url: 'https://kalimatimarket.gov.np/',
                        title: "तरकारी बजार",
                      ));
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Image.asset(
                  'images/main/weather.png',
                  width: 20,
                  height: 20,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('मौसम'),
                onTap: () {
                  Get.back();
                  // title: 'मौसम'
                  Get.to(
                    () => MyTipotsScreen(),
                  );
                },
              ),
              const Divider(
                thickness: 0.5,
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Icon(Icons.info_outlined,
                    color: customDrawerIconColorChanged(context)),
                title: const Text('हाम्रो बारेमा'),
                onTap: () {
                  try {
                    Get.back();
                    Get.to(() => const CoWebViewer(
                          link: 'https://agromateapp.com/about',
                          url: 'https://agromateapp.com/about',
                          title: "हाम्रो बारेमा",
                        ));
                  } on Exception {
                    throw Exception();
                  }
                },
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Icon(
                  Icons.share_outlined,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('सेयर गर्नुहोस'),
                onTap: () => Share.share(
                    'https://play.google.com/store/apps/details?id=com.sdtechcompany.agromate'),
              ),
              ListTile(
                minVerticalPadding: 0,
                visualDensity: const VisualDensity(horizontal: 0, vertical: -4),
                leading: Icon(
                  Icons.settings_outlined,
                  color: customDrawerIconColorChanged(context),
                ),
                title: const Text('सेटिंग'),
                onTap: () {
                  Get.offAll(() => const BottomSheetNavSD(
                        indexofTab: 3,
                      ));
                },
              ),
              isLoginUser()
                  ? ListTile(
                      minVerticalPadding: 0,
                      visualDensity:
                          const VisualDensity(horizontal: 0, vertical: -4),
                      leading: Icon(
                        Icons.exit_to_app_outlined,
                        color: customDrawerIconColorChanged(context),
                      ),
                      title: const Text('लग आउट'),
                      onTap: () {
                        Get.find<UserController>().signOut();
                      },
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
