// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class SearchOptionScreen extends StatefulWidget {
  const SearchOptionScreen({Key? key}) : super(key: key);

  @override
  _SearchOptionScreenState createState() => _SearchOptionScreenState();
}

class _SearchOptionScreenState extends State<SearchOptionScreen> {
  final FocusNode _focusNode = FocusNode();
  TextEditingController searchText = TextEditingController();
  @override
  void initState() {
    _focusNode.requestFocus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 6.0, 16.0, 16.0),
          child: Container(
            height: 34.0,
            width: double.infinity,
            child: CupertinoTextField(
              focusNode: _focusNode,
              controller: searchText,
              keyboardType: TextInputType.text,
              placeholder: 'खोजि गर्नुहोस',
              placeholderStyle: const TextStyle(
                color: Color(0xff4db6ac),
                fontSize: 14.0,
                fontFamily: 'Brutal',
              ),
              prefix: const Padding(
                padding: EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
                child: Icon(
                  Icons.search,
                  color: Color(0xff4db6ac),
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: const Color(0xffF0F1F5),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _focusNode.dispose();
    searchText.dispose();
    super.dispose();
  }
}
